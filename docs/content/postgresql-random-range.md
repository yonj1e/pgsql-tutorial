## 生成随机数

> 摘要：本教程向您展示如何开发用户定义的函数，该函数在两个数字之间生成随机数。

PostgreSQL提供 `random()` 函数，该函数返回0到1之间的随机数。以下语句返回0到1之间的随机数。

```sql
SELECT random();
      random
-------------------
 0.226101292762905
(1 row)
```

要生成1到10之间的随机数，请使用以下语句：

```sql
SELECT random() * 10 + 1 AS RAND_1_10;
    rand_1_10
------------------
 8.04037921968848
(1 row)
```

如果要将随机数生成为整数，请将 `floor()` 函数应用于表达式，如下所示：

```sql
SELECT floor(random() * 10 + 1)::int;
 floor
-------
     6
(1 row)
```

通常，要在两个整数l和h之间生成一个随机数，请使用以下语句：

```sql
SELECT floor(random() * (h-l+1) + l)::int;
```

您可以开发一个[用户定义函数](content/postgresql-create-function.md)，该函数返回两个数字l和h之间的随机数：

```sql
CREATE OR REPLACE FUNCTION random_between(low INT ,high INT) 
RETURNS INT 
AS $$
BEGIN
   RETURN floor(random()* (high-low + 1) + low);
END;
$$ language 'plpgsql' STRICT;
```

以下语句调用 `random_between()` 函数并返回1到100之间的随机数：

```sql
SELECT random_between(1,100);
 random_between
----------------
             20
(1 row)
```

如果要在两个整数之间获取多个随机数，请使用以下语句：

```sql
SELECT random_between(1,100)
FROM generate_series(1,5);
 random_between
----------------
             10
             84
             76
              2
             56
(5 rows)
```

在本教程中，您学习了如何在两个数字范围之间生成随机数。