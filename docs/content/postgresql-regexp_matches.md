## REGEXP_MATCHES

`REGEXP_MATCHES()` 函数将POSIX正则表达式与字符串匹配，并返回匹配的子字符串。

#### 语法

以下说明了 `REGEXP_MATCHES()` 函数的语法：

```sql
REGEXP_MATCHES(source_string, pattern [, flags])
```

#### 参数

`REGEXP_MATCHES()` 函数接受三个参数：

**1) source**

`source` 是一个字符串，您要提取与正则表达式匹配的子字符串。

**2) pattern**

`pattern` 是用于匹配的POSIX正则表达式。

**3) flags**

`flags` 参数是控制函数行为的一个或多个字符。例如，我允许您匹配不区分大小写。

#### 返回值

`REGEXP_MATCHES()` 函数返回一组文本，即使结果数组只包含单个元素。

#### 示例

假设您有一个社交网络的帖子如下：

```sql
'Learning #PostgreSQL #REGEXP_MATCHES'
```

以下语句允许您提取 `PostgreSQL` 和 `REGEXP_MATCHES` 等主题标签：

```sql
SELECT 
    REGEXP_MATCHES(
        'Learning #PostgreSQL #REGEXP_MATCHES', 
        '#([A-Za-z0-9_]+)', 
        'g'
    );
```

在此示例中，以下正则表达式：

```sql
#([A-Za-z0-9_]+)
```

匹配任何以井号字符（`＃`）开头的单词，后跟任何字母数字字符或下划线（`_`）。

flags参数 `g` 用于全局搜索。

以下是结果：

```sql
regexp_matches
-----------------
 {PostgreSQL}
 {REGEX_MATCHES}
(2 rows)
```

结果集有两行，每行都是一个[数组](content/postgresql-array.md)，表示有两个匹配。

注意到 `REGEXP_MATCHES()` 将每一行作为数组返回，而不是字符串。因为如果使用组来捕获文本的一部分，则数组将包含组，如以下示例所示：

```sql
SELECT REGEXP_MATCHES('ABC', '^(A)(..)$', 'g');
 regexp_matches
----------------
 {A,BC}
(1 row)
```

在本教程中，您学习了如何使用 `REGEXP_MATCHES()` 函数根据正则表达式提取文本。