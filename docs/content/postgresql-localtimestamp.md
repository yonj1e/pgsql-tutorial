## LOCALTIMESTAMP

`LOCALTIMESTAMP` 函数返回当前事务开始的当前日期和时间。

#### 语法

以下说明了 `LOCALTIMESTAMP` 函数的语法：

```sql
LOCALTIMESTAMP(precision)
```

#### 参数

`LOCALTIMESTAMP` 函数接受一个参数：

**1) precision**

`precision` 参数指定第二个字段的小数秒精度。

`precision` 参数是可选的。如果省略它，则其默认值为6。

#### 返回值

`LOCALTIMESTAMP` 函数返回 `TIMESTAMP` 值，该值表示当前事务开始的日期和时间。

#### 示例

以下示例显示如何获取当前事务的当前日期和时间：

```sql
SELECT LOCALTIMESTAMP;
       localtimestamp
----------------------------
 2018-10-07 11:05:52.725227
(1 row)
```

要使用特定的小数秒精度获取当前事务的时间戳，请使用 `precision` 参数，如下所示：

```sql
SELECT LOCALTIMESTAMP(2);
     localtimestamp
------------------------
 2018-10-07 11:06:18.02
(1 row)
```

#### 备注

`LOCALTIMESTAMP` 函数返回不带时区的 `TIMESTAMP` 值，而 `CURRENT_TIMESTAMP` 函数返回带时区的 `TIMESTAMP`。

在本教程中，您学习了如何使用 `LOCALTIMESTAMP` 函数返回当前事务开始的日期和时间。