## 服务器与数据库对象

> 摘要：在本教程中，您将熟悉PostgreSQL提供的最常见的服务器和数据库对象。 了解这些对象及其功能非常重要，这样您就不会错过您可能希望在系统中使用的一些很酷的功能。

[安装PostgreSQL](content/install-postgresql.md)，[加载示例数据库](content/load-postgresql-sample-database.md)并[使用pgAdmin GUI应用程序连接到数据库服务器](content/connect-to-postgresql-databas.md)后，您将看到PostgreSQL提供了许多服务器和数据库对象。 为了有效利用PostgreSQL提供的每个对象的功能，您应该很好地理解每个对象是什么以及如何有效地使用它。

![PostgreSQL Server Database Objects](/imgs/PostgreSQL-Server-Database-Objects.png)

让我们熟悉这些PostgreSQL服务器和数据库对象。

#### 服务器服务

安装PostgreSQL实例时，您将拥有相应的PostgreSQL服务器服务。 它也被称为PostgreSQL服务器。您可以在物理服务器上安装多个PostgreSQL服务器，并使用不同的端口和不同的位置来存储数据。

![PostgreSQL Server](/imgs/server.png)

#### 数据库

数据库是其他对象的容器，例如表，视图，函数，索引等。您可以在PostgreSQL服务器中创建任意数量的数据库。

![PostgreSQL databases](/imgs/postgresql-databases.png)

#### 表

表用于存储数据。 您可以在数据库中包含许多表。 PostgreSQL表的一个特殊功能是继承。意味着一个表(子表)可以从另一个表(父表)继承，因此当您从子表查询数据时，父表的数据也会显示出来。

![PostgreSQL tables](/imgs/postgresql-tables.png)

#### 模式

模式是数据库中表和其他对象的逻辑容器。 每个PostgreSQL数据库可能有多个模式。 值得注意的是，模式是ANSI-SQL标准的一部分。

![postgresql schema](/imgs/postgresql-schema.png)

#### 表空间

表空间是PostgreSQL存储数据的地方。 [PostgreSQL表空间](content/postgresql-create-tablespace.md)使您可以使用简单的命令，轻松地将数据移动到不同物理位置。 默认情况下，PostgreSQL提供两个表空间：pg_default用于存储用户数据，pg_global用于存储系统数据。

![PostgreSQL Tablespaces](/imgs/postgresql-tablespace.png)

#### 视图

[视图](content/postgresql-views.md)是一个虚拟表，用于简化复杂查询并为一组记录应用安全性。 PostgreSQL还为您提供[可更新视图](content/postgresql-updatable-views.md)。

![PostgreSQL Views](/imgs/postgresql-views.png)

#### 函数

[函数](content/postgresql-stored-procedures.md)是一个块可重用的SQL代码，它返回记录列表的标量值。 在PostgreSQL中，函数也可以返回复合对象。

![PostgreSQL functions](/imgs/postgresql-functions.png)

#### 操作符

运算符是一个符号函数。PostgreSQL允许您定义自定义运算符。

#### 转换

通过强制转换，您可以将一种数据类型转换为另一种数据类型。转换实际上由执行转换的函数支持。您还可以创建自己的强制转换，以覆盖PostgreSQL提供的默认转换。

#### 序列

序列用于管理在表中定义为serial的自动递增列。

![PostgreSQL Sequences](/imgs/postgresql-sequence.png)

#### 扩展

PostgreSQL从版本9.1开始引入扩展概念，将包括类型，强制转换，索引，函数等在内的其他对象包装到一个单元中。 扩展的目的是使其更易于维护。

![PostgreSQL Extensions](/imgs/postgresql-extension.png)

在本教程中，我们向您介绍了最常见的PostgreSQL服务器和数据库对象。 在进入下一个教程之前，只需花几分钟时间浏览这些对象即可获得大致了解。