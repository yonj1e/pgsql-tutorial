## SPLIT_PART

`SPLIT_PART()` 函数在指定的分隔符上拆分[字符串](content/postgresql-char-varchar-text.md)并返回第n个子字符串。

#### 语法

以下说明了 `SPLIT_PART()` 函数的语法：

```sql
SPLIT_PART(string, delimiter, position)
```

#### 参数

`SPLIT_PART()` 函数需要三个参数：

**1) string**

是要拆分的字符串。

**2) delimiter**

分隔符是一个字符串，用作拆分的分隔符。

**3) position**

是从1开始返回的子串的位置。该位置必须是正整数。

如果位置大于拆分后的子串数，则 `SPLIT_PART()` 函数返回一个空字符串。

#### 返回值

`SPLIT_PART()` 函数在指定位置返回一个字符串作为字符串。

#### 示例

请参阅以下声明：

```sql
SELECT SPLIT_PART('A,B,C', ',', 2);
 split_part
------------
 B
(1 row)
```

字符串 `'A,B,C'` 在逗号分隔符（，）上分割，产生3个子字符串：'A'，'B'和'C'。

因为 `position` 是2，函数返回第二个子串，即'B'。

请参阅[示例数据库](content/postgresql-sample-database.md)中的以下 `payment` 表。

![payment table](/imgs/payment-table.png)

以下语句使用 `SPLIT_PART()` 函数返回付款日期的年份和月份：

```sql
SELECT
    split_part(payment_date::TEXT,'-', 1) y,
    split_part(payment_date::TEXT,'-', 2) m,
    amount
FROM
    payment;
  y   | m  | amount
------+----+--------
 2007 | 02 |   7.99
 2007 | 02 |   1.99
 2007 | 02 |   7.99
 2007 | 02 |   2.99
 2007 | 02 |   7.99
 2007 | 02 |   5.99
 2007 | 02 |   5.99
 2007 | 02 |   5.99
 2007 | 02 |   2.99
 2007 | 02 |   4.99
 2007 | 02 |   6.99
```

在本教程中，您学习了如何使用 `SPLIT_PART()` 函数在拆分后在指定位置获取字符串的一部分。