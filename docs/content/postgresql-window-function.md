## 窗口函数

> 摘要：在本教程中，您将学习如何使用PostgreSQL窗口函数在与当前行相关的一组行上执行计算。

#### 简介

我们将为演示创建两个名为 `products` 和 `product_groups` 的表。

```sql
CREATE TABLE product_groups (
 group_id serial PRIMARY KEY,
 group_name VARCHAR (255) NOT NULL
);
 
CREATE TABLE products (
 product_id serial PRIMARY KEY,
 product_name VARCHAR (255) NOT NULL,
 price DECIMAL (11, 2),
 group_id INT NOT NULL,
 FOREIGN KEY (group_id) REFERENCES product_groups (group_id)
);
 
INSERT INTO product_groups (group_name)
VALUES
 ('Smartphone'),
 ('Laptop'),
 ('Tablet');
 
INSERT INTO products (product_name, group_id,price)
VALUES
 ('Microsoft Lumia', 1, 200),
 ('HTC One', 1, 400),
 ('Nexus', 1, 500),
 ('iPhone', 1, 900),
 ('HP Elite', 2, 1200),
 ('Lenovo Thinkpad', 2, 700),
 ('Sony VAIO', 2, 700),
 ('Dell Vostro', 2, 800),
 ('iPad', 3, 700),
 ('Kindle Fire', 3, 150),
 ('Samsung Galaxy Tab', 3, 200);
```

理解窗口函数的最简单方法是首先查看聚合函数。聚合函数将来自一组行的数据聚合为单个行。

例如，以下[AVG](content/postgresql-avg-function.md)函数计算产品表中产品的平均价格。

```sql
SELECT AVG (price)
FROM products;
         avg
----------------------
 586.3636363636363636
(1 row)
```

要将聚合函数应用于行的子集，请使用[GROUP BY](content/postgresql-group-by.md)子句。以下语句返回每个产品组的平均价格。

```sql
SELECT group_name, AVG (price)
FROM products
INNER JOIN product_groups USING (group_id)
GROUP BY group_name;
 group_name |         avg
------------+----------------------
 Smartphone | 500.0000000000000000
 Tablet     | 350.0000000000000000
 Laptop     | 850.0000000000000000
(3 rows)
```

如您所见，`AVG` 聚合函数减少了两种情况下查询返回的行数。

与聚合函数一样，窗口函数对一组行进行操作，但不会减少查询返回的行数。

术语窗口描述窗口函数操作的行集。窗口函数从窗口中的行返回一个值。

例如，以下查询返回产品名称，价格，产品组名称以及每个产品组的平均价格。

```sql
SELECT product_name, price, group_name,
 AVG (price) OVER (PARTITION BY group_name)
FROM products
INNER JOIN product_groups USING (group_id);
    product_name    |  price  | group_name |         avg
--------------------+---------+------------+----------------------
 HP Elite           | 1200.00 | Laptop     | 850.0000000000000000
 Lenovo Thinkpad    |  700.00 | Laptop     | 850.0000000000000000
 Sony VAIO          |  700.00 | Laptop     | 850.0000000000000000
 Dell Vostro        |  800.00 | Laptop     | 850.0000000000000000
 Microsoft Lumia    |  200.00 | Smartphone | 500.0000000000000000
 HTC One            |  400.00 | Smartphone | 500.0000000000000000
 Nexus              |  500.00 | Smartphone | 500.0000000000000000
 iPhone             |  900.00 | Smartphone | 500.0000000000000000
 iPad               |  700.00 | Tablet     | 350.0000000000000000
 Kindle Fire        |  150.00 | Tablet     | 350.0000000000000000
 Samsung Galaxy Tab |  200.00 | Tablet     | 350.0000000000000000
(11 rows)
```

在此查询中，`AVG` 函数用作窗口函数，该函数对由 `OVER` 子句指定的一组行进行操作。每组行称为窗口。

此查询的新语法是 `AVG(price) OVER (PARTITION BY group_name)` 子句。

在幕后，PostgreSQL按 `group_name` 列中的值对行进行排序，`PARTITION BY` 将行分组为组，`AVG` 函数计算每个产品组的平均价格。

在[JOIN](content/postgresql-inner-join.md)，[WHERE]()，[GROUP BY](content/postgresql-group-by.md)和[HAVING](content/postgresql-having.md)子句完成之后，但在最终的[ORDER BY](content/postgresql-order-by.md)子句之前，窗口函数对结果集执行计算。

#### 语法

PostgreSQL为提供了[复杂的窗口函数调用语法](http://www.postgresql.org/docs/current/static/sql-expressions.html#SYNTAX-WINDOW-FUNCTIONS)。以下说明了窗口函数调用的语法。

```sql
window_function(arg1, arg2,..) OVER (PARTITION BY expression ORDER BY expression)
```

让我们更详细地检查上面的语法：

- `window_function(arg1,arg2,...)` 是窗口函数。您可以使用任何内置或用户定义的聚合函数作为窗口函数。此外，您可以使用PostgresQL[内置窗口函数](http://www.postgresql.org/docs/current/static/functions-window.html)，如 `row_number()`, `rank()`, `dense_rank()` 等。
- 要将行划分为组或分区，请使用`PARTITION BY`子句。与上面的示例一样，我们使用 `rating` 将电影表中的行划分为多个分区。如果省略 `PARTITION BY` 子句，则整个结果集是单个分区。
- 要对分区内的行进行排序，请使用 `ORDER BY` 子句。window函数将按 `ORDER BY` 子句指定的顺序处理行，特别是对于对顺序敏感的窗口函数，如 `first_value()`, `last_value()`, `nth_value()` 等。如果省略 `ORDER BY` 子句， window函数以未指定的顺序处理行。

#### ROW_NUMBER，RANK和DENSE_RANK函数

`ROW_NUMBER()`, `RANK()`, 和 `DENSE_RANK``()` 函数根据行的顺序为行分配整数值。

`ROW_NUMBER()` 函数为每个分区中的行分配一个正在运行的序列号。请参阅以下查询：

```sql
SELECT product_name, group_name, price,
 ROW_NUMBER () OVER (
 PARTITION BY group_name
 ORDER BY price
 )
FROM products
INNER JOIN product_groups USING (group_id);
    product_name    | group_name |  price  | row_number
--------------------+------------+---------+------------
 Sony VAIO          | Laptop     |  700.00 |          1
 Lenovo Thinkpad    | Laptop     |  700.00 |          2
 Dell Vostro        | Laptop     |  800.00 |          3
 HP Elite           | Laptop     | 1200.00 |          4
 Microsoft Lumia    | Smartphone |  200.00 |          1
 HTC One            | Smartphone |  400.00 |          2
 Nexus              | Smartphone |  500.00 |          3
 iPhone             | Smartphone |  900.00 |          4
 Kindle Fire        | Tablet     |  150.00 |          1
 Samsung Galaxy Tab | Tablet     |  200.00 |          2
 iPad               | Tablet     |  700.00 |          3
(11 rows)
```

有关 `ROW_NUMBER()` 及其用法的更多信息，请查看PostgreSQL [ROW_NUMBER教程](content/postgresql-row_number.md)。

`RANK()` 函数在有序分区中分配排名。如果两行的值相同，则 `RANK()` 函数分配相同的等级，跳过下一个等级。

请参阅以下查询：

```sql
SELECT product_name, group_name, price,
 RANK () OVER (
 PARTITION BY group_name
 ORDER BY price
 )
FROM products
INNER JOIN product_groups USING (group_id);
    product_name    | group_name |  price  | rank
--------------------+------------+---------+------
 Sony VAIO          | Laptop     |  700.00 |    1
 Lenovo Thinkpad    | Laptop     |  700.00 |    1
 Dell Vostro        | Laptop     |  800.00 |    3
 HP Elite           | Laptop     | 1200.00 |    4
 Microsoft Lumia    | Smartphone |  200.00 |    1
 HTC One            | Smartphone |  400.00 |    2
 Nexus              | Smartphone |  500.00 |    3
 iPhone             | Smartphone |  900.00 |    4
 Kindle Fire        | Tablet     |  150.00 |    1
 Samsung Galaxy Tab | Tablet     |  200.00 |    2
 iPad               | Tablet     |  700.00 |    3
(11 rows)
```

在笔记本电脑产品组中，`Dell Vostro` 和 `Sony VAIO` 产品具有相同的价格，因此，它们获得相同的排名1.该组中的下一行是 `HP Elite`，其获得等级3，因为排名2被跳过。

与 `RANK()` 函数类似，`DENSE_RANK()` 函数在有序分区中分配排名，但排名是连续的。换句话说，相同的等级被分配给多个行，并且不跳过任何等级。

```sql
SELECT product_name, group_name, price,
 DENSE_RANK () OVER (
 PARTITION BY group_name
 ORDER BY price
 )
FROM products
INNER JOIN product_groups USING (group_id);
    product_name    | group_name |  price  | dense_rank
--------------------+------------+---------+------------
 Sony VAIO          | Laptop     |  700.00 |          1
 Lenovo Thinkpad    | Laptop     |  700.00 |          1
 Dell Vostro        | Laptop     |  800.00 |          2
 HP Elite           | Laptop     | 1200.00 |          3
 Microsoft Lumia    | Smartphone |  200.00 |          1
 HTC One            | Smartphone |  400.00 |          2
 Nexus              | Smartphone |  500.00 |          3
 iPhone             | Smartphone |  900.00 |          4
 Kindle Fire        | Tablet     |  150.00 |          1
 Samsung Galaxy Tab | Tablet     |  200.00 |          2
 iPad               | Tablet     |  700.00 |          3
(11 rows)
```

在笔记本电脑产品组中，等级1被分配给 `Dell Vostro` 和 `Sony VAIO` 两次。下一个等级是2分配给 `HP Elite`。

#### FIRST_VALUE和LAST_VALUE函数

`FIRST_VALUE()` 函数返回有序集的第一行中的第一个值，而 `LAST_VALUE()` 函数返回结果集最后一行的最后一个值。

以下语句使用 `FIRST_VALUE()` 返回每个产品组的最低价格。

```sql
SELECT product_name, group_name, price,
 FIRST_VALUE (price) OVER (
 PARTITION BY group_name
 ORDER BY price
 ) AS lowest_price_per_group
FROM products
INNER JOIN product_groups USING (group_id);
    product_name    | group_name |  price  | lowest_price_per_group
--------------------+------------+---------+------------------------
 Sony VAIO          | Laptop     |  700.00 |                 700.00
 Lenovo Thinkpad    | Laptop     |  700.00 |                 700.00
 Dell Vostro        | Laptop     |  800.00 |                 700.00
 HP Elite           | Laptop     | 1200.00 |                 700.00
 Microsoft Lumia    | Smartphone |  200.00 |                 200.00
 HTC One            | Smartphone |  400.00 |                 200.00
 Nexus              | Smartphone |  500.00 |                 200.00
 iPhone             | Smartphone |  900.00 |                 200.00
 Kindle Fire        | Tablet     |  150.00 |                 150.00
 Samsung Galaxy Tab | Tablet     |  200.00 |                 150.00
 iPad               | Tablet     |  700.00 |                 150.00
(11 rows)
```

以下语句使用 `LAST_VALUE()` 函数返回每个产品组的最高价格。

```sql
SELECT product_name, group_name, price,
 LAST_VALUE (price) OVER (
 PARTITION BY group_name
 ORDER BY
 price RANGE BETWEEN UNBOUNDED PRECEDING
 AND UNBOUNDED FOLLOWING
 ) AS highest_price_per_group
FROM products
INNER JOIN product_groups USING (group_id);
    product_name    | group_name |  price  | highest_price_per_group
--------------------+------------+---------+-------------------------
 Sony VAIO          | Laptop     |  700.00 |                 1200.00
 Lenovo Thinkpad    | Laptop     |  700.00 |                 1200.00
 Dell Vostro        | Laptop     |  800.00 |                 1200.00
 HP Elite           | Laptop     | 1200.00 |                 1200.00
 Microsoft Lumia    | Smartphone |  200.00 |                  900.00
 HTC One            | Smartphone |  400.00 |                  900.00
 Nexus              | Smartphone |  500.00 |                  900.00
 iPhone             | Smartphone |  900.00 |                  900.00
 Kindle Fire        | Tablet     |  150.00 |                  700.00
 Samsung Galaxy Tab | Tablet     |  200.00 |                  700.00
 iPad               | Tablet     |  700.00 |                  700.00
(11 rows)
```

请注意，我们添加了窗口条款RANGE在UNBOUNDED PRECEDING和UNBOUNDED POLLOWING之间，因为默认情况下，windowing子句是UNBOUNDED PRECEDING和CURRENT ROW之间的范围。

#### LAG和LEAD函数

`LAG()` 函数能够访问前一行的数据，而 `LEAD()` 函数可以访问下一行的数据。

`LAG()` 和 `LEAD()` 函数具有相同的语法，如下所示：

```sql
LAG  (expression [,offset] [,default])
LEAD (expression [,offset] [,default])
```

- `expression` - 用于计算返回值的列或表达式。
- `offset` - 当前行之前（`LAG`）/后面（`LEAD`）的行数。`offset` 的默认值为1。
- `default` - 如果偏移超出窗口范围，则返回默认值。默认值为 `NULL`。

以下语句使用 `LAG()` 函数返回上一行的价格，并计算当前行和上一行的价格之间的差异。

```sql
SELECT product_name, group_name, price,
 LAG (price, 1) OVER (
 PARTITION BY group_name
 ORDER BY price
 ) AS prev_price,
 price - LAG (price, 1) OVER (
 PARTITION BY group_name
 ORDER BY price
 ) AS cur_prev_diff
FROM products
INNER JOIN product_groups USING (group_id);
    product_name    | group_name |  price  | prev_price | cur_prev_diff
--------------------+------------+---------+------------+---------------
 Sony VAIO          | Laptop     |  700.00 |            |
 Lenovo Thinkpad    | Laptop     |  700.00 |     700.00 |          0.00
 Dell Vostro        | Laptop     |  800.00 |     700.00 |        100.00
 HP Elite           | Laptop     | 1200.00 |     800.00 |        400.00
 Microsoft Lumia    | Smartphone |  200.00 |            |
 HTC One            | Smartphone |  400.00 |     200.00 |        200.00
 Nexus              | Smartphone |  500.00 |     400.00 |        100.00
 iPhone             | Smartphone |  900.00 |     500.00 |        400.00
 Kindle Fire        | Tablet     |  150.00 |            |
 Samsung Galaxy Tab | Tablet     |  200.00 |     150.00 |         50.00
 iPad               | Tablet     |  700.00 |     200.00 |        500.00
(11 rows)
```

以下语句使用 `LEAD()` 函数返回下一行的价格，并计算当前行和下一行的价格之间的差异。

```sql
SELECT product_name, group_name, price,
 LEAD (price, 1) OVER (
 PARTITION BY group_name
 ORDER BY price
 ) AS next_price,
 price - LEAD (price, 1) OVER (
 PARTITION BY group_name
 ORDER BY price
 ) AS cur_next_diff
FROM products
INNER JOIN product_groups USING (group_id);
    product_name    | group_name |  price  | next_price | cur_next_diff
--------------------+------------+---------+------------+---------------
 Sony VAIO          | Laptop     |  700.00 |     700.00 |          0.00
 Lenovo Thinkpad    | Laptop     |  700.00 |     800.00 |       -100.00
 Dell Vostro        | Laptop     |  800.00 |    1200.00 |       -400.00
 HP Elite           | Laptop     | 1200.00 |            |
 Microsoft Lumia    | Smartphone |  200.00 |     400.00 |       -200.00
 HTC One            | Smartphone |  400.00 |     500.00 |       -100.00
 Nexus              | Smartphone |  500.00 |     900.00 |       -400.00
 iPhone             | Smartphone |  900.00 |            |
 Kindle Fire        | Tablet     |  150.00 |     200.00 |        -50.00
 Samsung Galaxy Tab | Tablet     |  200.00 |     700.00 |       -500.00
 iPad               | Tablet     |  700.00 |            |
(11 rows)
```

在本教程中，我们向您介绍了PostgreSQL窗口函数，并向您展示了使用它们查询数据的示例。