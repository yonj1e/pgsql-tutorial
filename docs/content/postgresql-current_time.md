## CURRENT_TIME

`CURRENT_TIME` 函数返回带时区的当前时间。

#### 语法

以下说明了 `CURRENT_TIME` 函数的语法：

```sql
CURRENT_TIME(precision)
```

#### 参数

 `CURRENT_TIME` 函数接受一个可选参数：

**1) precision**

`precision` 参数指定返回的小数秒精度。如果省略 `precision` 参数，则结果将包括完整的可用精度。

#### 返回值

`CURRENT_TIME` 函数返回 `TIME WITH TIME ZONE` 值，表示带有时区的当前时间。

#### 示例

以下示例显示如何获取当前时间：

```sql
SELECT CURRENT_TIME;
```

输出为 `TIME WITH TIME ZONE` 值，如下所示：

```sql
       timetz
--------------------
 19:25:24.805985-07
(1 row)
```

在此示例中，我们未指定 `precision` 参数，因此，结果中包含可用的完整精度。

以下示例说明如何在精度设置为2的情况下使用 `CURRENT_TIME` 函数：

```sql
SELECT CURRENT_TIME(2);
     timetz
----------------
 19:26:43.01-07
(1 row)
```

`CURRENT_TIME` 函数可用作 `TIME` 列的默认值。

我们来看下面的例子。

首先，为演示创建一个名为 `log` 的表：

```sql
CREATE TABLE log (
    log_id SERIAL PRIMARY KEY,
    message VARCHAR(255) NOT NULL,
    created_at TIME DEFAULT CURRENT_TIME,
    created_on DATE DEFAULT CURRENT_DATE
);
```

`log` 表具有 `created_at` 列，其默认值是 `CURRENT_TIME` 函数的结果。

其次，在日志表中[插入](content/postgresql-insert.md)一行：

```sql
INSERT INTO log( message )
VALUES('Testing the CURRENT_TIME function');
```

在语句中，我们只为 `message` 列指定了一个值，因此，其他列获得了默认值。

第三，使用以下查询检查是否已将行插入到日志表中并正确填充 `created_at` 列：

```sql
SELECT * FROM log;
 log_id |              message              |   created_at    | created_on
--------+-----------------------------------+-----------------+------------
      1 | Testing the CURRENT_TIME function | 14:21:24.242957 | 2018-10-06
(1 row)
```

如您所见，`created_at` 列填充了执行 `INSERT` 语句的时间。

在本教程中，您学习了如何使用 `CURRENT_TIME` 函数来获取当前时间。