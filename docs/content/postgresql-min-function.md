## MIN

> 摘要：在本教程中，您将学习如何使用 `MIN` 函数来获取集合的最小值。

#### 简介

`MIN` 函数是一组聚合函数，它返回一组值中的最小值。

若要在表中查找最小列值，请将该列传递给 [SELECT](content/postgresql-select.md) 语句中的 `MIN` 函数。列的[数据类型](content/postgresql-data-types.md)可以是[数字](content/postgresql-integer.md)，[字符串](content/postgresql-char-varchar-text.md)或任何类似的类型。

 `MIN` 函数的语法如下：

```sql
SELECT MIN(column) FROM table;
```

#### 示例

我们来看一些使用 `MIN` 函数的例子。

##### SELECT

我们将使用[dvdrental示例数据库](content/postgresql-sample-database.md)中的`film` 表进行演示。

![Film Table](/imgs/film-table.png)

您经常在 `SELECT` 子句中使用 `MIN` 函数。例如，要获得电影表中电影的最低租金率，请使用以下查询：

```sql
SELECT MIN(rental_rate) FROM film;
 min
------
 0.99
(1 row)
```

查询返回0.99，这是最低租金率。

##### 子查询

要获得具有最低租金率的电影，请使用以下查询：

```sql
SELECT film_id, title, rental_rate
FROM film
WHERE rental_rate = ( 
    SELECT MIN (rental_rate) FROM film 
);
 film_id |          title          | rental_rate
---------+-------------------------+-------------
       1 | Academy Dinosaur        |        0.99
      11 | Alamo Videotape         |        0.99
      12 | Alaska Phantom          |        0.99
     213 | Date Speed              |        0.99
      14 | Alice Fantasia          |        0.99
      17 | Alone Trip              |        0.99
      18 | Alter Victory           |        0.99
      19 | Amadeus Holy            |        0.99
      23 | Anaconda Confessions    |        0.99
      26 | Annie Identity          |        0.99
      27 | Anonymous Human         |        0.99
      34 | Arabia Dogma            |        0.99
      36 | Argonauts Town          |        0.99
```

在此查询中，我们使用[子查询](content/postgresql-subquery.md)来选择最低租金率。在外部查询中，我们选择了所有租金率等于子查询返回的最低租金率的电影。下图说明了PostgreSQL执行查询的步骤：

![postgresql min function explain](/imgs/postgresql-min-function-explain.png)

##### GROUP BY

您可以在使用 [GROUP BY](content/postgresql-group-by.md) 子句的查询中使用 `MIN` 函数来查找所有组的每个组的最小值。例如，对于具有相同评级的每组电影，以下查询查找最低租金率。

```sql
SELECT rating, MIN (rental_rate)
FROM film
GROUP BY rating
ORDER BY rating;
```

![PostgreSQL MIN function group by](/imgs/postgresql-min-function-group-by.png)

##### HAVING

您不仅可以在 `SELECT` 子句中使用 `MIN` 函数，还可以在 [HAVING](content/postgresql-having.md) 子句中使用 `MIN` 函数来过滤最小值与特定条件匹配的组。

例如，以下查询查找按评级分组的电影的最低重置成本，并仅选择重置成本大于9的评级。

```sql
SELECT rating, MIN (replacement_cost)
FROM film
GROUP BY rating
HAVING MIN (replacement_cost) > 9;
 rating | min
--------+------
 PG-13  | 9.99
 NC-17  | 9.99
 G      | 9.99
 PG     | 9.99
 R      | 9.99
(5 rows)
```

##### 其他聚合函数

您可以在同一查询中将 `MIN` 函数与 [MAX](content/postgresql-max-function.md) 函数结合使用，以查找每组的最小值和最大值。例如，对于具有相同评级的每组电影，以下查询选择最小和最大租赁费率。

例如，对于具有相同评级的每组电影，以下查询选择最小和最大租赁费率。

```sql
SELECT rating, MIN (rental_rate), MAX (rental_rate)
FROM film
GROUP BY rating;
 rating | min  | max
--------+------+------
 PG-13  | 0.99 | 4.99
 NC-17  | 0.99 | 4.99
 G      | 0.99 | 4.99
 PG     | 0.99 | 4.99
 R      | 0.99 | 4.99
(5 rows)
```

#### 从两列或更多列中查找最小值

假设您有以下 `ranks` 表：

```sql
CREATE TABLE ranks ( 
    user_id INT PRIMARY KEY, 
    rank_1 int4 NOT NULL, 
    rank_2 int4 NOT NULL, 
    rank_3 int4 NOT NULL
);
```

及其样本数据：

```sql
INSERT INTO ranksVALUES (1, 6, 3, 5), (2, 2, 8, 5), (3, 5, 9, 8);
```

现在要求是为每个用户找到其最小可能的排名如下：

```sql
select * from ranks ;
 user_id | rank_1 | rank_2 | rank_3
---------+--------+--------+--------
       1 |      6 |      3 |      5
       2 |      2 |      8 |      5
       3 |      5 |      9 |      8
(3 rows)
```

您不能使用 `MIN` 函数，因为 `MIN` 函数应用于行而不是列。要执行此操作，请使用 `LEAST` 函数，该函数返回值列表中的最小值。

```sql
SELECT user_id, LEAST (rank_1, rank_2, rank_3) AS smallest_rank FROM ranks;
 user_id | smallest_rank
---------+---------------
       1 |             3
       2 |             2
       3 |             5
(3 rows)
```

查询返回我们预期的结果。

在本教程中，我们向您展示了如何使用 `MIN` 函数获取一组值中的最小值。

#### 相关教程

- [MAX](content/postgresql-max-function.md)
- [COUNT](content/postgresql-count-function.md)
- [AVG](content/postgresql-avg-function.md)
- [SUM](content/postgresql-sum-function.md)