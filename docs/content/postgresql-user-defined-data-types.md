## 用户自定义类型

> 摘要：本教程介绍如何使用 `CREATE DOMAIN` 和 `CREATE TYPE` 语句创建PostgreSQL用户定义的数据类型。

除了内置[数据类型](content/postgresql-data-types.md)，PostgreSQL还允许您通过以下语句创建用户定义的数据类型：

- `CREATE DOMAIN` 创建一个用户定义的数据类型，其中包含 `NOT NULL`，`CHECK` 等约束。
- `CREATE TYPE` 通常用于创建[存储过程](content/postgresql-stored-procedures.md)中使用的复合类型作为返回数据类型。

#### CREATE DOMAIN

在PostgreSQL中，`create domain` 可以创建域，域是具有可选约束的数据类型，例如，[NOT NULL](content/postgresql-not-null-constraint.md)，`CHECK` 等等。域在架构范围内具有唯一名称。

域可用于集中管理具有公共约束的字段。例如，某些表可能包含需要 ·CHECK· 约束的文本列，以确保值不为null且不包含空格。

以下语句创建一个名为 `mail_list` 的表：

```sql
CREATE TABLE mail_list (
    ID SERIAL PRIMARY KEY,
    first_name VARCHAR NOT NULL,
    last_name VARCHAR NOT NULL,
    email VARCHAR NOT NULL,
    CHECK (
        first_name !~ '\s'
        AND last_name !~ '\s'
    )
);
```

在此表中，`first_name` 和 `last_name` 列均不为null，并且不应包含空格。为了便于管理，您可以按如下方式创建 `contact_name` 域：

```sql
CREATE DOMAIN contact_name AS 
    VARCHAR NOT NULL CHECK (value !~ '\s');
```

并使用 `contact_name` 作为 `first_name` 和 `last_name` 列的数据类型：

```sql
CREATE TABLE mail_list (
    id serial PRIMARY KEY,
    first_name contact_name,
    last_name contact_name,
    email VARCHAR NOT NULL
);
```

让我们在 `mail_list` 表中插入一个新行：

```sql
INSERT INTO mail_list (first_name, last_name, email)
VALUES
 (
  'Jame V', 'Doe', 'jame.doe@example.com'
 );
```

PostgreSQL发出以下错误：

```sql
ERROR:  value for domain contact_name violates check constraint "contact_name_check"
```

如您所见，它按预期工作。

要更改或删除域，请分别使用 `ALTER DOMAIN` 或 `DROP DOMAIN`。

要查看当前数据库中的所有域，请使用 `\dD` 命令，如下所示：

```sql
test=#\dD
                                     List of domains
 Schema |     Name     |       Type        | Modifier |               Check
--------+--------------+-------------------+----------+-----------------------------------
 public | contact_name | character varying | not null | CHECK (VALUE::text !~ '\s'::text)
(1 row)
```

#### CREATE TYPE

`CREATE TYPE` 语句允许您创建复合类型，该类型可用作函数的返回类型。

假设您想要一个返回多个值的函数：`film_id`，`title` 和 `release_year`。第一步是创建一个类型，例如`film_summary`，如下所示：

```sql
CREATE TYPE film_summary AS (
    film_id INT,
    title VARCHAR,
    release_year YEAR
); 
```

其次，使用 `film_summary` 数据类型作为函数的返回类型：

```sql
CREATE OR REPLACE FUNCTION get_film_summary (f_id INT) 
    RETURNS film_summary AS 
$$ 
SELECT
    film_id,
    title,
    release_year
FROM
    film
WHERE
    film_id = f_id ; 
$$ 
LANGUAGE SQL;
```

三，调用 `get_film_summary()` 函数：

```sql
SELECT * FROM get_film_summary (40);
 film_id |      title       | release_year
---------+------------------+--------------
      40 | Army Flintstones |         2006
(1 row)
```

要更改类型或删除类型，请分别使用 `ALTER TYPE` 或 `DROP TYPE` 语句。

列出当前数据库中所有用户定义类型的命令是 `\dT` 或 `\dT+`：

```sql
dvdrental=# \dT
         List of data types
 Schema |     Name     | Description
--------+--------------+-------------
 public | film_summary |
 public | mpaa_rating  |
 public | year         |
(3 rows)
```

在本教程中，您学习了如何使用 `CREATE DOMAIN` 和 `CREATE TYPE` 语句创建PostgreSQL用户定义类型。