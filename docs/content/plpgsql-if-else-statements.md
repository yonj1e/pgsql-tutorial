## PL/pgSQL IF语句

> 摘要：在本教程中，我们将向您展示如何使用PL/pgSQL IF语句根据特定条件执行命令。

`IF` 语句用于有条件地执行命令。PL/pgSQL为您提供了三种形式的IF语句。

#### IF

如果条件为真，则 `IF` 语句执行语句。如果条件的计算结果为 `false`，则控制将传递到 `END IF` 部分之后的下一个语句。以下说明了 `IF` 语句的最简单形式：

```sql
IF condition THEN   
	statement;
END IF;
```

`condition` 是一个布尔表达式，其计算结果为true或false。

该语句是一个语句，如果条件为真，将执行该语句。它可以是任何有效的语句，甚至是另一个 `IF` 语句。`IF` 语句放在另一个 `IF` 语句中，称为嵌套IF语句。

以下流程图说明了简单的IF语句。

![PL/pgSQL IF statement](/imgs/plpgsql-if-statement.png)

See the following example:

请参阅以下示例：

```sql
DO $$
DECLARE
  a integer := 10;
  b integer := 20;
BEGIN 
  IF a > b THEN
 RAISE NOTICE 'a is greater than b';
  END IF;
 
  IF a < b THEN
 RAISE NOTICE 'a is less than b';
  END IF;
 
  IF a = b THEN
 RAISE NOTICE 'a is equal to b';
  END IF;
END $$;
```

在这个例子中，我们声明了两个[变量](content/plpgsql-variables.md)a和b。在块的主体中，我们使用IF语句的布尔表达式中的比较运算符 `>, < 和 =` 来比较a和b的值，并打印出相应的消息。

因为a小于b，所以在执行块时会收到以下输出：

```sql
NOTICE:  a is less than b
```

#### IF THEN ELSE

`IF THEN ELSE` 语句在条件为true时执行命令，并在条件为false时执行另一个命令。以下说明了 `IF THEN ELSE` 语句的语法：

```sql
IF condition THEN
  statements;
ELSE
  alternative-statements;
END IF;
```

以下流程图说明了 `IF ELSE` 语句。

![PL/pgSQL if else statement](/imgs/plpgsql-if-else-statement.png)

请参阅以下示例：

```sql
DO $$
DECLARE
  a integer := 10;
  b integer := 20;
BEGIN 
   IF a > b THEN 
      RAISE NOTICE 'a is greater than b';
   ELSE
      RAISE NOTICE 'a is not greater than b';
   END IF;
END $$;
```

因为表达式 a > b 为false，所以执行 `ELSE` 子句中的语句。我们得到以下输出：

```sql
NOTICE:  a is not greater than b
```

#### IF THEN ELSIF THEN ELSE

IF声明的最终形式如下：

```sql
IF condition-1 THEN
  if-statement;
ELSIF condition-2 THEN
  elsif-statement-2
...
ELSIF condition-n THEN
  elsif-statement-n;
ELSE
  else-statement;
END IF:
```

`IF` 和 `IF THEN ELSE` 语句允许您只有一个条件来判断。但是，使用这种形式的 `IF` 语句，您可能有多个条件需要判断。

条件为true，执行该分支中的相应语句。例如，如果condition-1，condition-2等为true，则执行相应的语句：if-statement，elseif-statement-2等。如果一个条件为真，PostgreSQL将停止判断下面的条件。

如果所有条件都为假，则执行最后一个 ·ELSE· 分支中的语句。

以下流程图说明了 ·IF ELSIF ELSE· 语句。

![PL/pgSQL IF ELSIF ELSE Statement](/imgs/if-elsif-else-statement.png)

我们来看看下面的例子：

```sql
DO $$
DECLARE
   a integer := 10;
   b integer := 10;
BEGIN 
  IF a > b THEN 
     RAISE NOTICE 'a is greater than b';
  ELSIF a < b THEN
     RAISE NOTICE 'a is less than b';
  ELSE
     RAISE NOTICE 'a is equal to b';
  END IF;
END $$;
```

在此示例中，因为a等于b，所以执行 ·ELSE· 分支中的语句。

```
NOTICE: a is equal to b
```

在本教程中，您学习了三种形式的IF语句，以根据特定条件执行语句。