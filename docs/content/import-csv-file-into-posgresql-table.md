## 将CSV文件导入表

> 摘要：在本教程中，我们将展示将CSV文件导入PosgreSQL表的各种方法。

我们将创建一个名为 `persons` 的新表，其中包含以下列：

- `id`：人员ID
- `first_name`：此人的名字
- `last_name`：此人的姓氏
- `dob`：该人的出生日期
- `email`：此人的电子邮件地址

我们执行以下[CREATE TABLE](content/postgresql-create-table.md)语句来创建 `persons` 表：

```sql
CREATE TABLE persons(  
    id serial NOT NULL,  
    first_name character varying(50),  
    last_name character varying(50),  
    dob date,  
    email character varying(255),  
    CONSTRAINT persons_pkey PRIMARY KEY (id)
);
```

![posgresql import csv](/imgs/posgresql-import-csv.jpg)

我们准备CSV数据文件。您可以使用PC中可用的任何应用程序来执行此操作。

```shell
$ cat /work/test/dvdrental/persons.csv
First Name,Last Name,Date of Birth,Email
John,Doe,1990-01-05,john.doe@postgres.cn
Lily,Bush,1995-02-05,lily.bush@postgres.cn
```

CSV文件的路径如下：`/work/test/dvdrental/persons.csv`

#### 使用COPY语句将CSV文件导入表中

要将此CSV文件导入到 `persons` 表中，请使用 `COPY` 语句，如下所示：

```sql
COPY persons(first_name,last_name,dob,email)
FROM '/work/test/dvdrental/persons.csv' DELIMITER ',' CSV HEADER;
COPY 2
```

我们来查看 `persons` 表。

```sql
SELECT * FROM persons;
 id | first_name | last_name |    dob     |         email
----+------------+-----------+------------+-----------------------
  1 | John       | Doe       | 1990-01-05 | john.doe@postgres.cn
  2 | Lily       | Bush      | 1995-02-05 | lily.bush@postgres.cn
(2 rows)
```

让我们更详细地介绍一下COPY声明。

```sql
COPY persons(first_name,last_name,dob,email)
FROM '/work/test/dvdrental/persons.csv' DELIMITER ',' CSV HEADER;
```

首先，在COPY关键字后指定具有列名的表。列的顺序必须与CSV文件中的顺序相同。如果CSV文件包含表的所有列，则不必明确指定它们，例如：

```sql
COPY sample_table
FROM '/work/test/dvdrental/sample_data.csv' DELIMITER ',' CSV HEADER;
```

其次，将CSV文件路径放在 `FROM` 关键字之后。由于使用了CSV文件格式，因此您需要指定 `DELIMITER` 以及 `CSV` 关键字。

第三，`HEADER关` 键字表示CSV文件包含带有列名的标题行。导入数据时，PostgreSQL会忽略第一行，即文件的标题行。

请注意，文件必须由PosgreSQL服务器直接读取，而不是由客户端应用程序读取。因此，它必须可供PosgreSQL服务器机器访问。如果您具有超级用户访问权限，也可以成功执行COPY语句。

在本教程中，我们向您展示了如何使用COPY语句将CSV文件中的数据导入表中。