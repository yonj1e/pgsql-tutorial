## TRUNCATE TABLE

> 摘要：本教程向您展示如何使用PostgreSQL TRUNCATE TABLE语句快速删除大表中的所有数据。

要从表中删除所有数据，请使用 `DELETE` 语句。但是，对于大型表，使用 `TRUNCATE TABLE` 语句更有效。

`TRUNCATE TABLE` 语句从表中删除所有行而不扫描它。这就是为什么它比 `DELETE` 语句更快的原因。

此外，`TRUNCATE TABLE` 语句立即回收存储，因此您不必执行后续的 `VACUMM` 操作，这在大型表的情况下很有用。

#### 从一个表中删除所有数据

`TRUNCATE TABLE` 语句的最简单形式如下：

```sql
TRUNCATE TABLE table_name;
```

例如，要从 `invoices` 表中删除所有行，请使用以下语句：

```sql
TRUNCATE TABLE invoices;
```

PostgreSQL `TRUNCATE TABLE` 语句不仅允许您从表中删除所有数据，还可以通过指定 `RESTART IDENTITY` 选项重置关联的序列生成器，如下所示：

```sql
TRUNCATE TABLE table_name RESTART IDENTITY;
```

例如，要从 `invoices` 表中删除所有行并重置与 `invoice_no` 列关联的序列，请使用以下语句：

```sql
TRUNCATE TABLE invoices RESTART IDENTITY;
```

默认情况下，`TRUNCATE TABLE` 语句不会重置关联的序列。您可以在语句中显式使用 `CONTINUE IDENTITY` 选项来保留序列。但是，这不是必需的。

#### 从多个表中删除所有数据

要一次删除多个表中的所有数据，请使用逗号（，）分隔每个表，如下所示：

```sql
TRUNCATE TABLE table_name1, table_name2, …
```

例如，以下语句从 `invoices` 和 `customers` 表中删除所有数据：

```sql
TRUNCATE TABLE invoices, customers;
```

#### 从带有外键引用的表中删除所有数据

实际上，要truncate的表通常具有其他表中的[外键](content/postgresql-foreign-key.md)引用，这些引用未在 `TRUNCATE TABLE` 语句中列出。默认情况下，`TRUNCATE TABLE` 语句不会从具有外键引用的表中删除任何数据。

要从主表和所有对主表具有外键引用的表中删除数据，请使用 `CASCADE` 选项，如下所示：

```sql
TRUNCATE TABLE table_name CASCADE;
```

例如，要从 `invoices` 表中删除所有数据并级联到通过[外键](content/postgresql-foreign-key.md)约束引用 `invoices` 表的任何表，请使用以下语句：

```sql
TRUNCATE TABLE invoices CASCADE;
```

使用 `CASCADE` 操作时应该小心，否则可能会从表中删除您不想要的数据。

#### TRUNCATE TABLE 和 ON DELETE 触发器

即使 `TRUNCATE TABLE` 语句从表中删除所有数据，它也不会触发与该表关联的任何 `ON DELETE` [触发器](content/postgresql-triggers.md)。

要在应用于表的 `TRUNCATE TABLE` 命令时触发触发器，必须为该表定义 `BEFORE TRUNCATE` 和/或 `AFTER TRUNCATE` 触发器。

#### TRUNCATE TABLE和事务

`TRUNCATE TABLE` 是事务安全的。这意味着如果将它放在诸如 `BEGIN ... ROLLBACK` 之类的事务语句中，truncate 操作将安全回滚。

在本教程中，我们向您展示了如何使用 `TRUNCATE TABLE` 语句快速有效地从大型表中删除所有数据。此外，我们向您介绍了该语句提供的其他一些选项。