## ALTER DATABASE

> 摘要：在本教程中，您将学习如何使用PostgreSQL ALTER DATABASE语句修改现有数据库。

#### 简介

创建数据库后，可以使用 `ALTER DATABASE` 语句更改其功能，如下所示：

```sql
ALTER DATABASE target_database action;
```

您可以在 `ALTER DATABASE` 之后指定要更改的数据库名称。PostgreSQL允许您对现有数据库执行各种操作。让我们更详细地研究每个操作。

##### Rename database

要重命名数据库，请使用 `ALTER DATABASE RENAME TO` 语句，如下所示：

```sql
ALTER DATABASE target_database RENAME TO new_database;
```

要重命名数据库，您必须连接到另一个数据库，例如 `postgres`。

##### Change owner

要更改数据库的所有者，请使用 `ALTER DATABASE OWNER TO` 作为以下语句：

```sql
ALTER DATABASE target_database OWNER TO new_onwer;
```

只有数据库的超级用户所有者才能更改数据库的所有者。数据库所有者还必须具有 `CREATEDB` 权限才能重命名数据库。

##### Change tablespace

要更改数据库的默认表空间，请使用 `ALTER DATABASE SET TABLESPACE`，如下所示：

```sql
ALTER DATABASE target_database SET TABLESPACE new_tablespace;
```

该语句将表和索引从旧表空间移动到新表空间。

##### Change session defaults for run-time configuration variables

每当您连接到数据库时，PostgreSQL都会加载postgresql.conf文件中显示的配置变量，并默认使用这些变量。

要覆盖特定数据库的这些设置，请使用 `ALTER DATABASE SET` 语句，如下所示：

```sql
ALTER DATABASE target_database SET configuration_parameter = value;
```

在随后的会话中，PostgreSQL将覆盖 `postgresql.conf` 文件中的设置。

请注意，只有 `supperuser` 或者数据库所有者才能更改数据库的默认会话变量。

#### 示例

首先，让我们以用户 `postgres` 身份登录，并为演示创建一个名为 `testdb2` 的新数据库。

```sql
CREATE DATABASE testdb2;
```

其次，使用以下语句将 `testdb2` 重命名为 `testhrdb`;

```sql
ALTER DATABASE testdb2 RENAME TO testhrdb;
```

第三，执行以下语句更改 `testhrdb` 数据库的所有者从 `postgres` 改为 `hr`，并假设角色 `hr` 已存在。

```sql
ALTER DATABASE testhrdb OWNER TO hr;
```

如果角色 `hr` 不存在，请使用以下语句创建它：

```sql
CREATE ROLE hr 
VALID UNTIL 'infinity';
```

第四，假设 `hr_default` 表空间已经存在，将 `testhrdb` 的默认表空间从 `pg_default` 更改为 `hr_default`。

```sql
ALTER DATABASE testhrdb
SET TABLESPACE hr_default;
```

如果 `hr_default` 表空间不存在，则可以使用以下语句创建它：

```sql
CREATE TABLESPACE hr_default 
OWNER hr 
LOCATION E'C:\pgdata\hr';
```

五，要将 `escape_string_warning` 变量配置设置为off，可以使用以下语句：

```sql
ALTER DATABASE testhrdb SET escape_string_warning TO off;
```

在本教程中，我们向您展示了如何使用PostgreSQL `ALTER DATABASE` 语句更改现有数据库的功能和配置参数。