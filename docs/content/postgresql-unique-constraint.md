## 唯一约束

> 摘要：在本教程中，您将了解PostgreSQL UNIQUE约束，以确保列或列组中的值在表中是唯一的。

有时，您希望确保整个表中列或列组中的值是唯一的，例如电子邮件地址，用户名，员工ID等。PostgreSQL为您提供UNIQUE约束，以便正确地维护数据的唯一性。

使用UNIQUE约束时，每次[插入](content/postgresql-insert.md)新行时，PostgreSQL都会检查该值是否已在表中。如果它发现新值已经存在，它将返回错误消息并拒绝更改。对[更新](ontent/postgresql-update.md)现有数据执行相同的过程。

向列或一组列添加UNIQUE约束时，PostgreSQL将自动在相应列或一组列上创建btree索引。

#### 示例

以下[CREATE TABLE](content/postgresql-create-table.md)语句创建一个名为 `person` 的新表，其中UNIQUE约束应用于 `email` 列。

```sql
CREATE TABLE person (
 id serial PRIMARY KEY,
 first_name VARCHAR (50),
 last_name VARCHAR (50),
 email VARCHAR (50) UNIQUE
);
```

可以将UNIQUE约束重写为表约束，如下所示：

```sql
CREATE TABLE person (
 id SERIAL  PRIMARY KEY,
 first_name VARCHAR (50),
 last_name  VARCHAR (50),
 email      VARCHAR (50),
 UNIQUE(email)
);
```

首先，我们使用[INSERT](content/postgresql-insert.md)语句在 `person` 表中插入一个新行：

```sql
INSERT INTO person(first_name,last_name,email)
VALUES
 (
     'john', 'doe', 'j.doe@postgresqltutorial.com'
 );
```

其次，我们插入另一行重复的电子邮件。

```sql
INSERT INTO person(first_name,last_name,email)
VALUES
 (
     'jack', 'doe', 'j.doe@postgresqltutorial.com'
 );
```

PostgreSQL给出了一条错误消息。

```sql
[Err] ERROR:  duplicate key value violates unique constraint "person_email_key"
DETAIL:  Key (email)=(j.doe@postgresqltutorial.com) already exists.
```

#### 在多列上应用UNIQUE约束

PostgreSQL允许您使用以下语法将UNIQUE约束应用于一组列：

```sql
CREATE TABLE table (
    c1 data_type,
    c2 data_type,
    c3 data_type,
    UNIQUE (c2, c3)
);
```

列c2和c3中值的组合在整个表中是唯一的。列c2或c3的值不必是唯一的。

#### 使用唯一索引添加唯一约束

有时，您可能希望使用现有唯一索引向列或列组添加唯一约束。我们来看看下面的例子。

首先，假设我们有一个名为 `equipment` 的表：

```sql
CREATE TABLE equipment (
 id serial PRIMARY KEY,
 name VARCHAR (50) NOT NULL,
 equip_id VARCHAR (16) NOT NULL
);
```

其次，我们基于 `equip_id` 列创建唯一索引。

```sql
CREATE UNIQUE INDEX CONCURRENTLY equipment_equip_id 
ON equipment (equip_id);
```

第三，我们使用`equipment_equip_id` 索引向 `equipment` 表添加唯一约束。

```sql
ALTER TABLE equipment 
ADD CONSTRAINT unique_equip_id 
UNIQUE USING INDEX equipment_equip_id;
```

请注意，[ALTER TABLE](content/postgresql-alter-table.md)语句需要对表进行独占锁定。如果您有许多待处理的事务，它将在更改表之前等待这些事务完成。您应该使用以下查询检查 `pg_stat_activity` 表以查看有多少挂起的事务：

```sql
SELECT datid, datname, usename, state
FROM pg_stat_activity;
```

您应该查看结果以查找事务中值为 `idle` 的 `state` 列。这些是待完成的事务。

在本教程中，我们向您展示了如何使用UNIQUE约束使整个表中的列或列组的值唯一。

#### 相关教程

- [Foreign Key](content/postgresql-foreign-key.md)
- [CHECK Constraint](content/postgresql-check-constraint.md)
- [ CREATE TABLE](content/postgresql-create-table.md)
- [Primary Key](content/postgresql-primary-key.md)
- [Not-Null Constraint](content/postgresql-not-null-constraint.md)