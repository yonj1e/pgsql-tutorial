## 临时表

![PostgreSQL Temporary Table](/imgs/PostgreSQL-Temporary-Table-300x254.png)

> 摘要：在本教程中，您将了解PostgreSQL临时表以及如何有效地管理它。

#### 创建临时表

临时表，如其命名所暗示的，是一个在数据库会话期间存在的临时表。PostgreSQL会在会话或事务结束时自动[删除](content/postgresql-drop-table.md)临时表。

要创建临时表，请使用 `CREATE TEMPORARY TABLE` 语句。

```sql
CREATE TEMPORARY TABLE temp_table(
   ...
);
```

在上面的语句中可以使用 `TEMP` 而不是 `TEMPORARY` 关键字：

```sql
CREATE TEMP TABLE temp_table(
   ...
);
```

临时表仅对创建它的会话可见。换句话说，它对其他会话仍然是不可见的。

我们来看一个例子吧。

首先，使用 `psql` 程序登录数据库服务器并创建一个新的数据库 `test`：

```sql
postgres=# CREATE DATABASE test;
CREATE DATABASE
postgres-# \c test;
You are now connected to database "test" as user "postgres".
```

接下来，创建一个名为 `mytemp` 的临时表，如下所示：

```sql
test=# CREATE TEMP TABLE mytemp(c INT);
CREATE TABLE
test=# SELECT * FROM mytemp;
 c
---
(0 rows)
```

然后，启动另一个会话连接到 `test` 数据库并从 `mytemp` 表查询数据：

```sql
test=# SELECT * FROM mytemp;
ERROR:  relation "mytemp" does not exist
LINE 1: SELECT * FROM mytemp;
```

可以看出，第二个会话无法看到 `mytemp` 表。只有第一个会话才能访问它。

之后，退出所有会话：

```sql
test=# \q
```

最后，再次登录数据库服务器并从 `mytemp` 表中查询数据：

```sql
test=# SELECT * FROM mytemp;
ERROR:  relation "mytemp" does not exist
LINE 1: SELECT * FROM mytemp;
                      ^
```

`mytemp` 表不存在，因为它在会话结束时自动被删除，因此，PostgreSQL发出错误。

#### 临时表名

临时表可以与永久表共享相同的名称，即使不建议使用。创建与永久表同名的临时表时，在删除临时表之前无法访问永久表。请考虑以下示例：

首先，创建一个名为 `customers` 的表：

```sql
test=# CREATE TABLE customers(id SERIAL PRIMARY KEY, name VARCHAR NOT NULL);
```

其次，创建一个具有相同名称的临时表：`customers`

```sql
test=# CREATE TEMP TABLE customers(customer_id INT);
```

现在，我们从 `customers` 表中查询数据：

```sql
test=# SELECT * FROM customers;
 customer_id
-------------
(0 rows)
```

这次PostgreSQL访问了临时表 `customers` 而不是永久表 `customers`。从现在开始，只有在显式删除临时表 `custoemrs` 时，才能访问当前会话中的永久客户表。

请注意，PostgreSQL在特殊模式中创建临时表，因此，您不能在 `CREATE TEMP TABLE` 语句中指定模式。

如果[列出](content/postgresql-show-tables.md)测试数据库中的表，则只能看到临时客户表，而不是永久客户表：

```sql
                 List of relations
  Schema   |       Name       |   Type   |  Owner
-----------+------------------+----------+----------
 pg_temp_3 | customers        | table    | postgres
 public    | customers_id_seq | sequence | postgres
(2 rows)
```

输出显示 `customers` 临时表的模式为 `pg_temp_3` 。

#### 删除临时表

要删除临时表，请使用 [DROP TABLE](content/postgresql-drop-table.md) 语句。以下语句说明了如何删除临时表：

```sql
DROP TABLE temp_table_name;
```

与 `CREATE TABLE` 语句不同，`DROP TABLE` 语句没有专门为临时表创建的 `TEMP `或 `TEMPORARY` 关键字。

例如，以下语句删除了我们在上面的示例中创建的临时表 `customers`：

```sql
test=# DROP TABLE customers;
```

如果再次列出测试数据库中的表，则永久表 `customers` 将显示如下：

```sql
test=# \d
                List of relations
 Schema |       Name       |   Type   |  Owner
--------+------------------+----------+----------
 public | customers        | table    | postgres
 public | customers_id_seq | sequence | postgres
(2 rows)
```

在本教程中，您已了解临时表以及如何使用 `CREATE TEMP TABLE` 和 `DROP TABLE` 语句创建和删除它。

#### 相关教程

- [DROP TABLE](content/postgresql-drop-table.md)
- [CREATE TABLE](content/postgresql-create-table.md)