## SELECT

> 摘要：在本教程中，您将学习如何使用基本的 `SELECT` 语句来查询表中的数据。

使用PostgreSQL时，最常见的操作之一是使用 `SELECT` 语句从表中查询数据。 `SELECT` 语句是PostgreSQL中最复杂的语句之一。它有许多子句，您可以组合起来形成一个强大的查询。

由于其复杂性，我们将 `SELECT` 语句教程分为许多较短的教程，以便您可以更轻松地学习 `SELECT` 语句的每个子句。以下是 `SELECT` 语句中出现的子句：

- 使用 `DISTINCT` 运算符选择不同的行。
- 使用` WHERE` 子句筛选行。
- 使用 `ORDER BY` 子句对行进行排序。
- 根据 `BETWEEN` ，`IN` 和 `LIKE` 等各种运算符选择行。
- 使用 `GROUP BY` 子句将行分组
- 使用 `HAVING` 子句为组应用条件。
- 使用 `INNER JOIN`，`LEFT JOIN`，`FULL OUTER JOIN`，`CROSS JOIN`子句将表连接到其他表。

在本教程中，您将重点关注 `SELECT` 和 `FROM` 子句。

####  语法

让我们从 `SELECT` 语句的基本形式开始，该语句从单个表中检索数据。

以下说明了 `SELECT` 语句的语法：

```sql
SELECT column_1, column_2, ...FROM table_name;
```

让我们更详细地研究 `SELECT` 语句：

- 首先，要在 `SELECT` 子句中指定查询表的列。 如果从多个列检索数据，则使用逗号分隔两列。 如果要查询所有列中的数据，可以使用星号（*）作为简写。
- 其次，在 `FROM` 关键字后面指定表名。

请注意，SQL语言不区分大小写。 这意味着 `SELECT` 或 `select` 具有相同的效果。 按照惯例，SQL关键字我们将使用大写来使代码更容易阅读。

#### 示例

我们来看一些使用 `SELECT` 语句的例子。 我们将使用[示例数据库](/content/postgresql-sample-database.md)中的 `customers` 表进行演示。

![customer table](/imgs/customer-table.png)

要查询 `customer` 表的所有行和列的数据，请使用以下查询：

```sql
SELECT *FROM customer;
 customer_id | store_id | first_name  |  last_name   |                  email                   | address_id | activebool | create_date |       last_update       | active
-------------+----------+-------------+--------------+------------------------------------------+------------+------------+-------------+-------------------------+--------
         524 |        1 | Jared       | Ely          | jared.ely@sakilacustomer.org             |        530 | t          | 2006-02-14  | 2013-05-26 14:49:45.738 |      1
           1 |        1 | Mary        | Smith        | mary.smith@sakilacustomer.org            |          5 | t          | 2006-02-14  | 2013-05-26 14:49:45.738 |      1
           2 |        1 | Patricia    | Johnson      | patricia.johnson@sakilacustomer.org      |          6 | t          | 2006-02-14  | 2013-05-26 14:49:45.738 |      1
           3 |        1 | Linda       | Williams     | linda.williams@sakilacustomer.org        |          7 | t          | 2006-02-14  | 2013-05-26 14:49:45.738 |      1
           4 |        2 | Barbara     | Jones        | barbara.jones@sakilacustomer.org         |          8 | t          | 2006-02-14  | 2013-05-26 14:49:45.738 |      1
           5 |        1 | Elizabeth   | Brown        | elizabeth.brown@sakilacustomer.org       |          9 | t          | 2006-02-14  | 2013-05-26 14:49:45.738 |      1
           6 |        2 | Jennifer    | Davis        | jennifer.davis@sakilacustomer.org        |         10 | t          | 2006-02-14  | 2013-05-26 14:49:45.738 |      1
           7 |        1 | Maria       | Miller       | maria.miller@sakilacustomer.org          |         11 | t          | 2006-02-14  | 2013-05-26 14:49:45.738 |      1
           8 |        2 | Susan       | Wilson       | susan.wilson@sakilacustomer.org          |         12 | t          | 2006-02-14  | 2013-05-26 14:49:45.738 |      1
```

请注意，我们在 `SELECT` 语句的末尾添加了一个分号（;）。 分号不是SQL语句的一部分。 只有PostgreSQL才能指定SQL语句的结尾。

在 `SELECT`语句中使用 `*` 不是一个好习惯。 想象一下，你有一个包含许多列的大表，带有星号的 `SELECT` 语句将检索整个列中的所有数据，这可能不是必需的。 此外，从表中检索不必要的数据会增加数据库服务器和应用程序之间的流量。 因此，您的应用程序将变得缓慢且可扩展性降低。 因此，最好在 `SELECT` 子句中显式指定列名，以便只从表中获取所需的数据。

假设您只想知道客户的名字，姓氏和电子邮件，您可以在 `SELECT` 语句中指定列名，如下所示：

```sql
SELECT first_name, last_name, email FROM customer;
 first_name  |  last_name   |                  email
-------------+--------------+------------------------------------------
 Jared       | Ely          | jared.ely@sakilacustomer.org
 Mary        | Smith        | mary.smith@sakilacustomer.org
 Patricia    | Johnson      | patricia.johnson@sakilacustomer.org
 Linda       | Williams     | linda.williams@sakilacustomer.org
 Barbara     | Jones        | barbara.jones@sakilacustomer.org
 Elizabeth   | Brown        | elizabeth.brown@sakilacustomer.org
```

在本教程中，您学习了如何使用 `SELECT` 语句的基本形式来查询数据库表中的数据。

#### 相关教程

- [LIMIT](/content/postgresql-limit.md)