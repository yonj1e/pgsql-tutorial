## DELETE

> 摘要：在本教程中，您将学习如何使用PostgreSQL DELETE语句从表中删除数据。

#### 简介

要从表中删除数据，请使用 `DELETE` 语句，如下所示：

```sql
DELETE FROM table
WHERE condition;
```

在这个语法中：

- 首先，在 `DELETE FROM` 子句中指定要从中删除数据的表。
- 其次，使用 `WHERE` 子句中的条件指定要删除的行。`WHERE` 子句是可选的。但是，如果省略它，`DELETE` 语句将删除表中的所有行。

`DELETE` 语句返回已删除行的数量。它可能与 `WHERE` 子句中的条件指定的行数不同，因为该表可能包含 `BEFORE DELETE` 触发器，它在删除之前执行其他操作。如果没有删除行，则 `DELETE` 语句返回零。

如果要引用另一个表中的一个或多个列做检查条件，请使用 `USING` 子句，如下所示：

```sql
DELETE FROM table
USING another_table
WHERE table.id = another_table.id AND …
```

如果您不想使用 `USING` 子句，则可以使用[子查询](content/postgresql-subquery.md)，如以下语句所示：

```sql
DELETE FROM table
WHERE table.id = (SELECT id FROM another_table);
```

#### 示例

我们将使用我们在[插入](content/postgresql-insert.md)数据教程中创建的 `link` 和 `link_tmp` 表。以下说明了 `link` 表的内容：

```sql
SELECT * FROM link;
 id |                url                |        name         |          description           |   rel    | last_update
----+-----------------------------------+---------------------+--------------------------------+----------+-------------
  6 | http://www.facebook.com           | Facebook            | Facebook                       | nofollow | 2013-06-01
  7 | https://www.tumblr.com/           | Tumblr              | Tumblr                         | nofollow | 2018-09-14
  8 | http://www.postgresql.org         | PostgreSQL          | PostgreSQL                     | nofollow | 2018-09-14
  2 | http://www.oreilly.com            | O'Reilly Media      | O'Reilly Media                 | nofollow | 2018-09-14
  3 | http://www.google.com             | Google              | Google                         | nofollow | 2018-09-14
  4 | http://www.yahoo.com              | Yahoo               | Yahoo                          | nofollow | 2018-09-14
  5 | http://www.bing.com               | Bing                | Bing                           | nofollow | 2018-09-14
  1 | http://www.postgresqltutorial.com | PostgreSQL Tutorial | Learn PostgreSQL fast and easy | follow   | 2018-09-14
(8 rows)
```

如果要将这些表导入数据库进行练习，可以通过以下链接下载脚本：

[下载用于创建示例表的脚本](https://gitlab.com/yonj1e/pgsql-tutorial/raw/master/docs/content/dvdrental.zip)

##### where

以下语句删除id列中值为8的行：

```sql
DELETE FROM link
WHERE id = 8;
DELETE 1

SELECT * FROM link;
 id |                url                |        name         |          description           |   rel    | last_update
----+-----------------------------------+---------------------+--------------------------------+----------+-------------
  6 | http://www.facebook.com           | Facebook            | Facebook                       | nofollow | 2013-06-01
  7 | https://www.tumblr.com/           | Tumblr              | Tumblr                         | nofollow | 2018-09-14
  2 | http://www.oreilly.com            | O'Reilly Media      | O'Reilly Media                 | nofollow | 2018-09-14
  3 | http://www.google.com             | Google              | Google                         | nofollow | 2018-09-14
  4 | http://www.yahoo.com              | Yahoo               | Yahoo                          | nofollow | 2018-09-14
  5 | http://www.bing.com               | Bing                | Bing                           | nofollow | 2018-09-14
  1 | http://www.postgresqltutorial.com | PostgreSQL Tutorial | Learn PostgreSQL fast and easy | follow   | 2018-09-14
(7 rows)
```

该语句返回一个表示已删除一行的语句。

请注意，如果 `link` 表没有任何id为8的行，则 `DELETE` 语句不执行任何操作并返回零（0）。

##### USING

假设您要删除 `link` 表中具有id列值都在 `link_tmp` 表中的所有行。

首先，检查 `link_tmp` 表数据：

```sql
SELECT * FROM link_tmp;
 id |           url           |   name   | description |   rel    | last_update
----+-------------------------+----------+-------------+----------+-------------
  6 | http://www.facebook.com | Facebook | Facebook    | nofollow | 2013-06-01
  7 | https://www.tumblr.com/ | Tumblr   | Tumblr      | nofollow | 2018-09-14
(2 rows)
```

我们希望从 `link` 表中删除id为6和7的行：

其次，使用带有 `USING` 子句的以下 `DELETE` 语句来删除 `link` 表中的行：

```sql
DELETE FROM link 
USING link_tmp
WHERE
    link.id = link_tmp.id;
DELETE 2   

SELECT * FROM link;
 id |                url                |        name         |          description           |   rel    | last_update
----+-----------------------------------+---------------------+--------------------------------+----------+-------------
  2 | http://www.oreilly.com            | O'Reilly Media      | O'Reilly Media                 | nofollow | 2018-09-14
  3 | http://www.google.com             | Google              | Google                         | nofollow | 2018-09-14
  4 | http://www.yahoo.com              | Yahoo               | Yahoo                          | nofollow | 2018-09-14
  5 | http://www.bing.com               | Bing                | Bing                           | nofollow | 2018-09-14
  1 | http://www.postgresqltutorial.com | PostgreSQL Tutorial | Learn PostgreSQL fast and easy | follow   | 2018-09-14
(5 rows)
```

查询 `link` 表以验证删除操作。

从输出中可以清楚地看到，该语句按预期工作。

##### 删除所有行

要删除 `link` 表中的所有行，请在 `DELETE` 语句中省略 `WHERE` 子句，如下所示：

```sql
DELETE FROM link;
DELETE 5

SELECT * FROM link;
 id | url | name | description | rel | last_update
----+-----+------+-------------+-----+-------------
(0 rows)
```

 `link` 表现在是空的。

要删除 `link_tmp` 表中的所有行并返回已删除的行，请使用 `DELETE` 语句中的 `RETURNING` 子句，如下所示：

```sql
DELETE FROM link_tmp 
RETURNING *;
 id |           url           |   name   | description |   rel    | last_update
----+-------------------------+----------+-------------+----------+-------------
  6 | http://www.facebook.com | Facebook | Facebook    | nofollow | 2013-06-01
  7 | https://www.tumblr.com/ | Tumblr   | Tumblr      | nofollow | 2018-09-14
(2 rows)

DELETE 2

SELECT * FROM link_tmp;
 id | url | name | description | rel | last_update
----+-----+------+-------------+-----+-------------
(0 rows)
```

在本教程中，我们向您展示了如何使用PostgreSQL DELETE语句删除表中的数据。