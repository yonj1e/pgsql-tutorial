## 重命名数据库

> 摘要：在本教程中，您将逐步学习如何使用 `ALTER DATABASE RENAME TO` 语句重命名PostgreSQL数据库。

#### 示例

要重命名PostgreSQL数据库，请使用以下步骤：

1. 通过连接到其他数据库断开与要重命名的数据库的连接。
2. 检查并终止正在重命名的数据库的所有活动连接。
3. 使用 `ALTER DATABASE` 语句将数据库重命名为新数据库。

我们来看一个重命名数据库的例子。

假设您要重命名的数据库是 `db` ,如果数据库服务器中没有 `db` 数据库，则可以使用 [CREATE DATABASE](content/postgresql-create-database.md) 语句创建它，如下所示：

```sql
CREATE DATABASE db;
```

要将 `db` 数据库重命名为 `newdb`，请按照下列步骤操作：

首先，通过连接到另一个数据库（如 `postgres`）断开与要重命名的数据库的连接。如果使用[psql命令](content/psql-commands.md)，则可以使用以下命令连接到 `postgres` 数据库：

```sql
db=# \connect postgres;
```

通过连接到另一个数据库，您将自动与所连接的数据库断开连接。

接下来，使用以下查询检查 `db` 数据库的所有活动连接：

```sql
SELECT *
FROM pg_stat_activity
WHERE datname = 'db';
-[ RECORD 1 ]----+------------------------------
datid            | 35918
datname          | db
pid              | 6904
usesysid         | 10
usename          | postgres
application_name | psql
client_addr      | ::1
client_hostname  |
client_port      | 56412
backend_start    | 2017-02-21 08:25:05.083705+07
xact_start       |
query_start      |
state_change     | 2017-02-21 08:25:05.092168+07
waiting          | f
state            | idle
backend_xid      |
backend_xmin     |
query            |
```

从输出中可以看出，只有一个与 `db` 数据库的连接。

您可能会发现要重命名的数据库具有许多活动连接。在这种情况下，您需要在终止连接之前通知相应的用户以及应用程序所有者，以避免数据丢失。

然后，使用以下语句终止与 `db` 数据库的所有连接：

```sql
SELECT pg_terminate_backend (pid)
FROM pg_stat_activity
WHERE datname = 'db';
```

之后，使用 `ALTER DATABASE RENAME TO` 语句将 `db` 数据库重命名为 `newdb`，如下所示：

```sql
ALTER DATABASE db RENAME TO newdb;
```

最后但并非最不重要的是，如果应用程序正在使用您的数据库，则应修改连接字符串。

在本教程中，您学习了如何使用 `ALTER DATABASE RENAME TO` 语句将PostgreSQL数据库重命名为新数据库。