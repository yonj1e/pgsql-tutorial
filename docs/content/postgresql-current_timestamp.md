## CURRENT_TIMESTAMP

`CURRENT_TIMESTAMP()` 函数返回时区的当前日期和时间，时区是事务开始的时间。

#### 语法

以下说明了 `CURRENT_TIMESTAMP()` 函数的语法：

```sql
CURRENT_TIMESTAMP(precision)
```

#### 参数

`CURRENT_TIMESTAMP()` 函数接受一个可选参数。

**1) precision**

精度指定结果中小数秒精度的位数。

如果省略 `precision` 参数，则 `CURRENT_TIMESTAMP()` 函数将返回 `TIMESTAMP`，其时区包含可用的完整小数秒精度。

#### 返回值

 `CURRENT_TIMESTAMP()` 函数返回 [TIMESTAMP WITH TIME ZONE](content/postgresql-timestamp.md)，表示事务开始的日期和时间。

#### 示例

以下示例显示如何使用 `CURRENT_TIMESTAMP()` 函数获取当前日期和时间：

```sql
SELECT CURRENT_TIMESTAMP;
              now
-------------------------------
 2017-08-15 21:05:15.723336+07
(1 row)
```

在内部，`CURRENT_TIMESTAMP()` 是使用 `NOW()` 函数实现的，因此列别名是 `NOW`。

与 `NOW()` 函数一样， `CURRENT_TIMESTAMP()` 函数可以用作时间戳列的默认值。

我们来看看下面的例子。

首先，创建一个名为 `note` 的表，其中 `created_at` 列是 `TIMESTAMP WITH TIME ZONE` 类型。

```sql
CREATE TABLE note(
    note_id serial PRIMARY KEY,
    message varchar(255) NOT NULL,
    created_at TIMESTAMP WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP
);
```

`created_at` 列的默认值由 `CURRENT_TIMESTAMP()` 函数的结果提供。

其次，在 `note` 表中[插入](content/postgresql-insert.md)一个新行：

```sql
INSERT INTO note(message) 
VALUES('Testing current_timestamp function');
```

在此语句中，我们未指定 `created_at` 列的值，因此，它默认为事务开始的时间戳。

第三，使用以下查询验证插入是否已正确进行：

```sql
SELECT * FROM note;
 note_id |              message               |          created_at
---------+------------------------------------+-------------------------------
       1 | Testing current_timestamp function | 2018-10-06 14:44:43.149704+08
(1 row)
```

如您所见，`created_at` 列由执行语句的日期和时间填充。

#### 备注

在PostgreSQL中，`TRANSACTION_TIMESTAMP()` 函数等效于 `CURRENT_TIMESTAMP` 函数。但是，函数名` TRANSACTION_TIMESTAMP` 清楚地反映了函数返回的内容。

在本教程中，您学习了如何使用 `CURRENT_TIME()` 来获取事务开始的日期和时间。