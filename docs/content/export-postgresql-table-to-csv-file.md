## 将表导出CSV文件

> 摘要：本教程向您展示了将数据从PosgreSQL表导出到CSV文件的各种技术。

在上一个教程中，我们向您展示了如何[将CSV文件中的数据导入表](content/import-csv-file-into-posgresql-table.md)中。我们将使用我们创建的人员表来从CSV文件导入数据。

![posgresql export csv](/imgs/posgresql-import-csv.jpg)

让我们检查 `persons` 表的数据。

```sql
SELECT * FROM persons;
 id | first_name | last_name |    dob     |         email
----+------------+-----------+------------+-----------------------
  1 | John       | Doe       | 1990-01-05 | john.doe@postgres.cn
  2 | Lily       | Bush      | 1995-02-05 | lily.bush@postgres.cn
(2 rows)
```

我们在表中有两条记录。

#### 使用COPY语句将数据从表导出到CSV

将表数据导出到CSV文件的最简单方法是使用 `COPY` 语句。例如，如果要将人员表的数据导出到 `/work/test/dvdrental` 中名为 `persons_db.csv` 的CSV文件，则可以使用以下语句：

```sql
COPY persons TO '/work/test/dvdrental/persons_db.csv' DELIMITER ',' CSV HEADER;
COPY 2
```

PostgreSQL将 `persons` 表的所有列中的所有数据导出到 `persons_db.csv` 文件。

```shell
$ cat /work/test/dvdrental/persons_db.csv
id,first_name,last_name,dob,email
1,John,Doe,1990-01-05,john.doe@postgres.cn
2,Lily,Bush,1995-02-05,lily.bush@postgres.cn
```

在某些情况下，您希望将数据从表的某些列导出到CSV文件。为此，您可以在COPY关键字后指定列名和表名。例如，以下语句将 `persons` 表的 `first_name`，`last_name` 和 `email` 列中的数据导出到 `person_partial_db.csv`

```sql
COPY persons(first_name,last_name,email) 
TO '/work/test/dvdrental/persons_db.csv' DELIMITER ',' CSV HEADER;
COPY 2
```

```shell
$ cat /work/test/dvdrental/persons_db.csv
first_name,last_name,email
John,Doe,john.doe@postgres.cn
Lily,Bush,lily.bush@postgres.cn
```

如果您不想导出包含表的列名的标头，只需删除 `COPY` 语句中的 `HEADER` 标志即可。以下语句仅将 `persons` 表的 `email` 列中的数据导出到CSV文件。

```sql
COPY persons(email) 
TO '/work/test/dvdrental/persons_db.csv' DELIMITER ',' CSV;
COPY 2
```

```shell
$ cat /work/test/dvdrental/persons_db.csv
john.doe@postgres.cn
lily.bush@postgres.cn
```

请注意，您在 `COPY` 命令中指定的CSV文件名必须由服务器直接写入。这意味着CSV文件必须驻留在数据库服务器计算机上，而不是本地计算机上。CSV文件也需要由PostgreSQL服务器运行的用户写入。

#### 使用\copy命令将数据从表导出到CSV文件

如果您可以访问远程PostgreSQL数据库服务器，但没有足够的权限写入其上的文件，则可以使用PostgreSQL内置命令 `\copy`。

`\copy` 命令基本上运行上面的 `COPY` 语句。但是，服务器不写入CSV文件，而是psql写入CSV文件，将数据从服务器传输到本地文件系统。要使用 `\copy` 命令，您只需要拥有本地计算机的足够权限。它不需要PostgreSQL超级用户权限。psql 写入 csv 文件, 将数据从服务器传输到本地文件系统, 而不是编写 csv 文件的服务器。

例如，如果要将 `persons` 表的所有数据导出到 `persons_client.csv` 文件中，则可以从psql客户端执行 `\copy` 命令，如下所示：

```sql
\copy (SELECT * FROM persons) to '/work/test/dvdrental/persons_client.csv' with csv
COPY 2
```

在本教程中，我们向您展示了如何使用 `COPY` 语句和 `\copy` 命令将数据从表导出到CSV文件。