## PL/pgSQL 游标

> 摘要：在本教程中，我们将向您展示如何使用PL/pgSQL Cursor，并为您提供一些使用游标的实际示例。

PL/pgSQL游标允许我们封装查询并一次处理每个单独的行。当我们想要将大的结果集划分为多个部分并单独处理每个部分时，我们使用游标。如果我们立即处理它，我们可能会出现内存溢出错误。

另外，我们可以开发一个返回对游标的引用的[函数](content/postgresql-create-function.md)。这是从函数返回大型结果集的有效方法。函数的调用者可以根据游标引用处理结果集。

下图说明了如何在PostgreSQL中使用游标：

![PL/pgSQL Cursor](/imgs/plpgsql-cursor.png)

 

1. First, declare a cursor.
2. Next, open the cursor.
3. Then, fetch rows from the result set into a target.
4. After that, check if there is more row left to fetch. If yes, go to step 3, otherwise, go to step 5.
5. Finally, close the cursor.

We will examine each step in more detail in the following sections.

#### Declaring cursors

To access to a cursor, you need to declare a cursor [variable](content/plpgsql-variables.md) in the [declaration section of a block. ](content/plpgsql-block-structure.md)PostgreSQL provides us with a special type called `REFCURSOR` to declare a cursor variable.

1. 首先，定义一个游标
2. 下一步，打开这个游标
3. 然后，从结果集中获取行到目标中
4. 之后，检查是否还有更多的行要取。 如果是，则转到步骤3，否则转到步骤5
5. 最后，关闭游标

我们将在以下部分中更详细地研究每个步骤。

#### 声明游标

要访问一个游标，需要先在块的声明部分来声明一个游标变量。PostgreSQL 提供了一个特殊的 `REFCURSOR`类型来定义游标变量。

```sql
DECLARE my_cursor REFCURSOR;
```

另一种方法是，声明绑定到查询的游标，使用以下语法：

```sql
cursor_name [ [NO] SCROLL ] CURSOR [( name datatype, name data type, ...)] FOR query;
```

首先指定了游标的名字。

下一步，可以使用 `SCROLL` 指定是否可以向后滚动游标。如果使用 `NO SCROLL`，则游标不能向后滚动。

然后，是 `CURSOR` 关键字。并在其后使用逗号分隔的参数列表来定义查询的参数。游标打开时，这些参数将被替换为具体的值。

之后，在 `FOR` 关键字后指定查询。可以在这里使用任何合法的 [SELECT](content/postgresql-select.md) 语句。

下面是一个例子：

```sql
DECLARE
    cur_films  CURSOR FOR SELECT * FROM film;
    cur_films2 CURSOR (year integer) FOR SELECT * FROM film WHERE release_year = year;
```

`cur_films` 是一个游标，封装了电影表中的所有行。

`cur_films2` 是一个游标，用于在胶片表中封装具有特定发行年份的胶片。

#### 打开游标

必须先打开游标，然后才能使用它们查询行。PostgreSQL提供了打开未绑定和绑定游标的语法。

##### 打开未绑定的游标

我们使用以下语法打开一个未绑定的游标：

```sql
OPEN unbound_cursor_variable [ [ NO ] SCROLL ] FOR query;
```

因为在我们声明它时，未绑定的游标变量不受任何查询限制，所以我们必须在打开它时指定查询。请参阅以下示例：

```sql
OPEN my_cursor FOR SELECT * FROM city WHERE counter = p_country;
```

PostgreSQL允许我们打开游标并将其绑定到动态查询。这是语法：

```sql
OPEN unbound_cursor_variable[ [ NO ] SCROLL ] 
FOR EXECUTE query_string [USING expression [, ... ] ];
```

在下面的示例中，我们构建了一个动态查询，该查询根据 `sort_field` 参数对行进行排序，并打开执行动态查询的游标。

```sql
query := 'SELECT * FROM city ORDER BY $1';
 
OPEN cur_city FOR EXECUTE query USING sort_field;
```

##### 打开绑定游标

因为绑定游标在我们声明它时已经绑定到查询，所以当我们打开它时，我们只需要在必要时将参数传递给查询。

```sql
OPEN cursor_variable[ (name:=value,name:=value,...)];
```

在下面的示例中，我们打开上面声明的绑定游标 `cur_films` 和 `cur_films2`：

```sql
OPEN cur_films;
OPEN cur_films2(year:=2005);
```

#### 使用游标

打开游标后，我们可以使用 `FETCH`, `MOVE`, [UPDATE](content/postgresql-update.md) 或 [DELETE](content/postgresql-delete.md) 语句对其进行操作。

##### 获取下一行

```sql
FETCH [ direction { FROM | IN } ] cursor_variable INTO target_variable;
```

`FETCH` 语句从游标获取下一行并为其分配一个 `target_variable`，它可以是记录，行变量或逗号分隔的变量列表。如果找不到更多行，则 `target_variable` 设置为 `NULL`（s）。

默认情况下，如果未明确指定方向，则游标将获取下一行。以下内容对游标有效：

- NEXT
- LAST
- PRIOR
- FIRST
- ABSOLUTE count
- RELATIVE count
- FORWARD
- BACKWARD

请注意，`FORWARD` 和 `BACKWARD` 方向仅适用于使用 `SCROLL` 选项声明的游标。

请参阅以下获取游标的示例。

```sql
FETCH cur_films INTO row_film;
FETCH LAST FROM row_film INTO title, release_year;
```

##### 移动游标

```sql
MOVE [ direction { FROM | IN } ] cursor_variable;
```

如果只想移动游标而不检索任何行，则使用 `MOVE` 语句。方向接受与 `FETCH` 语句相同的值。

```sql
MOVE cur_films2;MOVE LAST FROM cur_films;
MOVE RELATIVE -1 FROM cur_films;
MOVE FORWARD 3 FROM cur_films;
```

##### 删除或更新行

定位游标后，我们可以使用 `DELETE WHERE CURRENT OF` 或 `UPDATE WHERE CURRENT OF` 语句删除或更新游标的行标识，如下所示：

```sql
UPDATE table_name 
SET column = value, ... 
WHERE CURRENT OF cursor_variable;
 
DELETE FROM table_name 
WHERE CURRENT OF cursor_variable;
```

See the following example.

请参阅以下示例。

```sql
UPDATE film SET release_year = p_year 
WHERE CURRENT OF cur_films;
```

#### 关闭游标

要关闭一个开始游标，我们使用 `CLOSE` 语句，如下所示：

```sql
CLOSE cursor_variable;
```

`CLOSE` 语句释放资源或释放游标变量以允许使用 `OPEN` 语句再次打开它。

#### 综合实例

`get_film_titles(integer)` 函数接受表示电影发行年份的参数。在函数内部，我们查询所有发行年份等于发布年份的电影。我们使用游标循环遍历行并连接标题和发行年份的电影，标题包含 `ful`。

```sql
CREATE OR REPLACE FUNCTION get_film_titles(p_year INTEGER)
   RETURNS text AS $$
DECLARE 
 titles TEXT DEFAULT '';
 rec_film   RECORD;
 cur_films CURSOR(p_year INTEGER) 
 FOR SELECT 
 FROM film
 WHERE release_year = p_year;
BEGIN
   -- Open the cursor
   OPEN cur_films(p_year);
 
   LOOP
    -- fetch row into the film
      FETCH cur_films INTO rec_film;
    -- exit when no more row to fetch
      EXIT WHEN NOT FOUND;
 
    -- build the output
      IF rec_film.title LIKE '%ful%' THEN 
         titles := titles || ',' || rec_film.title || ':' || rec_film.release_year;
      END IF;
   END LOOP;
  
   -- Close the cursor
   CLOSE cur_films;
 
   RETURN titles;
END; $$
 
LANGUAGE plpgsql;
```

```sql
SELECT get_film_titles(2006);
```

```sql
,Grosse Wonderful:2006,Day Unfaithful:2006,Reap Unfaithful:2006,Unfaithful Kill:2006,Wonderful Drop:2006
```

在本教程中，我们向您展示了如何使用PL/pgSQL游标循环遍历一组行并单独处理每一行。