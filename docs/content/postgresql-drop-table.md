## DROP TABLE

> 摘要：在本教程中，您将学习如何使用PostgreSQL DROP TABLE语句从数据库中删除现有表。

#### 简介

要从数据库中删除现有表，请使用 `DROP TABLE` 语句，如下所示：

```sql
DROP TABLE [IF EXISTS] table_name [CASCADE | RESTRICT];
```

您可以在 `DROP TABLE` 关键字后指定表名，以从数据库中永久删除表。

如果删除不存在的表，PostgreSQL会发出错误。要避免这种情况，可以使用 `IF EXISTS` 参数，后跟 `DROP TABLE` 子句。

如果要删除的表在视图，约束或任何其他对象中使用，`CASCADE` 允许您自动将这些依赖对象与表一起删除。

如果存在依赖表的任何对象，`RESTRICT` 拒绝删除该表。PostgreSQL默认使用 `RESTRICT` 。

您可以在 `DROP TABLE` 之后放置一个表列表，一次删除多个表，每个表用逗号分隔。

请注意，只有超级用户，模式所有者和表所有者才有足够的权限来删除该表。

#### 示例

以下语句删除数据库中名为 `author` 的表：

```sql
DROP TABLE author;
```

PostgreSQL发出错误，因为 `author` 表不存在。

```sql
[Err] ERROR:  table "author" does not exist
```

要避免此错误，可以使用 `IF EXISTS`参数。

```sql
DROP TABLE IF EXISTS author;
```

从输出中可以清楚地看到，PostgreSQL发出通知而不是错误。

我们为下一个演示创建了名为 `author` 和 `page` 的新表：

```sql
CREATE TABLE author (
 author_id INT NOT NULL PRIMARY KEY,
 firstname VARCHAR (50),
 lastname VARCHAR (50)
);
 
CREATE TABLE page (
 page_id serial PRIMARY KEY,
 title VARCHAR (255) NOT NULL,
 CONTENT TEXT,
 author_id INT NOT NULL,
 FOREIGN KEY (author_id) REFERENCES author (author_id)
);
```

您可以使用以下语句删除 `author` 表：

```sql
DROP TABLE IF EXISTS author;
```

由于页面表上的约束取决于 `author` 表，PostgreSQL会发出错误消息。

```sql
[Err] ERROR:  cannot drop table author because other objects depend on it
DETAIL:  constraint page_author_id_fkey on table page depends on table author
HINT:  Use DROP ... CASCADE to drop the dependent objects too.
```

在这种情况下，您需要先删除所有依赖对象，然后再删除 `author` 表或使用 `CASCADE` 参数，如下所示：

```sql
DROP TABLE author CASCADE;
```

PostgreSQL删除 `page` 表和 `author` 表中的约束。此外，它还发出通知：

```sql
DROP TABLE author CASCADE;
```

在本教程中，您学习了如何使用PostgreSQL DROP TABLER语句从数据库中删除现有表。

#### 相关教程

- [临时表](http://www.postgresqltutorial.com/postgresql-temporary-table/)