## 自连接

> 摘要：在本教程中，您将学习如何使用PostgreSQL自连接技术来比较同一个表中的行。

#### 简介

自连接是一个表连接到自身的查询。自连接对于比较同一表中的一列行中的值很有用。

要形成自连接，请使用不同的别名指定同一个表两次，设置比较，并消除值等于自身的情况。

以下查询显示如何将表A连接到自身：

```sql
SELECT column_list
FROM A a1
INNER JOIN B b1 ON join_predicate;
```

在此语法中，表 `A` 使用 `INNER JOIN` 子句连接到自身。请注意，您还可以使用 `LEFT JOIN` 或 `RIGHT JOIN` 子句。

#### 示例

##### 1) 查询层次结构数据

我们为演示设置一个示例表。

假设，我们有以下组织结构：

![PostgreSQL Self Join - Reporting Structure](/imgs/PostgreSQL-Self-Join-Reporting-Structure.png)

以下SQL脚本创建 `employee` 表并插入一些示例数据：

```sql
CREATE TABLE employee ( 
    employee_id INT PRIMARY KEY, 
    first_name VARCHAR (255) NOT NULL, 
    last_name VARCHAR (255) NOT NULL, 
    manager_id INT, 
    FOREIGN KEY (manager_id) 
    REFERENCES employee (employee_id)  
    ON DELETE CASCADE
);

INSERT INTO employee ( 
    employee_id, 
    first_name, 
    last_name, 
    manager_id)
VALUES 
(1, 'Windy', 'Hays', NULL), 
(2, 'Ava', 'Christensen', 1), 
(3, 'Hassan', 'Conner', 1), 
(4, 'Anna', 'Reeves', 2), 
(5, 'Sau', 'Norman', 2), 
(6, 'Kelsie', 'Hays', 3), 
(7, 'Tory', 'Goff', 3), 
(8, 'Salley', 'Lester', 3);
```

`manager_id` 列中的值表示员工向其报告的经理。如果 `manager_id` 列中的值为null，则员工不会向任何人报告。换句话说，该员工是最高经理。

要查找向谁报告，请使用以下查询：

```sql
SELECT    
	e.first_name || ' ' || e.last_name employee,    
	m .first_name || ' ' || m .last_name manager
FROM employee e
INNER JOIN employee m ON m .employee_id = e.manager_id
ORDER BY manager;
    employee     |     manager
-----------------+-----------------
 Sau Norman      | Ava Christensen
 Anna Reeves     | Ava Christensen
 Salley Lester   | Hassan Conner
 Kelsie Hays     | Hassan Conner
 Tory Goff       | Hassan Conner
 Ava Christensen | Windy Hays
 Hassan Conner   | Windy Hays
(7 rows)
```

此查询两次引用 `employees` 表，一个作为员工，另一个作为管理员。它为员工使用表别名 `e`，为经理使用 `m`。

连接谓词通过匹配 `employee_id` 和 `manager_id` 列中的值来查找员工/经理对。

最高管理者没有出现在输出中。

要在结果集中包含最高管理者，请使用 `LEFT JOIN` 而不是 `INNER JOIN` 子句，如以下查询所示：

```sql
SELECT    
	e.first_name || ' ' || e.last_name employee,    
	m .first_name || ' ' || m .last_name manager
FROM employee e
LEFT JOIN employee m ON m .employee_id = e.manager_id
ORDER BY manager;
    employee     |     manager
-----------------+-----------------
 Anna Reeves     | Ava Christensen
 Sau Norman      | Ava Christensen
 Salley Lester   | Hassan Conner
 Kelsie Hays     | Hassan Conner
 Tory Goff       | Hassan Conner
 Hassan Conner   | Windy Hays
 Ava Christensen | Windy Hays
 Windy Hays      |
(8 rows)
```

##### 2) 比较具有相同表的行

请参阅DVD租赁数据库中的 `film` 表：

![Film Table](/imgs/film_table.png)

以下查询查找具有相同长度的所有电影对。

```sql
SELECT f1.title, f2.title, f1.length
FROM film f1
INNER JOIN film f2 ON f1.film_id <> f2.film_id AND f1.length = f2.length;
            title            |            title            | length
-----------------------------+-----------------------------+--------
 Chamber Italian             | Resurrection Silverado      |    117
 Chamber Italian             | Magic Mallrats              |    117
 Chamber Italian             | Graffiti Love               |    117
 Chamber Italian             | Affair Prejudice            |    117
 Grosse Wonderful            | Hurricane Affair            |     49
 Grosse Wonderful            | Hook Chariots               |     49
 Grosse Wonderful            | Heavenly Gun                |     49
 Grosse Wonderful            | Doors President             |     49
 Airport Pollock             | Sense Greek                 |     54
 Airport Pollock             | October Submarine           |     54
 Airport Pollock             | Kill Brotherhood            |     54
 Airport Pollock             | Juggler Hardly              |     54
 Airport Pollock             | Go Purple                   |     54
```

此查询将电影表连接到自身。连接谓词匹配由 `film_id` 指定的具有相同长度的不同影片。

在本教程中，您学习了如何使用自连接技术将表连接到自身。