## 角色管理

> 摘要：在本教程中，我们将向您介绍PostgreSQL角色概念，并向您展示如何创建用户角色和组角色。

![PostgreSQL Roles](/imgs/PostgreSQL-Roles.png)

PostgreSQL使用角色概念来管理数据库访问权限。角色可以是用户或组，具体取决于您设置角色的方式。具有登录权限的角色称为用户。角色可以是其他角色的成员，称为组。

#### 创建角色

从版本8.1开始，PostgreSQL使用角色概念来合并用户和组概念。要创建新角色，请使用 `CREATE ROLE` 语句，如下所示：

```sql
CREATE ROLE role_name;
```

要获取集群中的所有可用角色，请从pg_roles系统目录中查询以下语句：

```sql
SELECT rolname FROM pg_roles;
```

如果使用psql工具，则可以使用 `\du` 命令列出所有现有角色。

##### 角色属性

数据库角色的属性定义角色的权限，包括登录，超级用户，数据库创建，角色创建，密码等。

以下语句创建具有登录权限，密码和有效日期的角色。

```sql
CREATE ROLE doe WITH PASSWORD 'pgSecpas1970' VALID UNTIL '2020-01-01';
```

以下语句创建具有超级用户状态的角色，这意味着此角色可以绕过所有授权检查：

```sql
CREATE ROLE bigboss SUPERUSER;
```

请注意，您必须是超级用户才能创建另一个超级用户。

如果您希望角色具有数据库创建权限，请使用以下语句：

```sql
CREATE ROLE admin CREATEDB;
```

使用以下语句创建具有创建权限的角色：

```sql
CREATE ROLE security CREATEROLE;
```

##### 组角色

将角色作为一个组进行管理更加容易，这样您就可以从一个组中授予或撤消权限。在PostgreSQL中，您创建一个代表组的角色，然后将组角色的成员身份授予各个用户角色。

按照惯例，组角色没有 `LOGIN` 权限。

要创建组角色，请使用 `CREATE ROLE` 语句，如下所示：

```sql
CREATE ROLE group_role;
```

例如，以下语句创建销售组角色：

```sql
CREATE ROLE sales;
```

现在，您可以使用 `GRANT` 语句将用户角色添加到组角色：

```sql
GRANT group_role to user_role;
```

例如，要将 `doe` 用户角色添加到销售组角色，请使用以下语句：

```sql
GRANT sales TO doe;
```

要从组角色中删除用户角色，请使用 `REVOKE` 语句：

```sql
REVOKE group_role FROM user_role;
```

例如，要从销售组角色中删除doe用户角色，请使用以下语句：

```sql
REVOKE sales FROM doe;
```

请注意，PostgreSQL不允许您使用循环组角色，其中角色是另一个角色的成员，反之亦然。

##### 组和用户角色继承

用户角色可以通过以下方式使用组角色的权限：

- 首先，用户角色可以使用 `SET ROLE` 语句临时成为组角色，这意味着用户角色使用组角色的权限而不是原始权限。此外，在会话中创建的任何数据库对象都归组角色所有，而不是用户角色。
- 其次，具有 `INHERIT` 属性的用户角色将自动拥有其所属的组角色的权限，包括组角色继承的所有权限。

请参阅以下示例：

```sql
CREATE ROLE doe LOGIN INHERIT;
CREATE ROLE sales NOINHERIT;
CREATE ROLE marketing NOINHERIT;
GRANT sales to doe;
GRANT marketing to sales;
```

如果您以doe身份连接到PostgreSQL，您将拥有doe权限以及授予sales的权限，因为doe用户角色具有 `INHERIT` 属性。但是，您没有营销权限，因为为销售用户角色定义了 `NOINHERIT` 属性。

执行以下语句后：

```sql
SET ROLE sales;
```

您将只拥有授予销售的权限，而不是授予doe的权限。

并在执行以下语句后：

```sql
SET ROLE marketing;
```

您只有授予营销的权限，而不是授予管理员和doe的权限。

要恢复原始权限，可以使用以下语句：

```sql
RESET ROLE;
```

请注意，只有数据库对象的权限是可继承的。`LOGIN`，`SUPERUSER`，`CREATEROLE` 和 `CREATEDB` 是特殊角色，不能作为普通权限继承。

#### 删除角色

您可以使用 `DROP ROLE` 语句删除组角色或用户角色。

```sql
DROP ROLE role_name;
```

在删除角色之前，必须重新分配或删除其拥有的所有对象并撤消其权限。

如果删除组角色，PostgreSQL会自动撤消组中的所有成员资格。组的用户角色不受影响。