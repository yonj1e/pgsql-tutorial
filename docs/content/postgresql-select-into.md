## SELECT INTO

> 摘要：在本教程中，您将学习如何使用PostgreSQL SELECT INTO语句从查询的结果集创建新表。

#### 简介

PostgreSQL `SELECT INTO` 语句允许您创建新表并插入查询返回的数据。新表列具有与 `SELECT` 子句的输出列关联的名称和数据类型。与[SELECT](content/postgresql-select.md)语句不同，`SELECT INTO` 语句不会将数据返回给客户端。

以下说明了PostgreSQL `SELECT INTO` 语句的语法：

```sql
SELECT
    column_list 
INTO [ TEMPORARY | TEMP | UNLOGGED ] [ TABLE ] new_table_name
FROM
    table_name
WHERE
    condition;
```

要使用从结果集派生的结构和数据创建新表，请在 `INTO` 子句中指定新表名。

`TEMP` 或 `TEMPORARY` 关键字是可选的; 它允许您创建[临时表](content/postgresql-temporary-table.md)。

`UNLOGGED` 关键字（如果可用）将使新表不记录log。

`WHERE` 子句允许您指定应插入新表的原始表中的数据。除了 `WHERE` 子句之外，您还可以在 `SELECT` 语句中使用 `SELECT INTO` 语句中的其他子句，例如 `INNER JOIN`，`LEFT JOIN`，`GROUP BY` 和 `HAVING`。

请注意，您不能在[PL/pgSQL](PL/pgSQL)或ECPG中使用 `SELECT INTO` 语句，因为它们以不同方式解释 `INTO` 子句。在这种情况下，您可以使用 `CREATE TABLE AS` 语句，该语句提供比 `SELECT INTO` 语句更多的功能。

#### 示例

我们将使用[示例数据库]()中的 `file` 表进行演示。

![PostgreSQL SELECT INTO sample table](/imgs/film_table.png)

以下语句创建一个名为 `film_r` 的新表，其中包含电影表中租赁5天时间评级为 `R`  的所有电影。

```sql
SELECT
    film_id,
    title,
    rental_rate
INTO TABLE film_r
FROM
    film
WHERE
    rating = 'R'
AND rental_duration = 5
ORDER BY
    title;
```

要验证表创建，您可以查询film_r表中的数据：

To verify the table creation, you can query data from the `film_r` table:

```sql
SELECT * FROM film_r;
 film_id |          title          | rental_rate
---------+-------------------------+-------------
      54 | Banger Pinocchio        |        0.99
     115 | Campus Remember         |        2.99
     138 | Chariots Conspiracy     |        2.99
     159 | Closer Bang             |        4.99
     168 | Comancheros Enemy       |        0.99
     246 | Doubtfire Labyrinth     |        4.99
     287 | Entrapment Satisfaction |        0.99
     296 | Express Lonely          |        2.99
     310 | Fever Empire            |        4.99
```

以下语句创建一个名为 `short_film` 的临时表，其中包含长度小于60分钟的所有电影。

```sql
SELECT
    film_id,
    title,
    length 
INTO TEMP TABLE short_film
FROM
    film
WHERE
    length < 60
ORDER BY
    title;
```

以下显示了 `short_film` 表中的数据：

```sql
SELECT * FROM short_film;
```

在本教程中，您学习了如何使用PostgreSQL `SELECT INTO` 语句从查询的结果集创建新表。