## ALL

> 摘要：在本教程中，您将学习如何使用 `ALL` 运算符将值与子查询返回的值列表进行比较。

#### 简介

`ALL` 运算符允许您通过将值与[子查询](content/postgresql-subquery.md)返回的值列表进行比较来查询数据。

以下说明了 `ALL` 运算符的语法：

```sql
comparison_operator ALL (subquery)
```

在这个语法中：

- ALL运算符必须以比较运算符开头，例如等于（=），不等于（！=），大于（>），大于或等于（> =），小于（<），小于或等于（<=）。
- ALL运算符后面必须跟一个子查询，子查询也必须用括号括起来。

假设子查询返回一些行，ALL运算符的工作方式如下：

1. `column_name > ALL (subquery)` 如果值大于子查询返回的最大值，则表达式的计算结果为true。
2. `column_name >= ALL (subquery)` 如果值大于或等于子查询返回的最大值，则表达式的计算结果为true。
3. `column_name < ALL (subquery)` 如果值小于子查询返回的最小值，则表达式的计算结果为true。
4. `column_name <= ALL (subquery)` 如果值小于或等于子查询返回的最小值，则表达式的计算结果为true。
5. `column_name = ALL (subquery)` 如果值等于子查询返回的任何值，则表达式的计算结果为true。
6. `column_name != ALL (subquery)` 如果值不等于子查询返回的任何值，则表达式的计算结果为true。

如果子查询没有返回任何行，则ALL运算符始终求值为true。

#### 示例

让我们使用[示例数据库](content/postgresql-sample-database.md)中的电影表进行演示。

![PostgreSQL ALL: Film table](/imgs/film_table.png)

以下查询返回按电影评级分组的所有电影的[平均](content/postgresql-avg-function.md)长度：

```sql
SELECT ROUND(AVG(length), 2) avg_length
FROM film
GROUP BY rating
ORDER BY avg_length DESC;
 avg_length
------------
     120.44
     118.66
     113.23
     112.01
     111.05
(5 rows)
```

要查找长度大于上述平均长度列表的所有影片，请使用ALL和大于运算符（>），如下所示：

```sql
SELECT film_id, title, length
FROM film
WHERE
    length > ALL (
            SELECT ROUND(AVG (length),2)
            FROM film
            GROUP BY rating
    )
ORDER BY length;
 film_id |            title            | length
---------+-----------------------------+--------
     207 | Dangerous Uptown            |    121
      86 | Boogie Amelie               |    121
     403 | Harry Idaho                 |    121
      93 | Brannigan Sunrise           |    121
     704 | Pure Runner                 |    121
      37 | Arizona Bang                |    121
     658 | Paris Weekend               |    121
     490 | Jumanji Blade               |    121
      68 | Betrayed Rear               |    122
     218 | Deceiver Betrayed           |    122
```

从输出中可以清楚地看到，查询返回所有长度大于子查询返回的平均长度列表中最大值的影片。

在本教程中，您学习了如何使用ALL运算符将值与子查询返回的值列表进行比较。