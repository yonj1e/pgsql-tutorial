## 如何获取数据库对象大小

> 摘要：本教程介绍如何使用各种方便的函数获取PostgreSQL数据库，表，索引，表空间和值大小。

#### 表大小

要获取特定表的大小，请使用 `pg_relation_size()` 函数。例如，您可以在 `dvdrental` [示例数据库]()中获取 `actor` 表的大小，如下所示：

```sql
select pg_relation_size('actor');
```

`pg_relation_size()` 函数以字节为单位返回特定表的大小：

```
pg_relation_size
------------------
            16384
```

要使结果更具人性化，请使用 `pg_size_pretty()` 函数。`pg_size_pretty()` 函数获取另一个函数的结果，并根据需要使用字节，kB，MB，GB或TB对其进行格式化。例如：

```sql
SELECT pg_size_pretty (pg_relation_size('actor'));
```

以下是以kB为单位的输出

```
 pg_size_pretty
    ----------------
     16 kB
    (1 row)
```

`pg_relation_size()` 函数仅返回表的大小，不包括索引或其他对象。

要获取表的总大小，请使用 `pg_total_relation_size()` 函数。例如，要获取 `actor` 表的总大小，请使用以下语句：

```sql
SELECT
    pg_size_pretty (
        pg_total_relation_size ('actor')
    );
 pg_size_pretty
----------------
 72 kB
(1 row)
```

您可以使用 `pg_total_relation_size()` 函数查找包括索引在内的最大表的大小。

例如，以下查询返回 `dvdrental` 数据库中前5大表：

```sql
SELECT
    relname AS "relation",
    pg_size_pretty (
        pg_total_relation_size (C .oid)
    ) AS "total_size"
FROM
    pg_class C
LEFT JOIN pg_namespace N ON (N.oid = C .relnamespace)
WHERE
    nspname NOT IN (
        'pg_catalog',
        'information_schema'
    )
AND C .relkind <> 'i'
AND nspname !~ '^pg_toast'
ORDER BY
    pg_total_relation_size (C .oid) DESC
LIMIT 5;
  relation  | total_size
------------+------------
 rental     | 2472 kB
 payment    | 2232 kB
 film       | 688 kB
 film_actor | 536 kB
 inventory  | 464 kB
(5 rows)
```

#### 数据库大小

要获取整个数据库的大小，请使用 `pg_database_size()` 函数。例如，以下语句返回 `dvdrental` 数据库的大小：

```sql
SELECT
    pg_size_pretty (
        pg_database_size ('dvdrental')
    );
pg_size_pretty
----------------
 15 MB
(1 row)
```

要获取当前数据库服务器中每个数据库的大小，请使用以下语句：

```sql
SELECT
    pg_database.datname,
    pg_size_pretty(pg_database_size(pg_database.datname)) AS size
    FROM pg_database;
    datname     |  size
----------------+---------
 postgres       | 7055 kB
 template1      | 7055 kB
 template0      | 6945 kB
 dvdrental      | 15 MB
```

#### 索引大小

要获取附加到表的所有索引的总大小，请使用 `pg_indexes_size()` 函数。

`pg_indexes_size()` 函数接受OID或表名作为参数，并返回该表附加的所有索引使用的总磁盘空间。

例如，要获取附加到影片表的所有索引的总大小，请使用以下语句：

```sql
SELECT pg_size_pretty (pg_indexes_size('actor'));
 pg_size_pretty
----------------
 32 kB
(1 row)
```

#### 表空间大小

要获取表空间的大小，请使用 `pg_tablespace_size()` 函数。`pg_tablespace_size()` 函数接受表空间名称并以字节为单位返回大小。

以下语句返回 `pg_default` 表空间的大小：

```sql
SELECT
    pg_size_pretty (
        pg_tablespace_size ('pg_default')
    );
 pg_size_pretty
----------------
 43 MB
(1 row)
```

#### 值大小

要查找需要存储特定值的空间大小，可以使用 `pg_column_size()` 函数，例如：

```sql
dvdrental=# select pg_column_size(5::smallint);
 pg_column_size
----------------
              2
(1 row)
 
 
dvdrental=# select pg_column_size(5::int);
 pg_column_size
----------------
              4
(1 row)
 
 
dvdrental=# select pg_column_size(5::bigint);
 pg_column_size
----------------
              8
(1 row)
```

在本教程中，您学习了各种方便的函数来获取数据库，表，索引，表空间和值的大小。