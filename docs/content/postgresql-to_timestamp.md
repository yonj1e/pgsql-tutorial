## TO_TIMESTAMP

`TO_TIMESTAMP()` 函数根据指定的格式将字符串转换为[时间戳](content/postgresql-timestamp.md)。

#### 语法

以下说明了 `TO_TIMESTAMP()` 函数的语法：

```sql
TO_TIMESTAMP(timestamp, format)
```

#### 参数

`TO_TIMESTAMP()` 函数需要两个参数：

**1) timestamp**

`timestamp` 是字符串，表示格式指定格式的时间戳值。

**2) format**

`timestamp` 参数的格式。

要构造格式字符串，可以使用模板模式格式化日期和时间值。

| **Pattern**              | **Description**                                              |
| ------------------------ | ------------------------------------------------------------ |
| Y,YYY                    | year in 4 digits with comma                                  |
| YYYY                     | year in 4 digits                                             |
| YYY                      | last 3 digits of year                                        |
| YY                       | last 2 digits of year                                        |
| Y                        | The last digit of year                                       |
| IYYY                     | ISO 8601 week-numbering year (4 or more digits)              |
| IYY                      | Last 3 digits of ISO 8601 week-numbering year                |
| IY                       | Last 2 digits of ISO 8601 week-numbering year                |
| I                        | Last digit of ISO 8601 week-numbering year                   |
| BC, bc, AD or ad         | Era indicator without periods                                |
| B.C., b.c., A.D. ora.d.  | Era indicator with periods                                   |
| MONTH                    | English month name in uppercase                              |
| Month                    | Full capitalized English month name                          |
| month                    | Full lowercase English month name                            |
| MON                      | Abbreviated uppercase month name e.g., JAN, FEB, etc.        |
| Mon                      | Abbreviated capitalized month name e.g, Jan, Feb,  etc.      |
| mon                      | Abbreviated lowercase month name e.g., jan, feb, etc.        |
| MM                       | month number from 01 to 12                                   |
| DAY                      | Full uppercase day name                                      |
| Day                      | Full capitalized day name                                    |
| day                      | Full lowercase day name                                      |
| DY                       | Abbreviated uppercase day name                               |
| Dy                       | Abbreviated capitalized day name                             |
| dy                       | Abbreviated lowercase day name                               |
| DDD                      | Day of year (001-366)                                        |
| IDDD                     | Day of ISO 8601 week-numbering year (001-371; day 1 of the year is Monday of the first ISO week) |
| DD                       | Day of month (01-31)                                         |
| D                        | Day of the week, Sunday (1) to Saturday (7)                  |
| ID                       | ISO 8601 day of the week, Monday (1) to Sunday (7)           |
| W                        | Week of month (1-5) (the first week starts on the first day of the month) |
| WW                       | Week number of year (1-53) (the first week starts on the first day of the year) |
| IW                       | Week number of ISO 8601 week-numbering year (01-53; the first Thursday of the year is in week 1) |
| CC                       | Century e.g, 21, 22, etc.                                    |
| J                        | Julian Day (integer days since November 24, 4714 BC at midnight UTC) |
| RM                       | Month in upper case Roman numerals (I-XII; >                 |
| rm                       | Month in lowercase Roman numerals (i-xii; >                  |
| HH                       | Hour of day (0-12)                                           |
| HH12                     | Hour of day (0-12)                                           |
| HH24                     | Hour of day (0-23)                                           |
| MI                       | Minute (0-59)                                                |
| SS                       | Second (0-59)                                                |
| MS                       | Millisecond (000-9999)                                       |
| US                       | Microsecond (000000-999999)                                  |
| SSSS                     | Seconds past midnight (0-86399)                              |
| AM, am, PM or pm         | Meridiem indicator (without periods)                         |
| A.M., a.m., P.M. or p.m. | Meridiem indicator (with periods)                            |

#### 返回值

`TO_TIMESTAMP()` 函数返回带时区的时间戳。

#### 示例

以下语句使用 `TO_TIMESTAMP()` 函数将字符串转换为时间戳：

```sql
SELECT TO_TIMESTAMP('2017-03-31 9:30:20','YYYY-MM-DD HH:MI:SS');
      to_timestamp
------------------------
 2017-03-31 09:30:20+08
(1 row)
```

在这个例子中：

- `YYYY` 是2017年
- `MM` 是03月
- `DD` 是第31天
- `HH` 是9小时
- `MI` 是30分钟
- `SS` 是20秒

#### 备注

1) 除非使用固定格式全局选项（FX前缀），否则 `TO_TIMESTAMP()` 函数会跳过输入字符串中的空格。

此示例在输入字符串中使用多个空格：

```sql
SELECT TO_TIMESTAMP('2017 Aug','YYYY MON');
      to_timestamp
------------------------
 2017-08-01 00:00:00+08
(1 row)
```

`TO_TIMESTAMP()` 函数只是跳过它们并返回正确的时间戳值：

但是，以下示例返回错误：

```sql
SELECT TO_TIMESTAMP('2017     Aug','FXYYYY MON');
ERROR:  invalid value "   " for "MON"
DETAIL:  The given value did not match any of the allowed values for this field.
```

因为FX指示 `TO_TIMESTAMP()` 仅接受一个空格的输入字符串。

2)  `TO_TIMESTAMP()` 函数以最小的错误检查验证输入字符串。它会尽可能地将输入字符串转换为有效的时间戳，这有时会产生意外的结果。

以下示例使用无效的时间戳值：

```sql
-- PostgreSQL 9.x
SELECT TO_TIMESTAMP('2017-02-31 30:8:00', 'YYYY-MM-DD HH24:MI:SS');
      to_timestamp
------------------------
 2017-03-04 06:08:00+08
(1 row)

-- PostgreSQL 10
SELECT TO_TIMESTAMP('2017-02-31 30:8:00', 'YYYY-MM-DD HH24:MI:SS');
ERROR:  date/time field value out of range: "2017-02-31 30:8:00"
```

3) 将字符串转换为时间戳时， `TO_TIMESTAMP()` 函数将毫秒或微秒视为小数点后的秒数。

```sql
SELECT TO_TIMESTAMP('01-01-2017 10:2', 'DD-MM-YYYY SS:MS');
       to_timestamp
--------------------------
 2017-01-01 00:00:10.2+08
(1 row)
```

在这个例子中，2不是2毫秒而是200.这意味着：

```sql
SELECT TO_TIMESTAMP('01-01-2017 10:2', 'DD-MM-YYYY SS:MS');
```

和

```sql
SELECT TO_TIMESTAMP('01-01-2017 10:2', 'DD-MM-YYYY SS:MS');
```

返回相同的结果。

```sql
       to_timestamp
--------------------------
 2017-01-01 00:00:10.2+08
(1 row)
```

要获得2毫秒，您必须使用 `01-01-2017 10：002`。在这种情况下，002被解释为0.002秒，相当于2毫秒。

4) 如果年份少于四位，`TO_TIMESTAMP()` 将调整到最接近的年份，例如99变为1999,17变为2017。

```sql
SELECT TO_TIMESTAMP('12 31 99 12:45', 'MM DD YY HH:MI');
      to_timestamp
------------------------
 1999-12-31 00:45:00+08
(1 row)
```

请参考以下示例：

```sql
SELECT TO_TIMESTAMP('12 31 16 12:45', 'MM DD YY HH:MI');
      to_timestamp
------------------------
 2016-12-31 00:45:00+08
(1 row)
```

最近的16年是2016年，因此，它返回以上结果。

在本教程中，您学习了如何使用 `TO_TIMESTAMP()` 函数将字符串转换为时间戳。