## RENAME COLUMN



![PostgreSQL RENAME COLUMN](/imgs/postgresql-rename-column-300x254.png)

> 摘要：在本教程中，您将学习如何使用 `ALTER TABLE` 语句中的PostgreSQL `RENAME COLUMN` 子句重命名表的一个或多个列。

#### 简介

要重命名表的列，请使用带有 `RENAME COLUMN` 子句的 [ALTER TABLE](content/postgresql-alter-table.md) 语句，如下所示：

```sql
ALTER TABLE table_name 
RENAME COLUMN column_name TO new_column_name;
```

在这个声明中：

- 首先，在 `ALTER TABLE` 子句之后指定包含要重命名的列的表。
- 其次，在 `RENAME COLUMN` 子句后面提供列名。
- 第三，在`TO` 关键字后面给出新的列名。

因此，语句中的 `COLUMN` 关键字可选，您可以省略它，如以下语句所示：

```sql
ALTER TABLE table_name 
RENAME column_name TO new_column_name;
```

出于某些原因，如果您尝试重命名不存在的列，PostgreSQL将发出错误。不幸的是，PostgreSQL没有为 `RENAME` 子句提供 `IF EXISTS` 选项。

要重命名多个列，请为每个列添加每个RENAME子句，每个子句用逗号（，）分隔：

```sql
ALTER TABLE table_name
RENAME column_name_1 TO new_column_name_1,
RENAME column_name_2 TO new_column_name_2,
...;
```

如果重命名由其他数据库对象引用的列，例如[视图](content/postgresql-views.md)，[外键约束](content/postgresql-foreign-key.md)，[触发器](content/postgresql-triggers.md)，[存储过程](content/postgresql-stored-procedures.md)等，PostgreSQL也将更改其所有依赖对象。

#### 示例

我们将创建两个新表 `customer` 和 `customer_groups` 进行演示。

```sql
CREATE TABLE customer_groups (
    id serial PRIMARY KEY,
    name VARCHAR NOT NULL
);
 
CREATE TABLE customers (
    id serial PRIMARY KEY,
    name VARCHAR NOT NULL,
    phone VARCHAR NOT NULL,
    email VARCHAR,
    group_id INT,
    FOREIGN KEY (group_id) REFERENCES customer_groups (id)
);
```

此外，我们基于 `customers` 和 `customer_groupstables` 创建名为 `customer_data` 的新视图。

```sql
CREATE VIEW customer_data 
AS SELECT
    c.id,
    c.name,
    g.name customer_group
FROM
    customers c
INNER JOIN customer_groups g ON g.id = c.group_id;
```

以下语句将 `customers` 表的 `email` 列重命名为 `contact_email`：

```sql
ALTER TABLE customers 
RENAME COLUMN email TO contact_email;
```

让我们尝试重命名具有依赖对象的列，例如 `customer_groups` 表的 `name` 列：

```sql
ALTER TABLE customer_groups RENAME COLUMN name TO group_name;
```

现在，您可以检查名称列的更改是否已级联到 `customer_data` 视图：

```sql
test=# \d+ customer_data;
                       View "public.customer_data"
     Column     |       Type        | Modifiers | Storage  | Description
----------------+-------------------+-----------+----------+-------------
 id             | integer           |           | plain    |
 name           | character varying |           | extended |
 customer_group | character varying |           | extended |
View definition:
 SELECT c.id,
    c.name,
    g.group_name AS customer_group
   FROM customers c
     JOIN customer_groups g ON g.id = c.group_id;
```

正如您在视图定义中看到的那样，`name` 列已更改为 `group_name`。

以下说明如何在单个语句中将 `customers` 表的两列 `name` 和 `phone` 分别重命名为 `customer_name` 和`contact_phone`：

```sql
ALTER TABLE customers 
RENAME COLUMN name TO customer_name,
RENAME COLUMN phone TO contact_phone.
```

在本教程中，您学习了如何使用 `ALTER TABLE` 语句中的PostgreSQL `RENAME COLUMN` 子句来更改一个或多个列的名称。