## 恢复数据库

> 摘要：在本教程中，您将学习如何使用PostgreSQL 恢复工具（包括 `pg_restore` 和 `psql`）来恢复数据库。

在还原数据库之前，需要终止与该数据库的所有连接并准备备份文件。在PostgreSQL中，您可以通过两种方式恢复数据库：

- 使用 `psql` 恢复由 `pg_dump` 和 `pg_dumpall` 工具生成的纯SQL脚本文件。
- 使用 `pg_restore` 恢复 `pg_dump` 工具创建的tar和目录格式文件。

`psql` 实用程序允许您恢复由 `pg_dump`，`pg_dumpall` 或任何其他工具生成的SQL脚本文件，这些工具可以生成兼容的备份文件。通过使用 `psql` 工具，您必须执行整个脚本。

要还原完整备份并忽略还原过程中发生的任何错误，请使用以下命令：

```
psql -U username -f backupfile.sql
```

如果要在发生错误时停止还原数据库，请使用以下命令：

```sql
psql -U username --set ON_ERROR_STOP=on -f backupfile
```

请注意，我们添加了一个附加选项 `--set ON_ERROR_STOP=on`

如果备份特定数据库中的特定数据库对象，则可以使用以下命令将其还原：

```sql
psql -U username -d database_name -f objects.sql
```

#### 如何使用pg_restore恢复数据库

除了  `psql` 工具之外，您还可以使用 `pg_restore` 程序来恢复由 `pg_dump` 或者 `pg_dumpall`  工具备份的数据库。使用 `pg_restore` 程序，您可以使用各种恢复数据库选项，例如：

- `pg_restore` 允许您使用 `-j` 选项执行并行还原，以指定还原的线程数。每个线程同时恢复一个单独的表，这大大加快了过程。目前，`pg_restore` 支持此选项，仅用于自定义文件格式。
- 使用 `pg_restore` 可以还原包含完整数据库的备份文件中的特定数据库对象。
- `pg_restore` 可以在旧版本中备份数据库，并在较新版本中恢复它。

让我们创建一个名为 `newdvdrental`的新数据库以联系 `pg_restore` 工具。

```sql
CREATE DATABASE newdvdrental;
```

在PostgreSQL备份数据库教程中，您可以使用 `pg_dump` 工具备份数据库，以下命令生成的 `tar` 文件格式恢复`dvdrental` 数据库：

您可以用tar格式备份文件还原 `dvdrent` 数据库。备份文件由[备份数据库](content/postgresql-backup-database.md)教程中的 `pg_dump` 工具生成：

```sql
pg_restore --dbname=newdvdrental --verbose /work/test/pgbackup/dvdrental.tar
```

如果还原数据库（与备份的数据库相同），则可以使用以下命令：

```sql
pg_restore --dbname=dvdrental --create --verbose /work/test/pgbackup/dvdrental.tar
```

PostgreSQL 9.2开始，您可以使用 `--section` 选项仅恢复表结构。这允许您使用新数据库作为模板来创建其他数据库。

首先，您可以创建一个名为 ·dvdrental_tpl· 的新数据库。

```sql
CREATE DATABASE dvdrental_tpl;
```

其次，我们只能使用以下命令从 `dvdrental.tar` 备份文件恢复表结构：

```sql
pg_restore --dbname=dvdrental_tpl --section=pre-data  /work/test/pgbackup/dvdrental.tar
```

在本教程中，我们向您展示了使用PostgreSQL还原工具还原数据库的实用方法。

#### 相关教程

- [pg_restore官方文档](http://www.postgresql.org/docs/current/static/app-pgrestore.html)
- [备份数据库](content/postgresql-backup-database.md)