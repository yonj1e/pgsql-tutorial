## UPDATE Join

> 摘要：本教程介绍如何使用PostgreSQL UPDATE join语法根据另一个表中的值更新表中的数据。

#### 简介

有时，您需要根据另一个表中的值更新表的数据。在这种情况下，您可以使用PostgreSQL UPDATE 连接语法，如下所示：

```sql
UPDATE A
SET A.c1 = expresion
FROM B
WHERE A.c2 = B.c2;
```

要连接到 `UPDATE` 语句中的另一个表，请在 `FROM` 子句中指定连接表，并在 `WHERE` 子句中提供连接条件。 `FROM` 子句必须紧跟在 `SET` 子句之后出现。

如果表A和B中的每一行在列c2中具有匹配值，则此形式的 `UPDATE` 语句更新表A中的列值c1。

#### 示例

让我们看一个例子来了解 `UPDATE` 连接的工作原理。我们将使用以下数据库表进行演示：

![PostgreSQL UPDATE join sample database](/imgs/PostgreSQL-UPDATE-JOIN-Sample-Database.png)

首先，创建一个名为 `product_segment` 的新表，用于存储豪华奢侈品，奢侈品和大众产品等产品细分。

`product_segment` 表具有折扣列，该列存储基于特定段的折扣百分比。例如，豪华奢侈品有5％的折扣，而奢侈品和大众产品分别有6％和10％的折扣。

```sql
CREATE TABLE product_segment (
    ID SERIAL PRIMARY KEY,
    segment VARCHAR NOT NULL,
    discount NUMERIC (4, 2)
);
 
INSERT INTO product_segment (segment, discount)
VALUES
    ('Grand Luxury', 0.05),
    ('Luxury', 0.06),
    ('Mass', 0.1);
```

其次，创建另一个名为 `product` 的表来存储产品数据。`product` 表具有链接到 `segment` 表的id的外键列`segment_id`。

```sql
CREATE TABLE product(
    id serial primary key,
    name varchar not null,
    price numeric(10,2),
    net_price numeric(10,2),
    segment_id int not null,
    foreign key(segment_id) references product_segment(id)
);
 
 
INSERT INTO product (name, price, segment_id) 
VALUES ('diam', 804.89, 1),
    ('vestibulum aliquet', 228.55, 3),
    ('lacinia erat', 366.45, 2),
    ('scelerisque quam turpis', 145.33, 3),
    ('justo lacinia', 551.77, 2),
    ('ultrices mattis odio', 261.58, 3),
    ('hendrerit', 519.62, 2),
    ('in hac habitasse', 843.31, 1),
    ('orci eget orci', 254.18, 3),
    ('pellentesque', 427.78, 2),
    ('sit amet nunc', 936.29, 1),
    ('sed vestibulum', 910.34, 1),
    ('turpis eget', 208.33, 3),
    ('cursus vestibulum', 985.45, 1),
    ('orci nullam', 841.26, 1),
    ('est quam pharetra', 896.38, 1),
    ('posuere', 575.74, 2),
    ('ligula', 530.64, 2),
    ('convallis', 892.43, 1),
    ('nulla elit ac', 161.71, 3);
```

第三，假设您必须根据产品细分的折扣计算每种产品的净价。为此，您可以应用 `UPDATE` join语句，如下所示：

```sql
UPDATE product
SET net_price = price - price * discount
FROM product_segment
WHERE
product.segment_id = product_segment.id;
```

此语句将 `product` 表连接到 `product_segment` 表。如果两个表中都匹配，则从 `product_segment` 表中获取折扣，根据以下公式计算净价，并更新 `net_price` 列。

```sql
net_price = price - price * discount;
```

让我们使用以下SELECT语句来检查 `product` 表的数据：

```sql
SELECT * FROM product;
```

如您所见，`net_price` 列已使用正确的值进行更新。

在本教程中，您学习了如何使用PostgreSQL UPDATE join语句根据另一个表中的值更新表中的数据。