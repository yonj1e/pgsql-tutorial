## MD5

`MD5()` 函数计算字符串的[MD5](https://en.wikipedia.org/wiki/MD5)哈希值并以十六进制形式返回结果。

#### 语法

以下说明了 `MD5()` 函数的语法：

```sql
MD5(string)
```

#### 参数

`MD5()` 函数接受一个参数。

**1) string**

字符串参数是计算MD5哈希的字符串。

#### 返回值

`MD5()` 函数返回 `TEXT` 数据类型的字符串。

#### 示例

以下示例显示如何使用 `MD5()` 函数返回 `'PostgreSQL MD5'` 的MD5哈希值：

```sql
SELECT MD5('PostgreSQL MD5');
               md5
----------------------------------
 f78fdb18bf39b23d42313edfaf7e0a44
(1 row)
```

在本教程中，您学习了如何使用 `MD5()` 函数来计算字符串的MD5哈希值。