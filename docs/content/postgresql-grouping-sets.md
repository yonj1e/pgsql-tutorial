## GROUPING SETS

> 摘要：在本教程中，您将学习如何使用 `GROUPING SETS` 生成一个等效于多个 `GROUP BY` 子句的 `UNION ALL` 生成的结果集。

#### 前提

在开始之前，让我们为演示创建一个名为 `sales` 的新表。

```sql
CREATE TABLE sales (
    brand VARCHAR NOT NULL,
    segment VARCHAR NOT NULL,
    quantity INT NOT NULL,
    PRIMARY KEY (brand, segment)
);
 
INSERT INTO sales (brand, segment, quantity)
VALUES
    ('ABC', 'Premium', 100),
    ('ABC', 'Basic', 200),
    ('XYZ', 'Premium', 100),
    ('XYZ', 'Basic', 300);
    
select * from sales;
 brand | segment | quantity
-------+---------+----------
 ABC   | Premium |      100
 ABC   | Basic   |      200
 XYZ   | Premium |      100
 XYZ   | Basic   |      300
(4 rows)
```

销售表存储品牌，细分市场(基础/高级)和产品数量。

#### 简介

分组集是一组列，根据这些列进行分组。通常，单个聚合查询定义单个分组集。

例如，下面的查询定义了品牌和市场的分组集。它返回按品牌和细分市场的产品数量。 

```sql
SELECT brand, segment, SUM (quantity)
FROM sales
GROUP BY brand, segment;
 brand | segment | sum
-------+---------+-----
 XYZ   | Basic   | 300
 ABC   | Premium | 100
 ABC   | Basic   | 200
 XYZ   | Premium | 100
(4 rows)
```

以下查询查找按品牌销售的产品数量。它定义了品牌的分组集合：

```sql
SELECT brand, SUM (quantity)
FROM sales
GROUP BY brand;
 brand | sum
-------+-----
 ABC   | 300
 XYZ   | 400
(2 rows)
```

以下查询查找按细分市场的产品数。它定义了细分市场的分组集：

```sql
SELECT segment, SUM (quantity)
FROM sales
GROUP BY segment;
 segment | sum
---------+-----
 Basic   | 500
 Premium | 200
(2 rows)
```

以下查询查找所有品牌和细分市场的销售产品数量。它定义了一个空的分组集。

```sql
SELECT SUM (quantity) FROM sales;
 sum
-----
 700
(1 row)
```

假设您想要查看包含所有分组集的聚合数据的统一结果集，而不是四个结果集。为此，您可以使用 `UNION ALL` 统一上面的所有查询。

由于 `UNION ALL` 要求所有结果集具有相同数量的具有兼容数据类型的列，因此需要通过将 `NULL` 添加到每个选择列表中来调整查询，如下所示：

```sql
SELECT brand, segment, SUM (quantity)
FROM sales
GROUP BY brand, segment
UNION ALL
SELECT brand, NULL, SUM (quantity)
FROM sales
GROUP BY brand
UNION ALL
SELECT NULL, segment, SUM (quantity)
FROM sales
GROUP BY segment
UNION ALL
SELECT NULL, NULL, SUM (quantity)
FROM sales;
 brand | segment | sum
-------+---------+-----
 XYZ   | Basic   | 300
 ABC   | Premium | 100
 ABC   | Basic   | 200
 XYZ   | Premium | 100
 ABC   |         | 300
 XYZ   |         | 400
       | Basic   | 500
       | Premium | 200
       |         | 700
(9 rows)
```

此查询生成一个结果集，其中包含所有分组集的聚合。

尽管代码按预期工作，但它有两个主要问题。首先，这是非常冗长的。其次，它存在性能问题，因为PostgreSQL必须为每个查询单独扫描销售表。

为了提高效率，PostgreSQL提供了  `GROUPING SETS`，它是 `GROUP BY` 子句的子句。

`GROUPING SETS` 允许您在同一查询中定义多个分组集。`GROUPING SETS` 的一般语法如下：

```sql
SELECT c1, c2, aggregate_function(c3)
FROM table_name
GROUP BY
    GROUPING SETS (
        (c1, c2),
        (c1),
        (c2),
        ()
	);
```

在此语法中，您有四个分组集  (c1,c2), (c1), (c2), 和 ().。

要将此语法应用于上面的示例，您可以使用 `GROUPING SETS` 而不是 `UNION ALL`，如以下查询中所示：

```sql
SELECT brand, segment, SUM (quantity)
FROM sales
GROUP BY
    GROUPING SETS (
        (brand, segment),
        (brand),
        (segment),
        ()
    );
 brand | segment | sum
-------+---------+-----
       |         | 700
 XYZ   | Basic   | 300
 ABC   | Premium | 100
 ABC   | Basic   | 200
 XYZ   | Premium | 100
 ABC   |         | 300
 XYZ   |         | 400
       | Basic   | 500
       | Premium | 200
(9 rows)
```

此查询更短，更易读。此外，PostgreSQL将优化其扫描销售表的次数，并且不会针对每个分组集单独扫描它。

#### 函数

`GROUPING` 函数接受列的名称，如果列是当前分组集的成员，则返回位0，否则返回1。请参阅以下示例：

```sql
SELECT
    GROUPING(brand) grouping_brand,
	GROUPING(segment) grouping_segement,
	brand, segment, SUM (quantity)
FROM sales
GROUP BY
	GROUPING SETS (
		(brand, segment),
		(brand),
		(segment),
		()
	)
ORDER BY brand, segment;
 grouping_brand | grouping_segement | brand | segment | sum
----------------+-------------------+-------+---------+-----
              0 |                 0 | ABC   | Basic   | 200
              0 |                 0 | ABC   | Premium | 100
              0 |                 1 | ABC   |         | 300
              0 |                 0 | XYZ   | Basic   | 300
              0 |                 0 | XYZ   | Premium | 100
              0 |                 1 | XYZ   |         | 400
              1 |                 0 |       | Basic   | 500
              1 |                 0 |       | Premium | 200
              1 |                 1 |       |         | 700
(9 rows)
```

在本教程中，您学习了如何使用 ` GROUPING SETS `生成多个分组集。