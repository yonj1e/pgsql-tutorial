## EXISTS

> 摘要：在本教程中，您将学习如何使用  `EXISTS` 运算符来测试子查询中是否存在行。

#### 简介

`EXISTS` 运算符用于测试[子查询](content/postgresql-subquery.md)中是否存在行。

以下说明了 `EXISTS` 运算符的语法：

```sql
EXISTS (subquery)
```

`EXISTS` 接受一个参数，该参数是一个子查询。

如果子查询返回至少一行，则 `EXISTS` 的结果为true。如果子查询没有返回任何行，则结果为false。

`EXISTS` 通常与相关子查询一起使用。

`EXISTS` 的结果取决于子查询返回的任何行，而不是行的内容。因此，子查询的 `SELECT` 子句中出现的列并不重要。

因此，常见的是以下列形式编写 `EXISTS`：

```sql
SELECT  column_1 
FROM  table_1
WHERE 
    EXISTS( 
        SELECT  1 
		FROM table_2 
		WHERE column_2 = table_1.column_1
    );
```

请注意，如果子查询返回NULL，则 `EXISTS` 的结果为true。

#### 示例

我们将使用[示例数据库](content/postgresql-sample-database.md)中的以下 `customer` 和 `payment` 表进行演示：

![customer and payment tables](/imgs/customer-and-payment-tables.png)

##### A) 查找至少有一笔金额大于11的付款的客户。

A）查找至少有一笔金额大于11的付款的客户。

以下报表返回至少已支付租金超过11的的客户：

```sql
SELECT first_name, last_name
FROM customer c
WHERE EXISTS
    (SELECT 1 FROM payment p
     WHERE p.customer_id = c.customer_id
       AND amount > 11 
    )
ORDER BY first_name, last_name;
 first_name | last_name
------------+-----------
 Karen      | Jackson
 Kent       | Arsenault
 Nicholas   | Barfield
 Rosemary   | Schmidt
 Tanya      | Gilbert
 Terrance   | Roush
 Vanessa    | Sims
 Victoria   | Gibson
(8 rows)
```

在此示例中，对于 `customer` 表中的每个客户，子查询检查 `payment` 表以查找该客户是否至少进行了一次付款（`p.customer_id = c.customer_id`）且金额大于11（`amount > 11`）

##### B) NOT EXISTS

`NOT EXISTS` 与 `EXISTS` 相反，这意味着如果子查询没有返回任何行，则 `NOT EXISTS` 返回true。如果子查询返回任何行，则 `NOT EXISTS` 返回false。

以下示例返回客户未进行任何超过11的付款。

```sql
SELECT first_name, last_name
FROM customer c
WHERE NOT EXISTS
    (SELECT 1
     FROM payment p
     WHERE p.customer_id = c.customer_id
       AND amount > 11 
    )
ORDER BY first_name, last_name;
 first_name  |  last_name
-------------+--------------
 Aaron       | Selby
 Adam        | Gooch
 Adrian      | Clary
 Agnes       | Bishop
 Alan        | Kahn
 Albert      | Crouse
 Alberto     | Henning
 Alex        | Gresham	
```

##### C) EXISTS 和 NULL

如果子查询返回 `NULL`，则 `EXISTS` 返回true。请参阅以下示例：

```sql
SELECT first_name, last_name
FROM customer
WHERE EXISTS( SELECT NULL )
ORDER BY first_name, last_name;
 first_name  |  last_name
-------------+--------------
 Aaron       | Selby
 Adam        | Gooch
 Adrian      | Clary
 Agnes       | Bishop
 Alan        | Kahn
 Albert      | Crouse
 Alberto     | Henning
 Alex        | Gresham
```

在此示例中，子查询返回 `NULL`，因此，查询返回 `customer` 表中的所有行。

在本教程中，您学习了如何使用 `EXISTS` 来测试子查询中是否存在行。