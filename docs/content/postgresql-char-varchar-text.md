## 字符串类型

> 摘要：在本教程中，我们将向您介绍PostgreSQL字符数据类型，包括char，varchar和text，并为您提供一些正确设计数据库表的选择提示。

#### 简介

PostgreSQL提供三种主要字符类型：`character(n)` 或 `char(n)`，字符 `vary(n)` 或 `varchar(n)` 和 `text`，其中n是正整数。

| 字符串类型                           | 描述             |
| ------------------------------------ | ---------------- |
| character varying(*n*), varchar(*n*) | 变长             |
| character(*n*), char(*n*)            | 定长，不足补空白 |
| text, varchar                        | 变长，无长度限制 |

`char(n)` 和 `varchar(n)` 都可以存储最多n个字符。如果您尝试在 `char(n)` 或 `varchar(n)` 列中存储较长的字符串，PostgreSQL将发出错误。

但是，一个例外是如果多余的字符都是空格，PostgreSQL会将空格截断为最大长度并存储字符串。

如果字符串显式地转换为 `char(n)` 或 `varchar(n)`，PostgresQL将在插入表之前将字符串截断为n个字符。

`text` 数据类型可以存储长度不限的字符串。

如果未为 `varchar` 数据类型指定n整数，则其行为类似于 `text` 数据类型。`varchar`（没有n）和 `text` 的性能是相同的。

指定 `varchar` 数据类型的长度说明符的唯一优点是，如果您尝试将更长的字符串插入 `varchar(n)` 列，PostgreSQL将检查并发出错误。

与 `varchar` 不同，不带长度说明的 `character` 或 `char` 与 `character(1)` 或  `char(1)` 相同。

与其他数据库系统不同，在PostgreSQL中，三种字符类型之间没有性能差异。在大多数情况下，如果希望PostgreSQL检查长度限制，则应使用 `text` 或 `varchar` 和 `varchar(n)`。

#### 示例

让我们看一个示例来查看 `char`，`varchar` 和 `text` 数据类型的工作方式。

首先，我们为演示创建一个新表。

```sql
CREATE TABLE character_tests (
 id serial PRIMARY KEY,
 x CHAR (1),
 y VARCHAR (10),
 z TEXT
);
```

然后，我们在 `character_tests` 表中插入一个新行。

```sql
INSERT INTO character_tests (x, y, z)
VALUES
 (
 'Yes',
 'This is a test for varchar',
 'This is a very long text for the PostgreSQL text column'
 );
```

PostgreSQL发出错误：

```sql
ERROR:  value too long for type character(1)
```

这是因为 `x` 列的数据类型是 `char(1)`，我们却尝试将包含三个字符的字符串插入此列。我们来解决它。

```sql
INSERT INTO character_tests (x, y, z)
VALUES
 (
 'Y',
 'This is a test for varchar',
 'This is a very long text for the PostgreSQL text column'
 );
```

现在我们遇到了不同的错误。

```sql
ERROR:E   value too long for type character varying(10)
```

这是因为我们尝试将包含10个以上字符的字符串插入到使用 `varchar(10)` 数据类型的 `y` 列中。

以下语句成功将新行插入 `character_tests` 表。

```sql
INSERT INTO character_tests (x, y, z)
VALUES
 (
 'Y',
 'varchar(n)',
 'This is a very long text for the PostgreSQL text column'
 );
```

```sql
SELECT * FROM character_tests;
 id | x |     y      |                            z
----+---+------------+---------------------------------------------------------
  1 | Y | varchar(n) | This is a very long text for the PostgreSQL text column
(1 row)
```

现在您应该知道如何为数据库表选择正确的字符数据类型。大多数情况下，您应该选择没有长度说明符的 `text` 或 `varchar`。

#### 相关教程

- [数据类型](content/postgresql-data-types.md)