## POSITION

`POSITION()` 函数返回字符串中子字符串的位置。

#### 语法

下面说明了 `POSITION()` 函数的语法：

```sql
POSITION(substring in string)
```

#### 参数

`POSITION()` 函数需要两个参数：

**1) substring**

`substring` 参数是您要查找的字符串。

**2) string**

`string` 参数是搜索子字符串的字符串。

#### 返回值

`POSITION()` 函数返回一个[整数](content/postgresql-integer.md)，表示字符串中子字符串的位置。

如果在字符串中找不到子字符串，则 `POSITION()` 函数返回零（0）。如果 `substring` 或 `string` 参数为null，则返回null。

#### 示例

以下示例返回字符串 `'PostgreSQL Tutorial'` 中 `'Tutorial'` 的位置：

```sql
SELECT POSITION('Tutorial' IN 'PostgreSQL Tutorial');
position
----------
       12
(1 row)
```

请注意， `POSITION()` 函数不区分大小写地搜索子字符串。

请参阅以下示例：

```sql
ELECT POSITION('tutorial' IN 'PostgreSQL Tutorial');
```

它返回零（0），表示字符串 `'PostgreSQL Tutorial'` 中不存在字符串 `tutorial`。

#### 备注

`POSITION()` 函数返回字符串中第一个子字符串实例的位置。

请考虑以下示例：

```sql
SELECTS  POSITION('is' IN 'This is a cat');
 position
----------
        3
(1 row)
```

即使字符串 `'is'` 在字符串 `'This is a cat'` 中出现两次，`POSITION()` 函数也只返回第一个匹配。

在本教程中，您学习了如何使用 `POSITION()` 函数在字符串中查找子字符串。