## Describe Table

> 摘要：本教程将向您展示如何使用 `psql` 工具和 `information_schema` 来描述PostgreSQL中的表。

如果您使用的是MySQL，则使用 [DESCRIBE](http://www.mysqltutorial.org/mysql-show-columns/) 语句查找有关特定表的列的信息。PostgreSQL不提供 `DESCRIBE` 语句。但是，您可以通过多种方式查询表的列信息。

#### psql

首先，连接到PostgreSQL服务器，数据库 `dvdrental`。

其次，发出命令 `\d table_name` 或 `\d + table_name` 以查找有关表的列的信息。以下示例查询有关 `city` 表的列的信息。

```sql
\d city
                                           Table "public.city"
   Column    |            Type             | Collation | Nullable |                Default
-------------+-----------------------------+-----------+----------+---------------------------------------
 city_id     | integer                     |           | not null | nextval('city_city_id_seq'::regclass)
 city        | character varying(50)       |           | not null |
 country_id  | smallint                    |           | not null |
 last_update | timestamp without time zone |           | not null | now()
Indexes:
    "city_pkey" PRIMARY KEY, btree (city_id)
    "idx_fk_country_id" btree (country_id)
Foreign-key constraints:
    "fk_city" FOREIGN KEY (country_id) REFERENCES country(country_id)
Referenced by:
    TABLE "address" CONSTRAINT "fk_address_city" FOREIGN KEY (city_id) REFERENCES city(city_id)
Triggers:
    last_updated BEFORE UPDATE ON city FOR EACH ROW EXECUTE PROCEDURE last_updated()
```

该命令发布了关于 `city` 表列的大量信息。此外，它还返回索引，外键约束和[触发器](content/postgresql-triggers.md)。

如果您只想知道表的列列表，可以使用第二种方法。

#### information_schema

这样，您只需使用 `SELECT` 语句来查询 `information_schema` 数据库中列表的列名。。

例如，以下查询返回 `city` 表的所有列名：

```sql
SELECT COLUMN_NAME
FROM information_schema.COLUMNS
WHERE TABLE_NAME = 'city';
 column_name
-------------
 city_id
 city
 country_id
 last_update
(4 rows)
```

在本教程中，我们向您展示了如何使用 `psql` 工具和 `information_schema` 查询特定表的列的信息。

#### 相关教程

- [Show Tables](content/postgresql-show-tables.md)
- [Show Databases](content/postgresql-show-databases.md)
- [psql Commands](content/psql-commands.md)