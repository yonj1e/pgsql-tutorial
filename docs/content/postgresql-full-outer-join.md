## FULL OUTER JOIN

> 摘要：在本教程中，您将学习如何使用 `FULL OUTER JOIN` 来查询来自两个或多个表的数据。

#### 简介

假设您要执行两个表的全外连接：A和B。

下面说明了 `FULL OUTER JOIN` 的语法：

```sql
SELECT * FROM A
FULL [OUTER] JOIN B on A.id = B.id;
```

`OUTER` 关键字是可选的。

全外连接组合了[左连接](content/postgresql-left-join.md)和右连接的结果。如果连接表中的行不匹配，则全外连接将为缺少匹配行的表的每列设置NULL值。对于匹配的行，结果集中包含一行，其中包含从两个连接表填充的列。

以下维恩图说明了 `FULL OUTER JOIN` 操作：

![PostgreSQL full outer join](/imgs/PostgreSQL-full-outer-join.png)

结果包括两个表中的匹配行以及不匹配的行。

#### 示例

First, we will [create two new tables](http://www.postgresqltutorial.com/postgresql-create-table/) for the demonstration: employees and departments:

首先，我们将为演示[创建两个新表](content/postgresql-create-table.md)： `employees` 和 `departments`：

```sql
CREATE TABLE IF NOT EXISTS departments ( 
    department_id serial PRIMARY KEY, 
    department_name VARCHAR (255) NOT NULL
);

CREATE TABLE IF NOT EXISTS employees ( 
    employee_id serial PRIMARY KEY, 
    employee_name VARCHAR (255), 
    department_id INTEGER
);
```

每个部门都有零个或多个员工，每个员工属于零个或一个部门。

以下[INSERT](content/postgresql-insert.md)语句将一些示例数据添加到 `departments` 和` employees` 表中。

```sql
INSERT INTO departments (department_name)
VALUES ('Sales'), ('Marketing'), ('HR'), ('IT'), ('Production'); 

INSERT INTO employees ( employee_name, department_id)
VALUES 
('Bette Nicholson', 1), 
('Christian Gable', 1), 
('Joe Swank', 2), 
('Fred Costner', 3), 
('Sandra Kilmer', 4), 
('Julia Mcqueen', NULL);
```

接下来，我们从部门和员工表中查询数据：

```sql
# SELECT * FROM departments; 
 department_id | department_name
---------------+-----------------
             1 | Sales
             2 | Marketing
             3 | HR
             4 | IT
             5 | Production
(5 rows)
```

```sql
# SELECT * FROM employees; 
 employee_id |  employee_name  | department_id
-------------+-----------------+---------------
           1 | Bette Nicholson |             1
           2 | Christian Gable |             1
           3 | Joe Swank       |             2
           4 | Fred Costner    |             3
           5 | Sandra Kilmer   |             4
           6 | Julia Mcqueen   |
(6 rows)
```

然后，我们使用 `FULL OUTER JOIN` 查询来自员工和部门表的数据。

```sql
SELECT employee_name, department_name
FROM employees e
FULL OUTER JOIN departments d ON d.department_id = e.department_id;
  employee_name  | department_name
-----------------+-----------------
 Bette Nicholson | Sales
 Christian Gable | Sales
 Joe Swank       | Marketing
 Fred Costner    | HR
 Sandra Kilmer   | IT
 Julia Mcqueen   |
                 | Production
(7 rows)
```

结果集包括属于某个部门的每个员工以及拥有员工的每个部门。此外，它还包括不属于某个部门的每个员工以及没有员工的每个部门。

要查找拥有任何员工的部门，我们使用[WHERE](content/postgresql-where.md)子句，如下所示：

```sql
SELECT employee_name, department_name
FROM employees e
FULL OUTER JOIN departments d ON d.department_id = e.department_id
WHERE employee_name IS NULL;
 employee_name | department_name
---------------+-----------------
               | Production
(1 row)
```

结果表明  `Production` 部门没有任何员工。

要查找不属于任何部门的员工，我们将在 `WHERE` 子句中检查 `department_name` 为 `NULL ` 的数据，如下所示：

```sql
SELECT employee_name, department_name
FROM employees e
FULL OUTER JOIN departments d ON d.department_id = e.department_id
WHERE department_name IS NULL;
 employee_name | department_name
---------------+-----------------
 Julia Mcqueen |
(1 row)
```

如你所见，`Juila Mcqueen` 不属于任何部门。

在本教程中，您学习了如何使用 `FULL OUTER JOIN` 子句来连接两个或多个表。

#### 相关教程

- [LEFT JOIN](http://www.postgresqltutorial.com/postgresql-left-join/)
- [Cross Join](http://www.postgresqltutorial.com/postgresql-cross-join/)
- [INNER JOIN](http://www.postgresqltutorial.com/postgresql-inner-join/)
- [NATURAL JOIN](http://www.postgresqltutorial.com/postgresql-natural-join/)