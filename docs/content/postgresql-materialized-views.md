## 物化视图

> 摘要：本教程向您介绍PostgreSQL物化视图，它允许您物理存储查询结果并定期更新数据。

在[视图](content/managing-postgresql-views.md)教程中，您了解到视图是表示基础表数据的虚拟表。[可更新视图](content/postgresql-updatable-views.md)。PosgreSQL将视图概念扩展到下一个级别，允许视图在物理上存储数据，我们将这些视图称为物化视图。物化视图会缓存复杂昂贵查询的结果，然后允许您定期刷新此结果。

物化视图在许多需要快速数据访问的情况下非常有用，因此它们通常用于数据仓库或商业智能应用程序。

#### 创建物化视图

要创建物化视图，请使用 `CREATE MATERIALIZED VIEW` 语句，如下所示：

```sql
CREATE MATERIALIZED VIEW view_name
AS
query
WITH [NO] DATA;
```

首先，在 `CREATE MATERIALIZED VIEW` 子句后指定 `view_name`

其次，添加在 `AS` 关键字后从基础表获取数据的查询。

第三，如果要在创建时将数据加载到物化视图中，则使用 `WITH DATA` 选项，否则使用 `WITH NO DATA`。如果您使用 `WITH NO DATA`，则视图被标记为不可读。这意味着在将数据加载到视图中之前，您无法从视图中查询数据。

#### 刷新物化视图数据

要将数据加载到物化视图中，请使用 `REFRESH MATERIALIZED VIEW` 语句，如下所示：

```sql
REFRESH MATERIALIZED VIEW view_name;
```

刷新物化视图的数据时，会锁定整个表，因此您无法查询数据。为避免这种情况，您可以使用 `CONCURRENTLY` 选项。

```sql
REFRESH MATERIALIZED VIEW CONCURRENTLY view_name;
```

使用 `CONCURRENTLY` 选项，创建物化视图的临时更新版本，比较两个版本，并仅执行 [INSERT](content/postgresql-insert.md) 和 [UPDATE](content/postgresql-update.md) 差异。您可以在更新时查询物化视图。使用 `CONCURRENTLY` 选项的一个要求是物化视图必须具有 `UNIQUE` 索引。请注意，`CONCURRENTLY` 选项仅适用于PosgreSQL 9.4。

#### 删除物化视图

正如我们对表或视图所做的那样，删除物化视图非常简单。这是使用以下语句完成的：

```sql
DROP MATERIALIZED VIEW view_name;
```

#### 示例

以下语句创建名为 `rental_by_category` 的物化视图：

```sql
CREATE MATERIALIZED VIEW rental_by_category
AS
 SELECT c.name AS category,
    sum(p.amount) AS total_sales
   FROM (((((payment p
     JOIN rental r ON ((p.rental_id = r.rental_id)))
     JOIN inventory i ON ((r.inventory_id = i.inventory_id)))
     JOIN film f ON ((i.film_id = f.film_id)))
     JOIN film_category fc ON ((f.film_id = fc.film_id)))
     JOIN category c ON ((fc.category_id = c.category_id)))
  GROUP BY c.name
  ORDER BY sum(p.amount) DESC
WITH NO DATA;
```



Because we used the `WITH NO DATA` option, we cannot query data from the view. If we try to do so, we will get an error message as follows:

因为我们使用 `WITH NO DATA` 选项，所以我们无法从视图中查询数据。如果我们尝试这样做，我们将收到如下错误消息：

```sql
SELECT * FROM rental_by_category;
ERROR:  materialized view "rental_by_category" has not been populated
HINT:  Use the REFRESH MATERIALIZED VIEW command.
```

这给我们一个提示，要求将数据加载到视图中。让我们通过执行以下语句来完成它：

```sql
REFRESH MATERIALIZED VIEW rental_by_category;
```

现在，如果我们再次查询数据，我们将按预期获得结果。

```sql
SELECT * FROM rental_by_category;
  category   | total_sales
-------------+-------------
 Sports      |     4892.19
 Sci-Fi      |     4336.01
 Animation   |     4245.31
 Drama       |     4118.46
 Comedy      |     4002.48
 New         |     3966.38
 Action      |     3951.84
 Foreign     |     3934.47
 Games       |     3922.18
 Family      |     3830.15
 Documentary |     3749.65
 Horror      |     3401.27
 Classics    |     3353.38
 Children    |     3309.39
 Travel      |     3227.36
 Music       |     3071.52
(16 rows)
```

从现在开始，我们可以使用 `REFRESH MATERIALIZED VIEW` 语句刷新 `rental_by_category` 视图中的数据。但是，要使用 `CONCURRENTLY` 选项刷新它，我们需要首先为视图创建一个 `UNIQUE` 索引。

```sql
CREATE UNIQUE INDEX rental_category ON rental_by_category (category);
```

让我们同时刷新 `rental_by_category` 视图的数据。

```sql
REFRESH MATERIALIZED VIEW CONCURRENTLY rental_by_category;
```

在本教程中，我们向您展示了如何使用物化视图，这些视图对于需要快速数据检索的分析应用程序非常有用。

#### 相关教程

- [管理视图](content/managing-postgresql-views.md)
- [可更新视图](content/postgresql-updatable-views.md)