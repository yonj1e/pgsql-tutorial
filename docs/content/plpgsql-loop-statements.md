## PL/pgSQL 循环语句

> 摘要：在本教程中，我们将向您介绍各种重复执行语句块的PL/pgSQL循环语句。

PostgreSQL为您提供了三个循环语句：LOOP，WHILE循环和FOR循环。

#### LOOP

有时，您需要重复执行一个语句块，直到条件成立为止。为此，请使用PL/pgSQL LOOP语句。以下说明了 `LOOP` 语句的语法：

```sql
<<label>>
LOOP
   Statements;
   EXIT [<<label>>] WHEN condition;
END LOOP;
```

`LOOP` 语句也称为无条件循环语句，因为它执行语句，直到 `EXIT` 语句中的条件求值为true。请注意，`EXIT` 语句中 `WHEN` 关键字之后指定的条件是布尔表达式，其计算结果为true或false。

循环语句可以嵌套。`LOOP` 语句放在另一个 `LOOP` 语句中，称为嵌套循环。在这种情况下，您需要标签 label 以明确指定要在 `EXIT` 语句中终止的循环。

##### 示例

在这个例子中，我们将使用 `LOOP` 语句开发一个返回第n个序列号的函数 fibonacci。

```sql
CREATE OR REPLACE FUNCTION fibonacci (n INTEGER) 
 RETURNS INTEGER AS $$ 
DECLARE
   counter INTEGER := 0 ; 
   i INTEGER := 0 ; 
   j INTEGER := 1 ;
BEGIN
 
 IF (n < 1) THEN
 RETURN 0 ;
 END IF; 
 
 LOOP 
 EXIT WHEN counter = n ; 
 counter := counter + 1 ; 
 SELECT j, i + j INTO i, j ;
 END LOOP ; 
 
 RETURN i ;
END ; 
$$ LANGUAGE plpgsql;
```

Fibonacci函数接受一个整数并返回第n个Fibonacci数。根据定义，Fibonacci数是从0和1开始的整数序列，每个后续数字是前两个数字的乘积，例如 1, 1, 2 (1+1), 3 (2+1), 5 (3 +2), 8 (5+3), …

在声明部分中，`counter` 变量初始化为零（0）。在循环内部，当计数器等于n时，循环退出。该声明：

```sql
SELECT j, i + j INTO i, j ;
```

在不使用临时变量的情况下同时交换 `i` 和 `j`。

#### WHILE

`WHILE` 循环语句执行语句块，直到条件计算为false。在 `WHILE` 循环语句中，PostgreSQL在执行语句块之前判断条件。如果条件为真，则执行语句块，直到将其计算为false。

以下流程图说明了 ` WHILE`循环语句。

![PL/pgSQL WHILE loop](/imgs/plpgsql-WHILE-loop.png)

以下是 `WHILE` 循环语句的语法。

```sql
[ <<label>> ]
WHILE condition LOOP
   statements;
END LOOP;
```

##### 示例

我们可以使用 `WHILE` 循环语句在第一个示例中重写Fibonacci函数，如下所示：

```sql
CREATE OR REPLACE FUNCTION fibonacci (n INTEGER) 
 RETURNS INTEGER AS $$ 
DECLARE
   counter INTEGER := 0 ; 
   i INTEGER := 0 ; 
   j INTEGER := 1 ;
BEGIN
 
 IF (n < 1) THEN
 RETURN 0 ;
 END IF; 
 
 WHILE counter <= n LOOP
 counter := counter + 1 ; 
 SELECT j, i + j INTO i, j ;
 END LOOP ; 
 
 RETURN i ;
END ;
```

#### FOR

`FOR` 循环是PostgreSQL中最复杂的循环语句。我们将详细讨论 `FOR` 循环语句的每种形式。

##### 遍历范围内整数的 FOR 循环

语法如下：

```sql
[ <<label>> ]
FOR loop_counter IN [ REVERSE ] from.. to [ BY expression ] LOOP
    statements
END LOOP [ label ];
```

首先，PostgreSQL 创建一个只存在于 `FOR` 循环内部的 `loop_counter` 变量。默认情况下，循环计数器在每次迭代之后被添加，如果使用 `REVERSE` 关键字，PostgreSQL 将会减去循环计数器。

下一步，`from` 和 `to` 指定了范围的起始和结束位置。PostgreSQL 会在进入循环之前执行这个表达式。

第三，`BY` 子句指定了每次迭代的步长。如果省略了此子句，将使用默认步行 `1`。PostgreSQL 在循环时也执行这个表达式。

以下流程图说明了 `FOR` 循环语句：

![PL/pgSQL FOR loop](/imgs/plpgsql-FOR-loop.png)

以下是使用 `FOR` 循环语句的一些示例。

从 1 循环到 5，并在每次迭代时打印消息。`counter` 在每次循环迭代时，PostgreSQL 会给它加1，所以它的值分别是 1, 2, 3, 4, 5。

```sql
DO $$
BEGIN
   FOR counter IN 1..5 LOOP
 RAISE NOTICE 'Counter: %', counter;
   END LOOP;
END; $$
```

```sql
NOTICE:  Counter: 1
NOTICE:  Counter: 2
NOTICE:  Counter: 3
NOTICE:  Counter: 4
NOTICE:  Counter: 5
```

从 5 循环到 1， 并在每次迭代时打印消息。

```sql
DO $$
BEGIN
   FOR counter IN REVERSE 5..1 LOOP
      RAISE NOTICE 'Counter: %', counter;
   END LOOP;
END; $$
```

```sql
NOTICE:  Counter: 5
NOTICE:  Counter: 4
NOTICE:  Counter: 3
NOTICE:  Counter: 2
NOTICE:  Counter: 1
```

从 1 循环到 6，并在每次迭代时打印消息。`counter` 的值分别是 1, 3, 5，因为每次循环 PostgreSQL 会给它的值加 2。

```sql
DO $$
BEGIN 
  FOR counter IN 1..6 BY 2 LOOP
    RAISE NOTICE 'Counter: %', counter;
  END LOOP;
END; $$
```

```sql
NOTICE:  Counter 1
NOTICE:  Counter 3
NOTICE:  Counter 5
```

##### 使用 FOR 循环迭代查询结果

语法如下：

```sql
[ <<label>> ]
FOR target IN query LOOP
    statements
END LOOP [ label ];
```

下面的函数接受一个整型参数，用于从示例数据库的 `film` 表里查询指定数量的记录。`FOR` 循环迭代并打印这个从 `film` 表里查询出来的影片标题：

```sql
CREATE OR REPLACE FUNCTION for_loop_through_query(
   n INTEGER DEFAULT 10
) 
RETURNS VOID AS $$
DECLARE
    rec RECORD;
BEGIN
    FOR rec IN SELECT title 
        FROM film 
        ORDER BY title
        LIMIT n 
    LOOP 
 RAISE NOTICE '%', rec.title;
    END LOOP;
END;
$$ LANGUAGE plpgsql;
```

```sql
SELECTS  for_loop_through_query(5);
```

```sql
NOTICE:  Academy Dinosaur
NOTICE:  Ace Goldfinger
NOTICE:  Adaptation Holes
NOTICE:  Affair Prejudice
NOTICE:  African Egg
```

##### 使用 FOR 循环迭代动态查询结果

有时候，需要构造一个动态查询，并使用 `FOR` 循环来迭代查询结果。要实现这个功能，可以使用以下语法。

```sql
[ <<label>> ]
FOR row IN EXECUTE string_expression [ USING query_param [, ... ] ] 
LOOP
    statements
END LOOP [ label ];
```

您可以使用字符串表达式而不是SQL语句，该表达式是文本格式的SQL语句。这允许您动态构造查询。

如果查询具有参数，则使用 `USING` 语句将参数传递给查询。

以下函数演示了如何使用 `FOR` 循环语句循环遍历动态查询。它接受两个参数：

- sort_type：1表示按标题对查询结果进行排序，2表示按发布年份对结果进行排序。
- n：从影片表中查询的行数。请注意，它将在 `USING` 子句中使用。

首先，我们基于输入参数构建查询，然后在FOR循环函数内执行查询。

```sql
CREATE OR REPLACE FUNCTION for_loop_through_dyn_query(
   sort_type INTEGER,
   n INTEGER
) 
RETURNS VOID AS $$
DECLARE
    rec RECORD;
    query text;
BEGIN
 
 query := 'SELECT title, release_year FROM film ';
 IF sort_type = 1 THEN
 query := query || 'ORDER BY title';
 ELSIF sort_type = 2 THEN
   query := query || 'ORDER BY release_year';
 ELSE 
 RAISE EXCEPTION 'Invalid sort type %s', sort_type;
 END IF;
 
 query := query || ' LIMIT $1';
 
 FOR rec IN EXECUTE query USING n 
        LOOP
    RAISE NOTICE '% - %', rec.release_year, rec.title;
 END LOOP;
   
END;
$$ LANGUAGE plpgsql;
```

以下语句调用 `for_loop_through_dyn_query()` 函数获取5部电影并按标题对其进行排序：

```sql
SELECT for_loop_through_dyn_query(1,5);
```

```sql
NOTICE:  2006 - Academy Dinosaur
NOTICE:  2006 - Ace Goldfinger
NOTICE:  2006 - Adaptation Holes
NOTICE:  2006 - Affair Prejudice
NOTICE:  2006 - African Egg
```

以下语句调用 `for_loop_through_dyn_query()` 函数获取5部电影并按发行年份对其进行排序：

```sql
SELECT for_loop_through_dyn_query(2,5);
```

```sql
NOTICE:  2006 - Grosse Wonderful
NOTICE:  2006 - Airport Pollock
NOTICE:  2006 - Bright Encounters
NOTICE:  2006 - Academy Dinosaur
NOTICE:  2006 - Chamber Italian
```

在本教程中，我们向您展示了如何使用重复执行语句块的PL/pgSQL循环语句。