## 时间戳类型

> 摘要：在本教程中，我们将向您介绍PostgreSQL时间戳数据类型，包括timestamp和timestamptz，并向您展示如何使用一些方便的函数来更有效地处理时间戳数据。

#### 简介

![PostgreSQL Timestamp](/imgs/PostgreSQL-Timestamp-300x171.jpg)

PostgreSQL提供了两种用于处理时间戳的数据类型，一种没有时区（`timestamp`），另一种带有时区（`timestamptz`）。

时间戳数据类型允许您存储日期和时间。但是，它没有任何时区数据。这意味着当您更改数据库服务器的时区时，存储在数据库中的时间戳值不会更改。

`timestamptz` 数据是带时区的时间戳。`timestamptz` 是时区日期和时间数据类型。PostgreSQL以UTC值存储 `timestamptz`。将值插入 `timestamptz` 列时，PostgreSQL会将 `timestamptz` 值转换为UTC值，并将UTC值存储在表中。

从数据库查询 `timestamptz` 时，PostgreSQL会将UTC值转换回数据库服务器，用户或当前数据库连接设置的时区的时间值。

请注意，`timestamp` 和 `timestamptz` 都使用8个字节来存储时间戳值，如以下查询所示：

```sql
SELECT typname, typlen
FROM pg_type
WHERE typname ~ '^timestamp';
   typname   | typlen
-------------+--------
 timestamp   |      8
 timestamptz |      8
(2 rows)
```

请务必注意，`timestamptz` 值存储为UTC值。PostgreSQL不存储具有 `timestamptz` 值的任何时区数据。

#### 示例

让我们看一下使用 `timestamp` 和 `timestamptz` 来更好地理解PostgresQL如何处理它们的示例。

首先，创建一个由 `timestamp` 和 `timestamptz` 列组成的表。

```sql
CREATE TABLE timestamp_demo (ts TIMESTAMP, tstz TIMESTAMPTZ);
```

接下来，将数据库服务器的时区设置为 `America/Los_Angeles`。

```sql
SET timezone = 'America/Los_Angeles';
```

顺便说一下，您可以使用 `SHOW TIMEZONE` 命令查看当前时区：

```sql
SHOW TIMEZONE;
      TimeZone
---------------------
 America/Los_Angeles
(1 row)
```

然后，在 `timstamp_demo` 表中[插入](content/postgresql-insert.md)一个新行：

```sql
INSERT INTO timestamp_demo (ts, tstz)
VALUES
 (
 '2016-06-22 19:10:25-07',
 '2016-06-22 19:10:25-07'
 );
```

之后，查询 `timestamp` 和 `timestamptz` 列中的数据。

```sql
SELECT ts FROM timestampz_demo;
         ts
---------------------
 2016-06-22 19:10:25
(1 row)
```

它返回与插入时相同的值。

最后，将当前会话的时区更改为 `America/New_York` 并再次查询数据。

```sql
SET timezone = 'America/New_York';
```

```sql
SELECT ts, tstz FROM timestamp_demo;
         ts          |          tstz
---------------------+------------------------
 2016-06-22 19:10:25 | 2016-06-22 22:10:25-04
(1 row)
```

`timestamp` 列中的值不会更改，而 `timestamptz` 列中的值将调整为 `“America/New_York”` 的新时区。

通常最佳做法是使用 `timestamptz` 数据类型来存储时间戳数据。

#### 函数

为了有效地处理时间戳数据，PostgreSQL提供了一些方便的函数，如下所示：

##### 获取当前时间

要获取当前时间戳，请使用 `NOW()` 函数，如下所示：

```sql
SELECT NOW();
              now
-------------------------------
 2018-09-19 22:49:28.474751-04
(1 row)
```

或者您可以使用 `CURRENT_TIMESTAMP`：

```sql
SELECT CURRENT_TIMESTAMP;
```

要获取没有日期的当前时间，请使用 `CURRENT_TIME`：

```
SELECT CURRENT_TIME;
    current_time
--------------------
 22:52:18.154971-04
(1 row)
```

请注意，`CURRENT_TIMESTAMP` 和 `CURRENT_TIME` 都返回带有时区的当前时间。

要以字符串格式获取时间，请使用 `timeofday()` 函数。

```sql
SELECT TIMEOFDAY();
              timeofday
-------------------------------------
 Wed Sep 19 22:53:56.139429 2018 EDT
(1 row)
```

##### 时区转换

要将时间戳转换为另一个时区，请使用时区 `timezone(zone, timestamp) `功能。

```sql
SHOW TIMEZONE;
     TimeZone
------------------
 America/New_York
(1 row)
```

当前时区是 `America/Los_Angeles`。

要将 `2016-06-01 00:00` 转换为 `America/New_York`时区，请使用 `timezone()` 函数，如下所示：

```sql
SELECT timezone('America/New_York','2016-06-01 00:00');
      timezone
---------------------
 2016-06-01 00:00:00
(1 row)
```

请注意，我们将时间戳作为字符串传递给 `timezone()` 函数，PostgreSQL隐式地将其转换为 `timestamptz`。最好将 `timestamp` 值显式地转换为 `timestamptz` 数据类型，如下面的语句：

```sql
SELECT timezone('America/New_York','2016-06-01 00:00'::timestamptz);
```

在本教程中，我们向您介绍了PostgreSQL时间戳数据类型，并向您展示了如何使用一些有用的函数来操作时间戳值。

#### 相关教程

- [数据类型](content/postgresql-data-types.md)