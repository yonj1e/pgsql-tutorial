## CONCAT

> 摘要：在本教程中，我们将向您展示如何使用 `CONCAT` 和 `CONCAT_WS` 函数将两个或多个字符串连接成一个。

#### 简介

要将两个或多个字符串连接成一个字符串，请使用字符串连接运算符 `||` 如下例所示：

```sql
SELECT 'Concatenation' || ' ' || 'Operator' AS result_string;
     result_string
------------------------
 Concatenation Operator
(1 row)
```

以下语句将字符串与 `NULL` 值连接：

```sql
SELECT 'Concat with ' || NULL AS result_string;
```

它返回一个 `NULL` 值。

从版本9.1开始，PostgreSQL引入了一个名为 `CONCAT` 的内置字符串函数，用于将两个或多个字符串连接成一个字符串。

以下说明了 `CONCAT` 函数的语法：

```sql
CONCAT(str_1, str_2, ...)
```

`CONCAT` 函数接受参数列表。参数需要可转换为字符串。此上下文中的字符串表示以下任何[数据类型](content/postgresql-data-types.md)：char，varchar或text。

`CONCAT` 函数是可变的。这意味着 `CONCAT` 函数接受一个[数组](content/postgresql-array.md)作为参数。在这种情况下，您需要使用 `VARIADIC` 关键字标记数组。`CONCAT` 函数将每个数组元素视为参数。

如果您想了解有关 `VARIADIC` 的更多信息，请查看[函数参数教程](content/plpgsql-function-parameters.md)以获取详细信息。

与串联运算符 `||` 不同，`CONCAT` 函数忽略 `NULL` 参数。我们将在下一节中详细介绍它。

#### 示例

以下[SELECT](content/postgresql-select.md)语句使用 `CONCAT` 函数将两个字符串连接成一个：

```sql
SELECT CONCAT('CONCAT',' ', 'function');
     concat
-----------------
 CONCAT function
(1 row)
```

以下语句连接[示例数据库](content/postgresql-sample-database.md)中 `customer` 表的 `first_name` 和 `last_name` 列中的值。

```sql
SELECT CONCAT(first_name, ' ', last_name) AS "Full name"
FROM customer;
       Full name
-----------------------
 Jared Ely
 Mary Smith
 Patricia Johnson
 Linda Williams
 Barbara Jones
 Elizabeth Brown
 Jennifer Davis
 Maria Miller
 Susan Wilson
 Margaret Moore
```

在下面的示例中，我们使用 `CONCAT` 函数将字符串与 `NULL` 值连接起来。

```sql
SELECT CONCAT('Concat with ',NULL) AS result_string;
 result_string
---------------
 Concat with
(1 row)
```

如您所见，与字符串连接运算符 `||` 不同，`CONCAT` 函数忽略 `NULL` 参数。

以下语句将字符串与[LENGTH函数](content/postgresql-length-function.md)返回的数字连接起来。

```sql
SELECT first_name,
  concat ('Your first name has ', LENGTH (first_name), ' characters')
FROM customer;
 first_name  |              concat
-------------+-----------------------------------
 Jared       | Your first name has 5 characters
 Mary        | Your first name has 4 characters
 Patricia    | Your first name has 8 characters
 Linda       | Your first name has 5 characters
 Barbara     | Your first name has 7 characters
 Elizabeth   | Your first name has 9 characters
 Jennifer    | Your first name has 8 characters
 Maria       | Your first name has 5 characters
```

#### CONCAT_WS

除了 `CONCAT` 函数之外，PostgreSQL还为您提供了 `CONCAT_WS` 函数，该函数将字符串连接成一个由特定分隔符分隔的字符串。顺便说一句，WS 代表分隔符 **w**ith **s**eparator。

与 `CONCAT` 函数一样，`CONCAT_WS` 函数也是可变参数并忽略 NULL` 值。

以下说明了 `CONCAT_WS` 函数的语法。

```sql
CONCAT_WS(separator,str_1,str_2,...);
```

`separator` 是一个字符串，用于分隔结果字符串中的所有参数。

`str_1`，`str_2` 等是字符串或可以转换为字符串的任何参数。

`CONCAT_WS` 函数返回一个组合字符串，它是 `str_1`，`str_2` 等的组合，由分隔符分隔。

##### 示例

以下语句连接姓氏和名字，并用逗号和空格分隔：

```sql
SELECT concat_ws(', ', last_name, first_name) AS full_name
FROM customer
ORDER BY last_name;
       full_name
------------------------
 Abney, Rafael
 Adam, Nathaniel
 Adams, Kathleen
 Alexander, Diana
 Allard, Gordon
 Allen, Shirley
 Alvarez, Charlene
 Anderson, Lisa
 Andrew, Jose
 Andrews, Ida
```

在本教程中，我们向您展示了如何使用 ` CONCAT` 和 `CONCAT_WS` 函数将两个或多个字符串连接成一个。