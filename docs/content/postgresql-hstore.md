## hstore

> 摘要：在本教程中，我们将向您展示如何使用PostgreSQL hstore数据类型。

hstore模块实现用于将键值对存储在单个值中的hstore数据类型。在许多情况下，hstore数据类型非常有用，例如半结构化数据或具有许多很少查询的属性的行。请注意，键和值只是文本字符串。

#### 启用hstore扩展

在使用hstore数据类型之前，需要启用hstore扩展，该扩展将contrib模块加载到PostgreSQL实例。

```sql
CREATE EXTENSION hstore;
```

#### 使用hstore数据类型创建表

我们创建了一个名为 `books` 的表，其中包含三列：

- `id` 是标识该书的主键。
- `title` 为产品的标题
- `attr` 存储书籍的属性，如ISBN，重量和平装本。attr列的数据类型是hstore。

我们使用[CREATE TABLE](content/postgresql-create-table.md)语句创建 `books` 表，如下所示：

```sql
CREATE TABLE books (
 id serial primary key,
 title VARCHAR (255),
 attr hstore
);
```

#### 将数据插入hstore列

我们使用 `INSERT` 语句将数据插入到hstore列中，如下所示：

```sql
INSERT INTO books (title, attr)
VALUES
 (
 'PostgreSQL Tutorial',
 '"paperback" => "243",
    "publisher" => "postgresqltutorial.com",
    "language"  => "English",
    "ISBN-13"   => "978-1449370000",
 "weight"    => "11.2 ounces"'
 );
```

我们插入到hstore列中的数据是逗号分隔的key => value对的列表。键和值均使用双引号（“”）引用。

让我们再插一行。

```sql
INSERT INTO books (title, attr)
VALUES
 (
 'PostgreSQL Cheat Sheet',
 '
"paperback" => "5",
"publisher" => "postgresqltutorial.com",
"language"  => "English",
"ISBN-13"   => "978-1449370001",
"weight"    => "1 ounces"'
 );
```

#### 查询来自hstore列的数据

从hstore列查询数据类似于使用 `SELECT` 语句从具有本机数据类型的列查询数据，如下所示：

```sql
SELECT attr FROM books;
                                                                  attr
----------------------------------------------------------------------------------------------------------------------------------------
 "weight"=>"11.2 ounces", "ISBN-13"=>"978-1449370000", "language"=>"English", "paperback"=>"243", "publisher"=>"postgresqltutorial.com"
 "weight"=>"1 ounces", "ISBN-13"=>"978-1449370001", "language"=>"English", "paperback"=>"5", "publisher"=>"postgresqltutorial.com"
(2 rows)
```

#### 查询特定键的值

Postgresql hstore提供  `->` 运算符来查询hstore列中特定键的值。例如，如果我们想知道 `books` 表中所有可用书籍的ISBN-13，我们可以使用 `->` 运算符，如下所示：

```sql
SELECT attr -> 'ISBN-13' AS isbn FROM books;
      isbn
----------------
 978-1449370000
 978-1449370001
(2 rows)
```

#### 在WHERE子句中使用值

您可以在 `WHERE` 子句中使用 `->` 运算符来过滤hstore列的值与输入值匹配的行。例如，以下查询检索 `ISBN-13` 值与 `978-1449370000` 匹配的书籍的标题和重量：

```sql
SELECT attr -> 'weight' AS weight
FROM books
WHERE attr -> 'ISBN-13' = '978-1449370000';
   weight
-------------
 11.2 ounces
(1 row)
```

#### 将键值对添加到现有行

使用hstore列，您可以轻松地向现有行添加新的键值对，例如，您可以将免费送货密钥添加到 `books` 表的 `attr` 列，如下所示：

```sql
UPDATE books
SET attr = attr || '"freeshipping"=>"yes"' :: hstore;
```

现在，您可以检查是否已成功添加 `“freeshipping”=>“yes”` 对。

```sql
SELECT title, attr -> 'freeshipping' AS freeshipping
FROM books;
         title          | freeshipping
------------------------+--------------
 PostgreSQL Tutorial    | yes
 PostgreSQL Cheat Sheet | yes
(2 rows)
```

#### 更新现有的键值对

您可以使用 `UPDATE` 语句更新现有键值对。以下语句将 `“freeshipping”` 键的值更新为`“no”` 。

```sql
UPDATE books
SET attr = attr || '"freeshipping"=>"no"' :: hstore;
```

#### 删除现有的键值对

PostgreSQL允许您从hstore列中删除现有的键值对。例如，以下语句删除 `attr` 列中的 `“freeshipping”=>“no”` 键值对。

```sql
UPDATE books 
SET attr = delete(attr, 'freeshipping');
```

#### 检查hstore列中的特定键

您可以使用 `?` 检查hstore列中的特定键。`WHERE` 子句中的运算符。例如，以下语句返回包含密钥发布者 `attr` 的所有行。

```sql
SELECT title, attr->'publisher' as publisher, attr
FROM books
WHERE attr ? 'publisher';
         title          |       publisher        |                                                                  attr
------------------------+------------------------+----------------------------------------------------------------------------------------------------------------------------------------
 PostgreSQL Tutorial    | postgresqltutorial.com | "weight"=>"11.2 ounces", "ISBN-13"=>"978-1449370000", "language"=>"English", "paperback"=>"243", "publisher"=>"postgresqltutorial.com"
 PostgreSQL Cheat Sheet | postgresqltutorial.com | "weight"=>"1 ounces", "ISBN-13"=>"978-1449370001", "language"=>"English", "paperback"=>"5", "publisher"=>"postgresqltutorial.com"
(2 rows)
```

#### 检查键值对

您可以使用 `@>` 运算符基于hstore键值对进行查询。以下语句检索 `attr` 列包含与 `“weight”=>“11.2 ounces”` 匹配的键值对的所有行。

```sql
SELECT title
FROM books
WHERE attr @> '"weight"=>"11.2 ounces"' :: hstore;
        title
---------------------
 PostgreSQL Tutorial
(1 row)
```

#### 查询包含多个指定键的行

您可以使用 `?&`  运算符查询其hstore列包含多个键的行。例如，您可以获取其中 `attr` 列包含 `language` 和 `weight` 的书籍。

```sql
SELECT title
FROM books
WHERE attr ?& ARRAY [ 'language', 'weight' ];
         title
------------------------
 PostgreSQL Tutorial
 PostgreSQL Cheat Sheet
(2 rows)
```

要检查其hstore列是否包含键列表中的任何键的行，请使用 `?|` 运算符而不是 `?&` 运算符。

#### 从hstore列获取所有密钥

要从hstore列获取所有键，请使用 `akeys()` 函数，如下所示：

```sql
SELECT akeys (attr) FROM books;
                     akeys
-----------------------------------------------
 {weight,ISBN-13,language,paperback,publisher}
 {weight,ISBN-13,language,paperback,publisher}
(2 rows)
```

或者，如果希望PostgreSQL将结果作为集合返回，则可以使用 `skey()` 函数。

```sql
SELECT skeys (attr) FROM books;
   skeys
-----------
 weight
 ISBN-13
 language
 paperback
 publisher
 weight
 ISBN-13
 language
 paperback
 publisher
(10 rows)
```

#### 从hstore列获取所有值

与键一样，您可以使用数组形式的 `avals()` 函数从hstore列获取所有值。

```sql
SELECT avals (attr) FROM books;
                               avals
-------------------------------------------------------------------
 {"11.2 ounces",978-1449370000,English,243,postgresqltutorial.com}
 {"1 ounces",978-1449370001,English,5,postgresqltutorial.com}
(2 rows)
```

或者，如果要将结果作为集合，则可以使用 `svals()` 函数。

```sql
SELECT svals (attr) FROM books;
         svals
------------------------
 11.2 ounces
 978-1449370000
 English
 243
 postgresqltutorial.com
 1 ounces
 978-1449370001
 English
 5
 postgresqltutorial.com
(10 rows)
```

#### 将hstore数据转换为JSON

PostgreSQL提供了 `hstore_to_json()` 函数来将hstore数据转换为[JSON](content/postgresql-json.md)。请参阅以下声明：

```sql
SELECT title, hstore_to_json (attr) json
FROM books;
         title          |                                                                   json
------------------------+------------------------------------------------------------------------------------------------------------------------------------------
 PostgreSQL Tutorial    | {"weight": "11.2 ounces", "ISBN-13": "978-1449370000", "language": "English", "paperback": "243", "publisher": "postgresqltutorial.com"}
 PostgreSQL Cheat Sheet | {"weight": "1 ounces", "ISBN-13": "978-1449370001", "language": "English", "paperback": "5", "publisher": "postgresqltutorial.com"}
(2 rows)
```

#### 将hstore数据转换为集合

要将hstore数据转换为集合，可以使用 `each()` 函数，如下所示：

```sql
SELECT title, (EACH(attr) ).*
FROM books;
         title          |    key    |         value
------------------------+-----------+------------------------
 PostgreSQL Tutorial    | weight    | 11.2 ounces
 PostgreSQL Tutorial    | ISBN-13   | 978-1449370000
 PostgreSQL Tutorial    | language  | English
 PostgreSQL Tutorial    | paperback | 243
 PostgreSQL Tutorial    | publisher | postgresqltutorial.com
 PostgreSQL Cheat Sheet | weight    | 1 ounces
 PostgreSQL Cheat Sheet | ISBN-13   | 978-1449370001
 PostgreSQL Cheat Sheet | language  | English
 PostgreSQL Cheat Sheet | paperback | 5
 PostgreSQL Cheat Sheet | publisher | postgresqltutorial.com
(10 rows)
```

在本教程中，我们向您展示了如何使用PostgreSQL hstore数据类型，并向您介绍了可以针对hstore数据类型执行的最有用的操作。

#### 相关教程

- [JSON](content/postgresql-json.md)