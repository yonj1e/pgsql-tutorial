## 如何更改用户的密码

> 摘要：在本教程中，您将学习如何在PostgreSQL中更改用户的密码。

要更改[PostgreSQL用户]()的密码，请使用 `ALTER ROLE` 语句，如下所示：

```sql
ALTER ROLE username   
WITH PASSWORD 'password';
```

在此声明中，要更改用户的密码：

- 首先，指定要更改密码的用户名。
- 其次，提供用单引号（'）包装的新密码。

例如，以下语句将超级用户的密码更改为 `secret123`。

```sql
ALTER ROLE super WITH PASSWORD 'secret123';
```

有时，您希望将密码设置为直到日期和时间有效。在这种情况下，使用 `VALID UNTIL` 子句：

```sql
ALTER ROLE username
WITH PASSWORD 'new_password'
VALID UNTIL timestamp;
```

请注意，如果省略 `VALID UNTIL`子句，则密码将始终有效。

以下语句将超级用户密码的截止日期设置为2020年12月31日：

```sql
ALTER ROLE super
VALID UNTIL 'December 31, 2020';
```

要验证结果，您可以[查看用户的详细信息](content/postgresql-list-users.md)：

```sql
postgres=# \du super;
                            List of roles
 Role name |                 Attributes                  | Member of
-----------+---------------------------------------------+-----------
 super     | Superuser, Cannot login                    +| {}
           | Password valid until 2020-12-31 00:00:00+07 |
```

请注意，使用 `ALTER ROLE` 语句将以明文形式将密码传输到服务器。此外，可以在psql的命令历史记录或服务器日志中记录明文密码。

在本教程中，您学习了如何使用ALTER ROLE语句更改PostgreSQL用户的密码。