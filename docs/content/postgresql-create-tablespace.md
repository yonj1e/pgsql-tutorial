## 创建表空间

> 摘要：在本教程中，您将学习如何使用PostgreSQL CREATE TABLESPACE语句创建表空间。

#### 简介

表空间是磁盘上的一个位置，PostgreSQL存储包含数据库对象（如索引和表）的数据文件。PostgreSQL使用表空间将逻辑名映射到磁盘上的物理位置。

PostgreSQL附带两个默认表空间：

- `pg_default` 表空间存储所有用户数据。
- `pg_global` 表空间存储所有全局数据。

表空间允许您控制PostgreSQL的磁盘布局。使用表空间有两个主要优点：

- 首先，如果初始化集群的分区空间不足，则可以在其他分区上创建新的表空间并使用它直到重新配置系统。
- 其次，您可以使用数据库对象的用法统计信息来优化数据库的性能。例如，您可以将频繁访问索引或表放在执行速度非常快的设备（例如固态设备）上，并将包含很少使用的归档数据的表放在较慢的设备上。

#### 语法

要创建新的表空间，请使用 `CREATE TABLESPACE` 语句，如下所示：

```sql
CREATE TABLESPACE tablespace_name
OWNER user_name
LOCATION directory_path;
```

表空间的名称不应以 `pg_` 开头，因为这些名称是为系统表空间保留的。

默认情况下，执行 `CREATE TABLESPAC` E的用户是表空间的所有者。该语句还允许将表空间的所有权分配给 `OWNER` 子句中指定的另一个用户。

`directory_path` 是用于表空间的空目录的绝对路径。PostgreSQL系统用户必须拥有此目录才能读取和写入数据。

创建表空间后，可以在 [CREATE DATABASE](content/postgresql-create-database.md)，[CREATE TABLE](content/postgresql-create-table.md) 和 `CREATE INDEX` 语句中指定它，以在表空间中存储对象的数据文件。

#### 示例

以下语句创建一个名为dvdrental的新表空间，其物理位置为 `/work/test/tblspace`。

```sql
CREATE TABLESPACE dvdrental LOCATION '/work/test/tblspace';
```

##### 在UNIX中创建表空间

在UNIX系统中创建表空间时，即使授予表空间目录的777权限，也可能发生权限错误。要解决此问题，您需要使用 `chwon` 命令将数据目录的所有者更改为 `postgres` 用户，如下所示：

```shell
# chown postgres /work/test/tblspace
```

它将 `/work/test/tblspace` 目录的所有者更改为 `postgres` 用户。一旦 `postgres` 用户接管数据目录所有权，它将删除所有其他访问，例如700。

在本教程中，我们向您展示了如何使用PostgreSQL CREATE TABLE语句创建新的表空间。