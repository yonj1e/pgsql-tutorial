# PL/pgSQL函数重载

> 摘要：在本教程中，我们将向您介绍PL/pgSQL函数重载功能，以帮助您开发灵活的函数。

PostgreSQL允许多个函数具有相同的名称，只要参数不同即可。如果多个函数具有相同的名称，我们说这些函数是重载的。调用函数时，PostgreSQL根据输入参数确定正在调用的函数。

我们来看看下面的 `get_rental_duration()` 函数。

```sql
CREATE OR REPLACE FUNCTION get_rental_duration(p_customer_id INTEGER)
 RETURNS INTEGER AS $$
DECLARE 
 rental_duration INTEGER; 
BEGIN
 -- get the rate based on film_id
 SELECT INTO rental_duration SUM( EXTRACT( DAY FROM return_date - rental_date)) 
  FROM rental 
 WHERE customer_id=p_customer_id;
 
 RETURN rental_duration;
END; $$
LANGUAGE plpgsql;
```

`get_rental_function` 函数接受 `p_customer_id` 作为参数。它返回特定客户租用DVD的持续时间（以天为单位）的总和。例如，我们可以获得客户ID为232的客户的租期，使用 `get_rental_duration` 函数，如下所示：

```sql
SELECT get_rental_duration(232);
 get_rental_duration
---------------------
                  90
(1 row)
```

假设，我们想知道从特定日期到现在的客户的租赁期限。我们可以在 `get_retal_duration()` 函数中再添加一个参数 `p_from_date`，或者我们可以开发一个具有相同名称但具有两个参数的新函数，如下所示：

```sql
CREATE OR REPLACE FUNCTION get_rental_duration(
 p_customer_id INTEGER, 
 p_from_date DATE)
 RETURNS INTEGER AS $$
DECLARE 
 rental_duration integer;
BEGIN
 -- get the rental duration based on customer_id and rental date
 SELECT INTO rental_duration
  SUM( EXTRACT( DAY FROM return_date + '12:00:00' - rental_date)) 
 FROM rental 
 WHERE customer_id= p_customer_id AND 
  rental_date >= p_from_date;
 
 RETURN rental_duration;
END; $$
LANGUAGE plpgsql;
```

此函数与第一个函数同名，但它有两个参数。我们说 `get_rental_duration(integer)` 函数被 `get_rental_duration(integer, date)` 函数重载。
以下声明获取自2005年7月1日起客户ID为232的客户的租赁期限：

```sql
SELECT get_rental_duration(232,'2005-07-01');
 get_rental_duration
---------------------
                  85
(1 row)
```

它返回85天。请注意，如果我们忽略第二个参数，PostgreSQL将调用第一个函数。

#### PL/pgSQL函数重载和默认值

在 `get_rental_duration(integer, date)` 函数中，如果我们要为第二个参数添加默认值，如下所示：

```sql
CREATE OR REPLACE FUNCTION get_rental_duration(
 p_customer_id INTEGER, 
 p_from_date DATE DEFAULT '2005-01-01'
 )
 RETURNS INTEGER AS $$
DECLARE 
 rental_duration integer;
BEGIN
 -- get the rental duration based on customer_id and rental date
 SELECT INTO rental_duration
  SUM( EXTRACT( DAY FROM return_date + '12:00:00' - rental_date)) 
 FROM rental 
 WHERE customer_id= p_customer_id AND 
  rental_date >= p_from_date;
 
 RETURN rental_duration;
END; $$
LANGUAGE plpgsql;
```

这意味着如果我们忽略传递 `p_from_date` 参数，PostgreSQL将使用2015年1月1日作为默认值。我们来试试吧。

```sql
SELECT get_rental_duration(232);
```

哦，我们收到一条错误消息：

```sql
[Err] ERROR:  function get_rental_duration(integer) is not unique
LINE 1: SELECT get_rental_duration(232);
               ^
HINT:  Could not choose a best candidate function. You might need to add explicit type casts.
```

PostgreSQL应该使用一个参数调用第一个函数，还是使用两个参数调用第二个函数？ 它无法选择调用哪一个。

因此，当您重载现有函数时，应始终使它们的函数接口不同。现在，通过显式传递第二个参数，只能使用第二个函数。第一个不能使用，因为它混淆了调用哪个函数。

我们需要使用默认值删除 `get_rental_duration()` 函数，并再次使用两个参数重新创建 `get_rental_duration` 函数，而不指定第二个参数的默认值。

```sql
DROP FUNCTION get_rental_duration(INTEGER,DATE);
```

请注意，在删除函数时必须将参数与函数名一起指定。事实上，在PostgreSQL中，函数的完整名称包括名称和参数。

最后一个重要的注意事项是，您应该避免使用过多的重载，这使得开发人员很难通过查看代码来了解将调用哪个函数。

在本教程中，您学习了如何通过向其添加更多参数来重载现有函数。此外，如果在重载函数中使用参数的默认值，您还是了解了一些陷阱。