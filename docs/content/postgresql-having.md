## HAVING

> 摘要：在本教程中，您将学习如何使用 `HAVING` 子句来消除不满足指定条件的行组。

#### 简介

我们经常将 `HAVING` 子句与 [GROUP BY](content/postgresql-group-by.md) 子句结合使用来过滤不满足指定条件的行组。

以下语句说明了 `HAVING` 子句的典型语法：

```sql
SELECT column_1, aggregate_function (column_2)
FROM tbl_name
GROUP BY column_1
HAVING condition;
```

`HAVING` 子句设置 `GROUP BY` 子句应用后由 `GROUP BY` 子句创建的组行的条件，而 [WHERE](content/postgresql-where.md) 子句则为 `GROUP BY` 子句应用之前的各个行设置条件。这是 `HAVING` 和 `WHERE` 子句之间的主要区别。

在PostgreSQL中，您可以使用不带 `GROUP BY` 子句的 `HAVING` 子句。在这种情况下，`HAVING` 子句会将查询转换为单个组。此外，`SELECT` 列表和 `HAVING` 子句只能引用聚合函数中的列。如果 `HAVING` 子句中的条件为真，则此类查询返回单行;如果为假，则返回零行。

#### 示例

我们来看看[示例数据库](content/postgresql-sample-database.md)中的 `payment` 表。

![payment table](/imgs/payment-table.png)

##### SUM函数

以下查询使用 `GROUP BY` 子句获取每个客户的总金额：

```sql
SELECT customer_id, SUM (amount)
FROM payment
GROUP BY customer_id;
 customer_id |  sum
-------------+--------
         184 |  80.80
          87 | 137.72
         477 | 106.79
         273 | 130.72
         550 | 151.69
          51 | 123.70
         394 |  77.80
         272 |  65.87
          70 |  75.83
         190 | 102.75
```

您可以在以下查询中应用 `HAVING` 子句来选择花费超过200的客户：

```sql
SELECT customer_id, SUM (amount)
FROM payment
GROUP BY customer_id
HAVING SUM (amount) > 200;
 customer_id |  sum
-------------+--------
         526 | 208.58
         148 | 211.55
(2 rows)
```

##### COUNT函数

我们来看看 `customer` 的内容。

![customer table](/imgs/customer-table.png)

以下查询返回每个商店的客户数量：

```sql
SELECT store_id, COUNT (customer_id)
FROM customer
GROUP BY store_id;
 store_id | count
----------+-------
        1 |   326
        2 |   273
(2 rows)
```

您可以使用 `HAVING` 子句选择拥有超过300个客户的商店：

```sql
SELECT store_id, COUNT (customer_id)
FROM customer
GROUP BY store_id
HAVING COUNT (customer_id) > 300;
 store_id | count
----------+-------
        1 |   326
(1 row)
```

在本教程中，我们向您展示了如何使用 `HAVING` 子句根据指定的条件过滤行组。