## COUNT

> 摘要：在本教程中，您将学习如何使用 `COUNT` 函数来计算表中的行数。

#### 简介

`COUNT` 函数返回与查询的特定条件匹配的行数。以下语句说明了使用 `COUNT` 函数的各种方法。

```sql
SELECT COUNT(*) FROM table_name;
```

`COUNT()` 函数返回[SELECT](content/postgresql-select.md)语句返回的行数。

将 `COUNT()` 函数应用于整个表时，PostgreSQL必须按顺序扫描整个表。如果在大表上使用 `COUNT()` 函数，则查询将很慢。这与PostgreSQL MVCC实现有关。因为多个事务同时看到不同的数据状态，所以 `COUNT()` 函数没有直接的方法可以计算整个表，因此PostgreSQL必须扫描所有行。

```sql
SELECT COUNT(column) FROM table_name;
```

与 `COUNT(*)` 函数类似，`COUNT(column)` 函数返回 `SELECT` 子句返回的行数。但是，它不考虑列中的 `NULL` 值。

```sql
SELECT COUNT(DISTINCT column) FROM table;
```

在此表单中，`COUNT(DISTINCT column)` 返回列的值不为null的不同行的数量。

#### 示例

我们使用[示例数据库](content/postgresql-sample-database.md)中的 `payment` 表进行演示。

![payment table](/imgs/payment-table.png)

要获取支付表中的交易数，请使用以下查询：

```sql
SELECT COUNT (*)FROM payment;
 count
-------
 14596
(1 row)
```

##### DISTINCT

要获得客户支付的不同金额，您可以使用带有 [DISTINCT](content/postgresql-select-distinct.md) 的 `COUNT` 函数，如下所示：

```sql
SELECT COUNT(DISTINCT amount) FROM payment;
```

##### GROUP BY

要获取客户付款的数量，您可以使用 [GROUP BY](content/postgresql-group-by.md) 子句根据客户ID将付款分组，并使用 `COUNT` 函数计算每个组的付款。

以下查询说明了这个想法：

```sql
SELECT customer_id, COUNT (customer_id)
FROM payment
GROUP BY customer_id;
 customer_id | count
-------------+-------
         184 |    20
          87 |    28
         477 |    21
         273 |    28
         550 |    31
          51 |    30
         394 |    20
         272 |    13
          70 |    17
```

##### HAVING

您可以在[HAVING](content/postgresql-having.md)子句中使用 `COUNT` 函数将特定条件应用于组。例如，要返回已支付超过40笔付款的客户，请使用以下查询：

```sql
SELECT customer_id, COUNT (customer_id)
FROM payment
GROUP BY customer_id
HAVING COUNT (customer_id) > 40;
 customer_id | count
-------------+-------
         526 |    42
         148 |    45
(2 rows)
```

在本教程中，您学习了如何使用 `COUNT` 函数返回表中的行数。

#### 相关教程

- [MAX](content/postgresql-max-function.md)
- [MIN](content/postgresql-min-function.md)
- [AVG](content/postgresql-avg-function.md)
- [SUM](content/postgresql-sum-function.md)