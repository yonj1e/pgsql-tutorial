## ALTER TABLE

> 摘要：在本教程中，您将学习如何使用PostgreSQL ALTER TABLE语句修改表的结构。

#### 简介

要更改现有表结构，请使用PostgreSQL `ALTER TABLE` 语句。`ALTER TABLE` 的语法如下：

```sql
ALTER TABLE table_name action;
```

PostgreSQL提供了许多操作，允许您：

- [添加列](content/postgresql-add-column.md)，[删除列](content/postgresql-drop-column.md)，[重命名列](content/postgresql-rename-column.md)或[更改列的数据类型](content/postgresql-change-column-type.md)。
- 为列设置默认值。
- 给列添加[CHECK约束](content/postgresql-check-constraint.md)。
- [重命名表](content/postgresql-rename-table.md)。

以下说明了 `ALTER TABLE` 语句变体。

要向表中添加新列，请使用 `ALTER TBLE ADD COLUMN` 语句：

```sql
ALTER TABLE table_name ADD COLUMN new_column_name TYPE;
```

要删除现有列，请使用 `ALTER TABLE DROP COLUMN` 语句：

```sql
ALTER TABLE table_name DROP COLUMN column_name;
```

要重命名现有列，请使用 `ALTER TABLE RENAME COLUMN TO` 语句：

```sql
ALTER TABLE table_name RENAME COLUMN column_name TO new_column_name;
```

要更改列的默认值，请使用 `ALTER TABLE ALTER COLUMN SET DEFAULT` 或 `DROP DEFAULT`：

```sql
ALTER TABLE table_name ALTER COLUMN column_name [SET DEFAULT value | DROP DEFAULT]
```

要更改 `NOT NULL` 约束，请使用 `ALTER TABLE ALTER COLUMN` 语句：

```sql
ALTER TABLE table_name ALTER COLUMN column_name [SET NOT NULL| DROP NOT NULL]
```

要添加 `CHECK` 约束，请使用 `ALTER TABLE ADD CHECK` 语句：

```sql
ALTER TABLE table_name ADD CHECK expression;
```

要添加约束，请使用 `ALTER TABLE ADD CONSTRAINT` 语句：

```sql
ALTER TABLE table_name ADD CONSTRAINT constraint_name constraint_definition
```

要重命名表，请使用 `ALTER TABLE RENAME TO` 语句：

```sql
ALTER TABLE table_name RENAME TO new_table_name;
```

#### 示例

让我们创建一个名为 `link` 的新表，用于练习 `ALTER TABLE` 语句。

```sql
CREATE TABLE link (
 link_id serial PRIMARY KEY,
 title VARCHAR (512) NOT NULL,
 url VARCHAR (1024) NOT NULL UNIQUE
);
```

要添加名为 `active` 的新列，请使用以下语句：

```sql
ALTER TABLE link ADD COLUMN active boolean;
```

以下语句从 `link` 表中删除 `active` 列：

```sql
ALTER TABLE link DROP COLUMN active;
```

要将 `title`  列重命名为 `link_title`，请使用以下语句：

```sql
ALTER TABLE link RENAME COLUMN title TO link_title;
```

以下语句将一个名为 `target` 的新列添加到 `link` 表：

```sql
ALTER TABLE link ADD COLUMN target VARCHAR(10);
```

要将 `_blank` 设置为 `link` 表中 `target` 列的默认值，请使用以下语句：

```sql
ALTER TABLE link ALTER COLUMN target SET DEFAULT '_blank';
```

如果将新行插入 `link` 表而未指定目标列的值，则 `target`  列将 `_blank` 作为默认值。请参阅以下示例：

```sql
INSERT INTO link (link_title, url)
VALUES('PostgreSQL Tutorial','content/');
```

查询 `link` 表中的数据：

```sql
SELECT * FROM link;
 link_id |     link_title      |   url    | target
---------+---------------------+----------+--------
       1 | PostgreSQL Tutorial | content/ | _blank
(1 row)
```

以下语句向目标列添加 `CHECK` 约束，以便 `target` 列仅接受以下值：`_self`, `_blank`, `_parent`, 和 `_top`:

```sql
ALTER TABLE link ADD CHECK (target IN ('_self', '_blank', '_parent', '_top'));
```

如果您尝试插入违反为 `target`  列设置的 `CHECK` 约束的新行，PostgreSQL将发出错误，如以下示例所示：

```sql
INSERT INTO link(link_title,url,target) VALUES('PostgreSQL','http://www.postgresql.org/','whatever');
ERROR:  duplicate key value violates unique constraint "link_url_key"
DETAIL:  Key (url)=(http://www.postgresql.org/) already exists.
```

要将 `link` 表重命名为 `url`，请使用以下语句：

```sql
ALTER TABLE link RENAME TO url;
```

在本教程中，我们向您展示了如何使用PostgreSQL `ALTER TABLE` 语句来更改现有表的结构。

#### 相关教程

- [ADD COLUMN](content/postgresql-add-column.md)
- [DROP COLUMN](content/postgresql-drop-column.md)