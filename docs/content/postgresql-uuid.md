## UUID类型

> 摘要：在本教程中，您将了解PostgreSQL UUID数据类型以及如何使用提供的模块生成UUID值。

#### 简介

UUID代表[RFC 4122](https://tools.ietf.org/html/rfc4122)和其他相关标准定义的通用唯一标识符。UUID值是由算法生成的128位的数，使用相同的算法使其在已知宇宙中唯一。以下显示了UUID值的一些示例：

```
40e6215d-b5c6-4896-987c-f30f3678f608
6ecd8c99-4036-403d-bf84-cf8400f67836
3f333df6-90a4-4fda-8dd3-9485d27cee36
```

如您所见，UUID是一个由十六进制数字组成的32位数字序列，以连字符分隔。

由于其唯一性功能，您经常在分布式系统中找到UUID，因为它比仅在单个数据库中生成唯一值的[SERIAL](content/postgresql-serial.md)数据类型具有更好的唯一性。

要在PostgreSQL数据库中存储UUID值，请使用UUID数据类型。

#### 生成UUID值

PostgreSQL允许您存储和比较UUID值，但它不包括在其核心中生成UUID值的函数。

相反，它依赖于提供特定算法来生成UUID的第三方模块。例如，`uuid-ossp` 模块提供了一些方便的功能，这些功能实现了用于生成UUID的标准算法。

要安装 `uuid-ossp` 模块，请使用 `CREATE EXTENSION` 语句，如下所示：

```sql
CREATE EXTENSION IF NOT EXISTS "uuid-ossp";
```

`IF NOT EXISTS` 子句允许您避免重新安装模块。

要根据计算机的MAC地址，当前[时间戳](content/postgresql-timestamp.md)和[随机值](content/postgresql-random-range.md)的组合生成UUID值，请使用 `uuid_generate_v1()` 函数：

```sql
SELECT uuid_generate_v1();
           uuid_generate_v1
--------------------------------------
 beb213e2-bd41-11e8-ac1b-ebc20b7bfe7a
(1 row)
```

如果要仅基于随机数生成UUID值，可以使用 `uuid_generate_v4()` 函数。例如：

```sql
SELECT uuid_generate_v4();
           uuid_generate_v4
--------------------------------------
 8a734e59-664a-4067-84fb-befb68f76204
(1 row)
```

有关UUID生成功能的更多信息，请查看[uuid-ossp](https://www.postgresql.org/docs/current/static/uuid-ossp.html)模块文档。

#### 使用UUID字段

我们将创建一个[主键](content/postgresql-primary-key.md)为UUID数据类型的表。此外，将使用 `uuid_generate_v4()` 函数自动生成主键列的值。

首先，使用以下语句创建 `contacts` 表：

```sql
CREATE TABLE contacts (
    contact_id uuid DEFAULT uuid_generate_v4 (),
    first_name VARCHAR NOT NULL,
    last_name VARCHAR NOT NULL,
    email VARCHAR NOT NULL,
    phone VARCHAR,
    PRIMARY KEY (contact_id)
);
```

在此语句中，`contact_id` 列的数据类型是 `UUID`。`contact_id` 列具有 `uuid_generate_v4()` 函数提供的默认值，因此，无论何时插入新行而未指定 `contact_id` 列的值，PostgreSQL都将调用 `uuid_generate_v4()` 函数为其生成值。

其次，将一些数据[插入](content/postgresql-insert.md)联系人表：

```sql
INSERT INTO contacts (
    first_name,
    last_name,
    email,
    phone
)
VALUES
    (
        'John',
        'Smith',
        'john.smith@example.com',
        '408-237-2345'
    ),
    (
        'Jane',
        'Smith',
        'jane.smith@example.com',
        '408-237-2344'
    ),
    (
        'Alex',
        'Smith',
        'alex.smith@example.com',
        '408-237-2343'
    );
```

第三，使用以下[SELECT](content/postgresql-select.md)语句查询 `contacts` 表中的所有行：

```sql
SELECT * FROM contacts;
              contact_id              | first_name | last_name |         email          |    phone
--------------------------------------+------------+-----------+------------------------+--------------
 df54a1e8-4f9c-4257-a8eb-acf8d2845cf8 | John       | Smith     | john.smith@example.com | 408-237-2345
 c6441f68-8a23-4157-95ee-4cd7836f68f2 | Jane       | Smith     | jane.smith@example.com | 408-237-2344
 e9c3ea3d-5c01-44ab-ae6f-1dbc26075146 | Alex       | Smith     | alex.smith@example.com | 408-237-2343
(3 rows)
```

如您所见，`contact_id` 列已由 `uuid_generate_v4()` 函数生成的UUID值填充。

在本教程中，您学习了如何使用PostgreSQL UUID数据类型以及如何使用 `uuid-ossp` 模块生成UUID值。