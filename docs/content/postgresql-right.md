## RIGHT

`RIGHT()` 函数返回字符串中的最后 `n` 个字符。

#### 语法

以下显示了 `RIGHT()` 函数的语法：

```sql
RIGHT(string, n)
```

#### 参数

`RIGHT()` 函数需要两个参数：

**1) string**

一个字符串。

**2) n**

一个正整数，指定应返回的字符串中最右边的字符数。

如果 `n` 为负数，则 `RIGHT()` 函数返回除了 `|n|` （绝对值）个字符外字符串中的所有字符。

#### 返回值

`RIGHT()` 函数返回字符串中的最后 `n` 个字符。

#### 示例

我们来看一些使用 `RIGHT()` 函数的例子。

以下语句获取字符串 `'XYZ'` 中的最后一个字符：

```sql
SELECT RIGHT('XYZ', 1);
 right
-------
 Z
(1 row)
```

要获取最后两个字符，请将值 `2` 作为第二个参数传递，如下所示：

```sql
SELECT RIGHT('XYZ', 2);
 right
-------
 YZ
(1 row)
```

以下语句说明如何使用负整数作为第二个参数：

```sql
SELECT RIGHT('XYZ', - 1);
 right
-------
 YZ
(1 row)
```

在此示例中， `RIGHT()` 函数返回除第一个字符之外的所有字符。

请参阅[示例数据库](content/postgresql-sample-database.md)中的以下 `customer` 表：

![customer table](/imgs/customer-table.png)

以下语句在 [WHERE](content/postgresql-where.md) 子句中使用 `RIGHT()` 函数来获取姓氏以 `'son'`结尾的所有客户：

```sql
SELECT last_name
FROM customer
WHERE RIGHT(last_name, 3) = 'son';
  last_name
-------------
 Johnson
 Wilson
 Anderson
 Jackson
 Thompson
 Robinson
 Nelson
 Richardson
 Peterson
 Watson
```

#### 备注

如果要返回最左边的 `n` 个字符，请查看 `LEFT()` 函数以获取更多详细信息。

在本教程中，您已经学习了如何使用 `RIGHT()` 函数来获取字符串中最右边的 `n` 个字符。