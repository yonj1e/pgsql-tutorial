## CREATE DATABASE

> 摘要：在本教程中，您将学习如何使用PostgreSQL CREATE DATABASE语句创建具有各种选项的新数据库。

#### 简介

要创建新的PostgreSQL数据库，请使用 `CREATE DATABASE` 语句，如下所示：

```sql
CREATE DATABASE db_name
 OWNER =  role_name
 TEMPLATE = template
 ENCODING = encoding
 LC_COLLATE = collate
 LC_CTYPE = ctype
 TABLESPACE = tablespace_name
 CONNECTION LIMIT = max_concurrent_connection
```

创建新数据库时，`CREATE DATABASE` 语句为您提供了各种选项。让我们更详细地研究这些选项：

- db_name：是要创建的新数据库的名称。PostgreSQL数据库服务器中的数据库名称必须是唯一的。如果您尝试创建与现有数据库同名的新数据库，PostgreSQL将发出错误。
- role_name：将拥有新数据库的用户的角色名称。PostgreSQL使用用户的角色名称执行 `CREATE DATABASE` 语句作为默认角色名称。
- template：是新数据库创建的数据库模板的名称。PostgreSQL允许您基于模板数据库创建数据库。template1是默认模板数据库。
- encoding：指定新数据库的字符集编码。默认情况下，它是模板数据库的编码。
- collate：指定新数据库的排序规则。排序规则指定影响[SELECT](content/postgresql-select.md)语句中[ORDER BY](content/postgresql-order-by.md)子句结果的字符串的排序顺序。如果未在 `LC_COLLATE` 参数中明确指定，则模板数据库的排序规则是新数据库的默认排序规则。
- ctype：指定新数据库的字符分类。`ctype` 会影响分类，例如，数字，下和上。默认值是模板数据库的字符分类。
- tablespace_name：指定新数据库的[表空间]()名称。默认值是模板数据库的表空间。
- max_concurrent_connection：指定新数据库的最大并发连接数。默认值为-1，即无限制。此功能在共享主机环境中非常有用，您可以在其中配置特定数据库的最大并发连接数。

除了 `CREATE DATABASE` 语句之外，您还可以使用 `createdb` 程序创建新数据库。`createdb` 程序在内部也是使用 `CREATE DATABASE` 语句。

#### 示例

创建新数据库的最简单方法是使用所有默认设置，仅指定数据库名称，如以下查询：

```sql
CREATE DATABASE testdb1;
```

PostgreSQL创建了一个名为 `testdb1` 的新数据库，它具有默认模板数据库中的默认参数，即 `template1`。

以下语句使用以下参数创建新数据库名称 `hrdb`：

- 编码：utf-8。
- 所有者：`hr`，假设数据库服务器中存在 `hr` 用户。
- 最大并发连接数：25。

```sql
CREATE DATABASE hrdb
 WITH ENCODING='UTF8'
 OWNER=hr
 CONNECTION LIMIT=25;
```

在本教程中，您学习了如何使用PostgreSQL `CREATE DATABASE` 语句创建新数据库。