## 日期类型

> 摘要：本教程讨论了PostgreSQL DATE数据类型，并向您展示了如何使用一些方便的日期函数来处理日期值。

#### 简介

要存储日期值，请使用PostgreSQL DATE [数据类型](content/postgresql-data-types.md)。PostgreSQL使用4个字节来存储日期值。`DATE` 数据类型的最低和最高值是4713 BC和5874897 AD。

存储日期值时，PostgreSQL使用 `yyyy-mm-dd` 格式，例如2000-12-31。它还使用此格式将数据[插入](content/postgresql-insert.md)日期列。

如果创建一个具有 `DATE` 列的表，并且希望使用当前日期作为列的默认值，则可以在 `DEFAULT` 关键字后使用`CURRENT_DATE`。

例如，以下语句创建的 `document` 表具有带有DATE数据类型的 `posting_date` 列。`posting_date` 列接受当前日期作为默认值。

```sql
CREATE TABLE documents (
 document_id serial PRIMARY KEY,
 header_text VARCHAR (255) NOT NULL,
 posting_date DATE NOT NULL DEFAULT CURRENT_DATE
);
 
INSERT INTO documents (header_text)
VALUES
 ('Billing to customer XYZ');
 
SELECT
 *
FROM
 documents;
```

以下显示了上面查询的输出。请注意，您可能会根据数据库服务器的当前日期获得不同的发布日期值。

```sql
 document_id |       header_text       | posting_date
-------------+-------------------------+--------------
           1 | Billing to customer XYZ | 2016-06-23
(1 row)
```

#### 日期函数

对于演示，我们将创建一个新的 `employees` 表，其中包含 `employee_id`，`first_name`，`last_name`，`birth_date` 和 `hire_date` 列，其中 `birth_date` 和 `hire_date` 列的数据类型为 `DATE`。

用于创建 `employees` 表的SQL脚本

##### 获取当前日期

要获取当前日期和时间，请使用内置的 `NOW()` 函数。但是，要仅获取日期部分（没有时间部分），可以使用双冒号 `::` 将 `DATETIME` 值转换为 `DATE` 值。

以下语句返回数据库服务器的当前日期：

```sql
SELECT NOW()::date;
```

获取当前日期的另一种方法是使用 `CURRENT_DATE`，如下所示：

```sql
SELECT CURRENT_DATE;
```

结果格式为：`yyyy-mm-dd`。但是，您可以输出各种格式的日期值。

##### 以特定格式输出日期值

要以特定格式输出日期值，请使用 `TO_CHAR()` 函数。`TO_CHAR()` 函数接受两个参数。第一个参数是您要格式化的值，第二个参数是定义输出格式的模板。

例如，要以 `dd/mm/yyyy` 格式显示当前日期，请使用以下语句：

```sql
SELECT TO_CHAR(NOW() :: DATE, 'dd/mm/yyyy');
  to_char
------------
 23/06/2016
(1 row)
```

或者以 `Jun 22, 2016` 的格式显示日期，您可以使用以下语句：

```sql
SELECT TO_CHAR(NOW() :: DATE, 'Mon dd, yyyy');
   to_char
--------------
 Jun 23, 2016
(1 row)
```

##### 获取两个日期之间的间隔

要获取两个日期之间的[间隔](content/postgresql-interval.md)，请使用减号（ - ）运算符。例如，要获取员工的服务日期，您可以使用从今天的日期中减去 `hire_date` 列中的值作为以下查询：

```sql
SELECT
 first_name,
 last_name,
 now() - hire_date as diff
FROM employees;
 first_name | last_name |           diff
------------+-----------+---------------------------
 Shannon    | Freeman   | 4191 days 08:25:30.634458
 Sheila     | Wells     | 4922 days 08:25:30.634458
 Ethel      | Webb      | 5652 days 08:25:30.634458
(3 rows)
```

##### 计算年龄(以年、月和天为单位)

要计算当前日期（年，月和日）的年龄，请使用 `AGE()` 函数。以下语句使用 `AGE()` 函数计算 `employees` 表中员工的年龄。

```sql
SELECT
 employee_id,
 first_name,
 last_name,
 AGE(birth_date)
FROM employees;
 employee_id | first_name | last_name |           age
-------------+------------+-----------+-------------------------
           1 | Shannon    | Freeman   | 36 years 5 mons 22 days
           2 | Sheila     | Wells     | 38 years 4 mons 18 days
           3 | Ethel      | Webb      | 41 years 5 mons 22 days
(3 rows)
```

如果将日期值传递给 `AGE()` 函数，它将从当前日期中减去该日期值。如果将两个参数传递给 `AGE()` 函数，它将从第一个参数中减去第二个参数。

例如，要在2015年1月1日获得员工年龄，请使用以下语句：

```sql
SELECT
 employee_id,
 first_name,
 last_name,
 age('2015-01-01', birth_date)
FROM
 employees;
 employee_id | first_name | last_name |           age
-------------+------------+-----------+--------------------------
           1 | Shannon    | Freeman   | 35 years
           2 | Sheila     | Wells     | 36 years 10 mons 24 days
           3 | Ethel      | Webb      | 40 years
(3 rows)
```

##### 从日期值中截取年，季度，月，周，日

要从日期值的日期值中获取年，季度，月，周，日，可以使用 `EXTRACT()` 函数。以下语句将员工的出生日期提取为年，月和日：

```sql
SELECT
 employee_id,
 first_name,
 last_name,
 EXTRACT (YEAR FROM birth_date) AS YEAR,
 EXTRACT (MONTH FROM birth_date) AS MONTH,
 EXTRACT (DAY FROM birth_date) AS DAY
FROM
 employees;
 employee_id | first_name | last_name | year | month | day
-------------+------------+-----------+------+-------+-----
           1 | Shannon    | Freeman   | 1980 |     1 |   1
           2 | Sheila     | Wells     | 1978 |     2 |   5
           3 | Ethel      | Webb      | 1975 |     1 |   1
(3 rows)
```

在本教程中，您已经了解了PostgreSQL DATE数据类型以及一些处理日期值的便捷函数。