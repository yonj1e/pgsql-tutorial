## LEFT JOIN

> 摘要：在本教程中，您将学习如何使用 `LEFT JOIN` 子句从多个表中查找数据。

#### 简介

假设我们有两个表：`A` 和 `B`。

![A and B tables](/imgs/A-and-B-tables1.png)

`B` 表中的数据通过 `fka` 字段与 `A` 表中的数据相关联。

`A` 表中的每一行可以在 `B` 表中具有零个或多个对应的行。`B` 表中的每一行在 `A` 表中只有一行且只有一行。

如果要从 `A` 表中查找 `B` 表中具有相应行的行，请使用 [INNER JOIN](content/postgresql-inner-join.md) 子句。

如果要从 `A` 表中查找在 `B` 表中可能有也可能没有相应行的行，可以使用 `LEFT JOIN` 子句。如果 `B` 表中没有匹配的行，则 `B` 表中的列值将被 `NULL` 值替换。

以下语句演示将 `A` 表连接到 `B` 表的 `LEFT JOIN` 语法：

```sql
SELECT A.pka, A.c1, B.pkb, B.c2
FROM ALEFT JOIN B ON A .pka = B.fka;
```

要将 `A` 表连接到 `B` 表，您需要：

- 在 `SELECT`子句中指定要从中选择数据的列。
- 在 `FROM` 子句中指定左表，即要获取所有行的表。
- 在 `LEFT JOIN` 子句中指定右表，即 `B` 表。另外，指定连接两个表的条件。

`LEFT JOIN` 子句返回左表（`A`）中与右表（`B`）中的行组合的所有行，即使右表（`B`）中没有对应的行。

`LEFT JOIN` 也称为 `LEFT OUTER JOIN`。

以下维恩图说明了 `LEFT JOIN` 子句的工作原理。交集是 `A` 表在 `B` 表中具有相应的行。

![PostgreSQL LEFT JOIN Venn Diagram](/imgs/PostgreSQL-LEFT-JOIN-Venn-Diagram.png)

#### 示例

我们来看看下面的ER图，它是[DVD租赁示例数据库](content/postgresql-sample-database.md)的一部分。

![Film and Inventory tables](/imgs/film-and-inventory-tables.png)

`film` 表中的每一行在 `inventory` 表中可能具有零行或多行。`inventory` 表中的每一行在 `film` 表中只有一行。

您可以使用 `LEFT JOIN` 子句将 `film` 表连接到 `inventory` 表，如下所示：

```sql
SELECT film.film_id, film.title, inventory_id
FROM film
LEFT JOIN inventory ON inventory.film_id = film.film_id;
dvdrental-# LEFT JOIN inventory ON inventory.film_id = film.film_id;
 film_id |            title            | inventory_id
---------+-----------------------------+--------------
       1 | Academy Dinosaur            |            1
       1 | Academy Dinosaur            |            2
       1 | Academy Dinosaur            |            3
       1 | Academy Dinosaur            |            4
       1 | Academy Dinosaur            |            5
       1 | Academy Dinosaur            |            6
       1 | Academy Dinosaur            |            7
```

由于影片表中的某些行在库存表中没有对应的行，因此库存ID的值为 `NULL`。

您可以添加 `WHERE` 子句以仅选择不在库存中的电影作为以下查询：

```sql
SELECT film.film_id, film.title, inventory_id
FROM film
LEFT JOIN inventory ON inventory.film_id = film.film_id
WHERE inventory.film_id IS NULL;
 film_id |         title          | inventory_id
---------+------------------------+--------------
      14 | Alice Fantasia         |
      33 | Apollo Teen            |
      36 | Argonauts Town         |
      38 | Ark Ridgemont          |
      41 | Arsenic Independence   |
      87 | Boondock Ballroom      |
     108 | Butch Panther          |
```

当您希望在一个表中选择与另一个表不匹配的数据时，此技术非常有用。

在本教程中，我们向您展示了如何使用 `LEFT JOIN` 子句从一个表中选择在另一个表中可能有或没有相应行的行。

#### 相关教程

- [NATURAL JOIN](http://www.postgresqltutorial.com/postgresql-natural-join/)
- [Cross Join](http://www.postgresqltutorial.com/postgresql-cross-join/)
- [FULL OUTER JOIN](http://www.postgresqltutorial.com/postgresql-full-outer-join/)