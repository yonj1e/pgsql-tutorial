## IN

> 摘要：在本教程中，您将学习如何在 `WHERE` 子句中使用 `IN` 运算符来检查值列表。

#### 语法

您可以在 `WHERE` 子句中使用 `IN` 运算符来检查值是否与值列表中的任何值匹配。 

`IN` 运算符的语法如下：

```sql
value IN (value1,value2,...)
```

如果值与列表中的任何值匹配，即 `value1` 和 `value2` ，则表达式返回 `true`。 值列表可以是数字或字符串的列表，也可以是 `SELECT` 语句的结果集，如以下查询所示：

```sql
value IN (SELECT value FROM tbl_name);
```

括号内的语句称为[子查询](content/postgresql-subquery.md)，它是嵌套在另一个查询中的查询。

#### 示例

假设您想知道客户ID 1和2的租赁信息，您可以在 `WHERE` 子句中使用 `IN` 运算符，如下所示：

```sql
SELECT customer_id, rental_id, return_date
FROM rental
WHERE customer_id IN (1, 2)
ORDER BY return_date DESC;
 customer_id | rental_id |     return_date
-------------+-----------+---------------------
           2 |     15145 | 2005-08-31 15:51:04
           1 |     15315 | 2005-08-30 01:51:46
           2 |     14743 | 2005-08-29 00:18:56
           1 |     15298 | 2005-08-28 22:49:37
           2 |     14475 | 2005-08-27 08:59:32
           1 |     14825 | 2005-08-27 07:01:57
```

您可以使用等号（=）和 `OR` 运算符重写上面的查询，如下所示：

```sql
SELECT rental_id, customer_id, return_date
FROM rental
WHERE customer_id = 1 OR customer_id = 2
ORDER BY return_date DESC;
```

使用 `IN` 运算符的查询比使用等号（=）和 `OR` 运算符的查询更短，更易读。 此外，PostgreSQL使用 `IN` 运算符执行查询的速度比使用 `OR` 运算符查询快得多。

#### NOT IN

您可以将 `IN` 运算符与 `NOT` 运算符组合以选择其值与列表中的值不匹配的行。 

以下声明查找客户ID不是1或2的所有租赁。

```sql
SELECT customer_id, rental_id, return_date
FROM rental
WHERE customer_id NOT IN (1, 2);
 customer_id | rental_id |     return_date
-------------+-----------+---------------------
         459 |         2 | 2005-05-28 19:40:33
         408 |         3 | 2005-06-01 22:12:39
         333 |         4 | 2005-06-03 01:43:41
         222 |         5 | 2005-06-02 04:33:21
         549 |         6 | 2005-05-27 01:32:07
         269 |         7 | 2005-05-29 20:34:53
         239 |         8 | 2005-05-27 23:33:46
         126 |         9 | 2005-05-28 00:22:40
```

您还可以使用不等于（<>）和 `AND` 运算符重写 `NOT IN` 运算符，如下所示：

```sql
SELECT customer_id, rental_id, return_date
FROM rental
WHERE customer_id <> 1 AND customer_id <> 2;
```

查询返回与使用 `NOT IN` 运算符输出相同。

#### 子查询

以下查询返回2005-05-27归还租赁DVD客户的客户id列表：

```sql
SELECT customer_id
FROM rental
WHERE CAST (return_date AS DATE) = '2005-05-27';
 customer_id
-------------
         549
         239
         575
         185
         350
          37
         272
```

您可以使用客户ID列表作为 `IN` 运算符的输入，如下所示：

```sql
SELECT first_name, last_name
FROM customer
WHERE customer_id IN ( 
    SELECT customer_id 
    FROM rental 
    WHERE CAST (return_date AS DATE) = '2005-05-27' 
);
 first_name |  last_name
------------+-------------
 Pamela     | Baker
 Frances    | Parker
 Ann        | Evans
 Rose       | Howard
 Beverly    | Brooks
 Tammy      | Sanders
```

有关子查询的更多信息，请查看[子查询](content/postgresql-subquery.md)教程。

在本教程中，我们向您展示了如何使用 `IN` 运算符来匹配值列表。