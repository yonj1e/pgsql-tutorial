## CURRENT_DATE

`CURRENT_DATE` 函数返回当前日期。

#### 语法

`CURRENT_DATE` 函数非常简单，不需要参数如下：

```sql
CURRENT_DATE
```

#### 返回值

`CURRENT_DATE` 函数返回表示当前日期的 `DATE` 值。

#### 示例

以下示例显示如何使用 `CURRENT_DATE` 函数获取当前日期：

```sql
SELECT CURRENT_DATE;
```

输出是 `DATE` 值，如下所示：

```sql
2017-08-15
```

您可以使用 `CURRENT_DATE` 函数作为列的默认值。请考虑以下示例。

首先，创建一个名为 `delivery` 的表以供演示：

```sql
CREATE TABLE delivery(
    delivery_id serial PRIMARY KEY,
    product varchar(255) NOT NULL,
    delivery_date DATE DEFAULT CURRENT_DATE
);
```

在 `delivery` 表中，我们有 `delivery_date`，其默认值是 `CURRENT_DATE` 函数的结果。

其次，在 `delivery` 表中[插入](content/postgresql-insert.md)一个新行：

```sql
INSERT INTO delivery(product)
VALUES('Sample screen protector');
```

在这个 `INSERT` 语句中，我们没有指定交付日期，因此，PostgreSQL使用当前日期作为默认值。

第三，使用以下查询验证是否已使用当前日期成功插入行：

```sql
SELECT * FROM delivery;
 delivery_id |         product         | delivery_date
-------------+-------------------------+---------------
           1 | Sample screen protector | 2018-10-06
(1 row)
```

如您所见，当前日期已插入到 `delivery_date` 列中。

注意，您可能会在 `delivery_date` 列中看到不同的值，具体取决于您执行查询的日期。

在本教程中，您学习了如何使用 `CURRENT_DATE` 函数来获取当前日期。