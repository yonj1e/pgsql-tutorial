## 删除重复行

> 摘要：在本教程中，您将学习如何使用各种技术删除PostgreSQL中的重复行。

#### 准备样本数据

首先，创建一个名为 `basket` 的新表来存储水果：

```sql
CREATE TABLE basket(    
    id SERIAL PRIMARY KEY,    
    fruit VARCHAR(50) NOT NULL
);
```

其次，将一些水果[插入](content/postgresql-insert.md) `basket` 表。

```sql
INSERT INTO basket(fruit) values('apple');
INSERT INTO basket(fruit) values('apple'); 
INSERT INTO basket(fruit) values('orange');
INSERT INTO basket(fruit) values('orange');
INSERT INTO basket(fruit) values('orange'); 
INSERT INTO basket(fruit) values('banana');
```

第三，从 `basket` 表中[查询数据](content/postgresql-select.md)：

```sql
SELECT id, fruit FROM basket;
 id | fruit
----+--------
  1 | apple
  2 | apple
  3 | orange
  4 | orange
  5 | orange
  6 | banana
(6 rows)
```

如您所见，我们在 `basket` 表中有一些重复的行，例如2个苹果和3个橙子。

#### 查找重复的行

如果表中有几行，您可以看到哪些行立即重复。然而，大表并非如此。

找到重复的行，您使用以下语句：

```sql
SELECT fruit, COUNT( fruit )
FROM basket
GROUP BY fruit
HAVING COUNT(fruit)> 1
ORDER BY fruit;
 fruit  | count
--------+-------
 apple  |     2
 orange |     3
(2 rows)
```

#### 使用DELETE USING语句删除重复的行

以下语句使用 `DELETE USING` 语句删除重复的行：

```sql
DELETE FROM basket a        
USING basket b
WHERE a.id < b.id AND a.fruit = b.fruit;
```

在这个例子中，我们将 `basket` 表连接到自身，并检查两个不同的行（a.id <b.id）在 `fruit` 列中是否具有相同的值。

让我们再次查询 `basket` 表以验证是否删除了重复的行：

```sql
SELECT id, fruit FROM basket;
 id | fruit
----+--------
  2 | apple
  5 | orange
  6 | banana
(3 rows)
```

如您所见，该语句删除了具有最低ID的重复行，并保留具有最高ID的行。

如果要保留具有最低id的重复行，则只需要在 `WHERE` 子句中翻转运算符：

```sql
DELETE FROM basket a        
USING basket b
WHERE a.id > b.id AND a.fruit = b.fruit;
```

要检查语句是否正常工作，让我们验证 `basket` 表中的数据：

```sql
SELECT id, fruit FROM basket;
 id | fruit
----+--------
  1 | apple
  3 | orange
  6 | banana
(3 rows)
```

完善！ 保留具有最低ID的重复行。

#### 使用子查询删除重复的行

以下语句使用子查询删除重复行并保留具有最低id的行。

```sql
DELETE FROM basket
WHERE id IN 
	(SELECT id FROM (
        SELECT id, 
        ROW_NUMBER() OVER( PARTITION BY fruit 
        ORDER BY id ) AS row_num 
        FROM basket ) t 
    WHERE t.row_num > 1 );
```

在此示例中，子查询返回重复行，但重复组中的第一行除外。外部 `DELETE` 语句删除了子查询返回的重复行。

如果要保留具有最高id的重复行，只需更改子查询中的顺序：

```sql
DELETE FROM basket
WHERE id IN    
	(SELECT id    
 	FROM         
 		(SELECT id,         
  		ROW_NUMBER() OVER( PARTITION BY fruit        
		ORDER BY  id DESC ) AS row_num        
		FROM basket ) t        
	WHERE t.row_num > 1 );
```

如果您想根据多列的值删除重复，这里是查询模板：

```sql
DELETE FROM table_nameWHERE id IN    
	(SELECT id    
     FROM         
     	(SELECT id,         
         ROW_NUMBER() OVER( PARTITION BY column_1, column_2        
         ORDER BY  id ) AS row_num        
         FROM table_name ) t        
     WHERE t.row_num > 1 );
```

在这种情况下，该语句将删除 `column_1` 和 `column_2` 列中具有重复值的所有行。

#### 使用立即表删除重复行

要使用直接表删除行，请使用以下步骤：

1. [创建一个新表](content/postgresql-create-table.md)，其结构与应删除其重复行的结构相同。
2. 将源表中的不同行[插入](content/postgresql-select-distinct.md)到直接表中。
3. [删除](content/postgresql-drop-table.md)源表。
4. 将立即表[重命名](content/postgresql-rename-table.md)为源表的名称。

以下说明了从 `basket` 表中删除重复行的步骤：

```sql
-- step 1
CREATE TABLE basket_temp (LIKE basket); 

-- step 2
INSERT INTO basket_temp(fruit, id)
	SELECT DISTINCT ON (fruit) fruit, id
	FROM basket;  
	
-- step 3
DROP TABLE basket; 

-- step 4
ALTER TABLE basket_temp RENAME TO basket;
```

在本教程中，您学习了如何使用 `DELETE USING` 语句，子查询和直接表技术删除PostgreSQL中的重复行。