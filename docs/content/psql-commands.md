## psql实用命令

> 摘要：在本教程中，我们为您提供了一个常用psql命令列表，可帮助您更快，更有效地从PostgreSQL数据库服务器查询数据。

#### 连接到PostgreSQL数据库

以下命令连接到特定用户下的数据库。按Enter后PostgreSQL会询问用户的密码。

```sql
psql -d database -U  user -W
```

例如，要连接到 `postgres` 用户下的 `dvdrental` 数据库，请使用以下命令：

```
psql -d dvdrental -U postgres -W
Password for user postgres:
dvdrental=#
```

如果要连接到在其他主机上的数据库，请按如下方式添加 `-h` 选项：

```
psql -h host -d database -U user -W
```

如果要使用SSL模式进行连接，只需在命令中将其指定为以下命令：

```
psql -U user -h host "dbname=db sslmode=require"
```

#### 切换到新数据库的连接

连接到数据库后，可以在用户指定的用户下将连接切换到新数据库。之前的连接将被关闭。如果省略 `user` 参数，则假定为当前用户。

```
\c dbname username
```

以下命令连接到 `postgres` 用户下的 `dvdrental` 数据库：

```
postgres=# \c dvdrental
You are now connected to database "dvdrental" as user "postgres".
dvdrental=#
```

#### 列出数据库

要列出当前PostgreSQL数据库服务器中的所有数据库，请使用 `\l` 命令：

```
\l
```

#### 列出表

要列出当前数据库中的所有表，请使用 `\dt` 命令：

```
\dt
```

请注意，此命令仅显示当前连接数据库中的表。

#### 描述表

要一张表的描述，如列，类型，列的修饰符等，请使用以下命令：

```
\d table_name
```

#### 列出模式

要列出当前连接的数据库的所有模式，请使用 `\dn` 命令。

```
\dn
```

#### 列出函数

要列出当前数据库中的可用功能，请使用 `\df` 命令。

```
\df
```

#### 列出视图

要列出当前数据库中的可用视图，请使用 `\dv` 命令。

```
\dv
```

#### 列出用户及其角色

要列出所有用户及其分配角色，请使用 `\du` 命令：

```
\du
```

#### 执行上一个命令

要检索当前版本的PostgreSQL服务器，请使用 `version()` 函数，如下所示：

```
SELECT version();
```

现在，您希望节省再次键入上一个命令的时间，您可以使用 `\g` 命令执行上一个命令：

```
\g
```

psql再次执行上一个命令，即[SELECT语句](content/postgresql-select.md)。

#### 历史命令

要显示命令历史记录，请使用 `\s` 命令。

```
\s
```

如果要将命令历史记录保存到文件，则需要在 `\s`  命令后面指定文件名，如下所示：

```
\s filename
```

#### 执行文件中psql命令

如果要执行文件中的psql命令，请使用 `\i` 命令，如下所示：

```
\i filename
```

#### 获取有关psql命令的帮助

要了解所有可用的psql命令，请使用 `\?`  命令。

```
\?
```

要获取有关特定PostgreSQL语句的帮助，请使用 `\h` 命令。

例如，如果要了解有关[ALTER TABLE](content/postgresql-alter-table.md)语句的详细信息，请使用以下命令：

```
\h ALTER TABLE
```

#### 打开查询执行时间

要打开查询执行时间，请使用 `\timing` 命令。

```
dvdrental=# \timing
Timing is on.
dvdrental=# select count(*) from film;
 count
-------
  1000
(1 row)
 
Time: 1.495 ms
dvdrental=#
```

您使用相同的命令 `\timing` 将其关闭。

```
dvdrental=# \timing
Timing is off.
dvdrental=#
```

#### 在编辑器中编辑命令

如果您喜欢在编辑器中键入命令，这非常方便。要在psql中执行此操作，请使用 `\e` 命令。发出命令后，psql将打开由EDITOR环境变量定义的文本编辑器，并将您在psql中输入的最新命令放入编辑器中。

![](imgs/postgresql-psql-command-e.jpg)

在编辑器中键入命令，保存并关闭编辑器后，psql将执行命令并返回结果。

![](imgs/postgresql-psql-command-e-rel.jpg)

在编辑器中编辑函数时更有用。

```
\ef [function name]
```

![psql commadn ef edit function](imgs/postgresql-psql-command-ef.jpg)

#### 切换输出选项

psql支持某些类型的输出格式，并允许您自定义输出格式化的方式。

- `\a` 命令从对齐切换到非对齐列输出。
- `\H` 命令将输出格式化为HTML格式。

#### 退出psql

要退出psql，请使用 `\q` 命令并按 `Enter` 键退出psql。

```
\q
```

在本教程中，我们向您展示了如何使用psql命令执行各种常用任务。

#### 相关教程

- [Describe Table](content/postgresql-describe-table.md)
- [List Users](content/postgresql-list-users.md)