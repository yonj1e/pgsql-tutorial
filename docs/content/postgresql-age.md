## AGE

> 摘要：本教程向您展示如何使用 `age()` 函数计算年龄。

#### 简介

我们通常必须计算业务应用程序的年龄，例如人员年龄，员工服务年限等。在PostgreSQL中，您可以使用 `age()` 函数来完成这些任务。

以下说明了 `age()` 函数的语法：

```sql
age(timestamp,timestamp);
```

`age()` 函数接受两个 [TIMESTAMP](content/postgresql-timestamp.md) 值。它从第一个参数中减去第二个参数，并返回一个[时间间隔](content/postgresql-interval.md)作为结果。

请参阅以下示例：

```sql
SELECT age('2017-01-01','2011-06-24');
          age
-----------------------
 5 years 6 mons 7 days
(1 row)
```

如果要将当前日期作为第一个参数，可以使用以下形式的 `age()` 函数：

```sql
age(timestamp);
```

例如，如果出生日期为 `2000-01-01` 且当前日期为 `2017-03-20` 的人，年龄将为：

```sql
SELECT current_date, AGE(timestamp '2000-01-01');
    date    |           age
------------+-------------------------
 2017-03-20 | 17 years 2 mons 19 days
(1 row)
```

#### 示例

请参阅[示例数据库](content/postgresql-sample-database.md)中的以下 `rental` 表：

![PostgreSQL age Function: Rental Table Sample](/imgs/rental-table.png)

假设您希望获得租赁持续时间最长的前10名，您可以使用 `age()` 函数计算它，如下所示：

```sql
SELECT rental_id, customer_id, 
  age(return_date, rental_date) AS duration
FROM rental
WHERE return_date IS NOT NULL
ORDER BY duration DESC 
LIMIT 10;
```

在此示例中，使用 `age()` 函数根据 `rental_date` 和 `return_date` 列的值计算租赁期限。以下显示输出：

```sql
 rental_id | customer_id |    duration
-----------+-------------+-----------------
      2412 |         127 | 9 days 05:59:00
     14678 |         383 | 9 days 05:59:00
     13947 |         218 | 9 days 05:58:00
     14468 |         224 | 9 days 05:58:00
      7874 |          86 | 9 days 05:58:00
     11629 |         299 | 9 days 05:58:00
      5738 |         187 | 9 days 05:56:00
      9938 |          63 | 9 days 05:56:00
     12159 |         106 | 9 days 05:55:00
      3873 |         394 | 9 days 05:55:00
(10 rows)
```

在本教程中，您学习了如何使用 `age()` 函数来计算年龄。
