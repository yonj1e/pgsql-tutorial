## 重置密码

> 摘要：在本教程中，我们将逐步向您展示如何在PostgreSQL中重置忘记的 `postgres` 用户密码。

出于某些原因，在安装PostgreSQL之后，您可能会忘记 `postgres` 用户的密码。在这种情况下，您需要知道如何重置密码。

PostgreSQL使用存储在数据库数据目录中的 `pg_hba.conf` 配置文件来控制客户端身份验证。HBA表示基于主机的身份验证。要重置 `postgres` 用户的密码，您需要修改此配置文件中的一些参数。

步骤1. 通过将 `pg_dba.conf` 文件复制到其他位置来备份 `pg_dba.conf` 文件，或者将其重命名为 `pg_dba_bk.conf`

步骤2. 通过添加以下行作为注释行后面的第一行来编辑 `pg_dba.conf` 文件。注释行以 `＃` 符号开头。

```
local  all   all   trust
```

如果您的PostgreSQL安装不支持local（表示UNIX套接字），例如，如果您在Windows操作系统上安装PostgreSQL。如果在这种情况下使用local，则无法启动PostgreSQL服务。在这种情况下，您需要在 `pg_hba.conf `文件中使用以下条目：

```
host    all              postgres   	     127.0.0.1/32            trust
```

此步骤确保您无需使用密码即可登录PostgreSQL数据库服务器。

步骤3. 重新启动PostgreSQL服务器，例如，在Linux中，使用以下命令：

```
sudo /etc/init.d/postgresql restart
```

步骤4. [连接到PostgreSQL数据库服务器](content/connect-to-postgresql-database.md)并更改 `postgres` 用户的密码。

```sql
ALTER USER postgres with password 'very_secure_password';
```

步骤5. 恢复 `pg_db.conf` 文件并重新启动服务器，并使用新密码连接到PostgreSQL数据库服务器。

```shell
sudo /etc/init.d/postgresql restart
```

在本教程中，我们向您展示了如何重置忘记的 `postgres` 用户密码。