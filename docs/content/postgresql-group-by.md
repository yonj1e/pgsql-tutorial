## GROUP BY

> 摘要：在本教程中，您将学习如何使用 `GROUP BY` 子句将行划分为组。

#### 简介

`GROUP BY` 子句将[SELECT](content/postgresql-select.md)语句返回的行划分为组。对于每个组，您可以应用聚合函数（例如，[SUM](content/postgresql-sum-function.md)）来计算项目总和，或者使用[COUNT](content/postgresql-count-function.md)来获取组中的项目数。

以下语句说明了 `GROUP BY` 子句的语法：

```sql
SELECT column_1, aggregate_function(column_2)
FROM tbl_name
GROUP BY column_1;
```

`GROUP BY` 子句必须出现在 `FROM` 或 `WHERE` 子句之后。随后紧跟 `GROUP BY` 子句是一列或逗号分隔列的列表。您还可以在 `GROUP BY`子句中放置表达式。

#### 示例

我们来看看[示例数据库](content/postgresql-sample-database.md)中的 `payment` 表。

![payment table](http://www.postgresqltutorial.com/wp-content/uploads/2013/05/payment-table.png)

##### 无聚合函数

您可以使用 `GROUP BY` 子句而不应用聚合函数。以下查询从支付表中获取数据，并按客户ID对结果进行分组。

```sql
SELECT customer_id
FROM payment
GROUP BY customer_id;
 customer_id
-------------
         184
          87
         477
         273
         550
          51
         394
         272
          70
```

在这种情况下，`GROUP BY` 的作用类似于 `DISTINCT` 子句，该子句从结果集中删除重复的行。

##### SUM函数

当 `GROUP BY` 子句与聚合函数一起使用时，它是有用的。例如，要获得客户支付的金额，您可以使用 `GROUP BY` 子句将支付表分成组; 对于每个组，您使用 `SUM` 函数作为以下查询计算总金额：

```sql
SELECT customer_id, SUM (amount)
FROM payment
GROUP BY customer_id;
 customer_id |  sum
-------------+--------
         184 |  80.80
          87 | 137.72
         477 | 106.79
         273 | 130.72
         550 | 151.69
          51 | 123.70
         394 |  77.80
         272 |  65.87
          70 |  75.83
```

`GROUP BY` 子句按客户ID对结果集进行排序，并将属于同一客户的金额相加。每当 `customer_id` 发生更改时，它都会将行添加到返回的结果集中。

您可以使用 `GROUP BY` 子句和 [ORDER BY](content/postgresql-order-by.md) 子句对组进行排序：

```sql
SELECT customer_id, SUM (amount)
FROM payment
GROUP BY customer_id
ORDER BY SUM (amount) DESC;
 customer_id |  sum
-------------+--------
         148 | 211.55
         526 | 208.58
         178 | 194.61
         137 | 191.62
         144 | 189.60
         459 | 183.63
         181 | 167.67
         410 | 167.62
         236 | 166.61
```

##### COUNT函数

要计算每个员工正在处理的交易数量，您可以根据员工ID对支付表进行分组，并使用 `COUNT` 函数获取交易数量，如下所示：

```sql
SELECT staff_id, COUNT (payment_id)
FROM payment
GROUP BY staff_id;
 staff_id | count
----------+-------
        1 |  7292
        2 |  7304
(2 rows)
```

`GROUP BY` 子句按职员ID对结果集进行排序。它保持行的总计，并且只要员工ID发生更改，它就会将行添加到返回的结果集中。

要过滤组，请使用[HAVING](content/postgresql-having.md)子句而不是[WHERE](content/postgresql-where.md)子句。

在本教程中，我们向您展示了如何使用 `GROUP BY` 子句将结果集划分为组。