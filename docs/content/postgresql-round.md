## ROUND

`ROUND()` 函数将数值四舍五入为其最接近的[整数](content/postgresql-integer.md)或带小数位数的数字。

#### 语法

以下说明了 `ROUND()` 函数的语法：

```sql
ROUND (source [ , n ] )
```

#### 参数

`ROUND()` 函数接受2个参数：

**1) source**

`source` 参数是要舍入的数字或数字表达式。

**2) n**

`n` 参数是一个整数，用于确定舍入后的小数位数。

`n` 参数是可选的。如果省略 `n` 参数，则其默认值为0。

#### 返回值

如果省略第二个参数，`ROUND()` 函数将返回一个类型与输入相同的结果。

如果您使用两个参数，`ROUND()` 函数将返回一个数值。

#### 示例

##### A) 舍入到整数

以下示例显示如何使用 `ROUND()` 函数舍入小数：

```sql
SELECT ROUND(10.4);
 round
-------
    10
(1 row)
```

因为10.4的最接近整数是10，所以函数按预期返回10。

以下示例舍入10.5：

```sql
SELECT ROUND(10.5);
 round
-------
    11
(1 row)
```

##### B) 舍入到小数点后两位

以下示例说明如何舍入到2个小数位：

```sql
SELECT ROUND(10.812, 2);
 round
-------
 10.81
(1 row)
```

另一个将小数舍入为2位小数的示例：

```sql
SELECT ROUND(10.817, 2);
 round
-------
 10.82
(1 row)
```

您可以更改第二个参数以将数字四舍五入到特定的小数位。

##### C) 从表中取四舍五入的数据

我们将使用[示例数据库](content/postgresql-sample-database.md)中的以下 `payment` 和 `customer` 表进行演示。

![customer and payment tables](/imgs/customer-and-payment-tables.png)

以下声明检索每位客户已支付的平均租金。

```sql
SELECT
    first_name,
    last_name,
    ROUND( AVG( amount ), 2 ) avg_rental
FROM payment
INNER JOIN customer USING(customer_id)
GROUP BY customer_id
ORDER BY avg_rental DESC;
 first_name  |  last_name   | avg_rental
-------------+--------------+------------
 Brittany    | Riley        |       5.62
 Kevin       | Schuler      |       5.52
 Ruth        | Martinez     |       5.49
 Linda       | Williams     |       5.45
 Paul        | Trout        |       5.39
 Daniel      | Cabral       |       5.30
 Lonnie      | Tirado       |       5.30
 Milton      | Howland      |       5.29
 Lena        | Jensen       |       5.16
 Mae         | Fletcher     |       5.13
 Miriam      | Mckinney     |       5.12
```

在本声明中，我们使用 `ROUND()` 函数将平均租赁费用舍入到小数点后两位。

以下语句计算每个客户的平均租金数量。

```sql
WITH rental(customer_id,rent) AS
(
    SELECT customer_id, COUNT( rental_id )
    FROM payment
    GROUP BY customer_id
)
SELECT ROUND(AVG(rent)) 
FROM rental;
 round
-------
    24
(1 row)
```

在此示例中，我们使用 `ROUND()` 函数将结果舍入为整数。

在本教程中，您学习了如何使用 `ROUND()` 函数将数字舍入到其最接近的整数或指定的小数位数。