## FLOOR

`FLOOR()` 函数返回一个向下舍入到下一个整数的数字。

#### 语法

`FLOOR()` 函数的语法如下：

```sql
FLOOR(numeric_expression)
```

#### 参数

`FLOOR()` 函数需要一个参数：

**1) numeric_expression**

numeric_expression是向下舍入的数字（或计算结果为数字的表达式）。

#### 返回值

`FLOOR()` 函数返回一个数据类型与输入参数相同的值。

#### 示例

以下示例显示如何使用 `FLOOR()` 函数将数字向下舍入为最接近的整数：

```sql
SELECT FLOOR( 150.75 );
 floor
-------
   150
(1 row)
```

请参阅[示例数据](content/postgresql-sample-database.md)库中的以下 `payment` 表：

![payment table](imgs/payment-table.png)

以下声明返回客户支付的金额：

```sql
SELECT
 customer_id,
 FLOOR(SUM( amount )) amount_paid
FROM payment
GROUP BY customer_id
ORDER BY amount_paid DESC;
 customer_id | amount_paid
-------------+-------------
         148 |         211
         526 |         208
         178 |         194
         137 |         191
         144 |         189
         459 |         183
         410 |         167
         181 |         167
         236 |         166
         403 |         162
         522 |         161
```

#### 备注

要将数字舍入到最接近的整数，请使用 `CEIL()` 函数。

在本教程中，您学习了如何使用 `FLOOR()` 函数将数字向下舍入到最接近的整数，该整数小于或等于数字。