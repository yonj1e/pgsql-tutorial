## INSERT

> 摘要：在本教程中，您将学习如何使用PostgreSQL INSERT语句将新行插入表中。

[创建表](content/postgresql-create-table.md)时，它没有任何数据。您经常做的第一件事是在表中插入新行。PostgreSQL提供 `INSERT` 语句，允许您一次将一行或多行插入表中。

#### 语法

以下说明了 `INSERT` 语句的语法：

```sql
INSERT INTO table(column1, column2, …)
VALUES (value1, value2, …);
```

首先，在 `INSERT INTO` 子句之后指定要插入新行的表的名称，后跟逗号分隔的列列表。

其次，在 `VALUES` 子句后列出逗号分隔值列表。值列表必须与表名后指定的列列表的顺序相同。

要一次向表中添加多行，请使用以下语法：

```sql
INSERT INTO table (column1, column2, …)
VALUES
 (value1, value2, …),
 (value1, value2, …) ,...;
```

您只需在第一个列表后添加其他逗号分隔值列表，列表中的每个值都用逗号（，）分隔。

要插入来自另一个表的数据，请使用 `INSERT INTO SELECT` 语句，如下所示：

```sql
INSERT INTO table(column1,column2,...)
SELECT column1,column2,...
FROM another_table
WHERE condition;
```

[WHERE](content/postgresql-where.md)子句用于过滤允许您将 `another_tablein` 中的部分数据插入表中的行。

#### 示例

让我们为演示创建一个名为 `link` 的新表。

```sql
CREATE TABLE link (
    ID serial PRIMARY KEY,
    url VARCHAR (255) NOT NULL,
    name VARCHAR (255) NOT NULL,
    description VARCHAR (255),
    rel VARCHAR (50)
);
```

您将在后面的教程中学习如何创建一个新表，只需执行该语句即可。

##### 插入一行

以下语句在 `link` 表中插入一个新行：

```sql
INSERT INTO link (url, name)
VALUES
 ('http://www.postgresqltutorial.com','PostgreSQL Tutorial');
```

要插入[字符数据](content/postgresql-char-varchar-text.md)，必须将其括在单引号（'）中，例如'PostgreSQL Tutorial'。对于[数字数据类型](content/postgresql-numeric.md)，您不需要这样做，只需使用普通数字，如1,2,3。

如果省略任何在 `INSERT` 语句中接受 `NULL` 值的列，则该列将采用其默认值。如果没有为列设置默认值，该列将采用 `NULL` 值。

PostgreSQL自动为[自增列](content/postgresql-serial.md)提供一个值，因此您不应该也不应该在自增列中插入值。

您可以使用 `SELECT` 语句验证插入的行：

```sql
SELECT * FROM link;
 id |                url                |        name         | description | rel
----+-----------------------------------+---------------------+-------------+-----
  1 | http://www.postgresqltutorial.com | PostgreSQL Tutorial |             |
(1 row)
```

如果要插入包含单引号字符的字符串（如 `O'Reilly Media`），则必须使用单引号（'）转义字符，如以下查询所示：

```sql
INSERT INTO link (url, name)
VALUES
 ('http://www.oreilly.com','O''Reilly Media');
 
SELECT * FROM link;
  id |                url                |        name         | description | rel
----+-----------------------------------+---------------------+-------------+-----
  1 | http://www.postgresqltutorial.com | PostgreSQL Tutorial |             |
  2 | http://www.oreilly.com            | O'Reilly Media      |             |
(2 rows)
```

##### 插入多行

以下语句一次在 `link` 表中插入多行：

```sql
INSERT INTO link (url, name)
VALUES
 ('http://www.google.com','Google'),
 ('http://www.yahoo.com','Yahoo'),
 ('http://www.bing.com','Bing');
 
SELECT * FROM link;
 id |                url                |        name         | description | rel
----+-----------------------------------+---------------------+-------------+-----
  1 | http://www.postgresqltutorial.com | PostgreSQL Tutorial |             |
  2 | http://www.oreilly.com            | O'Reilly Media      |             |
  3 | http://www.google.com             | Google              |             |
  4 | http://www.yahoo.com              | Yahoo               |             |
  5 | http://www.bing.com               | Bing                |             |
(5 rows)
```

##### 插入日期

让我们在 `link` 表中添加一个名为 `last_updatein` 的新列，并将其默认值设置为 `CURRENT_DATE`。

```sql
ALTER TABLE link ADD COLUMN last_update DATE;
 
ALTER TABLE link ALTER COLUMN last_update
SET DEFAULT CURRENT_DATE;
```

以下语句将具有指定日期的新行插入到 `link` 表中。日期格式为 `YYYY-MM-DD`。

```sql
INSERT INTO link (url, name, last_update)
VALUES
 ('http://www.facebook.com','Facebook','2013-06-01');
 
SELECT * FROM link;
 id |                url                |        name         | description | rel | last_update
----+-----------------------------------+---------------------+-------------+-----+-------------
  1 | http://www.postgresqltutorial.com | PostgreSQL Tutorial |             |     |
  2 | http://www.oreilly.com            | O'Reilly Media      |             |     |
  3 | http://www.google.com             | Google              |             |     |
  4 | http://www.yahoo.com              | Yahoo               |             |     |
  5 | http://www.bing.com               | Bing                |             |     |
  6 | http://www.facebook.com           | Facebook            |             |     | 2013-06-01
(6 rows)
```

您还可以使用 `DEFAULT` 关键字来设置日期列或具有默认值的任何列的默认值。

```sql
INSERT INTO link (url, name, last_update)
VALUES
 ('https://www.tumblr.com/','Tumblr',DEFAULT);
 
SELECT * FROM link;
 id |                url                |        name         | description | rel | last_update
----+-----------------------------------+---------------------+-------------+-----+-------------
  1 | http://www.postgresqltutorial.com | PostgreSQL Tutorial |             |     |
  2 | http://www.oreilly.com            | O'Reilly Media      |             |     |
  3 | http://www.google.com             | Google              |             |     |
  4 | http://www.yahoo.com              | Yahoo               |             |     |
  5 | http://www.bing.com               | Bing                |             |     |
  6 | http://www.facebook.com           | Facebook            |             |     | 2013-06-01
  7 | https://www.tumblr.com/           | Tumblr              |             |     | 2018-09-14
(7 rows)
```

##### 从其他表插入日期

首先，创建另一个名为 `link_tmp`的表，其结构与 `link` 表相同：

```sql
CREATE TABLE link_tmp (LIKE link);
```

其次，从 `link` 表中插入日期列的值不为NULL的行：

```sql
INSERT INTO link_tmp 
    SELECT *
    FROM link
    WHERE last_update IS NOT NULL;
```

第三，通过查询 `link_tmp` 表中的数据来验证插入操作：

```sql
SELECT * FROM link_tmp;
 id |           url           |   name   | description | rel | last_update
----+-------------------------+----------+-------------+-----+-------------
  6 | http://www.facebook.com | Facebook |             |     | 2013-06-01
  7 | https://www.tumblr.com/ | Tumblr   |             |     | 2018-09-14
(2 rows)
```

##### 获取最后插入ID

要在插入新行后从表中获取最后一个插入ID，请使用 `INSERT` 语句中的 `RETURNING` 子句。这是PostgreSQL对SQL的扩展。

以下语句将新行插入到 `link` 表中并返回最后一个插入ID：

```sql
INSERT INTO link (url, NAME, last_update)
VALUES('http://www.postgresql.org','PostgreSQL',DEFAULT) 
RETURNING id;
 id
----
  8
(1 row)

INSERT 0 1
```

id与 `lind` 表中的最后一个插入ID匹配：

```sql
SELECT * FROM link;
 id |                url                |        name         | description | rel | last_update
----+-----------------------------------+---------------------+-------------+-----+-------------
  1 | http://www.postgresqltutorial.com | PostgreSQL Tutorial |             |     |
  2 | http://www.oreilly.com            | O'Reilly Media      |             |     |
  3 | http://www.google.com             | Google              |             |     |
  4 | http://www.yahoo.com              | Yahoo               |             |     |
  5 | http://www.bing.com               | Bing                |             |     |
  6 | http://www.facebook.com           | Facebook            |             |     | 2013-06-01
  7 | https://www.tumblr.com/           | Tumblr              |             |     | 2018-09-14
  8 | http://www.postgresql.org         | PostgreSQL          |             |     | 2018-09-14
(8 rows)
```

在本教程中，您学习了如何使用PostgreSQL INSERT语句将新行插入表中。

#### 相关教程

- [Upsert](http://www.postgresqltutorial.com/postgresql-upsert/)