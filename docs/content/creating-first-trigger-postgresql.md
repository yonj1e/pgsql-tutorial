## 创建触发器

> 摘要：在本教程中，我们将逐步向您展示如何使用 `CREATE TRIGGER` 语句创建第一个触发器。

要在PostgreSQL中创建新的触发器，您需要：

- 使用 `CREATE FUNCTION` 语句创建触发器函数。
- 使用 `CREATE TRIGGER` 语句将此触发器函数绑定到表。

如果您不熟悉创建用户定义的函数，可以在[PostgreSQL存储过程]()部分查看它。

#### 创建触发器函数

触发器函数类似于普通函数，它不接受任何参数并且返回值类型为 `trigger`，如下：

```sql
CREATE FUNCTION trigger_function() RETURN trigger AS
```

请注意，您可以使用PostgreSQL支持的任何语言创建触发器函数。在本教程中，我们将使用PL/pgSQL进行演示。

触发器函数通过一个名为TriggerData的特殊结构接收有关其调用环境的数据，该结构包含一组局部变量。例如， `OLD` 和 `NEW` 表示触发事件之前或之后表中行的状态。PostgreSQL提供以 `TG_` 开头的其他局部变量作为前缀，如 `TG_WHEN`，`TG_TABLE_NAME` 等。

定义触发器函数后，您可以将其绑定到表上的特定操作。

#### 创建触发器

要创建新触发器，请使用 `CREATE TRIGGER` 语句。`CREATE TRIGGER` 的完整语法很复杂，有很多选项。但是，为了演示，我们将使用 `CREATE TRIGGER` 语法的简单形式，如下所示：

```sql
CREATE TRIGGER trigger_name {BEFORE | AFTER | INSTEAD OF} {event [OR ...]}   
ON table_name   
[FOR [EACH] {ROW | STATEMENT}]   
EXECUTE PROCEDURE trigger_function
```

该事件可以是 `INSERT`，`UPDATE`，`DELETE` 或 `TRUNCATE`。您可以定义在事件之前（`BEFORE`）或之后（`AFTER`）触发触发器。`INSTEAD OF` 仅用于视图上的 `INSERT`，`UPDATE` 或 `DELETE`。

正如[触发器简介](content/introduction-postgresql-trigger.md)中所提到的，PostgreSQL提供了两种触发器：行级触发器和语句级触发器，可以由 `FOR EACH ROW`（行级触发器）或 `FOR EACH STATEMENT`（语句级触发器）指定。

#### 示例

我们来看一个创建新触发器的示例。在此示例中，我们将创建一个新表 `employees`，如下所示：

```sql
CREATE TABLE employees(   
    id serial primary key,   
    first_name varchar(40) NOT NULL,   
    last_name varchar(40) NOT NULL
);
```

每当员工的姓氏发生变化时，我们都会通过触发器将其记录到名为 `employee_audits` 的单独表中。

```sql
CREATE TABLE employee_audits (   
    id serial primary key,   
    employee_id int4 NOT NULL,   
    last_name varchar(40) NOT NULL,   
    changed_on timestamp(6) NOT NULL
);
```

首先，我们定义一个名为 `log_last_name_changes` 的新函数：

```sql
CREATE OR REPLACE FUNCTION log_last_name_changes()  
RETURNS trigger AS
$BODY$
BEGIN 
  IF NEW.last_name <> OLD.last_name THEN 
    INSERT INTO employee_audits(employee_id,last_name,changed_on)
      VALUES(OLD.id,OLD.last_name,now()); 
  END IF;  
  RETURN NEW;
END;
$BODY$
LANGUAGE plpgsql;
```

该函数检查员工的姓氏是否更改，它会将旧姓氏插入 `employee_audits` 表，包括员工ID，姓氏和更改时间。

其次，我们将触发器函数绑定到 `employees` 表。触发器名称为 `last_name_changes`。在更新 `last_name` 列的值之前，会自动调用触发器函数以记录更改。

```sql
CREATE TRIGGER last_name_changes  
BEFORE UPDATE  
ON employees  
FOR EACH ROW  
EXECUTE PROCEDURE log_last_name_changes();
```

第三，我们可以[插入](content/postgresql-insert.md)一些样本数据进行测试。我们在 `employees` 表中插入两行。

```sql
INSERT INTO employees (first_name, last_name)VALUES ('John', 'Doe'); 
INSERT INTO employees (first_name, last_name)VALUES ('Lily', 'Bush');
```

我们可以查看一下员工表：

```sql
SELECT * FROM employees;
```

假设 Lily Bush 结婚，她需要将她的姓改为 Lily Brown。我们可以将其更新为以下查询：

```sql
UPDATE employees
SET last_name = 'Brown'
WHERE ID = 2;
```

我们查看Lily的姓氏是否已更新。

```sql
SELECT * FROM employees;
 id | first_name | last_name
----+------------+-----------
  1 | John       | Doe
  2 | Lily       | Brown
(2 rows)
```

如您所见，Lily的姓氏已更新。我们来检查一下 `employee_audits` 表：

```sql
SELECT * FROM employee_audits;
 id | employee_id | last_name |         changed_on
----+-------------+-----------+----------------------------
  1 |           2 | Bush      | 2018-10-08 10:56:56.458743
(1 row)
```

如您所见，触发器将更改记录到 `employee_audits` 表中。

在本教程中，我们向您展示了一个创建PostgreSQL触发器以审核更改的简单示例。