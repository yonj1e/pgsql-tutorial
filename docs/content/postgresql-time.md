## 时间类型

> 摘要：在本教程中，我们将向您介绍PostgreSQL TIME数据类型，并向您展示一些处理时间值的便捷函数。

#### 简介

PostgreSQL提供 `TIME` 数据类型，允许您存储时间值。

以下语句说明了如何使用 `TIME` 数据类型声明列：

```sql
column_name TIME(precision);
```

时间值可以具有最多6位数的精度。精度指定放置在第二个字段中的小数位数。

`TIME` 数据类型需要8个字节，其允许范围为 `00:00:00` 到 `24:00:00`。以下说明了 `TIME` 值的常见格式：

```
HH:MM   
HH:MM:SS
HHMMSS
```

例如：

```
01:02
01:02:03
010203
```

如果要使用精度，可以使用以下格式：

```
MM:SS.pppppp    
HH:MM:SS.pppppp
HHMMSS.pppppp
```

在这种形式中，`p` 是精度。例如：

```
04:59.999999
04:05:06.777777
040506.777777
```

PostgreSQL实际上接受几乎任何合理的 `TIME` 格式，包括SQL兼容，`ISO 8601` 等。

#### 示例

我们经常将 `TIME` 数据类型用于存储时间的列，例如事件或班次的时间。请考虑以下示例。

首先，使用以下 `CREATE TABLE` 语句创建一个名为 `shift` 的新表：

```sql
CREATE TABLE shifts (
    id serial PRIMARY KEY,
    shift_name VARCHAR NOT NULL,
    start_at TIME NOT NULL,
    end_at TIME NOT NULL
);  
```

其次，在 `shift` 表中[插入](content/postgresql-insert.md)一些行：

```sql
INSERT INTO shifts(shift_name, start_at, end_at)
VALUES('Morning', '08:00:00', '12:00:00'),
      ('Afternoon', '13:00:00', '17:00:00'),
      ('Night', '18:00:00', '22:00:00');
```

第三，从 `shifts` 表中查询数据：

```sql
SELECT * FROM shifts;
 id | shift_name | start_at |  end_at
----+------------+----------+----------
  1 | Morning    | 08:00:00 | 12:00:00
  2 | Afternoon  | 13:00:00 | 17:00:00
  3 | Night      | 18:00:00 | 22:00:00
(3 rows)
```

#### TIME with time zone类型

除了 `TIME` 数据类型，PostgreSQL还提供 `TIME with time zone` 类型，允许您存储和操作带时区的时间。

以下语句说明如何声明数据类型为 `TIME with time zone` 的列：

```sql
column TIME with time zone
```

`TIME with time zone` 类型的存储大小为12个字节，允许您存储时区值，时区范围为 `00：00：00 + 1459` 到 `24：00：00-1459`。

以下说明 `TIME with time zone`：

```
04:05:06 PST    
04:05:06.789-8  
```

#### 函数

##### 获取当前时间

要获取带有时区的当前时间，请使用 `CURRENT_TIME` 函数，如下所示：

```sql
SELECT CURRENT_TIME;
 
timetz
--------------------
 00:51:02.746572-08
(1 row) 
```

要以特定精度获取当前时间，请使用 `CURRENT_TIME(precision)` 函数：

```sql
 SELECT CURREN_TIME(5);
 
      timetz
-------------------
 00:52:12.19515-08
(1 row)
```

请注意，如果不指定精度， `CURRENT_TIME` 函数将返回具有完整可用精度的时间值。

要获取本地时间，请使用 `LOCALTIME` 函数：

```sql
SELECT LOCALTIME;
 
      time
-----------------
 00:52:40.227186
(1 row)
```

同样，要获得具有特定精度的本地时间，请使用 `LOCALTIME(precision)` 函数：

```sql
 SELECT localtime(0);
 
   time
----------
 00:56:08
(1 row)  
```

##### 将时间转换为不同的时区

要将时间转换为其他时区，请使用以下格式：

```sql
[TIME with time zone] AT TIME ZONE time_zone
```

例如，要将本地时间转换为 `UTC-7` 时区的时间，请使用以下语句：

```sql
SELECT LOCALTIME AT TIME ZONE 'UTC-7';
 
      timezone
--------------------
 16:02:38.902271+07
(1 row)
```

##### 从时间值中截取小时，分钟，秒

要从时间值中提取小时，分钟，秒，请使用 ·EXTRACT· 函数，如下所示：

```sql
EXTRACT(field FROM time_value);
```

该字段可以是 hour, minute, second, milliseconds，如以下示例所示：

```sql
SELECT
    LOCALTIME,
    EXTRACT (HOUR FROM LOCALTIME) as hour,
    EXTRACT (MINUTE FROM LOCALTIME) as minute, 
    EXTRACT (SECOND FROM LOCALTIME) as second,
    EXTRACT (milliseconds FROM LOCALTIME) as milliseconds;
    localtime    | hour | minute |  second   | milliseconds
-----------------+------+--------+-----------+--------------
 17:35:54.631561 |   17 |     35 | 54.631561 |    54631.561
(1 row)
```

##### 对时间值的算术运算

PostgreSQL允许您在时间值和时间间隔值之间应用算术运算符，如+， - 和*。

以下语句返回两个时间值之间的[间隔](content/postgresql-interval.md)：

```sql
SELECT time '10:00' - time '02:00';
 
 ?column?
----------
 08:00:00
(1 row)
```

以下语句将当地时间增加2小时：

```sql
 SELECT LOCALTIME + interval '2 hours';
 
    ?column?
-----------------
 03:16:18.020418
(1 row)
```

在该示例中，时间值和间隔值之和是时间值。

在本教程中，您了解了PostgreSQL TIME数据类型以及如何使用与时间相关的函数处理时间值。