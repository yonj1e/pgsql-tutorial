## 数据类型

> 摘要：在本教程中，您将了解PostgreSQL数据类型，包括布尔值，字符，数字，时间，数组，json，uuid和特殊类型。

 ![PostgreSQL Data Types](/imgs/PostgreSQL-Data-Types-300x254.png)

#### 概述

PostgreSQL支持以下数据类型：

- [布尔类型](content/postgresql-boolean.md)
- [字符类型](content/postgresql-boolean.md)，如char，varchar和text。
- 数字类型，例如整数和浮点数。
- 时间类型，如 [date](content/postgresql-date.md), [time](content/postgresql-time.md), [timestamp](content/postgresql-timestamp.md), 和 [interval](content/postgresql-interval.md) 。
- [UUID](content/postgresql-uuid.md)，用于存储通用唯一标识符。
- [数组](content/postgresql-array.md)，用于存储字符串，数字等。
- [JSON](content/postgresql-json.md)，存储JSON数据
- [hstore](content/postgresql-hstore.md)，存储键值对
- 特殊类型，如网络地址和几何数据。

#### 布尔类型

[布尔](content/postgresql-hstore.md)数据类型可以包含三个可能值之一：true，false或null。使用 `boolean` 或 `bool` 关键字声明具有布尔数据类型的列。

当您将数据[插入](content/postgresql-insert.md)布尔列时，PostgreSQL将其转换为布尔值，例如，1，yes，y，t，true转换为true，0，no，n false，f转换为false。

当您从布尔列中[选择](content/postgresql-select.md)数据时，PostgreSQL会将值转换回来，例如，将 `t` 转换为true，将 `f` 转换为false，将空转换为null。

#### 字符类型

PostgreSQL提供三种[字符数据类型](content/postgresql-char-varchar-text.md)：`CHAR(n)`, `VARCHAR(n)`, 和 `TEXT`

- `CHAR(n)` 是带有空格填充的固定长度字符。如果插入的字符串短于列的长度，PostgreSQL会填充空格。如果插入的字符串长度超过列的长度，PostgreSQL将发出错误。
- `VARCHAR(n)` 是可变长度字符串。使用 `VARCHAR(N)`，您可以存储多达 `n` 个字符。当存储的字符串短于列的长度时，PostgreSQL不会填充空格。
- `TEXT` 是可变长度字符串。从理论上讲，文本数据是一个长度不限的字符串。

#### 数字类型

PostgreSQL提供两种不同的数字类型：

- [整数](content/postgresql-integer.md)
- 浮点数

##### 整数

PostgreSQL中有三种整数：

- `SMALLINT` 是2字节有符号整数，范围从-32,768到32,767。
- `INT` 是一个4字节的整数，范围从-2,147,483,648到-2,147,483,647。
- [Serial](content/postgresql-serial.md)与整数相同，只是PostgreSQL会自动生成并将值填充到 `SERIAL` 列中。这类似于MySQL中的 `AUTO_INCREMENT` 列或SQLite中的 `AUTOINCREMENT` 列

##### 浮点数

有三种主要的浮点数类型：

- `float(n)` 是一个浮点数，其精度至少为n，最大为8个字节。
- `real` 或 `float8` 是一个双精度（8字节）浮点数。
- `numeric ` 或 `numeric(p,s)` 是一个实数，p位数，小数点后面带有s数。`numeric(p,s)` 是确切的数字。

#### 时间类型

时间数据类型允许您存储日期和/或时间数据。PostgreSQL有五种主要的时间数据类型：

- `DATE` 存储日期值。
- `TIME` 存储时间值。
- `TIMESTAMP` 存储日期和时间值。
- `TIMESTAMPTZ` 是一种带时区的时间戳数据类型。它是 [timestamp](content/postgresql-timestamp.md) with time zone 的缩写。
- `INTERVAL` 存储一段时间。

`TIMESTAMPTZ` 是PostgreSQL对SQL标准的时间数据类型的扩展。

#### 数组

在PostgreSQL中，您可以存储字符串数组、整数数组等[数组](content/postgresql-array.md)。该阵列在某些情况下使用，例如，存储一周中的几天，一年中的几个月。

#### JSON

PostgreSQL提供了两种JSON数据类型：`JSON` 和 `JSONB`，用于存储JSON数据。

`JSON` 数据类型存储需要对每个处理进行重新分析的普通JSON数据，而 `JSONB` 数据类型以二进制格式存储JSON数据，处理速度更快但插入速度更慢。此外，`JSONB` 支持索引，这可能是一个优势。

#### UUID

`UUID` 数据类型允许您存储 [RFC 4122](https://tools.ietf.org/html/rfc4122) 定义的通用唯一标识符。`UUID` 保证比 `SERIAL` 更好的唯一性，可用于隐藏公开的敏感数据，例如URL中的 `id` 值。

#### 特殊类型

除原始数据类型外，PostgreSQL还提供了几种与几何和网络相关的特殊数据类型。

- `box` - 一个平面中的长方形。
- `line ` - 一组点。
- `point` - 一对几何数字。
- `lseg` - 一个线段。
- `polygon` - 一个封闭的几何。
- `inet` - 一个IP4地址。
- `macaddr` - 一个MAC地址。

在本教程中，我们向您介绍了PostgreSQL数据类型，以便您可以在下一个教程中使用它们来创建表。

#### 相关教程

- [Timestamp](content/postgresql-timestamp.md)
- [Boolean ](content/postgresql-boolean.md)
- [SERIAL](content/postgresql-serial.md)
- [Character](content/postgresql-char-varchar-text.md)