## INTERSECT

> 摘要：在本教程中，您将学习如何使用 `INTERSECT` 运算符组合两个或多个 `SELECT` 语句以形成单个结果集。

#### 简介

与 [UNION](content/postgresql-union.md) 和 [EXCEPT](content/postgresql-except.md) 运算符一样，`INTERSECT` 运算符将两个或多个 [SELECT](concent/postgresql-select.md) 语句的结果集合并到一个结果集中。 `INTERSECT` 运算符返回结果集中可用或两个查询返回的所有行。

下图显示了 `INTERSECT` 运算符生成的最终结果集。 最终结果集由圆圈A与圆圈B相交的黄色区域表示。

![PostgreSQL INTERSECT Operator](/imgs/PostgreSQL-INTERSECT-Operator-300x206.png)

以下说明了 `INTERSECT` 运算符的语法：

```sql
SELECT column_list FROM A
INTERSECT
SELECT column_list FROM B;
```

要使用 `INTERSECT` 运算符，`SELECT` 语句中出现的列必须遵循以下规则：

1. `SELECT` 子句中的列数及其顺序必须相同。
2. 列的[数据类型](concent/postgresql-data-types.md)必须兼容。

#### 示例

让我们为演示创建一些表。

以下 `CREATE TABLE` 语句创建三个表：`employees` , `keys` 和  `hipos`.。

```sql
CREATE TABLE employees (
	employee_id serial PRIMARY KEY,
	employee_name VARCHAR (255) NOT NULL
);
 
CREATE TABLE keys (
	employee_id INT PRIMARY KEY,
	effective_date DATE NOT NULL,
	FOREIGN KEY (employee_id) REFERENCES employees (employee_id)
);
 
CREATE TABLE hipos (
	employee_id INT PRIMARY KEY,
	effective_date DATE NOT NULL,
	FOREIGN KEY (employee_id) REFERENCES employees (employee_id)
);
```

`employees` 表存储员工主数据。`keys` 表存储关键员工，`hipos` 表存储具有高潜力和高影响力的员工。

以下[INSERT](content/postgresql-insert.md)语句将一些示例数据插入到 `employees`, `keys`  和 `hipos` 中：

```sql
INSERT INTO employees (employee_name)
VALUES
 ('Joyce Edwards'),
 ('Diane Collins'),
 ('Alice Stewart'),
 ('Julie Sanchez'),
 ('Heather Morris'),
 ('Teresa Rogers'),
 ('Doris Reed'),
 ('Gloria Cook'),
 ('Evelyn Morgan'),
 ('Jean Bell');
 
INSERT INTO keys
VALUES
 (1, '2000-02-01'),
 (2, '2001-06-01'),
 (5, '2002-01-01'),
 (7, '2005-06-01');
 
INSERT INTO hipos
VALUES
 (9, '2000-01-01'),
 (2, '2002-06-01'),
 (5, '2006-06-01'),
 (10, '2005-06-01');
```

以下语句从keys表返回 `employee_id` 列表。

```sql
SELECT employee_id FROM keys;
 employee_id
-------------
           1
           2
           5
           7
(4 rows)
```

以下语句从 `hipos` 表返回 `employee_id` 列表。

```sql
SELECT employee_id FROM hipos;
 employee_id
-------------
           9
           2
           5
          10
(4 rows)
```

To get the employees who are both keys, and high potential and high impact.

获得既是关键，又具有高潜力和高影响力的员工。

```sql
SELECT employee_id FROM keys
INTERSECT
SELECT employee_id FROM hipos;
 employee_id
-------------
           2
           5
(2 rows)
```

结果集显示员工ID 2和5是关键，高潜力和高影响力的员工。

要对 `INTERSECT` 运算符返回的结果集进行排序，请将 [ORDER BY](content/postgresql-order-by.md) 子句放在语句的末尾，而不是放在每个 `SELECT` 语句的末尾。

请参阅以下查询：

```sql
SELECT employee_id FROM keys
INTERSECT
SELECT employee_id FROM hipos
ORDER BY employee_id;
 employee_id
-------------
           2
           5
(2 rows)
```

在本教程中，我们向您展示了如何使用 `INTERSECT` 运算符组合多个结果集并返回两个结果集中出现的行。

#### 相关教程

- [UNION](http://www.postgresqltutorial.com/postgresql-union/)
- [EXCEPT](http://www.postgresqltutorial.com/postgresql-tutorial/postgresql-except/)