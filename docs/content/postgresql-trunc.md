## TRUNC

`TRUNC()` 函数返回一个截断为整数或截断到指定小数位的数字。

#### 语法

以下说明了 `TRUNC()` 函数的语法：

```sql
TRUNC(number [, precision])
```

#### 参数

`TRUNC()` 函数接受两个参数。

**1) number**

`number` 参数是要截断的数值

**2) precision**

`precision` 参数是一个整数，表示小数位数。

如果 `precision` 参数是正整数，则 `TRUNC()` 函数会截断小数点右侧的数字。

如果 `precision` 是负整数，则 `TRUNC()` 函数将替换小数点左侧的数字。

`precision` 参数是可选的。如果未指定，则默认为零（0）。换句话说，该数字被截断为整数。

#### 返回值

如果未指定第二个参数，`TRUNC()` 函数将返回与第一个参数相同的[数值数据类型](content/postgresql-numeric.md)。否则，如果使用两个参数，则函数返回数值。

#### 示例

**A）截断为整数**

以下示例使用 `TRUNC()` 函数将数字截断为整数：

```sql
SELECT TRUNC(10.6);
 trunc
-------
    10
(1 row)
```

**B）截断到指定的小数位**

以下语句将数字截断为2位小数：

```sql
SELECT TRUNC(1.234, 2);
 trunc
-------
  1.23
(1 row)
```

**C）第二个参数用负的截断数字**

请考虑以下示例：

```sql
SELECT TRUNC(150.45,-2)；
 trunc
-------
   100
(1 row)
```

第二个参数是 -2，因此， `TRUNC()` 函数替换了小数点左边的数字。

**D）截断查询返回的数字**

请参阅[示例数据库](content/postgresql-sample-database.md)中的以下电影，`film_category` 和 `category` 表：

![film film_category category tables](/imgs/film-film_category-category-tables.png)

以下声明按电影类别计算平均租金率：

```sql
SELECT NAME, TRUNC(AVG( rental_rate ),2)
FROM film
INNER JOIN film_category USING(film_id)
INNER JOIN category USING(category_id)
GROUP BY NAME
ORDER BY NAME;
    name     | trunc
-------------+-------
 Action      |  2.64
 Animation   |  2.80
 Children    |  2.89
 Classics    |  2.74
 Comedy      |  3.16
 Documentary |  2.66
 Drama       |  3.02
 Family      |  2.75
 Foreign     |  3.09
 Games       |  3.25
 Horror      |  3.02
 Music       |  2.95
 New         |  3.11
 Sci-Fi      |  3.21
 Sports      |  3.12
 Travel      |  3.23
(16 rows)
```

在此示例中，我们使用 `TRUNC()` 函数将平均租金截断为两位小数。

在本教程中，您学习了如何使用 `TRUNC()` 函数截断数字。