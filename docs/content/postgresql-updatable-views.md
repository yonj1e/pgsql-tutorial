## 可更新视图

> 摘要：在本教程中，我们将向您展示在PostgreSQL中可更新视图的要求以及如何创建可更新视图。

PostgreSQL视图在满足以下条件时可更新：

- 视图的定义查询必须在 `FROM` 子句中只有一个条目，可以是表或另一个可更新视图。
- 定义查询不得包含以下顶级子句之一：[GROUP BY](content/postgresql-group-by.md), [HAVING](content/postgresql-having.md), [LIMIT](content/postgresql-limit.md), OFFSET, [DISTINCT](content/postgresql-select-distinct.md), WITH, [UNION](content/postgresql-union.md), INTERSECT 和 EXCEPT。
- 选择列表不得包含任何窗口函数或[集合返回函数](content/plpgsql-function-returns-a-table.md)或任何聚合函数，如 [SUM](content/postgresql-sum-function.md), [COUNT](content/postgresql-count-function.md), [AVG](content/postgresql-avg-function.md), [MIN](content/postgresql-min-function.md), [MAX](content/postgresql-max-function.md) 等。

 可更新视图可能同时包含可更新列和不可更新列。如果尝试插入或更新不可更新的列，PostgreSQL将引发错误.。

当您执行诸如  [INSERT](content/postgresql-insert.md), [UPDATE](content/postgresql-update.md) 或 [DELETE](content/postgresql-delete.md) 之类的更新操作时，PosgreSQL会将此语句转换为基础表的相应语句。

如果在视图的定义查询中有 [WHERE](content/postgresql-where.md) 条件，您仍然可以更新或删除视图中不可见的行。但是，如果要避免这种情况，可以在定义视图时使用 `CHECK` 选项。

执行更新操作的用户必须在视图上拥有相应的权限，但他们不需要在底层表上拥有权限。但是，视图所有者必须拥有基础表的相关权限。  

#### 示例

首先，我们使用 [CREATE VIEW](content/managing-postgresql-views.md) 语句创建一个新的可更新视图 `usa_cities`。此视图包含城市表中属于美国的 `city`。

```sql
CREATE VIEW usa_cities AS 
 SELECT
  city,
  country_id
 FROM city
 WHERE country_id = 103;
```

Next, we can check the data in the `usa_cities` view by executing a simple `SELECT` statement:

接下来，我们可以通过执行一个简单的 `SELECT` 语句来检查 `usa_cities` 视图中的数据：

```sql
SELECT * FROM usa_cities;
          city           | country_id
-------------------------+------------
 Akron                   |        103
 Arlington               |        103
 Augusta-Richmond County |        103
 Aurora                  |        103
 Bellevue                |        103
 Brockton                |        103
 Cape Coral              |        103
 Citrus Heights          |        103
 Clarksville             |        103
 Compton                 |        103
 Dallas                  |        103
 Dayton                  |        103
 El Monte                |        103
 Fontana                 |        103
 Garden Grove            |        103
 Garland                 |        103
 Grand Prairie           |        103
 Greensboro              |        103
 Joliet                  |        103
 Kansas City             |        103
 Lancaster               |        103
 Laredo                  |        103
 Lincoln                 |        103
 Manchester              |        103
 Memphis                 |        103
 Peoria                  |        103
 Roanoke                 |        103
 Rockford                |        103
 Saint Louis             |        103
 Salinas                 |        103
 San Bernardino          |        103
 Sterling Heights        |        103
 Sunnyvale               |        103
 Tallahassee             |        103
 Warren                  |        103
(35 rows)
```

然后，我们使用以下 `INSERT` 语句通过 `usa_cities` 视图将新城市插入到城市表中：

```sql
INSERT INTO usa_cities (city, country_id)
VALUES('San Jose', 103);
```

之后，我们检查城市表中的数据。

```sql
SELECT city, country_id
FROM city
WHERE country_id = 103
ORDER BY last_update DESC;
          city           | country_id
-------------------------+------------
 San Jose                |        103
 Arlington               |        103
 Augusta-Richmond County |        103
 Aurora                  |        103
 Bellevue                |        103
 Brockton                |        103
 Cape Coral              |        103
 Citrus Heights          |        103
 Clarksville             |        103
 Compton                 |        103
 Dallas                  |        103
 Dayton                  |        103
 El Monte                |        103
 Fontana                 |        103
 Garden Grove            |        103
 Garland                 |        103
 Grand Prairie           |        103
 Greensboro              |        103
 Akron                   |        103
 Kansas City             |        103
 Lancaster               |        103
 Laredo                  |        103
 Lincoln                 |        103
 Manchester              |        103
 Memphis                 |        103
 Peoria                  |        103
 Roanoke                 |        103
 Rockford                |        103
 Saint Louis             |        103
 Salinas                 |        103
 San Bernardino          |        103
 Sterling Heights        |        103
 Sunnyvale               |        103
 Tallahassee             |        103
 Warren                  |        103
 Joliet                  |        103
(36 rows)
```

我们在城市表中添加了一个新条目。

最后，我们可以删除通过 `usa_cities` 视图添加的条目。

```sql
DELETE FROM usa_cities WHERE city = 'San Jose';
```

该条目已通过 `usa_cities` 视图从 `city` 表中删除。

在本教程中，我们展示了如何创建可更新视图，并向您介绍视图必须满足的条件才能自动更新。

#### 相关教程

- [视图管理](content/managing-postgresql-views.md)
- [物化视图](content/postgresql-materialized-views.md)