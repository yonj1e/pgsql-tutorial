## SELECT DISTINCT

> 摘要：在本教程中，您将学习如何使用 `SELECT DISTINCT` 子句从查询返回的结果集中删除重复的行。

#### 简介

[`SELECT`](/content/postgresql-select.md) 语句中使用 `DISTINCT` 子句从结果集中删除重复的行。`DISTINCT` 子句为每组重复项保留一行。`DISTINCT` 子句可用于表的一列或多列。

以下说明了 `DISTINCT` 子句的语法：

```sql
SELECT DISTINCT column_1 FROM table_name;
```

在此语句中，`column_1` 列中的值用于检查重复项。

如果指定多个列，则 `DISTINCT` 子句将根据这些列的值组合检查重复项。

```sql
SELECT DISTINCT column_1, column_2 FROM tbl_name;
```

在这种情况下，`column_1` 和 `column_2` 的组合将用于检查重复。

PostgreSQL还提供了 `DISTINCT ON (expression)`，以使用以下语法保留每组重复项的“第一”行：

```sql
SELECT DISTINCT ON (column_1), column_2 
FROM tbl_name 
ORDER BY column_1, column_2;
```

从 `SELECT` 语句返回的行的顺序是不可预测的，因此每个副本组的第一行也是不可预测的。作法是始终将 [ORDER BY](/content/postgresql-order-by.md) 子句与 `DISTINCT ON(expression)`  一起使用，以使结果集正常。

请注意，`DISTINCT ON` 表达式必须与 `ORDER BY` 子句中最左侧的表达式匹配。

#### 示例

让我们[创建一个新表](/content/postgresql-create-table.md) `t1`，并向表中[插入数据](/content/postgresql-insert.md)以练习 `DISTINCT` 子句。

首先，使用以下语句创建由三列组成的 t1 表：`id`，`bcolorand` 和 `fcolor`。

```sql
CREATE TABLE t1 ( 
    id serial NOT NULL PRIMARY KEY, 
    bcolor VARCHAR, 
    fcolor VARCHAR
);
```

其次，使用以下 `INSERT` 语句将一些行插入到 `t1` 表中：

```sql
INSERT INTO t1 (bcolor, fcolor) 
VALUES 
('red', 'red'), ('red', 'red'), ('red', NULL), (NULL, 'red'), 
('red', 'green'), ('red', 'blue'), ('green', 'red'), 
('green', 'blue'), ('green', 'green'), ('blue', 'red'), 
('blue', 'green'), ('blue', 'blue');
```

第三，使用 `SELECT` 语句查询 `t1` 表中的数据：

```sql
SELECT id, bcolor, fcolor FROM t1;
 id | bcolor | fcolor
----+--------+--------
  1 | red    | red
  2 | red    | red
  3 | red    |
  4 |        | red
  5 | red    | green
  6 | red    | blue
  7 | green  | red
  8 | green  | blue
  9 | green  | green
 10 | blue   | red
 11 | blue   | green
 12 | blue   | blue
(12 rows)
```

##### 一列使用DISTINCT

以下语句从 `t1` 表中选择 `bcolor` 列中的唯一值，并使用  [`ORDER BY`](content/postgresql-order-by.md)子句按字母顺序对结果集进行[排序](content/postgresql-order-by.md)。

```sql
SELECT DISTINCT bcolor 
FROM t1 
ORDER BY bcolor;
 bcolor
--------
 blue
 green
 red

(4 rows)
```

##### 多列使用DISTINCT

以下语句演示了如何在多个列上使用 `DISTINC` T子句：

```sql
SELECT DISTINCT bcolor, fcolor 
FROM t1 
ORDER BY bcolor, fcolor;
 bcolor | fcolor
--------+--------
 blue   | blue
 blue   | green
 blue   | red
 green  | blue
 green  | green
 green  | red
 red    | blue
 red    | green
 red    | red
 red    |
        | red
(11 rows)
```

因为我们在 `SELECT DISTINCT` 子句中指定了 `bcolor` 和 `fcolor` 列，所以PostgreSQL结合了 `bcolor` 和 `fcolor` 列中的值来检查行的唯一性。

查询从t1表返回 `bcolor` 和 `fcolor` 的唯一组合。请注意，`t1` 表在 `bcolor` 和` fcolor` 列中都有两行 `red`值。当我们将 `DISTINCT` 应用于两个列时，从结果集中删除了一行，因为它是重复的。

##### DISTINCT ON

以下语句对 `bcolor` 和 `fcolor` 的结果集进行排序，然后对于每组重复项，将第一行保留在返回的结果集中。

```sql
SELECT DISTINCT ON (bcolor) bcolor, fcolor 
FROM t1 
ORDER BY bcolor, fcolor;
 bcolor | fcolor
--------+--------
 blue   | blue
 green  | blue
 red    | blue
        | red
(4 rows)
```

在本教程中，您学习了如何使用 `SELECT DISTINCT` 语句从查询返回的结果集中删除重复的行。