## ASCII

`ASCII()` 函数返回字符的[ASCII](https://en.wikipedia.org/wiki/ASCII)代码值。对于UTF-8， `ASCII()` 函数返回字符的Unicode代码点。

#### 语法

以下说明了ASCII函数的语法：

```sql
ASCII(char)
```

#### 参数

`ASCII()` 函数需要一个参数：

**1) char**

`char` 参数是您想要获取ASCII码值的[字符](content/postgresql-char-varchar-text.md)。

如果将字符串传递给 `ASCII()` 函数，它将返回第一个字符的ASCII码。

#### 返回值

`ASCII()` 函数返回一个整数，表示输入字符的ASCII码值。对于UTF-8字符，它返回一个与Unicode代码点对应的整数。

#### 示例

以下示例使用 `ASCII()` 函数获取字符 `A` 和 `a` 的ASCII码值：

```sql
SELECT ASCII('A'), ASCII('a');
 ascii | ascii
-------+-------
    65 |    97
(1 row)
```

如果将一个字符串传递给 `ASCII()` 函数，您将获得第一个字符的ASCII码，如以下示例所示：

```sql
SELECT ASCII('ABC');
 ascii
-------
    65
(1 row)
```

该函数返回字母A的ASCII码，为65。

以下示例说明如何使用 `ASCII()` 函数获取UTF-8字符的Unicode代码点：

```sql
SELECT ASCII('Ω');
```

#### 备注

要获取整数的ASCII码值或Unicode代码点，请使用 `CHR()` 函数。

在本教程中，您学习了如何使用 `ASCII()` 函数来获取字符的ASCII码或Unicode代码点。