## 复制数据库

![PostgreSQL Copy Database](imgs/postgresql-copy-database-300x190.jpg)

> 摘要：在本教程中，您将了解如何将PostgreSQL数据库复制到同一服务器上或从一个服务器复制到另一个服务器。

#### 在同一服务器上复制数据库

有时，您希望在数据库服务器中复制PostgreSQL数据库以进行测试。PostgreSQL通过 [CREATE DATABASE](content/postgresql-create-database.md) 语句很容易实现，如下所示：

```sql
CREATE DATABASE targetdb WITH TEMPLATE sourcedb;
```

此语句将 `sourcedb` 复制到 `targetdb`。例如，要将 `dvdrental` [示例数据库](content/postgresql-sample-database.md)复制到 `dvdrental_test` 数据库，请使用以下语句：

```sql
CREATE DATABASE dvdrental_test WITH TEMPLATE dvdrental;
```

根据源[数据库的大小](content/postgresql-database-indexes-table-size.md)，完成复制可能需要一段时间。

#### 将数据库从服务器复制到另一个服务器

有几种方法可以在PostgreSQL数据库服务器之间复制数据库。如果源数据库的大小很大且数据库服务器之间的连接很慢，则可以将源数据库转储到文件，将文件复制到远程服务器并还原它。

以下是每个步骤的命令：

首先，将源数据库转储到文件中。

```sql
pg_dump -U postgres -O sourcedb sourcedb.sql
```

其次，将转储文件复制到远程服务器。

第三，在远程服务器中创建一个新数据库：

```sql
CREATE DATABASE targetdb;
```

第四，恢复远程服务器上的转储文件：

```sql
psql -U postgres -d targetdb -f sourcedb.sql
```

例如，要将 `dvdrental` 数据库从本地服务器复制到远程服务器，请按以下步骤操作：

首先，将 `dvdrental` 数据库转储到转储文件中，例如 `dvdrental.sql`：

```sql
pg_dump -U postgres -O dvdrental dvdrental.sql
```

其次，将转储文件复制到远程服务器。

第三，在远程服务器上创建 `dvdrental` 数据库：

```sql
CREATE DATABASE dvdrental;
```

第四，恢复远程服务器中的 `dvdrental.sql` 转储文件：

```sql
psql -U postgres -d dvdrental -f dvdrental.sql
```

如果服务器之间的连接速度很快且数据库的大小不大，您可以使用以下命令：

```sql
pg_dump -C -h local -U localuser sourcedb | psql -h remote -U remoteuser targetdb
```

例如，要将 `dvdrental` 数据库从 `localhost` 服务器复制到远程服务器，请按以下步骤操作：

```sql
pg_dump -C -h localhost -U postgres dvdrental | psql -h remote -U postgres dvdrental
```

在本教程中，您学习了如何在数据库服务器中复制PostgreSQL数据库，或从数据库服务器复制到另一个数据库服务器。