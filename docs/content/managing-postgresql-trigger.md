## 触发器管理

> 摘要：本教程介绍如何管理触发器，包括修改，禁用和删除触发器。

#### 修改触发器

要修改触发器，请使用 `ALTER TRIGGER` 语句。该语句是PostgreSQL对SQL标准的扩展。`ALTER TRIGGER` 语句的语法如下：

```sql
ALTER TRIGGER trigger_name ON table_nameRENAME TO new_name;
```

首先，指定与要更改的特定表关联的触发器的名称。

其次，将新的触发器名称放在 `RENAME TO` 子句中。

例如，如果要将与 `employees` 表关联的 `last_name_changes` 触发器更改为 `log_last_name_changes`，则使用 `ALTER TRIGGER` 语句，如下所示：

```sql
ALTER TRIGGER last_name_changes ON employees
RENAME TO log_last_name_changes;
```

#### 禁用触发器

PostgreSQL不提供任何特定语句，例如 `DISABLE TRIGGER`，用于禁用现有触发器。但是，您可以使用 [ALTER TABLE](content/postgresql-alter-table.md) 语句禁用触发器，如下所示：

```sql
ALTER TABLE table_name
DISABLE TRIGGER trigger_name | ALL
```

您可以在 `DISABLE TRIGGER` 子句后指定触发器名称以禁用特定触发器。要禁用与表关联的所有触发器，请使用 `ALL` 而不是特定的触发器名称。

请注意，数据库中仍有一个禁用触发器可用。但是，它的触发事件发生时不会触发它。

假设您要禁用与 `employees` 表相关联的 `log_last_name_changes` 触发器，您可以使用以下语句：

```sql
ALTER TABLE employees
DISABLE TRIGGER log_last_name_changes;
```

要禁用与 `employees` 表关联的所有触发器，请使用以下语句：

```sql
ALTER TABLE employees
DISABLE TRIGGER ALL;
```

#### 删除触发器

要删除现有的触发器定义，请使用 `DROP TRIGGER `语句，如下所示：

```sql
DROP TRIGGER [IF EXISTS] trigger_name ON table_name;
```

您可以在 `DROP TRIGGER` 子句和与触发器关联的表之后指定要删除的触发器名称。

为避免删除不存在的触发器的错误，请使用 `IF EXISTS` 选项。

例如，要删除 `log_last_name_changes` 触发器，请使用以下语句：

```sql
DROP TRIGGER log_last_name_changes ON employees;
```

在本教程中，我们向您展示了如何修改，禁用和删除现有触发器。