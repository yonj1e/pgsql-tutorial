## PostgreSQL vs. MySQL

在选择开源关系数据库管理系统时，PostgreSQL与MySQL是一个重要的抉择。PostgreSQL和MySQL都是久经考验的解决方案，可以与Oracle和SQL Server等企业解决方案竞争。

MySQL以其易用性和速度而闻名，而PostgreSQL具有更多高级功能，这就是PostgreSQL经常被描述为Oracle的开源版本的原因。

下表比较了PostgreSQL与MySQL的功能：

![](imgs/postgresql-vs-mysql-features.jpg)

|                                                              | PostgreSQL                                                   | MySQL                                                        |
| ------------------------------------------------------------ | ------------------------------------------------------------ | ------------------------------------------------------------ |
| 称号                                                         | 世界上最先进的开源数据库                                     | 世界上最流行的开源数据库                                     |
| 发展                                                         | PostgreSQL是一个开源项目                                     | MySQL是一个开源产品                                          |
| 发音                                                         | post gress queue ell                                         | my ess queue ell                                             |
| 许可                                                         | MIT-style license                                            | GNU General Public License                                   |
| 设计语言                                                     | C                                                            | C/C++                                                        |
| GUI 工具                                                     | PgAdmin                                                      | MySQL Workbench                                              |
| ACID                                                         | Yes                                                          | Yes                                                          |
| 存储引擎                                                     | 单个存储引擎                                                 | 多个[存储引擎](http://www.mysqltutorial.org/understand-mysql-table-types-innodb-myisam.aspx)，例如InnoDB和MyISAM |
| 全文检索                                                     | Yes                                                          | Yes                                                          |
| 删除[临时表](content/postgresql-temporary-table.md)          | `DROP TABLE` 语句中没有 `TEMP`或 `TEMPORARY` 关键字          | MySQL支持 `DROP TABLE` 语句中的 `TEMP` 或 `TEMPORARY` 关键字，它允许您仅删除临时表。 |
| [`DROP TABLE`](content/postgresql-drop-table.md)             | 支持 `CASCADE` 选项以删除表依赖对象，例如表，视图等，        | 不支持 `CASCADE` 选项                                        |
| [`TRUNCATE TABLE`](content/postgresql-truncate-table.md)     | PostgreSQL `TRUNCATE TABLE` 支持更多功能，如 `CASCADE`，`RESTART IDENTITY`，`CONTINUE IDENTITY`，事务安全等。 | [MySQL TRUNCATE TABLE](http://www.mysqltutorial.org/mysql-truncate-table/)不支持 `CASCADE` 和事务安全，即。删除数据后，无法回滚。 |
| 自增列                                                       | [`SERIAL`](content/postgresql-serial.md)                     | [`AUTO_INCREMENT`](http://www.mysqltutorial.org/mysql-sequence/) |
| Analytic functions                                           | Yes                                                          | No                                                           |
| [数据类型](content/postgresql-data-types.md)                 | 支持许多高级类型，例如[array](content/postgresql-array.md)，[hstore](content/postgresql-hstore.md)和用户定义的类型。 | SQL标准类型                                                  |
| 无符号[整数](content/postgresql-integer.md)                  | No                                                           | Yes                                                          |
| [布尔类型](content/postgresql-boolean.md)                    | Yes                                                          | 内部使用 `TINYINT(1)` 表示[布尔值](http://www.mysqltutorial.org/mysql-boolean/) |
| 网络地址类型                                                 | Yes                                                          | No                                                           |
| 设置列的默认值                                               | 支持常量和函数调用                                           | 对于 `TIMESTAMP` 或 `DATETIME` 列，必须是常量或 `CURRENT_TIMESTAMP` |
| CTE                                                          | Yes                                                          | No                                                           |
| `EXPLAIN`                                                    | 更详细                                                       | 不太详细                                                     |
| [物化视图](content/postgresql-materialized-views.md)         | Yes                                                          | No                                                           |
| [CHECK约束](content/postgresql-check-constraint.md)          | Yes                                                          | No (MySQL忽略[CHECK约束](http://www.mysqltutorial.org/mysql-check-constraint/)) |
| 表继承                                                       | Yes                                                          | No                                                           |
| [存储过程](content/postgresql-stored-procedures.md)          | Ruby, Perl, Python, TCL, PL/pgSQL, SQL, JavaScript, etc.     | SQL：2003[存储过程](http://www.mysqltutorial.org/mysql-stored-procedure-tutorial.aspx)的语法 |
| [`FULL OUTER JOIN`](content/postgresql-full-outer-join.md)   | Yes                                                          | No                                                           |
| [`INTERSECT`](content/postgresql-intersect.md)               | Yes                                                          | No                                                           |
| [`EXCEPT`](content/postgresql-tutorial/postgresql-except.md) | Yes                                                          | No                                                           |
| Partial indexes                                              | Yes                                                          | No                                                           |
| Bitmap indexes                                               | Yes                                                          | No                                                           |
| Expression indexes                                           | Yes                                                          | No                                                           |
| Covering indexes                                             | Yes (since version 9.2)                                      | Yes. MySQL支持覆盖索引，这些索引允许只扫描索引而不触及表数据来检索数据。对于具有数百万行的大型表，这是有利的。 |
| Common table expression (CTE)                                | Yes                                                          | Yes. (since version 8.0, MySQL has supported [CTE](http://www.mysqltutorial.org/mysql-cte/)) |
| [触发器](content/postgresql-triggers.md)                     | 支持触发器可以触发大多数类型的命令，除了影响全局数据库的命令，例如角色和表空间。 | 仅限于一些命令                                               |
| 分区表                                                       | RANGE, LIST, HASH                                            | RANGE, LIST, HASH, KEY, 和使用RANGE或LIST与HASH或KEY子分区的组合进行复合分区 |
| 定时任务                                                     | pgAgent                                                      | [Scheduled event](http://www.mysqltutorial.org/mysql-triggers/working-mysql-scheduled-event/) |
| Connection Scalability                                       | Each new connection is an OS process                         | Each new connection is an OS thread                          |