## 连接数据库

> 摘要：在本教程中，我们将向您展示使用 `psql` 和` pgAdmin` GUI应用程序的交互式终端程序连接PostgreSQL数据库的不同方法。

PostgreSQL在Windows安装程序已经集成了 `pgAdmin`，但我们使用源码[安装数据库](/content/install-postgresql.md)时，并没有安装[pgAdmin](https://www.pgadmin.org/)，可到官网自行下载安装，这里不做介绍。

现在，您可以使用psql或pgAdmin工具连接PostgreSQL数据库服务器。

#### psql

`psql` 是PostgreSQL提供的交互式终端程序。您可以使用 `psql` 工具做很多事情，例如，执行SQL语句，管理数据库对象等。

以下步骤说明如何使用 `psql` 连接PostgreSQL数据库服务器：

首先启动psql程序。

其次，输入指定必需的信息，例如服务器，数据库，端口，用户名和密码。

第三，您可以使用各种SQL语句与PostgreSQL数据库服务器进行交互。您可以尝试以下语句来测试它：

```sql
$ ./psql -p 5432 -d test
psql (10.5)
Type "help" for help.

test=# select version();
                                                 version
---------------------------------------------------------------------------------------------------------
 PostgreSQL 10.5 on x86_64-pc-linux-gnu, compiled by gcc (GCC) 4.8.5 20150623 (Red Hat 4.8.5-28), 64-bit
(1 row)

test=#
```

psql将为您提供系统中当前的PostgreSQL版本。

#### pgAdmin

连接数据库的第二种方法是使用pgAdmin GUI应用程序。通过使用pgAdmin GUI应用程序，您可以通过直观的用户界面与PostgreSQL数据库服务器进行交互。

省略。

#### 其他应用程序

任何支持ODBC或[JDBC](/content/postgresql-jdbc.md)的应用程序都可以连接PostgreSQL数据库服务器。此外，如果您开发使用适当驱动程序的应用程序，则应用程序也可以连接PostgreSQL数据库服务器。

在本教程中，您已经学习了如何使用不同的客户端工具（包括psql和pgAdmin GUI应用程序）连接PostgreSQL数据库服务器。让我们探索[数据库对象](/content/postgresql-server-and-database-objects.md)，并找出我们如何在我们的应用程序中使用它们。