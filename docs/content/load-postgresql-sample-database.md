## 加载示例数据库

> 摘要：在本教程中，我们将向您展示如何将PostgreSQL示例数据库加载到数据库服务器中。

在开始本教程之前，您必须具备：

- 系统上安装的PostgreSQL数据库服务器。 如果没有，可以按照[安装PostgreSQL](/content/install-postgresql.md)教程进行操作。
- 准备好[DVD rental示例数据库](/content/postgresql-sample-database.md)。

#### 创建新的DVD rental数据库

在将数据库模式和数据加载到数据库之前，需要在数据库服务器中[创建新数据库](/content/postgresql-create-database.md)。

首先，启动 `psql` 工具。

其次，指定相关信息连接到数据库服务器。

第三，输入以下 `CREATE DATABASE` 语句以创建新的dvdrental数据库。

```sql
CREATE DATABASE dvdrental;
```

将创建一个名为 `dvdrental` 的新数据库。

#### 使用psql工具加载DVD rental数据库

使用 `pg_restore` 工具将数据加载到 `dvdrental` 数据库中：

```shell
$ ./pg_restore -d dvdrental /work/pkg/dvdrental.tar
```

存储在 `dvdrental.tar` 中的数据将加载到 `dvdrental` 数据库中。

#### 验证示例数据库已加载

连接 `dvdrental` 数据库，您将看到模式中的表和其他数据库对象，如下所示：

```sql
dvdrental=# \d
                    List of relations
 Schema |            Name            |   Type   |  Owner
--------+----------------------------+----------+---------
 public | actor                      | table    | yangjie
 public | actor_actor_id_seq         | sequence | yangjie
 public | actor_info                 | view     | yangjie
 public | address                    | table    | yangjie
 public | address_address_id_seq     | sequence | yangjie
 public | category                   | table    | yangjie
 public | category_category_id_seq   | sequence | yangjie
 public | city                       | table    | yangjie
 public | city_city_id_seq           | sequence | yangjie
 public | country                    | table    | yangjie
 public | country_country_id_seq     | sequence | yangjie
 public | customer                   | table    | yangjie
 public | customer_customer_id_seq   | sequence | yangjie
 public | customer_list              | view     | yangjie
 public | film                       | table    | yangjie
 public | film_actor                 | table    | yangjie
 public | film_category              | table    | yangjie
 public | film_film_id_seq           | sequence | yangjie
 public | film_list                  | view     | yangjie
 public | inventory                  | table    | yangjie
 public | inventory_inventory_id_seq | sequence | yangjie
 public | language                   | table    | yangjie
 public | language_language_id_seq   | sequence | yangjie
 public | nicer_but_slower_film_list | view     | yangjie
 public | payment                    | table    | yangjie
 public | payment_payment_id_seq     | sequence | yangjie
 public | rental                     | table    | yangjie
 public | rental_rental_id_seq       | sequence | yangjie
 public | sales_by_film_category     | view     | yangjie
 public | sales_by_store             | view     | yangjie
 public | staff                      | table    | yangjie
 public | staff_list                 | view     | yangjie
 public | staff_staff_id_seq         | sequence | yangjie
 public | store                      | table    | yangjie
 public | store_store_id_seq         | sequence | yangjie
(35 rows)
```

我们已经向您展示了如何将 `dvdrental` 示例数据库加载到数据库服务器中以学习和练习PostgreSQL。

让我们开始学习PostgreSQL，玩得开心！