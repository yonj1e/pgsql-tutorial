## PL/pgSQL错误和消息

> 摘要：在本教程中，我们将向您展示如何使用 `RAISE` 语句报告消息并引发错误。另外，我们将向您介绍 `ASSERT` 语句，以便将调试检查插入到PL/pgSQL块中。

#### PL/pgSQL报告消息

要引发消息，请使用 `RAISE` 语句，如下所示：

```sql
RAISE level format;
```

让我们更详细地研究一下 `RAISE` 语句的组成部分。

`RAISE` 语句后面是 `level` 选项，它指定错误严重性。PostgreSQL中有以下级别：

- DEBUG
- LOG
- NOTICE
- INFO
- WARNING
- EXCEPTION

如果未指定 `level`，则默认情况下，`RAISE` 语句将使用引发错误的 `EXCEPTION` 级别并停止当前事务。我们将在下一节中讨论 `RAISE EXCEPTION`。

`format` 是指定消息的字符串。该格式使用百分比(`%`)占位符，这些占位符将被下一个参数替换。占位符的数量必须与参数的数量匹配，否则PostgreSQL将报告以下错误消息：

```sql
[Err] ERROR:  too many parameters specified for RAISE
```

以下示例说明了 `RAISE` 语句输出不同的提示消息。

```sql
DO $$ 
BEGIN 
  RAISE INFO 'information message %', now() ;
  RAISE LOG 'log message %', now();
  RAISE DEBUG 'debug message %', now();
  RAISE WARNING 'warning message %', now();
  RAISE NOTICE 'notice message %', now();
END $$;
```

```sql
INFO:  information message 2015-09-10 21:17:39.398+07
WARNING:  warning message 2015-09-10 21:17:39.398+07
NOTICE:  notice message 2015-09-10 21:17:39.398+07
```

请注意，并非所有消息都会返回客户端，只会向客户端报告INFO，WARNING和NOTICE级别消息。这由 `client_min_messages` 和 `log_min_messages` 配置参数控制。

#### PL/pgSQL引发错误

要引发错误，请在 `RAISE` 语句后使用 `EXCEPTION` 级别。请注意，`RAISE` 语句默认使用 `EXCEPTION` 级别。

除了引发错误之外，您还可以使用以下子句和 `RAISE` 语句添加更详细的信息：

```sql
USING option = expression
```

option 可以是：

- MESSAGE：设置错误消息文本
- HINT：提供提示消息，以便更容易发现错误的根本原因。
- DETAIL：提供有关错误的详细信息。
- ERRCODE：识别错误代码，可以是条件名称，也可以是直接五字符的SQLSTATE代码。请参阅[错误代码和条件名称表](https://www.postgresql.org/docs/current/static/errcodes-appendix.html)。

expression 是一个字符串值表达式。

以下示例引发重复的电子邮件错误消息：

```sql
DO $$ 
DECLARE
 email varchar(255) := 'info@postgresqltutorial.com';
BEGIN 
  -- check email for duplicate
  -- ...
  -- report duplicate email
  RAISE EXCEPTION 'Duplicate email: %', email 
 USING HINT = 'Check the email again';
END $$;
```

```
[Err] ERROR:  Duplicate email: info@postgresqltutorial.com
HINT:  Check the email again
```

以下示例说明了如何引发SQLSTATE及其相应的条件：

```sql
DO $$ 
BEGIN 
 --...
 RAISE SQLSTATE '2210B';
END $$;
```

```sql
DO $$ 
BEGIN 
 --...
 RAISE invalid_regular_expression;
END $$;
```

#### PL/pgSQL使用ASSERT语句进行调试检查

请注意，PostgreSQL从9.5版开始引入ASSERT语句。使用前请检查PostgreSQL版本。

有时，[PL/pgSQL函数](content/postgresql-create-function.md)非常大，使得检测错误变得更加困难。为此，PostgreSQL为您提供了 `ASSERT` 语句，用于将调试检查添加到PL/pgSQL函数中。

以下说明了 `ASSERT` 语句的语法：

```
ASSERT condition [, message];
```

condition 是布尔表达式。如果条件的计算结果为 `TRUE`，则 `ASSERT` 语句不执行任何操作。如果条件计算结果为 `FALSE` 或 `NULL`，则引发 `ASSERT_FAILURE`。

如果您不提供 `message`，PL/pgSQL默认使用 “`assertion failed`” 消息。如果提供了消息，`ASSERT` 语句将使用它来替换默认消息。

```sql
DO $$ 
DECLARE 
 counter integer := -1;
BEGIN 
   ASSERT counter = 0 
   MESSAGE 'Expect counter starts with 0';
END $$;
```

请务必注意，`ASSERT` 语句仅用于调试。

现在，您可以使用 `RAISE` 语句来引发消息或报告错误。