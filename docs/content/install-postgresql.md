## 安装数据库

> 摘要：在本教程中，我们将向您展示如何在本地系统上安装PostgreSQL以学习和练习PostgreSQL。

PostgreSQL是为类UNIX平台开发的，但它设计为可移植的。这意味着PostgreSQL也可以在其他平台上运行，例如Mac OS X，Solaris和Windows。

!> 出于开发目的，我们将在CentOS 7.5上源码安装PostgreSQL 10.5版本。原文使用的Windows + PostgreSQL 9.5 + pgAdmin III 。

完成PostgreSQL安装有三个步骤：

1. 下载PostgreSQL源代码
2. 安装PostgreSQL
3. 验证安装

#### 下载源码

您需要从PostgreSQL官方网站下载源代码。

- 转到PostgreSQL官方网站，下载源代码 <https://www.postgresql.org/ftp/source/>
- 选择要下载的最新版本。完成下载需要几分钟的时间。

#### 编译安装

解压源代码

```shell
# 下载
$ wget https://ftp.postgresql.org/pub/source/v10.5/postgresql-10.5.tar.gz

# 解压
$ tar zxvf postgresql-10.5.tar.gz
```

源码中 `INSTALL` 介绍源码安装步骤：

```shell
$ cd postgresql-10.5/
$ ls INSTALL
INSTALL

# Short Version

./configure
make
su
make install
adduser postgres
mkdir /usr/local/pgsql/data
chown postgres /usr/local/pgsql/data
su - postgres
/usr/local/pgsql/bin/initdb -D /usr/local/pgsql/data
/usr/local/pgsql/bin/postgres -D /usr/local/pgsql/data >logfile 2>&1 &
/usr/local/pgsql/bin/createdb test
/usr/local/pgsql/bin/psql test

# The long version is the rest of this document.
```

设置编译选项(出于开发目的，有debug等选项)，运行脚本：

```shell
#!/bin/sh

./configure --prefix=/work/pgsql/pgsql_10_5 \
--with-openssl \
--with-libxml \
--with-libxslt \
--enable-thread-safety \
--with-zlib \
--enable-debug \
--enable-cassert \
--with-ossp-uuid

$ sh mydevconf.sh
```

编译 & 安装数据库以及扩展：

```shell
$ make clean; make; make install;

···
make[1]: Leaving directory `/work/pkg/postgresql-10.5/config'
PostgreSQL installation complete.

$ cd contrib/
$ make clean; make; make install

# 查看
$ cd /work/pgsql/pgsql_10_5/
$ ls
bin  include  lib  share
```

初始化数据库集簇

```shell
$ ./initdb -D ../data
```

启动数据库服务

```shell
$ ./pg_ctl -D ../data -l logfile start
waiting for server to start.... done
server started
```

新建数据库并连接

```sql
$ ./createdb test
$ ./psql test
psql (10.5)
Type "help" for help.

test=# select version();
                                                 version
---------------------------------------------------------------------------------------------------------
 PostgreSQL 10.5 on x86_64-pc-linux-gnu, compiled by gcc (GCC) 4.8.5 20150623 (Red Hat 4.8.5-28), 64-bit
(1 row)
```

恭喜！ 您已成功安装了PostgreSQL数据库服务器。让我们学习[连接数据库服务器](/content/connect-to-postgresql-database.md)的各种方法。

