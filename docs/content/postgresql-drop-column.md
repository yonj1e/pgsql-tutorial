## 删除列

![PostgreSQL DROP COLUMN](/imgs/PostgreSQL-Drop-Column-300x128.png)

> 摘要：本教程介绍如何使用 `ALTER TABLE` 语句中的PostgreSQL `DROP COLUMN` 子句删除表的一个或多个列。

#### 简介

要删除表的列，请使用 [ALTER TABLE](content/postgresql-alter-table.md) 语句中的 `DROP COLUMN` 子句，如下所示：

```sql
ALTER TABLE table_name 
DROP COLUMN column_name;
```

从表中删除列时，PostgreSQL将自动删除涉及该列的所有索引和约束。

如果要删除的列在其他数据库对象（如[视图](content/postgresql-views.md)，[触发器](content/postgresql-triggers.md)，[存储过程](content/postgresql-stored-procedures.md)等）中使用，则不能删除该列，因为其他对象依赖于该列。在这种情况下，将 `CASCADE` 选项添加到 `DROP COLUMN` 子句以删除列及其所有关联对象：

```sql
ALTER TABLE table_name 
DROP COLUMN column_name CASCADE;
```

如果删除不存在的列，PostgreSQL将发出错误。为避免这种情况，您可以添加 `IF EXISTS` 选项，如下所示：

```sql
ALTER TABLE table_name 
DROP COLUMN IF EXISTS column_name;
```

在此表单中，如果删除不存在的列，PostgreSQL将发出通知而不是错误。

如果要在单个命令中删除表的多个列，请使用以下语句：

```sql
ALTER TABLE table_name
DROP COLUMN column_name_1,
DROP COLUMN column_name_2,
...;
```

请注意，每个 `DROP COLUMN` 子句都用逗号（，）分隔。

PostgreSQL允许您删除表的唯一列，从而导致零列表，这在SQL标准中是不允许的。

让我们看一些示例，看看 `ALTER TABLE DROP COLUMN` 语句是如何工作的。

#### 示例

我们将创建三个表：`books`, `categories` 和 `publishers`。

![PostgreSQL DROP COLUMN Example Diagram](/imgs/PostgreSQL-DROP-COLUMN-Example-Diagram.png)

在此表中，每本图书只有一个发布者，每个发布者都可以发布许多图书。每本书都分配到一个类别，每个类别可以有很多书。

以下语句创建了三个表：

```sql
CREATE TABLE publishers (
    publisher_id serial PRIMARY KEY,
    name VARCHAR NOT NULL
);
 
CREATE TABLE categories (
    category_id serial PRIMARY KEY,
    name VARCHAR NOT NULL
);
 
CREATE TABLE books (
    book_id serial PRIMARY KEY,
    title VARCHAR NOT NULL,
    isbn VARCHAR NOT NULL,
    published_date DATE NOT NULL,
    description VARCHAR,
    category_id INT NOT NULL,
    publisher_id INT NOT NULL,
    FOREIGN KEY (publisher_id) REFERENCES publishers (publisher_id),
    FOREIGN KEY (category_id) REFERENCES categories (category_id)
);
```

另外，我们根据 `books` 和 `publishers` 表创建一个视图，如下所示：

```sql
CREATE VIEW book_info AS SELECT
    book_id,
    title,
    isbn,
    published_date,
    name
FROM
    books b
INNER JOIN publishers P ON P .publisher_id = b.publisher_id
ORDER BY
    title;
```

假设您要删除 `books` 表的 category_id` 列，请使用以下语句：

```sql
ALTER TABLE books DROP COLUMN category_id;
```

我们来看一下 `books` 表：

```sql
test=# \d books
                                        Table "public.books"
     Column     |       Type        | Collation | Nullable |                Default
----------------+-------------------+-----------+----------+----------------------------------------
 book_id        | integer           |           | not null | nextval('books_book_id_seq'::regclass)
 title          | character varying |           | not null |
 isbn           | character varying |           | not null |
 published_date | date              |           | not null |
 description    | character varying |           |          |
 category_id    | integer           |           | not null |
 publisher_id   | integer           |           | not null |
Indexes:
    "books_pkey" PRIMARY KEY, btree (book_id)
Foreign-key constraints:
    "books_category_id_fkey" FOREIGN KEY (category_id) REFERENCES categories(category_id)
    "books_publisher_id_fkey" FOREIGN KEY (publisher_id) REFERENCES publishers(publisher_id)
```

如您所见，该语句不仅删除了 `category_id` 列，还删除了涉及 `category_id` 列的[外键](content/postgresql-foreign-key.md)约束。

我们尝试删除 `publisher_id` 列：

```sql
ALTER TABLE books DROP COLUMN publisher_id;
```

PostgreSQL发出以下错误：

```sql
ERROR:  cannot drop table books column publisher_id because other objects depend on it
DETAIL:  view book_info depends on table books column publisher_id
HINT:  Use DROP ... CASCADE to drop the dependent objects too.
```

它声明 `book_info` 视图使用 `books` 表的列 `publisher_id`。您需要使用 `CASCADE` 选项删除 `publisher_id` 列和 `book_info` 视图，如以下语句所示：

```sql
ALTER TABLE books DROP COLUMN publisher_id CASCADE;
```

该声明发出以下通知，这是我们的预期。

```sql
NOTICE:  drop cascades to view book_info
```

要在单个语句中删除 `isbn` 和 `description` 列，可以添加多个 `DROP COLUMN` 子句，如下所示：

```sql
ALTER TABLE books   
DROP COLUMN isbn,  
DROP COLUMN description;
```

它按预期工作。

在本教程中，您学习了如何在 `ALTER TABLE` 语句中使用PostgreSQL `DROP COLUMN` 子句来删除表的一个或多个列。

#### 相关教程

- [ALTER TABLE](content/postgresql-alter-table.md)