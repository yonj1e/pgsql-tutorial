## ROW_NUMBER function

> 摘要：在本教程中，您将学习如何使用PostgreSQL `ROW_NUMBER()` 函数为结果集中的每一行分配唯一的整数值。

#### 简介

`ROW_NUMBER()` 函数是一个[窗口函数](content/postgresql-window-function.md)，它为结果集中的每一行分配一个唯一的整数。以下说明了 `ROW_NUMBER()` 函数的语法：

```sql
ROW_NUMBER() OVER(
    [PARTITION BY column_1, column_2,…]
    [ORDER BY column_3,column_4,…]
)
```

`ROW_NUMBER()` 函数操作的行集称为窗口。

`PARTITION BY` 子句将窗口划分为较小的集合或分区。如果指定 `PARTITION BY` 子句，则每个分区的行号从1开始递增。

因为 `PARTITION BY` 子句对于 `ROW_NUMBER()` 函数是可选的，所以你可以省略它，并且 `ROW_NUMBER()` 函数会将整个窗口视为一个分区。

`OVER` 子句中的 [ORDER BY](content/postgresql-order-by.md) 子句确定数字的分配顺序。

#### 示例

我们将使用PostgreSQL[窗口函数教程](content/postgresql-window-function.md)中创建的 `products` 表来演示 `ROW_NUMBER()` 函数的功能。

![](imgs/products_product_groups_tables.png)

以下是 `products` 表中的数据。

```sql
 product_id |    product_name    |  price  | group_id
------------+--------------------+---------+----------
          1 | Microsoft Lumia    |  300.00 |        1
          2 | HTC One            |  400.00 |        1
          3 | Nexus              |  500.00 |        1
          4 | iPhone             |  800.00 |        1
          5 | HP Elite           |  900.00 |        2
          6 | Lenovo Thinkpad    | 1100.00 |        2
          7 | Sony VAIO          |  600.00 |        2
          8 | Dell Vostro        |  600.00 |        2
          9 | iPad               |  700.00 |        3
         10 | Kindle Fire        |  300.00 |        3
         11 | Samsung Galaxy Tab |  800.00 |        3
(11 rows)
```

请参阅以下查询。

```sql
SELECT
 product_id,
 product_name,
 group_id,
 ROW_NUMBER () OVER (ORDER BY product_id)
FROM
 products;
 product_id |    product_name    | group_id | row_number
------------+--------------------+----------+------------
          1 | Microsoft Lumia    |        1 |          1
          2 | HTC One            |        1 |          2
          3 | Nexus              |        1 |          3
          4 | iPhone             |        1 |          4
          5 | HP Elite           |        2 |          5
          6 | Lenovo Thinkpad    |        2 |          6
          7 | Sony VAIO          |        2 |          7
          8 | Dell Vostro        |        2 |          8
          9 | iPad               |        3 |          9
         10 | Kindle Fire        |        3 |         10
         11 | Samsung Galaxy Tab |        3 |         11
(11 rows)
```

因为我们没有使用 `PARTITION BY` 子句，所以 `ROW_NUMBER()` 函数将整个结果集视为一个分区。

`ORDER BY` 子句按 `product_id` 对结果集进行排序，因此，`ROW_NUMBER()` 函数根据 `product_id` 顺序为行分配整数值。

在以下查询中，我们将 `ORDER BY` 子句中的列更改为 `product_name`，`ROW_NUMBER()` 函数根据产品名称顺序将整数值分配给每一行。

```sql
SELECT
 product_id,
 product_name,
 group_id,
 ROW_NUMBER () OVER (ORDER BY product_name)
FROM
 products;
 product_id |    product_name    | group_id | row_number
------------+--------------------+----------+------------
          8 | Dell Vostro        |        2 |          1
          5 | HP Elite           |        2 |          2
          2 | HTC One            |        1 |          3
          9 | iPad               |        3 |          4
          4 | iPhone             |        1 |          5
         10 | Kindle Fire        |        3 |          6
          6 | Lenovo Thinkpad    |        2 |          7
          1 | Microsoft Lumia    |        1 |          8
          3 | Nexus              |        1 |          9
         11 | Samsung Galaxy Tab |        3 |         10
          7 | Sony VAIO          |        2 |         11
(11 rows)
```

在下面的查询中，我们使用 `PARTITION BY` 子句根据 `group_id` 列中的值将窗口划分为子集。在这种情况下，`ROW_NUMBER()` 函数将一个分配给每个分区的起始行，并为同一分区中的下一行增加一个。

`ORDER BY` 子句按照 `product_name` 列中的值对每个分区中的行进行排序。

```sql
SELECT
 product_id,
 product_name,
 group_id,
 ROW_NUMBER () OVER (
 PARTITION BY group_id
 ORDER BY
 product_name
 )
FROM
 products;
 product_id |    product_name    | group_id | row_number
------------+--------------------+----------+------------
          2 | HTC One            |        1 |          1
          4 | iPhone             |        1 |          2
          1 | Microsoft Lumia    |        1 |          3
          3 | Nexus              |        1 |          4
          8 | Dell Vostro        |        2 |          1
          5 | HP Elite           |        2 |          2
          6 | Lenovo Thinkpad    |        2 |          3
          7 | Sony VAIO          |        2 |          4
          9 | iPad               |        3 |          1
         10 | Kindle Fire        |        3 |          2
         11 | Samsung Galaxy Tab |        3 |          3
(11 rows)
```

#### DISTINCT

以下查询使用 `ROW_NUMBER()` 函数将整数分配给 `products` 表中的[不同](content/postgresql-select-distinct.md)价格。

```sql
SELECT DISTINCT
 price,
 ROW_NUMBER () OVER (ORDER BY price)
FROM
 products
ORDER BY
 price;
  price  | row_number
---------+------------
  300.00 |          1
  300.00 |          2
  400.00 |          3
  500.00 |          4
  600.00 |          5
  600.00 |          6
  700.00 |          7
  800.00 |          8
  800.00 |          9
  900.00 |         10
 1100.00 |         11
(11 rows)
```

但是，结果不是预期的，因为它包含重复的价格。原因是 `ROW_NUMBER()` 在应用 `DISTINCT` 之前对结果集进行操作。

为了解决这个问题，我们可以在CTE中获得不同价格的列表，在外部查询中应用 `ROW_NUMBER()` 函数，如下所示：

```sql
WITH prices AS (
 SELECT DISTINCT
 price
 FROM
 products
) SELECT
 price,
 ROW_NUMBER () OVER (ORDER BY price)
FROM
 prices;
  price  | row_number
---------+------------
  300.00 |          1
  400.00 |          2
  500.00 |          3
  600.00 |          4
  700.00 |          5
  800.00 |          6
  900.00 |          7
 1100.00 |          8
(8 rows)
```

或者我们可以在 `FROM` 子句中使用子查询来获取唯一价格的列表，然后在外部查询中应用 `ROW_NUMBER()` 函数。

```sql
SELECT
 price,
 ROW_NUMBER () OVER (ORDER BY price)
FROM
 (
 SELECT DISTINCT
 price
 FROM
 products
 ) prices;
  price  | row_number
---------+------------
  300.00 |          1
  400.00 |          2
  500.00 |          3
  600.00 |          4
  700.00 |          5
  800.00 |          6
  900.00 |          7
 1100.00 |          8
(8 rows) 
```

#### 使用ROW_NUMBER()函数进行分页

在应用程序开发中，您使用分页技术显示行的子集而不是表中的所有行。

除了使用 [LIMIT]() 子句，您还可以使用 `ROW_NUMBER()` 函数进行分页。

例如，以下查询选择从第6行开始的五行：

```sql
SELECT
 *
FROM
 (
 SELECT
 product_id,
 product_name,
 price,
 ROW_NUMBER () OVER (ORDER BY product_name)
 FROM
 products
 ) x
WHERE
 ROW_NUMBER BETWEEN 6 AND 10;
 product_id |    product_name    |  price  | row_number
------------+--------------------+---------+------------
         10 | Kindle Fire        |  300.00 |          6
          6 | Lenovo Thinkpad    | 1100.00 |          7
          1 | Microsoft Lumia    |  300.00 |          8
          3 | Nexus              |  500.00 |          9
         11 | Samsung Galaxy Tab |  800.00 |         10
(5 rows)
```

#### 使用ROW_NUMBER()函数获取第n个最高/最低行

例如，要获得第三个最昂贵的产品，首先，我们从产品表中获取不同的价格，然后选择行号为3的价格。然后，在外部查询中，我们得到的产品价格等于第3个 最高的价格。

```sql
SELECT
 *
FROM
 products
WHERE
 price = (
 SELECT
 price
 FROM
 (
 SELECT
 price,
 ROW_NUMBER () OVER (ORDER BY price DESC) nth
 FROM
 (
 SELECT DISTINCT
 (price)
 FROM
 products
 ) prices
 ) sorted_prices
 WHERE
 nth = 3
 )
  product_id |    product_name    | price  | group_id
------------+--------------------+--------+----------
          4 | iPhone             | 800.00 |        1
         11 | Samsung Galaxy Tab | 800.00 |        3
(2 rows)
```

在本教程中，我们向您展示了如何使用PostgreSQL `ROW_NUMBER()` 函数将整数值分配给结果集中的行。