## PostgreSQL示例数据库

> 摘要：在本教程中，我们将向您介绍PostgreSQL示例数据库，您可以使用它来学习和练习PostgreSQL。

我们将使用DVD rental数据库来演示PostgreSQL的功能。

DVD rental数据库从PostgreSQL的[sakila示例数据库](http://dev.mysql.com/doc/sakila/en/)移植并进行了一些调整。DVD rental数据库代表DVD租赁商店的业务流程。DVD rental数据库有许多对象，包括：

- 15 tables
- 1 trigger
- 7 views
- 8 functions
- 1 domain
- 13 sequences

#### DVD rental 数据库ER模型 

![PostgreSQL Sample Database Diagram](/imgs/dvd-rental-sample-database-diagram.png)

#### PostgreSQL示例数据库表 

DVD rental数据库中有15张表：

- actor – 存储演员数据，包括名字和姓氏。
- film – 存储电影数据，如标题，发行年份，长度，评级等。
- film_actor – 存储电影和演员之间的关系。
- category – 存储电影的类别数据。
- film_category- 存储电影和类别之间的关系。
- store – 包含商店数据，包括经理人员和地址。
- inventory – 存储库存数据。
- rental – 商店租赁数据。
- payment – 存储客户的付款。
- staff – 存储员工数据。
- customer – 存储客户数据。
- address – 存储员工和客户的地址数据
- city – 存储城市名称。
- country – 存储国家/地区名称。

#### 下载PostgreSQL示例数据库

您可以通过以下链接下载PostgreSQL DVD Rental示例数据库：

[下载DVD rental示例数据库](https://gitlab.com/yonj1e/pgsql-tutorial/raw/master/docs/content/dvdrental.zip)

数据库文件采用zip格式（dvdrental.zip），因此在[将示例数据库加载到PostgreSQL数据库服务器](/content/load-postgresql-sample-database.md)之前，需要将其解压缩到dvdrental.tar。



!> 本教程向您介绍了名为dvdrental的示例数据库。 我们将在PostgreSQL教程中使用此数据库，因此，请确保在您的数据库服务器上拥有它。