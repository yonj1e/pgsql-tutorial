## PL/pgSQL变量

> 摘要：在本教程中，我们将向您展示如何使用各种方式声明PL/pgSQL变量。

PL/pgSQL变量是内存位置的有意义名称。变量包含可以通过块或函数更改的值。变量总是与特定的[数据类型](content/postgresql-data-types.md)相关联。

在使用变量之前，必须在[PL/pgSQL块](PL/pgSQL块)的声明部分声明它。以下说明了声明变量的语法。

```sql
variable_name data_type [:= expression];
```

首先，指定变量的名称。为变量分配有意义的名称是一种好习惯。例如，不是命名变量 `i`，而是使用 `index` 或 `counter`。

其次，将特定数据类型与变量相关联。它可以是任何有效的PostgreSQL数据类型，如 integer, numeric, varchar, char 等。

第三，您可以为变量分配默认值。这是可选的。如果未对变量设置默认值，则将变量的值初始化为 `NULL` 值。

以下示例说明了如何声明和初始化各种变量：

```sql
DO $$ 
DECLARE
 counter integer := 1;
 first_name varchar(50) := 'John';
 last_name varchar(50) := 'Doe';
 payment numeric(11,2) := 20.5;
BEGIN 
 RAISE NOTICE '% % % has been paid % USD', counter, first_name, last_name, payment;
END $$;
```

`counter` 变量是一个整数，并初始化为1

`first_name` 和 `last_name` 是 `VARCHAR`，长度为50个字符，并初始化为 `John` 和 `Do` 字符串。

`payment` 是一个2位小数，初始化为20.5

请注意，PostgreSQL计算默认值，并在输入块时将其分配给变量。见以下示例：

```sql
DO $$ 
DECLARE
 created_at time := now();
BEGIN 
 RAISE NOTICE '%', created_at;
END $$;
```

我们执行块并获得以下输出：

```sql
NOTICE:  16:03:33.526
```

现在，我们再次执行上面的块，`created_at` 变量的值更改为：

```sql
NOTICE:  16:04:00.052
```

因此，PostgreSQL会对 `now()` 函数求值，并在运行时将结果赋值给 `created_at` 变量，而不是块的编译时间。

#### 复制数据类型

PostgreSQL使您能够定义一个变量，其数据类型引用表中列的数据类型甚至是另一个变量。请参阅以下语法：

```sql
variable_name table_name.column_name%TYPE;
```

```sql
variable_name variable%TYPE;
```

例如，您可以定义名为 `city_name` 的变量，其名称与 `city` 表的 `name` 列具有相同的数据类型，如下所示：

```sql
city_name city.name%TYPE := 'San Francisco';
```

通过使用复制类型功能，您将获得以下优势：

- 首先，您不需要关心列的数据类型。您声明一个变量只是在查询中保存该列的值。
- 其次，当列的数据类型发生更改时，您无需更改函数中的变量声明以适应新的更改。
- 第三，您可以将变量类型引用到函数参数的数据类型以创建多态函数，因为内部变量的类型可以从一个调用更改为下一个调用。

#### 为变量分配别名

PostgreSQL允许您为任何变量定义别名，如下所示：

```sql
new_name ALIAS FOR old_name;
```

别名主要用于触发过程，以为具有预定名称（例如 `NEW` 或 `OLD`）的变量分配更有意义的名称。

在本教程中，您学习了如何使用PL/pgSQL中的各种技术声明变量。