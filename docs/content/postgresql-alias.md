## 别名

> 摘要：在本教程中，您将了解别名，包括列和表别名。

别名在[查询](content/postgresql-select.md)中为表或列分配临时名称。别名仅在执行查询期间存在。

#### 列别名

列别名的语法如下：

```sql
SELECT column_name AS alias_name ROM table;
```

在此语法中，`column_name` 被指定为别名 `asias_name`。`AS` 关键字是可选的，因此您可以跳过它，如下所示：

```sql
SELECT column_name alias_name FROM table;
```

除列名外，您还可以在 `SELECT` 子句中为表达式分配别名，如下所示：

```sql
SELECT expression alias_name FROM table;
```

列别名的主要目的是使查询的输出更有意义。

##### 示例

以下查询查找所有客户的全名：

```sql
SELECT first_name || ' ' || last_name 
FROM customer
ORDER BY first_name || ' ' || last_name;
       ?column?
-----------------------
 Aaron Selby
 Adam Gooch
 Adrian Clary
 Agnes Bishop
 Alan Kahn
 Albert Crouse
 Alberto Henning
 Alexander Fennell
 Alex Gresham
```

要使查询简短并使其输出更有意义，可以使用列别名，如下所示：

```sql
SELECT first_name || ' ' || last_name AS full_name
FROM customer
ORDER BY full_name;
       full_name
-----------------------
 Aaron Selby
 Adam Gooch
 Adrian Clary
 Agnes Bishop
 Alan Kahn
 Albert Crouse
 Alberto Henning
 Alexander Fennell
 Alex Gresham
 Alfred Casillas
```

因为PostgreSQL在 `SELECT`子句之后计算 `ORDER BY` 子句，所以可以在 `ORDER BY` 子句中使用列别名。

对于在 `SELECT` 子句之前评估的其他子句（如 `WHERE`，`GROUP BY` 和 `HAVING`），您不能在这些子句中使用列别名。

#### 表别名

The following illustrates the syntax of the table alias:

表别名的语法如下：

```sql
SELECT column_list
FROM table_name AS alias_name;
```

与列别名类似，表别名语法中的 `AS` 关键字也是可选的。

请注意，您还可以在[视图](content/postgresql-views.md)中使用表别名。

表别名有几种用途。

首先，如果必须使用长表名限定列名，则可以使用表别名来减少按键并使查询更具可读性。

例如，而不是使用以下查询：

```sql
SELECT a_very_long_table_name.column_name FROM a_very_long_table_name;
```

使用表别名，如下所示：

```sql
SELECT t.column_name FROM a_very_long_table_name t;
```

在此示例中，`t` 是 `a_very_long_table_name` 的表别名。

表别名的实际用途是从多个具有相同列名的表中查询数据时。 在这种情况下，您必须使用表名限定列，如下所示：

```sql
SELECT table_name1.column_name, table_name2.column_name
FROM table_name1
INNER JOIN table_name2 ON join_predicate;
```

要简化查询，可以使用表别名来表示 `FROM` 和 `INNER JOIN` 子句中列出的表名：

```sql
SELECT t1.column_name, t2.column_name
FROM table_name1 t1
INNER JOIN table_name2 t2 ON join_predicate;
```

请注意，您将在下一个教程中了解 `INNER JOIN` 子句。

其次，当您将表连接到自身a.k.a自联接时，您必须使用表别名。 因为PostgreSQL不允许您在查询中多次引用同一个表。

```sql
SELECTcolum_list
FROM table_name table_alias
INNER JOIN table_name ON join_predicate;
```

在本教程中，您学习了如何使用别名在执行查询期间临时为列和表分配新名称。