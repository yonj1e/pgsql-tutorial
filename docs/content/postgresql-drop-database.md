## DROP DATABASE

> 摘要：在本教程中，您将学习如何使用PostgreSQL `DROP DATABASE` 语句删除现有数据库。

#### 简介

不再需要数据库后，可以使用 `DROP DATABASE` 语句将其删除。以下说明了 `DROP DATABASE` 语句的语法：

```sql
DROPD  DATABASE [IF EXISTS] name;
```

要删除数据库：

- 在 `DROP DATABASE` 子句后指定要删除的数据库的名称。
- 使用 `IF EXISTS` 防止错误删除不存在的数据库。PostgreSQL会发出通知。

`DROP DATABASE` 语句永久删除目录条目和数据目录。此操作无法撤消，因此您必须谨慎使用它。

只有数据库所有者才能执行 `DROP DATABASE` 语句。此外，如果存在与数据库的任何活动连接，则无法执行 `DROP DATABASE` 语句。您必须连接到另一个数据库，例如 `postgresql` 以执行 `DROP DATABASE` 语句。

PostgreSQL还提供了一个名为`dropdb` 的实用程序，允许您删除数据库。`dropdb` 程序在后台执行 `DROP DATABASE` 语句。

##### 删除具有活动连接的数据库

要删除仍具有活动连接的数据库，可以按照以下步骤操作：

首先，找到针对目标数据库发生的活动，您可以查询 `pg_stat_activity` 视图：

```sql
SELECT *
FROM pg_stat_activity
WHERE datname = 'target_database';
```

其次，通过发出以下查询来终止活动连接：

```sql
SELECT pg_terminate_backend (pg_stat_activity.pid)
FROM pg_stat_activity
WHERE pg_stat_activity.datname = 'target_database';
```

请注意，如果您使用PostgreSQL版本9.1或更早版本，请使用 `procpid` 列而不是 `pid` 列，因为PostgreSQL从版本9.2开始将 `procid` 列更改为 `pid` 列。

第三，执行 `DROP DATABASE` 语句：

```sql
DROP DATABASE target_database;
```

#### 示例

我们将使用PostgreSQL创[建数据库教程](content/postgresql-create-database.md)中创建的数据库进行演示。如果您没有这些数据库，可以通过执行以下语句来创建它们：

```sql
CREATE DATABASE hrdb;
CREATE DATABASE testdb1;
```

##### 删除没有活动连接示例的数据库

要删除 `hrdb` 数据库，请使用 `hrdb` 所有者连接到 `hrdb` 数据库以外的数据库，例如，`postgres` 并发出以下语句：

```sql
DROP DATABASE hrdb;
```

PostgreSQL删除了 `hrdb` 数据库。

##### 删除具有活动连接的数据库

以下语句删除 `testdb1` 数据库：

```sql
DROP DATABASE testdb1;
```

但是，PostgreSQL发出如下错误：

```sql
ERROR: database "testdb1" is being accessed by other users
SQL state: 55006
Detail: There is 1 other session using the database.
```

要删除 `testdb1` 数据库，您需要按照上一节中的步骤进行操作。

首先，查询 `pg_stat_activity` 视图以查找针对 `testdb1` 数据库发生的活动：

```sql
SELECT *
FROM pg_stat_activity
WHERE datname = 'testdb1';
-[ RECORD 1 ]----+------------------------------
datid            | 25321
datname          | testdb1
pid              | 30665
usesysid         | 10
usename          | postgres
application_name | psql
client_addr      | 127.0.0.1
```

`testdb1` 数据库有一个来自 `localhost` 的连接，因此终止该连接并删除数据库是安全的。

其次，使用以下语句终止到 `testdb1` 数据库的连接：  

```sql
SELECT pg_terminate_backend (pg_stat_activity.pid)
FROM pg_stat_activity
WHERE pg_stat_activity.datname = 'testdb1';
```

Third, issue the `DROP DATABASE` command to remove the `testdb1`database:

第三，发出 `DROP DATABASE` 命令以删除 `testdb1` 数据库：

```sql
DROP DATABASE testdb1;
```

PostgreSQL永久删除了 `testdb1`。

在本教程中，您学习了如何使用PostgreSQL `DROP DATABASE`语句删除数据库。此外，您还学习了如何删除具有活动连接的数据库。