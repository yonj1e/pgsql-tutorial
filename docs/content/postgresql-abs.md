## ABS

`ABS()` 函数返回数字的绝对值。

#### 语法

以下说明了 `ABS()` 函数的语法：

```
ABS(numeric_expression)
```

#### 参数

`ABS()` 函数需要一个参数：

**1) numeric_expression**

numeric_expression可以是数字或数值表达式，其值为数字。

#### 返回值

`ABS()` 函数返回一个值，其[数据类型](content/postgresql-data-types.md)与输入参数相同。

#### 示例

以下示例显示如何使用 `ABS()` 函数计算数字的绝对值：

```
SELECT ABS(-10.25);
  abs  
-------
 10.25
(1 row)
```

以下语句 `ABS()` 函数中使用表达式：

```
SELECT ABS( 100 - 250 );
 abs 
-----
 150
(1 row)
```

除了 `ABS()` 函数，您还可以使用绝对运算符 @，例如：

```
SELECT @ -15;
 ?column? 
----------
       15
(1 row)
```

在本教程中，您学习了如何使用 `ABS()` 函数来计算数字的绝对值。