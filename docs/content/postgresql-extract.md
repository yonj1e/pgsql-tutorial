## EXTRACT

`EXTRACT()` 函数从日期/时间值中检索诸如年，月，日等字段。

#### 语法

以下说明了 `EXTRACT()` 函数的语法：

```sql
EXTRACT(field FROM source)
```

#### 参数

`EXTRACT()` 函数需要两个参数：

**1) field**

`field` 参数指定从日期/时间值中提取的字段。

下表说明了有效的字段值：

| Field Value     | TIMESTAMP                                                    | Interval                                    |
| --------------- | ------------------------------------------------------------ | ------------------------------------------- |
| CENTURY         | The century                                                  | The number of centuries                     |
| DAY             | The day of the month (1-31)                                  | The number of days                          |
| DECADE          | The decade that is the year divided by 10                    | Sames as TIMESTAMP                          |
| DOW             | The day of week Sunday (0) to Saturday (6)                   | N/A                                         |
| DOY             | The day of year that ranges from 1 to 366                    | N/A                                         |
| EPOCH           | The number of seconds since 1970-01-01 00:00:00 UTC          | The total number of seconds in the interval |
| HOUR            | The hour (0-23)                                              | The number of hours                         |
| ISODOW          | Day of week based on ISO 8601 Monday (1) to Saturday (7)     | N/A                                         |
| ISOYEAR         | ISO 8601 week number of year                                 | N/A                                         |
| MICROSECONDS    | The seconds field, including fractional parts, multiplied by 1000000 | Sames as TIMESTAMP                          |
| MILLENNIUM      | The millennium                                               | The number of millennium                    |
| MILLISECONDS    | The seconds field, including fractional parts, multiplied by 1000 | Sames as TIMESTAMP                          |
| MINUTE          | The minute (0-59)                                            | The number of minutes                       |
| MONTH           | Month, 1-12                                                  | The number of months, modulo (0-11)         |
| QUARTER         | Quarter of the year                                          | The number of quarters                      |
| SECOND          | The second                                                   | The number of seconds                       |
| TIMEZONE        | The timezone offset from UTC, measured in seconds            | N/A                                         |
| TIMEZONE_HOUR   | The hour component of the time zone offset                   | N/A                                         |
| TIMEZONE_MINUTE | The minute component of the time zone offset                 | N/A                                         |
| WEEK            | The number of the ISO 8601 week-numbering week of the year   | N/A                                         |
| YEAR            | The year                                                     | Sames as TIMESTAMP                          |

**2) source**

`source` 是 `TIMESTAMP` 或 `INTERVAL` 类型的值。如果传递 `DATE` 值，函数会将其转换为 `TIMESTAMP` 值。

#### 返回值

`EXTRACT()` 函数返回一个双精度值。

#### 示例

**A) TIMESTAMP**

从时间戳中提取年份：

```sql
SELECT EXTRACT(YEAR FROM TIMESTAMP '2016-12-31 13:30:15');
 date_part
-----------
      2016
(1 row)
```

从时间戳中提取季度：

```sql
SELECT EXTRACT(QUARTER FROM TIMESTAMP '2016-12-31 13:30:15');
 date_part
-----------
         4
(1 row)
```

从时间戳中提取月份：

```sql
SELECT EXTRACT(MONTH FROM TIMESTAMP '2016-12-31 13:30:15');
 date_part
-----------
        12
(1 row)
```

从时间戳中提取日期：

```sql
SELECT EXTRACT(DAY FROM TIMESTAMP '2016-12-31 13:30:15');
 date_part
-----------
        31
(1 row)
```

从时间戳中提取世纪：

```sql
SELECT EXTRACT(CENTURY FROM TIMESTAMP '2016-12-31 13:30:15');
 date_part
-----------
        21
(1 row)
```

从时间戳中提取十年：

```sql
SELECT EXTRACT(DECADE FROM TIMESTAMP '2016-12-31 13:30:15');
 date_part
-----------
       201
(1 row)
```

从时间戳中提取星期几：

```sql
SELECT EXTRACT(DOW FROM TIMESTAMP '2016-12-31 13:30:15');
 date_part
-----------
         6
(1 row)
```

从时间戳中提取一年中的某一天：

```sql
SELECT EXTRACT(DOY FROM TIMESTAMP '2016-12-31 13:30:15');
 date_part
-----------
       366
(1 row)
```

从时间戳中提取纪元：

```sql
SELECT EXTRACT(EPOCH FROM TIMESTAMP '2016-12-31 13:30:15');
 date_part
------------
 1483191015
(1 row)
```

从时间戳中提取小时：

```sql
SELECT EXTRACT(HOUR FROM TIMESTAMP '2016-12-31 13:30:15');
 date_part
-----------
        13
(1 row)
```

从时间戳中提取分钟：

```sql
SELECT EXTRACT(MINUTE FROM TIMESTAMP '2016-12-31 13:30:15');
 date_part
-----------
        30
(1 row)
```

从时间戳中提取秒钟：

```sql
SELECT EXTRACT(SECOND FROM TIMESTAMP '2016-12-31 13:30:15.45');
 date_part
-----------
     15.45
(1 row)
```

结果包括秒和小数秒。

根据ISO 8601提取工作日：

```sql
SELECT EXTRACT(ISODOW FROM TIMESTAMP '2016-12-31 13:30:15');
 date_part
-----------
         6
(1 row)
```

从时间戳中提取毫秒：

```sql
SELECT EXTRACT(MILLISECONDS FROM TIMESTAMP '2016-12-31 13:30:15');
 date_part
-----------
     15000
(1 row)
```

从时间戳中提取微秒：

```sql
SELECT EXTRACT(MICROSECONDS FROM TIMESTAMP '2016-12-31 13:30:15');
 date_part
-----------
  15000000
(1 row)
```

**B) interval**

从间隔中提取年份：

```sql
SELECT EXTRACT(YEAR FROM INTERVAL '6 years 5 months 4 days 3 hours 2 minutes 1 second' );
 date_part
-----------
         6
(1 row)
```

从间隔中提取季度：

```sql
SELECT EXTRACT(QUARTER FROM INTERVAL '6 years 5 months 4 days 3 hours 2 minutes 1 second' );
 date_part
-----------
         2
(1 row)
```

从间隔中提取月份：

```sql
SELECT EXTRACT(MONTH FROM INTERVAL '6 years 5 months 4 days 3 hours 2 minutes 1 second' );
 date_part
-----------
         5
(1 row)
```

从间隔中提取日期：

```sql
SELECT EXTRACT(DAY FROM INTERVAL '6 years 5 months 4 days 3 hours 2 minutes 1 second' );
 date_part
-----------
         4
(1 row)
```

从间隔中提取小时：

```sql
SELECT EXTRACT(HOUR FROM INTERVAL '6 years 5 months 4 days 3 hours 2 minutes 1 second' );
 date_part
-----------
         3
(1 row)
```

从间隔中提取分钟：

```sql
SELECT EXTRACT(MINUTE FROM INTERVAL '6 years 5 months 4 days 3 hours 2 minutes 1 second' );
 date_part
-----------
         2
(1 row)
```

从间隔中提取第二个：

```sql
SELECT EXTRACT(SECOND FROM INTERVAL '6 years 5 months 4 days 3 hours 2 minutes 1 second' );
 date_part
-----------
         1
(1 row)
```

从间隔中提取毫秒：

```sql
SELECT EXTRACT(MILLISECONDS FROM INTERVAL '6 years 5 months 4 days 3 hours 2 minutes 1 second' );
 date_part
-----------
      1000
(1 row)
```

从间隔中提取微秒：

```sql
SELECT EXTRACT(MICROSECONDS FROM INTERVAL '6 years 5 months 4 days 3 hours 2 minutes 1 second' );
 date_part
-----------
   1000000
(1 row)
```

从间隔中提取十年：

```sql
SELECT EXTRACT(DECADE FROM INTERVAL '60 years 5 months 4 days 3 hours 2 minutes 1 second' );
 date_part
-----------
         6
(1 row)
```

从间隔中提取千年：

```sql
SELECT EXTRACT(MILLENNIUM FROM INTERVAL '1999 years 5 months 4 days 3 hours 2 minutes 1 second' );
 date_part
-----------
         1
(1 row)
```

从间隔中提取世纪：

```sql
SELECT EXTRACT(CENTURY FROM INTERVAL '1999 years 5 months 4 days 3 hours 2 minutes 1 second' );
 date_part
-----------
        19
(1 row)
```

在本教程中，您学习了如何从日期/时间或间隔值中提取字段。