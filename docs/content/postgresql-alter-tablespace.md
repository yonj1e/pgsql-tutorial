## 更改表空间

> 摘要：在本教程中，您将学习如何使用PostgreSQL ALTER TABLESPACE语句更改表空间的定义。

#### 简介

[创建表空间](content/postgresql-create-tablespace.md)后，可以使用 `ALTER TABLESPACE` 更改其定义，如下所示：

```sql
ALTER TABLESPACE action;
```

PostgreSQL提供了一些操作，例如重命名表空间名称，更改所有者和设置表空间的参数。

要更改表空间的名称，请使用以下语句：

```sql
ALTER TABLESPACE tablespace_name RENAME TO new_name;
```

要更改表空间的所有者，请使用以下语句：

```sql
ALTER TABLESPACE tablespace_name OWNER TO new_owner;
```

您还可以更改表空间的参数，包括 `seq_page_cost` 和 `random_page_cost`，它们指定从表空间中的表读取页面的成本。

```sql
ALTER TABLESPACE tablespace_name SET parameter = value;
```

要执行 `ALTER TABLESPACE` 语句，您必须是超级用户或表空间的所有者。

目前，PostgreSQL不支持更改表空间的位置。

请注意，`ALTER TABLESPACE` 语句是PostgreSQL对SQL标准的扩展。

#### 示例

以下语句将 `dvdrental` 表空间重命名为 `dvdrental_raid`：

```sql
ALTER TABLESPACE dvdrental RENAME TO dvdrental_raid;
```

要更改 `dvdrental_raid` 的所有者从` postgres` 改为 `hr`，请使用以下语句：

```sql
ALTER TABLESPACE dvdrental_raid OWNER to hr;
```

在本教程中，您学习了如何使用PostgreSQL ALTER TABLESPACE语句更改表空间的定义。