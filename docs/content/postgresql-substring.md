## Substring

> 摘要：在本教程中，我们将向您介绍从字符串中提取子字符串的 `substring` 函数。

#### 简介

`substring` 函数返回字符串的一部分。以下说明了 `substring` 函数的语法：

```sql
SUBSTRING ( string ,start_position , length )
```

让我们详细了解每个参数：

- string是一个字符串，其[数据类型](content/postgresql-data-types.md)为char，varchar，text等。
- start_position是一个整数，指定要提取子字符串的位置。如果 `start_position` 等于零，则子字符串从字符串的第一个字符开始。`start_position` 只能是正数。虽然在其他数据库系统（如MySQL）中， `substring` 函数可以接受负值。
- length是一个正整数，用于确定要从 `start_position` 开始的字符串中提取的字符数。如果 `start_position` 和 `length` 的总和大于字符串中的字符数，则 `substring` 函数返回从 `start_position` 开始的整个字符串。`length` 参数是可选的。如果省略 `length` 参数，则 `substring` 函数返回从 `start_position` 开始的整个字符串。

##### 示例

查看以下示例：

```sql
SELECT
 SUBSTRING ('PostgreSQL', 1, 8); -- PostgreS
SELECT
 SUBSTRING ('PostgreSQL', 8); -- SQL
```

在第一个语句中，我们提取一个长度为8的子字符串，它从 `PostgreSQL` 字符串的第一个字符开始。我们得到 `PostgreS` 作为结果。见下图：

![PostgreSQL substring function example](/imgs/PostgreSQL-substring-function-example.jpg)

在第二个语句中，我们提取从位置8开始的子字符串，并省略 `length` 参数。子字符串是一个从8开始的字符串，即 `SQL`。

![PostgreSQL substring function example with optional LENGH parameter](/imgs/PostgreSQL-substring-function-example-with-optional-LENGH-parameter.jpg)

PostgreSQL提供了子串函数的另一种语法，如下所示：

```sql
substring(string from start_position for length);
```

在这种形式中，PostgreSQL将三个参数放在一个中。请参阅以下示例：

```sql
SELECT
 SUBSTRING ('PostgreSQL' FROM 1 FOR 8); -- PostgreS
SELECT
 SUBSTRING ('PostgreSQL' FROM 8); -- SQL
```

结果与第一个例子中的结果相同。

在以下示例中，我们从 `customer` 表中查询数据。我们选择 `last_name` 和 `first_name` 列。我们通过提取 `first_name` 列的第一个字符来获取初始名称。

```sql
SELECT
 last_name,
 SUBSTRING( first_name, 1, 1 ) AS initial
FROM customer
ORDER BY last_name;
  last_name   | initial
--------------+---------
 Abney        | R
 Adam         | N
 Adams        | K
 Alexander    | D
 Allard       | G
 Allen        | S
 Alvarez      | C
 Anderson     | L
```

#### 提取匹配POSIX正则表达式的子字符串

除了SQL标准 `substring` 函数之外，PostgreSQL还允许您使用提取与[POSIX正则表达式](https://en.wikipedia.org/wiki/Regular_expression#POSIX_basic_and_extended)匹配的子字符串。以下说明了带有POSIX正则表达式的 `substring` 函数的语法：

```sql
SUBSTRING(string FROM pattern)
```

或者您可以使用以下语法：

```sql
SUBSTRING(string, pattern);
```

请注意，如果未找到匹配项，则 `substring` 函数将返回null值。如果模式包含任何括号，则 `substring` 函数返回与第一个带括号的子表达式匹配的文本。

以下示例从字符串中提取门牌号（最多4位，从0到9）：

```sql
SELECT
 SUBSTRING (
 'The house no. is 9001',
 '([0-9]{1,4})'
 ) as house_no;
  house_no
----------
 9001
(1 row)
```

#### 提取匹配SQL正则表达式的子字符串

除了POSIX正则表达式模式，您还可以使用SQL正则表达式模式使用以下语法从字符串中提取子字符串：

```sql
SUBSTRING(string FROM pattern FOR escape-character)
```

这种形式的 `substring` 函数接受三个参数：

- string是要提取子字符串的字符串。
- escape-character：转义字符。
- pattern是一种SQL正则表达式模式。它必须包含在转义字符后面跟一个双引号（`"`）。例如，如果 `＃` 是转义字符，则模式将是 `#"pattern#"`。此外，模式必须匹配整个字符串，否则子字符串 函数将失败并返回NULL值。

请参阅以下示例：

```sql
SELECT SUBSTRING (
 'PostgreSQL'
 FROM
 '%#"S_L#"%' FOR '#'
); -- SQL
 
SELECT SUBSTRING (
 'foobar'
 FROM
 '#"S_Q#"%' FOR '#'
); -- NULLL
```

PostgreSQL提供了另一个名为 `substr` 的函数，它具有与 `substring` 函数相同的功能。

在本教程中，我们向您展示了各种形式的 `substring` 函数，它们允许您根据起始位置和长度提取子字符串，或者基于正则表达式。