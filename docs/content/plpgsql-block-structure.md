## PL/pgSQL块结构

> 摘要：在本教程中，您将了解PL/pgSQL的块结构。您将编写并执行第一个PL/pgSQL块。

PL/pgSQL函数被组织成块。以下说明了PL/pgSQL中完整块的语法：

```sql
[ <<label>> ]
[ DECLARE
    declarations ]
BEGIN
    statements;
 ...
END [ label ];
```

让我们更详细地了解块结构：

- 每个块有两个部分，称为声明 declaration 和主体 body 部分。声明部分是可选的，而主体部分是必需的。该块以 `END` 关键字后面的分号（;）结束。
- 块可以在开头和结尾处具有可选标签 label。开头和结尾的标签必须相同。如果要在 `EXIT` 语句中使用块，或者要限定块中声明的变量的名称，则使用块标签。
- 声明部分是声明主体部分中使用的所有[变量](content/plpgsql-variables.md)的地方。声明部分中的每个语句都以分号（;）结束。
- 主体部分是您放置块的逻辑的地方。它包含任何有效的声明。主体部分中的每个语句也以分号（;）结尾。

#### 示例

以下示例说明了一个非常简单的块。

```sql
DO $$ 
<<first_block>>
DECLARE
  counter integer := 0;
BEGIN 
  counter := counter + 1;
  RAISE NOTICE 'The current value of counter is %', counter;
END first_block $$;
```

```sql
NOTICE:  The current value of counter is 1
```

请注意，`DO` 语句不属于该块。它用于执行匿名块。PostgreSQL从9.0版开始引入了 `DO` 语句。

在声明部分，我们声明了一个名为 `counter` 的变量并将其值设置为0。在body部分中，我们将计数器增加到1并使用 `RAISE NOTICE` 语句输出其值。`first_block` 标签仅用于演示目的。它在这个例子中什么都不做。

#### 子块

你可以把一个块放在另一个块的主体内。嵌套在另一个内部的这个块称为子块 subblock。包含子块的块称为外部块 outer block。

![PL/pgSQL Block Structure](/imgs/plpgsql-block-structure.png)

您经常使用子块来分组语句，以便可以将大块划分为更小和更逻辑的子块。子块中的变量可以将名称作为外部块中的变量，即使这不是一个好习惯。

当您在子块中定义一个与外部块中的变量同名的变量时，外部块中的变量将隐藏在子块中。如果要访问外部块中的变量，可以使用块标签来限定其名称; 请参阅以下示例：

```sql
DO $$ 
<<outer_block>>
DECLARE
  counter integer := 0;
BEGIN 
  counter := counter + 1;
  RAISE NOTICE 'The current value of counter is %', counter;
 
  DECLARE 
    counter integer := 0;
  BEGIN 
    counter := counter + 10;
    RAISE NOTICE 'The current value of counter in the subblock is %', counter;
    RAISE NOTICE 'The current value of counter in the outer block is %',
      outer_block.counter;
  END;
 
  RAISE NOTICE 'The current value of counter in the outer block is %', counter;
   
END outer_block $$;
```

```sql
NOTICE:  The current value of counter is 1
NOTICE:  The current value of counter in the subblock is 10
NOTICE:  The current value of counter in the outer block is 1
NOTICE:  The current value of counter in the outer block is 1
```

在这个例子中，我们首先在 `outer_block` 中声明了一个名为 `counter` 的变量。

接下来，在子块中，我们还声明了一个具有相同名称的变量。

然后，在进入子块之前，计数器的值是1。在子块中，我们将计数器的值增加到10并打印出来。请注意，更改仅影响子块中的计数器变量。

之后，我们使用块标签引用外部块中的计数器变量来限定其名称 `outer_block.counter`。

最后，我们在外部块中打印出值计数器变量，其值保持不变。

在本教程中，您已经了解了PL/pgSQL的块结构以及如何使用DO语句执行块。