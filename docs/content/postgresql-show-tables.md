## Show Tables

> 摘要：本教程向您展示了在PostgreSQL中使用 `psql` 工具和 `pg_catalog` 模式显示表的不同方法。

如果您来自MySQL，可能会错过显示特定数据库中所有表的[SHOW TABLES](http://www.mysqltutorial.org/mysql-show-tables/)语句。PostgreSQL不直接提供 `SHOW TABLES` 语句，但给你类似的东西。

#### 使用psql显示表

如果您使用的是[psql](content/psql-commands.md)，则可以使用以下命令在当前数据库中显示表。

```
\dt
```

例如，您可以连接到[dvdrental数据库](ontent/postgresql-sample-database.md)并显示所有表，如下所示：

```sql
dvdrental=# \dt
             List of relations
 Schema |     Name      | Type  |  Owner
--------+---------------+-------+----------
 public | actor         | table | postgres
 public | address       | table | postgres
 public | category      | table | postgres
 public | city          | table | postgres
 public | country       | table | postgres
 public | customer      | table | postgres
 public | film          | table | postgres
 public | film_actor    | table | postgres
 public | film_category | table | postgres
 public | inventory     | table | postgres
 public | language      | table | postgres
 public | payment       | table | postgres
 public | persons       | table | postgres
 public | rental        | table | postgres
 public | staff         | table | postgres
 public | store         | table | postgres
(16 rows)
```

#### 使用pg_catalog模式显示表

在PostgreSQL中显示表的另一种方法是使用[SELECT语句](content/postgresql-select.md)从系统表中查询数据，如下所示：

```sql
SELECT * FROM pg_catalog.pg_tables
WHERE 
schemaname != 'pg_catalog' AND schemaname != 'information_schema';
 schemaname |   tablename   | tableowner | tablespace | hasindexes | hasrules | hastriggers | rowsecurity
------------+---------------+------------+------------+------------+----------+-------------+-------------
 public     | staff         | postgres    |            | t          | f        | t           | f
 public     | category      | postgres    |            | t          | f        | t           | f
 public     | film_category | postgres    |            | t          | f        | t           | f
 public     | country       | postgres    |            | t          | f        | t           | f
 public     | actor         | postgres    |            | t          | f        | t           | f
 public     | language      | postgres    |            | t          | f        | t           | f
 public     | inventory     | postgres    |            | t          | f        | t           | f
 public     | payment       | postgres    |            | t          | f        | t           | f
 public     | rental        | postgres    |            | t          | f        | t           | f
 public     | city          | postgres    |            | t          | f        | t           | f
 public     | store         | postgres    |            | t          | f        | t           | f
 public     | film          | postgres    |            | t          | f        | t           | f
 public     | address       | postgres    |            | t          | f        | t           | f
 public     | film_actor    | postgres    |            | t          | f        | t           | f
 public     | customer      | postgres    |            | t          | f        | t           | f
 public     | categories    | postgres    |            | t          | f        | t           | f
 public     | products      | postgres    |            | t          | f        | t           | f
 public     | sales2007q1   | postgres    |            | f          | f        | f           | f
 public     | sales         | postgres    |            | t          | f        | f           | f
 public     | employee      | postgres    |            | t          | f        | t           | f
 public     | sales2007q2   | postgres    |            | f          | f        | f           | f
 public     | keys          | postgres    |            | t          | f        | t           | f
 public     | employees     | postgres    |            | t          | f        | t           | f
 public     | hipos         | postgres    |            | t          | f        | t           | f
 public     | departments   | postgres    |            | t          | f        | f           | f
 public     | t1            | postgres    |            | t          | f        | f           | f
 public     | t2            | postgres    |            | t          | f        | f           | f
(27 rows)
```

我们在[WHERE子句](content/postgresql-where.md)中使用条件来过滤系统表。如果省略 `WHERE` 子句，您将获得许多您可能不想要的系统表。

#### 相关教程

- [Show Databases](content/postgresql-show-databases.md)
- [List Users](content/postgresql-list-users.md)
- [Describe Table](content/postgresql-describe-table.md)