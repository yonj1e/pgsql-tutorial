## CUBE

> 摘要：在本教程中，您将学习如何使用 `CUBE` 生成多个分组集。

#### 简介

`CUBE` 是 `GROUP BY` 子句的子句。`CUBE` 允许您生成多个分组集。

分组集是要分组的一组列。有关分组集的更多信息，请查看 `GROUPING SETS` 教程。

以下说明了 `CUBE` 子句的语法：

```sql
SELECT c1, c2, c3, aggregate (c4)
FROM table_name
GROUP BY CUBE (c1, c2, c3);
```

在这个语法中：

- 首先，在 `SELECT` 语句的 `GROUP BY` 子句中指定 `CUBE` 子句。
- 其次，在选择列表中，指定要分析的列（维度或维度列）和[聚合函数](content/postgresql-aggregate-functions.md)表达式。
- 第三，在 `GROUP BY` 子句中，在 `CUBE` 子句的括号内指定维度列。

该查询基于 `CUBE` 中指定的度列生成所有可能的分组集。`CUBE` 子句是定义多个分组集的简短方法，因此以下内容是等效的：

```sql
CUBE(c1,c2,c3) 
 
GROUPING SETS (
    (c1,c2,c3), 
    (c1,c2),
    (c1,c3),
    (c2,c3),
    (c1),
    (c2),
    (c3), 
    ()
 ) 
```

通常，如果在 `CUBE` 中指定的列数为 `n`，那么您将具有2n个组合。

PostgreSQL允许您执行部分多维数据集以减少计算的聚合数。以下显示了语法：

```sql
SELECT c1, c2, c3, aggregate (c4)
FROM table_name
GROUP BY c1, CUBE (c1, c2);
```

#### 示例

我们将使用 `GROUPING SETS` 教程中创建的销售表进行演示。

```sql
select * from sales;
 brand | segment | quantity
-------+---------+----------
 ABC   | Premium |      100
 ABC   | Basic   |      200
 XYZ   | Premium |      100
 XYZ   | Basic   |      300
(4 rows)
```

以下查询使用 `CUBE` 子句生成多个分组集：

```sql
SELECT brand, segment, SUM (quantity)
FROM sales
GROUP BY CUBE (brand, segment)
ORDER BY brand, segment;
 brand | segment | sum
-------+---------+-----
 ABC   | Basic   | 200
 ABC   | Premium | 100
 ABC   |         | 300
 XYZ   | Basic   | 300
 XYZ   | Premium | 100
 XYZ   |         | 400
       | Basic   | 500
       | Premium | 200
       |         | 700
(9 rows)
```

以下查询执行部分多维数据集：

```sql
SELECT brand, segment, SUM (quantity)
FROM sales
GROUP BY brand, CUBE (segment)
ORDER BY brand, segment;
 brand | segment | sum
-------+---------+-----
 ABC   | Basic   | 200
 ABC   | Premium | 100
 ABC   |         | 300
 XYZ   | Basic   | 300
 XYZ   | Premium | 100
 XYZ   |         | 400
(6 rows)
```

在本教程中，您学习了如何使用 `CUBE` 生成多个分组集。