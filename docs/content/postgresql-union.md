## UNION

> 摘要：在本教程中，您将学习如何使用 `UNION` 运算符将多个查询的结果集合并为单个结果。

#### 简介

`UNION` 运算符将两个或多个 [SELECT](content/postgresql-select.md) 语句的结果集合并到一个结果集中。下面说明了组合来自两个查询的结果集的 `UNION` 运算符的语法：

```sql
SELECT column_1, column_2
FROM tbl_name_1
UNION
SELECT column_1, column_2
FROM tbl_name_2;
```

以下是应用于查询的规则：

- 两个查询都必须返回相同数量的列。
- 查询中的相应列必须具有兼容的数据类型。

除非使用 `UNION ALL`，否则 `UNION` 运算符将删除所有重复的行。

`UNION` 运算符可以将行放在第一个查询中，在第二个查询的结果集中的行之前，之后或之间。若要对指定列的组合结果集中的行进行排序，请使用 [ORDER BY](content/postgresql-order-by.md) 子句。

我们经常使用 `UNION` 运算符组合来自未完全标准化的类似表的数据。这些表通常位于报告或数据仓库系统中。

#### 示例

我们来看看下表：

- sales2007q1：存储2007年第一季度销售数据。
- sales2007q2：存储2007年第二季度销售数据。

sales2007q1数据：

```sql
select * from sales2007q1 ;
 name |  amount
------+-----------
 Mike | 150000.25
 Jon  | 132000.75
 Mary | 100000
(3 rows)
```

sales2007q2 data:

```sql
select * from sales2007q2 ;
 name |  amount
------+-----------
 Mike | 120000.25
 Jon  | 142000.75
 Mary | 100000
(3 rows)
```

##### UNION

我们使用 `UNION` 运算符组合两个表的数据，如下所示：

```sql
SELECT * FROM sales2007q1
UNION
SELECT * FROM sales2007q2;
 name |  amount
------+-----------
 Jon  | 142000.75
 Mike | 150000.25
 Mike | 120000.25
 Jon  | 132000.75
 Mary | 100000
(5 rows)
```

##### UNION ALL

组合结果集中有五行，因为 `UNION` 运算符会删除一个重复行。要获取包含重复的所有行，请使用 `UNION ALL` 运算符，如下所示：

```sql
SELECT * FROM sales2007q1
UNION ALL
SELECT * FROM sales2007q2;
 name |  amount
------+-----------
 Mike | 150000.25
 Jon  | 132000.75
 Mary | 100000
 Mike | 120000.25
 Jon  | 142000.75
 Mary | 100000
(6 rows)
```

##### ORDER BY

要对 `UNION` 运算符返回的组合结果进行排序，请使用 `ORDER BY` 子句。您需要将 `ORDER BY` 子句放在最后一个查询中，如下所示：

```sql
SELECT * FROM sales2007q1
UNION ALL
SELECT * FROM sales2007q2
ORDER BY  name ASC, amount DESC;
 name |  amount
------+-----------
 Jon  | 142000.75
 Jon  | 132000.75
 Mary | 100000
 Mary | 100000
 Mike | 150000.25
 Mike | 120000.25
(6 rows)
```

如果在每个查询的末尾放置 `ORDER BY` 子句，则组合的结果集将不会按预期排序。因为当 `UNION` 运算符组合每个查询的排序结果集时，它不保证最终结果集中的行顺序。

在本教程中，我们向您展示了如何使用 `UNION` 和 `UNION ALL` 将多个查询的结果集合并到一个结果集中。

#### 相关教程

- [EXCEPT](http://www.postgresqltutorial.com/postgresql-tutorial/postgresql-except/)
- [INTERSECT](http://www.postgresqltutorial.com/postgresql-intersect/)