## NOW

> 摘要：本教程向您展示如何使用 `NOW()` 函数获取带时区的日期和时间。

#### 简介

`NOW()` 函数返回当前日期和时间。 `NOW()` 函数的返回类型是[带时区的时间戳](content/postgresql-timestamp.md)。请参阅以下示例：

```sql
SELECT NOW();
              now
-------------------------------
2017-03-18 08:21:36.175627+07
(1 row)
```

请注意， `NOW()` 函数根据数据库服务器的时区设置返回当前日期和时间。

例如，如果我们将时区更改为 “America / Los_Angeles”：

```sql
SET TIMEZONE='America/Los_angeles';
```

并获取当前日期和时间：

```sql
SELECT NOW();
              now
-------------------------------
2017-03-17 18:29:21.758315-07
(1 row)
```

如您所见， `NOW()` 函数返回的值将调整为新的时区。

如果您想获得没有时区的当前日期和时间，可以按如下方式明确地转换它：

```sql
SELECT NOW()::timestamp;
            now
----------------------------
2017-03-17 18:37:29.229991
(1 row)
```

可以使用常见的 DATE 和 TIME 操作符来处理 `NOW()` 函数。例如，从现在开始的1小时内：

```sql
SELECT (NOW() + interval '1 hour') AS an_hour_later;
         an_hour_later
-------------------------------
2017-03-17 19:42:37.110567-07
(1 row)
```

要获得明天的这个时间，在当前时间的基础上再加上1天：

```sql
SELECT (NOW() + interval '1 hour') AS this_time_tomorrow;
      this_time_tomorrow
-------------------------------
2017-03-17 19:43:35.178882-07
(1 row)
```

要获得2小时30分钟前的数据，可以使用负(-)运算符，如下所示：

```sql
SELECT now() - interval '2 hours 30 minutes' AS two_hour_30_min_go; 
      two_hour_30_min_go
-------------------------------
2017-03-17 16:17:07.742688-07
(1 row)
```

#### NOW() 相关函数

除了 `NOW()` 函数，您还可以使用 `CURRENT_TIME` 或 `CURRENT_TIMESTAMP` 来获取时区的当前日期和时间：

```sql
SELECT CURRENT_TIME, CURRENT_TIMESTAMP;
       timetz       |              now
--------------------+-------------------------------
18:50:51.191353-07 | 2017-03-17 18:50:51.191353-07
(1 row)
```

要获取没有时区的当前日期和时间，请使用 `LOCALTIME` 和 `LOCAL TIMESTAMP` 函数。

```sql
SELECT LOCALTIME, LOCALTIMESTAMP;
      time       |         timestamp
-----------------+----------------------------
19:13:41.423371 | 2017-03-17 19:13:41.423371
(1 row)
```

请注意，`NOW()` 及其相关函数返回当前事务的开始时间。换句话说，函数调用的返回值在事务中是相同的。

以下示例说明了这一概念：

```sql
postgres=# BEGIN;
BEGIN
postgres=# SELECT now();
              now
-------------------------------
2017-03-17 19:21:43.049715-07
(1 row)
 
 
postgres=# SELECT pg_sleep(3);
pg_sleep
----------
 
(1 row)
 
 
postgres=# SELECT now();
              now
-------------------------------
2017-03-17 19:21:43.049715-07
(1 row)
 
 
postgres=# COMMIT;
COMMIT
```

在此示例中，我们在事务中调用 `NOW()` 函数，您可以看到它的返回值不会通过事务更改。

请注意，`pg_sleep()` 函数会暂停当前会话的进程睡眠指定的秒数。

如果要获取事务期间提前的当前日期和时间，可以使用 `TIMEOFDAY()` 函数。请考虑以下示例：

```sql
SELECT
    TIMEOFDAY(),
    pg_sleep(5),
    TIMEOFDAY();
              timeofday              | pg_sleep |              timeofday
-------------------------------------+----------+-------------------------------------
Fri Mar 17 19:36:09.216064 2017 PDT |          | Fri Mar 17 19:36:14.217636 2017 PDT
(1 row)
```

如您所见，暂停5秒后，当前日期和时间会增加。

#### NOW() 默认值

您可以使用 `NOW()` 函数作为表的列的默认值。请参阅以下示例：

首先，创建一个带有 `created_at` 列的新表 `posts`，该列具有 `NOW()` 函数提供的默认值：

```sql
CREATE TABLE posts (
     id         SERIAL PRIMARY KEY,
     title      VARCHAR NOT NULL,
     created_at TIMESTAMPTZ DEFAULT Now()
);
```

其次，在 `posts` 表中[插入](content/postgresql-insert.md)一个新行：

```sql
INSERT INTO posts (title) VALUES ('PostgreSQL NOW function');
```

第三，从 `posts` 表中查询数据：

```sql
SELECT * FROM posts;
id |          title          |          created_at
----+-------------------------+-------------------------------
  1 | PostgreSQL NOW function | 2017-03-18 09:41:26.208497+07
(1 row)
```

即使我们没有为 `created_at` 列提供值，该语句也使用 `NOW()` 函数为该列返回的值。

在本教程中，您学习了如何使用 `NOW()` 函数来获取时区的当前日期和时间。