## LIMIT

> 摘要：本教程介绍如何使用 `LIMIT` 子句获取查询生成的行的子集。

#### 简介

`LIMIT` 是 [`SELECT`](content/postgresql-select.md) 语句的可选子句，它获取查询返回的行的子集。

以下说明了 `LIMIT` 子句的语法：

```sql
SELECT * ROM table_name IMIT n;
```

该语句返回查询生成的n行。 如果n为零或NULL，则查询返回与省略 `LIMIT` 子句相同的结果集。

如果要在返回n行之前跳过多行，可以在 `LIMIT`子句后面使用 `OFFSET` 子句，如下语句：

```sql
SELECT * FROM table LIMIT n OFFSET m;
```

在返回查询生成的n行之前，语句首先跳过m行。 如果m为零，则该语句将像没有 `OFFSET` 子句一样工作。

由于数据库表中行的顺序是不可预测的，因此在使用 `LIMIT` 子句时，应始终使用 [ORDER BY](content/postgresql-order-by.md) 子句来控制行的顺序。 如果您不这样做，您将得到一个不可预测的结果集。

如果 `OFFSET` 较大，它可能效率不高，因为PostgreSQL必须计算数据库服务器内 `OFFSET` 跳过的行，即使没有返回跳过的行。

#### 示例

让我们举一些例子来更好地理解使用 `LIMIT`子句。 我们将使用[示例数据库](content/postgresql-sample-database.md)中的 `film` 表进行演示。

![PostgreSQL LIMIT Sample Table](/imgs/film-table.png)

要获得由 `film_id` 订购的前5部电影，请使用以下查询：

```sql
SELECT film_id, title, release_year
FROM film 
ORDER BY film_id
LIMIT 5;
 film_id |      title       | release_year
---------+------------------+--------------
       1 | Academy Dinosaur |         2006
       2 | Ace Goldfinger   |         2006
       3 | Adaptation Holes |         2006
       4 | Affair Prejudice |         2006
       5 | African Egg      |         2006
(5 rows)
```

要从由 `film_id` 订购的第三张电影开始检索4部电影，您可以使用 `LIMIT` 和`OFFSET` 子句，如下所示：

```sql
SELECT film_id, title, release_year
FROM film
ORDER BY film_id
LIMIT 4 OFFSET 3;
 film_id |      title       | release_year
---------+------------------+--------------
       4 | Affair Prejudice |         2006
       5 | African Egg      |         2006
       6 | Agent Truman     |         2006
       7 | Airplane Sierra  |         2006
(4 rows)
```

我们经常使用 `LIMT` 子句来获取表中最高或最低项的数量。例如，要获得最贵的10部电影，你可以按租金按降序排序，并使用 `LIMIT` 子句获得前10部电影。查询如下：

```sql
SELECT film_id, title, rental_rate
FROM film
ORDER BY rental_rate DESC
LIMIT 10;
 film_id |        title        | rental_rate
---------+---------------------+-------------
      13 | Ali Forever         |        4.99
      20 | Amelie Hellfighters |        4.99
       7 | Airplane Sierra     |        4.99
      10 | Aladdin Calendar    |        4.99
       2 | Ace Goldfinger      |        4.99
       8 | Airport Pollock     |        4.99
      98 | Bright Encounters   |        4.99
     133 | Chamber Italian     |        4.99
     384 | Grosse Wonderful    |        4.99
      21 | American Circus     |        4.99
(10 rows)
```

在本教程中，您学习了如何使用 `LIMIT` 子句来检索查询返回的行的子集。

#### 相关教程

- [SELECT](content/postgresql-select.md)
- [ORDER BY](content/postgresql-order-by.md)