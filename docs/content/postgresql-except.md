## EXCEPT

> 摘要：在本教程中，您将学习如何使用 `EXCEPT` 运算符返回第一个查询中未出现在第二个查询的输出中的行。

#### 简介

与 [UNION](content/postgresql-union.md) 和 `INTERSECT` 运算符一样，`EXCEPT` 运算符通过比较两个或多个查询的结果集来返回行。

`EXCEPT` 运算符返回第一个（左）查询中不在第二个（右）查询输出中的不同行。以下说明了 `EXCEPT` 运算符的语法。

```sql
SELECT column_list
FROM A
WHERE condition_a
EXCEPT 
SELECT column_list
FROM B
WHERE condition_b;
```

要使用 `EXCEPT` 运算符组合查询，必须遵守以下规则：

- 两个查询中的列数及其顺序必须相同。
- 各列的数据类型必须兼容。

以下维恩图说明了适用于A和B表的 `EXCEPT` 运算符的结果。

![PostgreSQL EXCEPT](/imgs/PostgreSQL-EXCEPT-300x202.png)

#### 示例

我们来看看[示例数据库](content/postgresql-sample-database.md)的 `film` 和 `inventory`表。

以下查询返回电影表中的电影。

```sql
SELECT film_id, title
FROM film
ORDER BY title;
 film_id |            title
---------+-----------------------------
       1 | Academy Dinosaur
       2 | Ace Goldfinger
       3 | Adaptation Holes
       4 | Affair Prejudice
       5 | African Egg
       6 | Agent Truman
       7 | Airplane Sierra
       8 | Airport Pollock
       9 | Alabama Devil
      10 | Aladdin Calendar
      11 | Alamo Videotape
      12 | Alaska Phantom
      14 | Alice Fantasia
      15 | Alien Center
      13 | Ali Forever
      16 | Alley Evolution
```

以下查询返回库存中的电影：

```sql
SELECT distinct inventory.film_id, title
FROM inventory
INNER JOIN film ON film.film_id = inventory.film_id
ORDER BY title;
 film_id |            title
---------+-----------------------------
       1 | Academy Dinosaur
       2 | Ace Goldfinger
       3 | Adaptation Holes
       4 | Affair Prejudice
       5 | African Egg
       6 | Agent Truman
       7 | Airplane Sierra
       8 | Airport Pollock
       9 | Alabama Devil
      10 | Aladdin Calendar
      11 | Alamo Videotape
      12 | Alaska Phantom
      15 | Alien Center
      13 | Ali Forever
      16 | Alley Evolution
      17 | Alone Trip
```

两个查询都返回一个包含两列的结果集：`film_id` 和 `title`。

要获取不在库存中的电影，请使用 `EXCEPT` 运算符，如下所示：

```sql
SELECT film_id, title
FROM film
EXCEPT
SELECT DISTINCT inventory.film_id, title
FROM inventory
INNER JOIN film ON film.film_id = inventory.film_id
ORDER BY title;
 film_id |         title
---------+------------------------
      14 | Alice Fantasia
      33 | Apollo Teen
      36 | Argonauts Town
      38 | Ark Ridgemont
      41 | Arsenic Independence
      87 | Boondock Ballroom
     108 | Butch Panther
     128 | Catch Amistad
     144 | Chinatown Gladiator
     148 | Chocolate Duck
     171 | Commandments Express
     192 | Crossing Divorce
     195 | Crowds Telemark
     198 | Crystal Breaking
     217 | Dazed Punk
     221 | Deliverance Mulholland
     318 | Firehouse Vietnam
```

请注意，我们在语句末尾使用了 [ORDER BY](content/postgresql-order-by.md) 子句，以按照标题对电影进行排序。如果在每个查询中放置 `ORDER BY` 子句，则可能不会对最终结果进行排序，因为每个查询将按标题列对结果集进行排序，然后将 `EXCEPT` 运算符应用于这两个查询。

在本教程中，我们向您展示了如何使用 `EXCEPT` 运算符组合两个查询以获取第一个查询中未出现在第二个查询的输出中的行。

#### 相关教程

- [UNION](http://www.postgresqltutorial.com/postgresql-union/)
- [INTERSECT](http://www.postgresqltutorial.com/postgresql-intersect/)