## FORMAT

`FORMAT()` 函数根据格式字符串格式化参数。

如果您使用过C编程语言，您会发现 `FORMAT()` 函数类似于 `sprintf()` 函数。

#### 语法

`FORMAT()` 函数的语法如下：

```sql
FORMAT(format_string [, format_arg [, ...] ])
```

`FORMAT()` 函数是可变参数，因此，您可以将参数传递给标记为 `VARIADIC` 关键字的数组。

`FORMAT()` 函数将数组的元素视为普通参数，将NULL视为零元素数组。

#### 参数

**1) format_string**

`format_string` 参数是一个字符串，它指定应如何格式化结果字符串。

`format_string` 由文本和格式说明符组成。将文本直接复制到结果字符串，而格式说明符是要插入到结果字符串中的参数的占位符。

以下显示格式说明符的语法：

```
%[position][flags][width]type
```

格式说明符以 `％` 字符开头。它有三个可选`position`, `flags`, `with` 选项，并且具有所需的类型 `type`。

**position**

`position` 指定要在结果字符串中插入的参数。该位置的形式为 `$n`，其中 `n` 是参数索引。第一个参数从1开始。

如果省略 `position` 选项，则默认值为列表中的下一个参数。

**flags**

目前，标志可以接受一个减号（` - `），指示格式说明符的输出是左对齐的。

`flags` 选项仅在指定 `width` 字段时生效。

**width**

可选的 `width` 字段指定用于显示格式说明符输出的最小字符数。

结果字符串可以向左或向右填充，并填充 `width` 所需的空格。

如果 `width` 太小，输出将按原样显示，没有任何截断。

 `width` 可以是以下值之一：

- 正整数。
- 星号（*）使用下一个函数参数作为宽度。
- 使用第n个函数参数作为宽度的 `*n$` 形式的字符串。

**type**

`type` 是用于生成格式说明符输出的格式转换类型。

`type` 参数的允许值如下：

- `s` 将参数值格式化为字符串。NULL被视为空字符串。
- `I` 将参数值视为SQL标识符。
- `L` 引用参数值作为SQL文字。

我们经常使用 `I` 和 `L` 来构造动态SQL语句。

如果要在结果字符串中包含 `％`，请使用 `%%`

**2) format_arg**

如前所述，您将许多格式参数传递给 `FORMAT()` 函数。

#### 返回值

`FORMAT()` 函数返回格式化的字符串。

#### 示例

我们来看一些使用 `FORMAT()` 函数的例子。

**A) 简单的格式**

以下语句使用 `FORMAT()` 函数格式化字符串：

```sql
SELECTS  FORMAT('Hello, %s','PostgreSQL');
      format
-------------------
 Hello, PostgreSQL
(1 row)
```

在此示例中，`％s` 由 `'PostgreSQL'` 字符串参数替换。

请参阅[示例数据库](content/postgresql-sample-database.md)中的以下 `customer` 表。

![customer table](/imgs/customer-table.png)

以下语句使用 `FORMAT()` 函数从名字和姓氏构造客户的全名：

```sql
SELECT 
 FORMAT('%s, %s',last_name, first_name) full_name 
FROM customer;
ORDER BY full_name;
       full_name
------------------------
 Ely, Jared
 Smith, Mary
 Johnson, Patricia
 Williams, Linda
 Jones, Barbara
 Brown, Elizabeth
 Davis, Jennifer
 Miller, Maria
 Wilson, Susan
 Moore, Margaret
 Taylor, Dorothy
 Anderson, Lisa
 Thomas, Nancy
 Jackson, Karen
 White, Betty
 Harris, Helen
 Martin, Sandra
 Thompson, Donna
```

在此示例中，我们使用了两个格式说明符 `％s％s` ，然后将其替换为 `first_name` 和 `last_name` 列中的值。

**B) 使用标志选项示例**

以下语句显示如何使用标志和格式说明符中的选项：

```sql
SELECT FORMAT('|%10s|', 'one');
    format
--------------
 |       one|
(1 row)
```

输出字符串左边用空格填充，右对齐。

要使其左对齐，您使用 `-` 作为标志：

```sql
SELECT FORMAT('|%-10s|', 'one');
    format
--------------
 |one       |
(1 row)
```

**C) 使用位置选项示例**

此示例显示如何使用格式说明符的位置选项：

```sql
SELECT FORMAT('%1s apple, %2s orange, %1$s banana', 'small', 'big');
```

在这个例子中，我们有两个参数，分别是 `'small'` 和 `'big'` 字符串。

`1$` 和 `2$` 位置指示 `FORMAT()` 函数将第一个（`'smaill'`）和第二个参数（`'big'`）注入相应的占位符。

`1$` 位置在格式字符串中出现两次，因此，第一个参数也会插入两次。

在本教程中，您学习了如何使用 `FORMAT()` 函数根据格式字符串格式化参数。