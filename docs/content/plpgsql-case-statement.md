## PL/pgSQL CASE语句

> 摘要：在本教程中，您将了解基于特定条件执行命令的PL/pgSQL CASE语句。

除了[IF语句](content/plpgsql-if-else-statements.md)之外，PostgreSQL还为您提供了 `CASE` 语句，允许您有条件地执行代码块。`CASE` 语句有两种形式：简单和搜索CASE语句。simple and searched CASE statements

请注意，您不应该混淆 `CASE` 语句和[CASE表达式](content/postgresql-case.md)。`CASE` 表达式求值为一个值，而 `CASE` 语句则根据条件执行语句。

#### 简单的CASE

让我们从简单CASE语句的语法开始。

```sql
CASE search-expression
   WHEN expression_1 [, expression_2, ...] THEN
      when-statements
  [ ... ]
  [ELSE
      else-statements ]
END CASE;
```

`search-expression` 是一个表达式，它将使用相等操作数（=）使用每个 `WHEN` 分支中的表达式进行求值。如果找到匹配项，则执行相应 `WHEN` 分支中的 `when-statements`。下面的后续表达式将不会被执行。

如果未找到匹配项，则执行 `ELSE` 分支中的 `else-statements`。`ELSE` 分支是可选的。如果找不到匹配且没有 `ELSE` 分支，PostgreSQL将引发 `CASE_NOT_FOUND` 异常。

以下是简单 `CASE` 语句的示例。

```sql
CREATE OR REPLACE FUNCTION get_price_segment(p_film_id integer)
   RETURNS VARCHAR(50) AS $$
DECLARE 
 rate   NUMERIC;
 price_segment VARCHAR(50);
BEGIN
   -- get the rate based on film_id
    SELECT INTO rate rental_rate 
    FROM film 
    WHERE film_id = p_film_id;
 
     CASE rate
 WHEN 0.99 THEN
            price_segment = 'Mass';
 WHEN 2.99 THEN
            price_segment = 'Mainstream';
 WHEN 4.99 THEN
            price_segment = 'High End';
 ELSE
     price_segment = 'Unspecified';
 END CASE;
 
   RETURN price_segment;
END; $$
LANGUAGE plpgsql;
```

在这个例子中，我们创建了一个名为 `get_price_segment` 的新函数，它接受 `p_film_id` 作为参数。根据电影的租金率，它返回价格段：质量，主流，高端。如果价格不是0.99，2.99或4.99，则函数返回未指定。

以下流程图演示了此示例中的简单CASE语句：

![PL/pgSQL simple CASE statement](/imgs/plpgsql-simple-case-statement.png)

我们来测试一下 `get_price_segment()` 函数。

```sql
SELECT get_price_segment(123) AS "Price Segment";
 Price Segment
---------------
 High End
(1 row)
```

#### 搜索的CASE

以下语法说明了搜索的CASE语句：

```sql
CASE
    WHEN boolean-expression-1 THEN
      statements
  [ WHEN boolean-expression-2 THEN
      statements
    ... ]
  [ ELSE
      statements ]
END CASE;
```

搜索的 `CASE` 语句基于每个 `WHEN` 子句中的布尔表达式的结果执行语句。PostgreSQL从上到下依次计算布尔表达式，直到一个表达式为真。然后计算停止并执行相应的语句。在 `END CASE` 之后，控件被传递给下一个语句。

如果未找到真实结果，则执行 `ELSE` 子句中的语句。`ELSE` 子句是可选的。如果省略 `ELSE` 子句并且没有真正的结果，PostgreSQL将引发 `CASE_NOT_FOUND` 异常。

请参阅以下示例：

```sql
CREATE OR REPLACE FUNCTION get_customer_service (p_customer_id INTEGER) 
 RETURNS VARCHAR (25) AS $$ 
DECLARE
    total_payment NUMERIC ; 
    service_level VARCHAR (25) ;
BEGIN
 -- get the rate based on film_id
     SELECT
 INTO total_payment SUM (amount)
     FROM
 payment
     WHERE
 customer_id = p_customer_id ; 
  
   CASE
      WHEN total_payment > 200 THEN
         service_level = 'Platinum' ;
      WHEN total_payment > 100 THEN
 service_level = 'Gold' ;
      ELSE
         service_level = 'Silver' ;
   END CASE ;
 
   RETURN service_level ;
END ; $$ 
LANGUAGE plpgsql;
```

`get_customer_service` 函数接受 `p_customer_id` 作为参数。它首先从 `payment` 表中获得客户支付的总支付。然后，根据总付款，该函数使用搜索的 `CASE` 语句返回服务级别白金，黄金和白银。下图说明了逻辑。

![PL/pgSQL searched CASE statement](/imgs/plpgsql-searched-case-statement.png)

我们来看看下面的测试用例：

```sql
SELECT
 148 AS customer,
 get_customer_service (148)
UNION
SELECT
 178 AS customer,
 get_customer_service (178)
UNION
SELECT
 81 AS customer,
 get_customer_service (81);
 customer | get_customer_service
----------+----------------------
      178 | Gold
       81 | Silver
      148 | Platinum
(3 rows)
```

请注意，搜索的case语句类似于[IF ELSIF ELSE语句](content/plpgsql-if-else-statements.md)。

在本教程中，我们向您介绍了基于特定条件执行语句的PL/pgSQL CASE语句。