## LPAD

`LPAD()` 函数在字符串左侧填充字符到指定长度。

#### 语法

以下说明了 `LPAD()` 函数的语法：

```sql
LPAD(string, length[, fill])
```

#### 参数

`LPAD()` 函数接受3个参数：

**1) string**

是一个应该在左边填充的字符串

**2) length**

是一个正整数，指定填充后结果字符串的长度。

注意，如果字符串长度超过 `length` 参数，则字符串将在右侧截断。

**3) fill**

是用于填充的字符串。

`fill` 参数是可选的。如果省略 `fill` 参数，则其默认值为空格。

#### 返回值

`LPAD()` 函数返回一个左填充字符到 `length` 长度的字符串。

#### 示例

让我们看一些使用 `LPAD()` 函数的例子。

以下语句使用 `LPAD()` 函数在字符串 'PostgreSQL' 左侧填充的 '*'：

```sql
SELECT LPAD('PostgreSQL',15,'*');
      lpad
-----------------
 *****PostgreSQL
(1 row)
```

在此示例中，PostgreSQL字符串的长度为10，结果字符串的长度应为15，因此， `LPAD()` 函数在字符串的左侧填充5个字符 `*`。

请参阅[示例数据库](content/postgresql-sample-database.md)中的以下 `customer` 和 `payment` 表：

![customer and payment tables](/imgs/customer-and-payment-tables.png)

以下语句说明了如何使用 `LPAD()` 函数根据每个客户的付款总额绘制图表。

```sql
SELECT first_name || ' ' || last_name fullname,
    SUM(amount) total,
    LPAD('*', CAST(TRUNC(SUM(amount) / 10) AS INT), '*') chart
FROM payment
INNER JOIN customer using (customer_id)
GROUP BY customer_id
ORDER BY SUM(amount) DESC;
       fullname        | total  |         chart
-----------------------+--------+-----------------------
 Eleanor Hunt          | 211.55 | *********************
 Karl Seal             | 208.58 | ********************
 Marion Snyder         | 194.61 | *******************
 Rhonda Kennedy        | 191.62 | *******************
 Clara Shaw            | 189.60 | ******************
 Tommy Collazo         | 183.63 | ******************
 Ana Bradley           | 167.67 | ****************
 Curtis Irby           | 167.62 | ****************
 Marcia Dean           | 166.61 | ****************
 Mike Way              | 162.67 | ****************
 Arnold Havens         | 161.68 | ****************
 Wesley Bull           | 158.65 | ***************
```

在这个例子中，

- 首先，我们使用 [SUM()](content/postgresql-sum-function.md) 函数和 `GROUP BY` 子句为每个客户添加了付款，
- 其次，我们使用各种函数计算基于付款总和的条形图长度：`TRUNC()` 截断总付款，`CAST()` 将 `TRUNC()` 的结果转换为整数。为了使条形图更具可读性，我们将支付金额除以10。
- 第三，我们应用 `LPAD()` 函数根据上面第二步的结果填充字符（*）。

在本教程中，您学习了如何使用 `LPAD()` 函数在字符串左侧填充字符到一定长度。