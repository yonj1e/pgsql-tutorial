## Copy Table


![PostgreSQL Copy Table](/imgs/PostgreSQL-Copy-Table-300x260.png)

> 摘要：在本教程中，我们将逐步向您展示如何使用各种形式的PostgreSQL复制表语句复制包含表结构和数据的现有表。

#### 简介

要完全复制表（包括表结构和数据），请使用以下语句：

```sql
CREATE TABLE new_table AS 
TABLE existing_table;
```

要复制没有数据的表结构，可以将 `WITH NO DATA` 子句添加到 `CREATE TABLE` 语句，如下所示：

```sql
CREATE TABLE new_table AS 
TABLE existing_table 
WITH NO DATA;
```

要从现有表复制包含部分数据的表，请使用以下语句：

```sql
CREATE TABLE new_table AS 
SELECT *
FROM existing_table
WHERE condition;
```

查询的 [WHERE](content/postgresql-where.md) 子句中的条件定义将现有表的哪些行复制到新表。

请注意，上面的所有语句都是复制表结构和数据，但不要复制现有表的索引和约束。

#### 示例

以下语句为演示创建一个名为 `contacts` 的新表：

```sql
CREATE TABLE contacts(
    id SERIAL PRIMARY KEY,
    first_name VARCHAR NOT NULL,
    last_name VARCHAR NOT NULL,
    email VARCHAR NOT NULL UNIQUE
);
```

在此表中，我们有两个索引：一个[主键](content/postgresql-primary-key.md)索引，另一个[UNIQUE](content/postgresql-unique-constraint.md)约束索引。

让我们在 `contacts` 表中[插入](content/postgresql-insert.md)一些行：

```sql
INSERT INTO contacts(first_name, last_name, email) 
VALUES('John','Doe','john.doe@postgresqltutorial.com'),
      ('David','William','david.william@postgresqltutorial.com');
```

要将 `contacts` 复制到新表（例如 `contacts_backup` 表），请使用以下语句：

```sql
CREATE TABLE contact_backup 
AS TABLE contacts;
```

此语句创建一个名为 `contact_backup` 的新表，其结构与 `contacts` 表相同。此外，它还将联系人表中的数据复制到 `contact_backup` 表。

让我们使用以下 [SELECT](content/postgresql-select.md) 语句检查 `contact_backup` 表的数据：

```sql
SELECT * FROM contact_backup;
 
id | first_name | last_name |                email
----+------------+-----------+--------------------------------------
  1 | John       | Doe       | john.doe@postgresqltutorial.com
  2 | David      | William   | david.william@postgresqltutorial.com
(2 rows)
```

它按预期返回两行。

要检查 `contact_backup` 表的结构：

```sql
test=# \d contact_backup;
       Table "public.contact_backup"
   Column   |       Type        | Modifiers
------------+-------------------+-----------
 id         | integer           |
 first_name | character varying |
 last_name  | character varying |
 email      | character varying |
```

正如您在输出中看到的那样，`contact_backup` 表的结构与除索引之外与 `contacts` 表相同。

要将主键和 `UNIQUE` 约束添加到 `contact_backup` 表，请使用以下 `ALTER TABLE` 语句：

```sql
ALTER TABLE contact_backup ADD PRIMARY KEY(id);
ALTER TABLE contact_backup ADD UNIQUE(email);
```

要再次查看 `contact_backup` 表的结构，请使用 `\d` 命令：

```sql
test=# \d contact_backup;
       Table "public.contact_backup"
   Column   |       Type        | Modifiers
------------+-------------------+-----------
 id         | integer           | not null
 first_name | character varying |
 last_name  | character varying |
 email      | character varying |
Indexes:
    "contact_backup_pkey" PRIMARY KEY, btree (id)
    "contact_backup_email_key" UNIQUE CONSTRAINT, btree (email)
```

在本教程中，您学习了如何使用各种表单PostgreSQL复制表语句将现有表复制到新表。