## BETWEEN

> 摘要：在本教程中，您将学习如何使用 `BETWEEN` 运算符将一个值与一个值范围相匹配。

#### 简介

您可以使用 `BETWEEN` 运算符将一个值与一个值范围进行匹配。以下说明了 `BETWEEN` 运算符的语法：

```sql
value BETWEEN low AND high;
```

如果该值大于等于 `low` 且小于等于 `high`，则表达式返回 `true`，否则返回 `false`。

您可以使用大于等于（>=）或小于等于（<=）运算符重写 `BETWEEN` 运算符，如下所示：

```sql
value >= low and value <= high
```

如果要检查某个值是否超出范围，请将 `NOT` 运算符与 `BETWEEN` operator组合，如下所示：

```sql
value NOT BETWEEN low AND high;
```

以下表达式等效于使用 `NOT` 和 `BETWEEN` 运算符的表达式：

```sql
value < low OR value > high
```

您经常在 [`SELECT`](content/postgresql-select.md)，[`INSERT`](content/postgresql-insert.md)，[`UPDATE`](content/postgresql-update.md) 或 [`DELETE`]() 语句的 [`WHERE`](content/postgresql-where.md) 子句中使用 `BETWEEN` 操作符。

#### 示例

我们来看看[示例数据库](content/postgresql-sample-database.md)中的 `payment`表。

![payment table](/imgs/payment-table.png)

以下查询选择金额介于8和9（USD）之间的付款：

```sql
SELECT customer_id, payment_id, amount
FROM payment
WHERE amount BETWEEN 8 AND 9;
 customer_id | payment_id | amount
-------------+------------+--------
         343 |      17517 |   8.99
         347 |      17529 |   8.99
         347 |      17532 |   8.99
         348 |      17535 |   8.99
         349 |      17540 |   8.99
         379 |      17648 |   8.99
         403 |      17747 |   8.99
```

要获取金额不在8和9范围内的付款，请使用以下查询：

```sql
SELECT customer_id, payment_id, amount
FROM payment
WHERE amount NOT BETWEEN 8 AND 9;
 customer_id | payment_id | amount
-------------+------------+--------
         341 |      17503 |   7.99
         341 |      17504 |   1.99
         341 |      17505 |   7.99
         341 |      17506 |   2.99
         341 |      17507 |   7.99
         341 |      17508 |   5.99
         342 |      17509 |   5.99
```

如果要根据日期范围检查值，则应使用ISO 8601格式的文字日期，即 `YYYY-MM-DD`。例如，要获得付款日期介于 `2007-02-07` 和 `2007-02-15` 之间的付款，请使用以下查询：

```sql
SELECT customer_id, payment_id, amount, payment_date
FROM payment
WHERE payment_date BETWEEN '2007-02-07' AND '2007-02-15';
 customer_id | payment_id | amount |        payment_date
-------------+------------+--------+----------------------------
         368 |      17610 |   0.99 | 2007-02-14 23:25:11.996577
         370 |      17617 |   6.99 | 2007-02-14 23:33:58.996577
         402 |      17743 |   4.99 | 2007-02-14 23:53:34.996577
         416 |      17793 |   2.99 | 2007-02-14 21:21:59.996577
         432 |      17854 |   5.99 | 2007-02-14 23:07:27.996577
         481 |      18051 |   2.99 | 2007-02-14 22:03:35.996577
         512 |      18155 |   6.99 | 2007-02-14 22:57:03.996577
```

在本教程中，您学习了如何使用 `BETWEEN` 操作符来选择值范围内的值。