## Cross Join

> 摘要：在本教程中，您将学习如何使用 `CROSS JOIN` 在连接表中生成行的笛卡尔积。

#### 简介

`CROSS JOIN` 子句允许您在两个或多个表中生成行的笛卡尔积。 与其他 `JOIN` 运算符（如 [LEFT JOIN](content/postgresql-left-join.md) 或 [INNER JOIN](content/postgresql-inner-join.md)）不同，`CROSS JOIN` 在 `join` 子句中没有任何匹配条件。

假设我们必须执行两个表T1和T2的 `CROSS JOIN`。 对于T1和T2中的每一行(即笛卡尔积)，结果集将包含一行，该行由T1表中的所有列和T2表中的所有列组成。。 如果T1有N行，T2有M行，结果集将有N×M行。

以下说明了 `CROSS JOIN` 子句的语法：

```sql
SELECT * FROM T1
CROSS JOIN T2;
```

以下语句也等同于上面的 `CROSS JOIN`：

```sql
SELECT * FROM T1, T2;
```

可以使用 `INNER JOIN` 子句，条件值为 `TRUE`，以执行交叉连接，如下所示：

```sql
SELECT * FROM T1
INNER JOIN T2 ON TRUE;
```

#### 示例

以下[CREATE TABLE](content/postgresql-create-table.md)语句创建T1和T2表，并[插入](content/postgresql-insert.md)一些示例数据，以演示交叉连接。

```sql
CREATE TABLE T1 (label CHAR(1) PRIMARY KEY);
 
CREATE TABLE T2 (score INT PRIMARY KEY);
 
INSERT INTO T1 (label)
VALUES ('A'), ('B');
 
INSERT INTO T2 (score)
VALUES (1), (2), (3);
```

The following statement uses the `CROSS JOIN` operator to join the T1 table with the T2 table.

以下语句使用 `CROSS JOIN` 运算符将T1表与T2表连接。

```sql
SELECT * FROM T1
CROSS JOIN T2;
 label | score
-------+-------
 A     |     1
 B     |     1
 A     |     2
 B     |     2
 A     |     3
 B     |     3
(6 rows)
```

下图说明了当我们使用 `CROSS JOIN` 运算符将T1表与T2表连接时的结果：

![PostgreSQL CROSS JOIN illustration](/imgs/PostgreSQL-CROSS-JOIN-illustration.png)

在本教程中，您学习了如何使用 `CROSS JOIN` 子句在两个或多个表中生成行的笛卡尔积。

#### 相关教程

- [INNER JOIN](http://www.postgresqltutorial.com/postgresql-inner-join/)
- [LEFT JOIN](http://www.postgresqltutorial.com/postgresql-left-join/)
- [NATURAL JOIN](http://www.postgresqltutorial.com/postgresql-natural-join/)
- [FULL OUTER JOIN](http://www.postgresqltutorial.com/postgresql-full-outer-join/)