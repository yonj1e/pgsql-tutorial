## 查看用户

> 摘要：本教程介绍如何使用PostgreSQL list users命令显示数据库服务器中的所有用户。

要列出PostgreSQL数据库服务器中的所有用户，请使用 `\du` [psql命令](content/psql-commands.md)，如下所示：

```sql
postgres=# \du
                                   List of roles
 Role name |                         Attributes                         | Member of
-----------+------------------------------------------------------------+-----------
 postgres  | Superuser, Create role, Create DB, Replication, Bypass RLS | {}
```

如果要显示更多信息，可以使用 `\du+` 命令：

```sql
postgres=# \du+
                                          List of roles
 Role name |                         Attributes                         | Member of | Description
-----------+------------------------------------------------------------+-----------+-------------
 postgres  | Superuser, Create role, Create DB, Replication, Bypass RLS | {}        |
```

在内部，当您使用 `\du` 命令时，PostgreSQL会发出以下SQL语句：

```sql
SELECT u.usename AS "Role name",
  CASE WHEN u.usesuper AND u.usecreatedb THEN CAST('superuser, create
database' AS pg_catalog.text)
       WHEN u.usesuper THEN CAST('superuser' AS pg_catalog.text)
       WHEN u.usecreatedb THEN CAST('create database' AS
pg_catalog.text)
       ELSE CAST('' AS pg_catalog.text)
  END AS "Attributes"
FROM pg_catalog.pg_user u
ORDER BY 1;
```

#### 相关教程

- [Show Tables](content/postgresql-show-tables.md)
- [Show Databases](content/postgresql-show-databases.md)
- [psql常用命令](content/psql-commands.md)
- [Cheat Sheet](content/postgresql-cheat-sheet.md)