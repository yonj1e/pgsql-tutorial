## WHERE

> 摘要：在本教程中，您将学习如何使用 `WHERE子` 句来过滤从 `SELECT` 语句返回的行。

在上一个教程中，您已经学习了如何使用 [SELECT](postgresql-select.md) 语句从表中查询数据。如果要查询表中的特定行，该怎么办？ 在这种情况下，您需要在 [SELECT](postgresql-select.md) 语句中使用 `WHERE` 子句。

我们来看看 `WHERE` 子句的语法。

#### 语法

PostgreSQL WHERE子句的语法如下：

```sql
SELECT column_1, column_2 … column_nFROM table_nameWHERE conditions;
```

`WHERE` 子句紧跟在 `SELECT` 语句的 `FROM` 子句之后。这些条件用于过滤从 `SELECT` 语句返回的行。PostgreSQL为您提供各种标准运算符来构造条件。

下表说明了标准比较运算符。

| 运算符   | 描述     |
| -------- | -------- |
| =        | 等于     |
| >        | 大于     |
| <        | 小于     |
| >=       | 大于等于 |
| <=       | 小于等于 |
| <> or != | 不等于   |
| AND      | 和       |
| OR       | 或       |

让我们练习一些使用 `WHERE` 子句的例子。

#### 示例

如果要获取名字为 `Jamie` 的所有客户，可以使用 `WHERE` 子句和等值（=）运算符，如下所示：

```sql
SELECT last_name, first_name
FROM customer
WHERE first_name = 'Jamie';
```

如果要选择名字为 `Jamie` 且姓氏为 `Rice` 的客户，可以使用结合两个条件的 `AND` 逻辑运算符作为以下查询：

```sql
SELECT last_name, first_name
FROM customer
WHERE first_name = 'Jamie' AND last_name = 'Rice';
```

如果您想知道谁支付租金的金额小于1美元或大于8美元，您可以使用以下查询与 `OR` 运算符：

```sql
SELECT customer_id, amount, payment_date
FROM payment
WHERE amount <= 1 OR amount >= 8;
```

有许多方法可以用标准运算符构造条件。在下一个教程中，您还将学习如何根据[模式匹配](content/postgresql-like.md)或使用特殊运算符（如`BETWEEN`，`IN`和`IS`）过滤行。因此，使用[示例数据库](concent/load-postgresql-sample-database.md)花几分钟时间练习使用带有比较运算符的 `WHERE` 子句。

在本教程中，您已经学习了如何将 `WHERE` 子句与 `SELECT` 语句一起使用，以根据使用标准运算符构造的条件来过滤行。
