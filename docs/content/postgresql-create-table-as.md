## CREATE TABLE AS

> 摘要：在本教程中，您将学习如何使用PostgreSQL CREATE TABLE AS语句从查询的结果集创建新表。

#### 简介

`CREATE TABLE AS` 语句[创建一个新表](content/postgresql-create-table.md)，并使用查询返回的数据填充它。以下显示了 `CREATE TABLE AS` 语句的语法：

```sql
CREATE TABLE new_table_name
AS query;
```

在这个语法中：

1.首先，在 `CREATE TABLE` 子句后指定新表名。
2.其次，提供一个查询，其结果集在 `AS` 关键字后添加到新表中。

`TEMPORARY` 或 `TEMP` 关键字允许您创建[临时表](content/postgresql-temporary-table.md)：

```sql
CREATE TEMP TABLE new_table_name 
AS query; 
```

`UNLOGGED` 关键字允许将新表创建为unlogged的表：

```sql
CREATE UNLOGGED TABLE new_table_name
AS query;
```

新表的列将具有与 `SELECT` 子句的输出列关联的名称和数据类型。

如果希望表列具有不同的名称，则可以在新表名后指定新表列：

```sql
CREATE TABLE new_table_name ( column_name_list)
AS query;
```

如果您想通过创建已存在的新表来避免错误，可以使用 `IF NOT EXISTS` 选项，如下所示：

```sql
CREATE TABLE IF NOT EXISTS new_table_name
AS query;
```

#### 示例

我们将使用示例数据库中的 `film` 和 `film_category` 表进行演示。

![film_and_film_category_tables](/imgs/film_and_film_category_tables.png)

以下语句创建一个包含具有类别1的动作影片的表。

```sql
CREATE TABLE action_film AS
SELECT
    film_id,
    title,
    release_year,
    length,
    rating
FROM
    film
INNER JOIN film_category USING (film_id)
WHERE
    category_id = 1 ; -- action
```

要验证表创建，您可以从 `action_film` 表中查询数据：

```sql
SELECT *
FROM action_film
ORDER BY title;
 film_id |          title          | release_year | length | rating
---------+-------------------------+--------------+--------+--------
      19 | Amadeus Holy            |         2006 |    113 | PG
      21 | American Circus         |         2006 |    129 | R
      29 | Antitrust Tomatoes      |         2006 |    168 | NC-17
      38 | Ark Ridgemont           |         2006 |     68 | NC-17
      56 | Barefoot Manchurian     |         2006 |    129 | G
      67 | Berets Agent            |         2006 |     77 | PG-13
      97 | Bride Intrigue          |         2006 |     56 | G
     105 | Bull Shawshank          |         2006 |    125 | NC-17
```

要检查 `action_film` 的结构，可以在psql工具中使用以下命令：

```sql
\d action_film;
                       Table "public.action_film"
    Column    |          Type          | Collation | Nullable | Default
--------------+------------------------+-----------+----------+---------
 film_id      | integer                |           |          |
 title        | character varying(255) |           |          |
 release_year | year                   |           |          |
 length       | smallint               |           |          |
 rating       | mpaa_rating            |           |          |
```

如输出中清楚显示的那样，`action_film` 表的名称和数据类型是从 `SELECT` 子句的列派生的。

如果 `SELECT` 子句包含表达式，则优先覆盖列，例如：

```sql
CREATE TABLE IF NOT EXISTS film_rating (rating, film_count) 
AS 
SELECT rating, COUNT (film_id)
FROM film
GROUP BY rating;
```

这个示例语句创建了一个新表 `film_rating`，并用电影表中的摘要数据填充它。它显式指定了新表的列名，而不是使用 `SELECT` 子句中的列名。

要检查 `film_rating` 表的结构，请在psql工具中使用以下命令：

```sql
\d film_rating
                Table "public.film_rating"
   Column   |    Type     | Collation | Nullable | Default
------------+-------------+-----------+----------+---------
 rating     | mpaa_rating |           |          |
 film_count | bigint      |           |          |
```

请注意，`CREATE TABLE AS` 语句类似于 `SELECT INTO` 语句，但 `CREATE TABLE AS` 语句是首选，因为它不会与[PL/pgSQL](content/postgresql-stored-procedures.md)中 `SELECT INTO` 语法的其他用法混淆。此外，`CREATE TABLE AS` 语句提供 `SELECT INTO` 语句提供的功能的超集。

在本教程中，您学习了如何使用PostgreSQL `CREATE TABLE AS` 语句从查询结果创建新表。