## 子查询

> 摘要：在本教程中，您将学习如何使用子查询来构建复杂查询。

#### 简介

让我们从一个简单的例子开始吧。

假设我们想找到租金率高于平均租金率的电影。 我们可以分两步完成：

- 使用 [SELECT](content/postgresql-select.md) 语句和平均值函数（`AVG`）查找平均租金率。
- 使用第二个 `SELECT` 语句中第一个查询的结果来查找我们想要的电影。

以下查询获得平均租金率：

```sql
SELECT AVG (rental_rate) FROM film;
        avg
--------------------
 2.9800000000000000
(1 row)
```

平均租金是2.98

现在，我们可以获得租金高于平均租金率的电影：

```sql
SELECT film_id, title, rental_rate 
FROM film 
WHERE rental_rate > 2.98;
 film_id |            title            | rental_rate
---------+-----------------------------+-------------
     133 | Chamber Italian             |        4.99
     384 | Grosse Wonderful            |        4.99
       8 | Airport Pollock             |        4.99
      98 | Bright Encounters           |        4.99
       2 | Ace Goldfinger              |        4.99
       3 | Adaptation Holes            |        2.99
       4 | Affair Prejudice            |        2.99
       5 | African Egg                 |        2.99
```

代码不是那么优雅，需要两个步骤。 我们想要一种方法将第一个查询的结果传递给一个查询中的第二个查询。 解决方案是使用子查询。

子查询是嵌套在另一个查询中的查询，例如 `SELECT`，`INSERT`，`DELETE` 和 `UPDATE`。 在本教程中，我们仅关注 `SELECT` 语句。

要构造子查询，我们将第二个查询放在括号中，并在 [WHERE](content/postgresql-where.md) 子句中将其用作表达式：

```sql
SELECT film_id, title, rental_rate 
FROM film 
WHERE rental_rate > ( 
    SELECT AVG (rental_rate) FROM film 
);
```

括号内的查询称为子查询或内部查询。 包含子查询的查询称为外部查询。

PostgreSQL按以下顺序执行包含子查询的查询：

- 首先，执行子查询。
- 其次，获取结果并将其传递给外部查询。
- 第三，执行外部查询。

#### IN

子查询可以返回零行或多行。在WHERE子句中使用IN运算符使用子查询。

例如，要获取在 `2005-05-29` 和 `2005-05-30` 之间返回日期的电影，请使用以下查询：

```sql
SELECT inventory.film_id 
FROM rental 
INNER JOIN inventory ON inventory.inventory_id = rental.inventory_id 
WHERE return_date 
BETWEEN '2005-05-29'AND '2005-05-30';
 film_id
---------
      15
      19
      45
      50
      52
      54
      68
      73
```

它返回多行，因此我们可以将此查询用作查询的 `WHERE` 子句中的子查询，如下所示：

```sql
SELECT film_id, title
FROM film
WHERE film_id IN ( 
    SELECT inventory.film_id 
    FROM rental 
    INNER JOIN inventory ON inventory.inventory_id = rental.inventory_id 
    WHERE return_date 
    BETWEEN '2005-05-29' AND '2005-05-30' 
);
 film_id |         title
---------+-----------------------
     307 | Fellowship Autumn
     255 | Driving Polish
     388 | Gunfight Moon
     130 | Celebrity Horn
     563 | Massacre Usual
     397 | Hanky October
     898 | Tourist Pelican
     228 | Detective Vision
     347 | Games Bowfinger
    1000 | Zorro Ark
```

#### EXISTS

以下表达式说明了如何将子查询与 `EXISTS` 运算符一起使用：

```sql
EXISTS subquery
```

子查询可以是 `EXISTS` 运算符的输入。 如果子查询返回任何行，则 `EXISTS` 运算符返回true。 如果子查询不返回任何行，则 `EXISTS` 运算符的结果为false。

`EXISTS` 运算符只关心从子查询返回的行数，而不是行的内容，因此，`EXISTS` 运算符的使用如下：

```sql
EXISTS (SELECT 1 FROM tbl WHERE condition);
```

请参阅以下查询：

```sql
SELECT first_name, last_name
FROM customer
WHERE EXISTS ( 
	SELECT 1 FROM payment 
	WHERE payment.customer_id = customer.customer_id 
);
 first_name  |  last_name
-------------+--------------
 Jared       | Ely
 Mary        | Smith
 Patricia    | Johnson
 Linda       | Williams
 Barbara     | Jones
 Elizabeth   | Brown
 Jennifer    | Davis
 Maria       | Miller
 Susan       | Wilson
```

该查询的作用类似于 `customer_id·`列上的 [inner join](content/postgresql-inner-join.md)。 但是，即使在 `payment` 表中有一些相应的行，它也会为 `customer` 表中的每一行返回最多一行。

在本教程中，您学习了如何使用子查询来构造复杂查询。