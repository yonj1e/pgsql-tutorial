## CREATE FUNCTION

> 摘要：在本教程中，我们将逐步向您展示如何使用 `CREATE FUNCTION` 语句开发第一个用户定义的函数。

#### 简介

要在PostgreSQL中创建新的用户定义函数，请使用 `CREATE FUNCTION` 语句，如下所示：

```sql
CREATE FUNCTION function_name(p1 type, p2 type)
RETURNS type AS
BEGIN
 -- logic
END;
LANGUAGE language_name;
```

让我们更详细地研究一下 `CREATE FUNCTION` 语句。

- 首先，在 `CREATE FUNCTION` 子句后，指定函数的名称。
- 然后，在函数名后面的括号内放置逗号分隔的参数列表。
- 接下来，在 `RETURNS` 关键字后指定函数的返回类型。
- 之后，将代码放在 `BEGIN` 和 `END` 块中。该函数始终以分号（;）结尾，后跟 `END` 关键字。
- 最后，指出函数的过程语言，例如，在使用PL pgSQL的情况下为 `plpgsql`。

#### 示例

我们将开发一个名为 `inc` 的非常简单的函数，它将整数增加1并返回结果。

输入以下命令以创建 `inc` 函数。

```sql
CREATE FUNCTION inc(val integer) 
RETURNS integer AS 
$$
BEGIN
  RETURN val + 1;
END;
$$
LANGUAGE PLPGSQL;
```

您提供给 `CREATE FUNCTION` 的整个函数定义必须是单引号字符串。这意味着如果函数有任何单引号（'），你必须转义它。

幸运的是，在8.0或更高版本中，PostgreSQL提供了一个名为美元引用的功能，允许您选择一个不在函数中出现的合适字符串，这样您就不必转义它。美元引用是 `$` 字符之间的字符串。

如果函数有效，PostgreSQL将创建函数并返回 `CREATE FUNCTION` 语句。

让我们测试 `inc` 函数。

您可以像任何内置函数一样调用inc函数，如下所示：

```sql
dvdrental=# SELECT inc(20);
 inc
-----
 21
(1 row)
 
dvdrental=# SELECT inc(inc(20));
 inc
-----
 22
(1 row)
```

它按预期工作。

恭喜！ 您已经迈出了在PostgreSQL中开发用户定义函数的第一步。让我们在下一篇教程中探索PL/pgSQL语言特性，以创建更复杂的用户定义函数。