## INNER JOIN

> 摘要：在本教程中，您将学习如何使用 `INNER JOIN` 子句从多个表中查找数据。

#### 简介

到目前为止，您已经学习了如何从表中[查找](content/postgresql-select.md)数据，查找所需的列和行，以及如何按特定顺序对结果集进行[排序](content/postgresql-order-by.md)。

现在是时候转移到数据库中一个最重要概念：连接 `join`，它允许您将一个表中的数据与其他表中的数据相关联。有几种连接包括 `INNER JOIN`，`OUTER JOIN` 和自连接。本教程重点介绍 `INNER JOIN`。

假设您要从名为 `A` 和 `B` 的两个表中获取数据。`B` 表的 `fka` 字段与 `A` 表的主键相关。

![A and B tables](/imgs/A-and-B-tables.png)

要从两个表中获取数据，请在 `SELECT` 语句中使用 `INNER JOIN` 子句，如下所示：

```sql
SELECT A.pka, A.c1, B.pkb, B.c2
FROM A
INNER JOIN B ON A.pka = B.fka;
```

要将A表连接到B表：

- 首先，在两个表中指定要从 `SELECT` 子句中选择数据的列。
- 其次，指定主表，即 `FROM` 子句中的A。
- 第三，指定主表连接的表，即 `INNER JOIN` 子句中的B。另外，在 `ON` 关键字后面加上连接条件，即 `A.pka = B.fka`。

对于A表中的每一行，PostgreSQL扫描B表以检查是否存在与该条件匹配的行，即 `A.pka = B.fka`。如果找到匹配项，它会将两行的列组合成一行，并将组合的行添加到返回的结果集中。

主键列（`pka`）和外键列（`fka`）通常有索引；因此，PostgreSQL只需检查索引中的匹配，这非常快。

有时，`A` 和 `B` 表具有相同的列名，因此我们必须将列引用为 `table_name.column_name` 以避免歧义。如果表的名称很长，您可以使用[表别名](content/postgresql-alias.md)，例如，`tbl` 将列引用为 `tbl.column_name`。

以下维恩图说明了 `INNER JOIN` 子句的工作原理。

![PostgreSQL INNER JOIN Venn Diagram](/imgs/PostgreSQL-INNER-JOIN-Venn-Diagram.png)

`INNER JOIN` 子句返回 `A` 表中在 `B` 表中具有相应行的行。

#### 示例

##### 连接两个表

我们来看看[示例数据库](content/postgresql-sample-database.md)中的 `customer` 和 `payment`。

![customer and payment tables](/imgs/customer-and-payment-tables.png)

每个客户可能有零次或多次付款。每笔付款都属于一个且只有一个客户。`customer_id` 字段建立两个表之间的链接。

您可以使用 `INNER JOIN` 子句将 `customer` 表连接到 `payment` 表，如下所示：

```sql
SELECT customer.customer_id, first_name, last_name, email, amount, payment_date
FROM customer
INNER JOIN payment ON payment.customer_id = customer.customer_id;
 customer_id | first_name  |  last_name   |                  email                   | amount |        payment_date
-------------+-------------+--------------+------------------------------------------+--------+----------------------------
         341 | Peter       | Menard       | peter.menard@sakilacustomer.org          |   7.99 | 2007-02-15 22:25:46.996577
         341 | Peter       | Menard       | peter.menard@sakilacustomer.org          |   1.99 | 2007-02-16 17:23:14.996577
         341 | Peter       | Menard       | peter.menard@sakilacustomer.org          |   7.99 | 2007-02-16 22:41:45.996577
         341 | Peter       | Menard       | peter.menard@sakilacustomer.org          |   2.99 | 2007-02-19 19:39:56.996577
         341 | Peter       | Menard       | peter.menard@sakilacustomer.org          |   7.99 | 2007-02-20 17:31:48.996577
         341 | Peter       | Menard       | peter.menard@sakilacustomer.org          |   5.99 | 2007-02-21 12:33:49.996577
         342 | Harold      | Martino      | harold.martino@sakilacustomer.org        |   5.99 | 2007-02-17 23:58:17.996577
         342 | Harold      | Martino      | harold.martino@sakilacustomer.org        |   5.99 | 2007-02-20 02:11:44.996577
         342 | Harold      | Martino      | harold.martino@sakilacustomer.org        |   2.99 | 2007-02-20 13:57:39.996577
         343 | Douglas     | Graf         | douglas.graf@sakilacustomer.org          |   4.99 | 2007-02-16 00:10:50.996577
         343 | Douglas     | Graf         | douglas.graf@sakilacustomer.org          |   6.99 | 2007-02-16 01:15:33.996577
```

您可以添加 `ORDER BY` 子句以按客户ID对结果集进行排序，如下所示：

```sql
SELECT customer.customer_id, first_name, last_name, email, amount, payment_date
FROM customer
INNER JOIN payment ON payment.customer_id = customer.customer_id
ORDER BY customer.customer_id;
 customer_id | first_name  |  last_name   |                  email                   | amount |        payment_date
-------------+-------------+--------------+------------------------------------------+--------+----------------------------
           1 | Mary        | Smith        | mary.smith@sakilacustomer.org            |   5.99 | 2007-02-14 23:22:38.996577
           1 | Mary        | Smith        | mary.smith@sakilacustomer.org            |   0.99 | 2007-02-15 16:31:19.996577
           1 | Mary        | Smith        | mary.smith@sakilacustomer.org            |   9.99 | 2007-02-15 19:37:12.996577
           1 | Mary        | Smith        | mary.smith@sakilacustomer.org            |   4.99 | 2007-02-16 13:47:23.996577
           1 | Mary        | Smith        | mary.smith@sakilacustomer.org            |   4.99 | 2007-02-18 07:10:14.996577
           1 | Mary        | Smith        | mary.smith@sakilacustomer.org            |   0.99 | 2007-02-18 12:02:25.996577
           1 | Mary        | Smith        | mary.smith@sakilacustomer.org            |   3.99 | 2007-02-21 04:53:11.996577
           1 | Mary        | Smith        | mary.smith@sakilacustomer.org            |   4.99 | 2007-03-01 07:19:30.996577
           1 | Mary        | Smith        | mary.smith@sakilacustomer.org            |   3.99 | 2007-03-02 14:05:18.996577
```

您还可以使用 `WHERE` 子句来过滤客户。以下查询返回客户ID 2的客户租赁数据：

```sql
SELECT customer.customer_id, first_name, last_name, email, amount, payment_date
FROM customer
INNER JOIN payment ON payment.customer_id = customer.customer_id
WHERE customer.customer_id = 2;
 customer_id | first_name  |  last_name   |                  email                   | amount |        payment_date
-------------+-------------+--------------+------------------------------------------+--------+----------------------------
           1 | Mary        | Smith        | mary.smith@sakilacustomer.org            |   5.99 | 2007-02-14 23:22:38.996577
           1 | Mary        | Smith        | mary.smith@sakilacustomer.org            |   0.99 | 2007-02-15 16:31:19.996577
           1 | Mary        | Smith        | mary.smith@sakilacustomer.org            |   9.99 | 2007-02-15 19:37:12.996577
           1 | Mary        | Smith        | mary.smith@sakilacustomer.org            |   4.99 | 2007-02-16 13:47:23.996577
           1 | Mary        | Smith        | mary.smith@sakilacustomer.org            |   4.99 | 2007-02-18 07:10:14.996577
           1 | Mary        | Smith        | mary.smith@sakilacustomer.org            |   0.99 | 2007-02-18 12:02:25.996577
           1 | Mary        | Smith        | mary.smith@sakilacustomer.org            |   3.99 | 2007-02-21 04:53:11.996577
           1 | Mary        | Smith        | mary.smith@sakilacustomer.org            |   4.99 | 2007-03-01 07:19:30.996577
           1 | Mary        | Smith        | mary.smith@sakilacustomer.org            |   3.99 | 2007-03-02 14:05:18.996577
```

##### 连接三个表

下图说明了三个表之间的关系：`staff`，`payment` 和 `customer`。

- 每位员工都涉及零或多笔款项。每笔付款由一名且仅一名员工处理。
- 每位客户都有零或多笔付款。每笔付款都属于唯一的客户。

![customer, payment and staff tables](/imgs/customer-payment-staff-tables.png)

要连接这三个表，请将第二个 `INNER JOIN` 子句放在第一个 `INNER JOIN` 子句之后，作为以下查询：

```sql
SELECT customer.customer_id, customer.first_name customer_first_name, 
	customer.last_name customer_last_name, customer.email, 
	staff.first_name staff_first_name, staff.last_name staff_last_name, 
	amount, payment_date
FROM customer
INNER JOIN payment ON payment.customer_id = customer.customer_id
INNER JOIN staff ON payment.staff_id = staff.staff_id;
 customer_id | customer_first_name | customer_last_name |                  email                   | staff_first_name | staff_last_name | amount |        payment_date
-------------+---------------------+--------------------+------------------------------------------+------------------+-----------------+--------+----------------------------
         341 | Peter               | Menard             | peter.menard@sakilacustomer.org          | Jon              | Stephens        |   7.99 | 2007-02-15 22:25:46.996577
         341 | Peter               | Menard             | peter.menard@sakilacustomer.org          | Mike             | Hillyer         |   1.99 | 2007-02-16 17:23:14.996577
         341 | Peter               | Menard             | peter.menard@sakilacustomer.org          | Mike             | Hillyer         |   7.99 | 2007-02-16 22:41:45.996577
         341 | Peter               | Menard             | peter.menard@sakilacustomer.org          | Jon              | Stephens        |   2.99 | 2007-02-19 19:39:56.996577
         341 | Peter               | Menard             | peter.menard@sakilacustomer.org          | Jon              | Stephens        |   7.99 | 2007-02-20 17:31:48.996577
         341 | Peter               | Menard             | peter.menard@sakilacustomer.org          | Mike             | Hillyer         |   5.99 | 2007-02-21 12:33:49.996577
         342 | Harold              | Martino            | harold.martino@sakilacustomer.org        | Jon              | Stephens        |   5.99 | 2007-02-17 23:58:17.996577
         342 | Harold              | Martino            | harold.martino@sakilacustomer.org        | Mike             | Hillyer         |   5.99 | 2007-02-20 02:11:44.996577
```

要连接三个以上的表，请应用相同的方式。

在本教程中，我们向您展示了如何通过使用 `INNER JOIN` 子句将一个表连接到其他表来从多个表中查找数据。

#### 相关教程

- [Cross Join](http://www.postgresqltutorial.com/postgresql-cross-join/)
- [NATURAL JOIN](http://www.postgresqltutorial.com/postgresql-natural-join/)
- [FULL OUTER JOIN](http://www.postgresqltutorial.com/postgresql-full-outer-join/)