## 整数类型

> 摘要：本教程向您介绍各种PostgreSQL整数类型，包括 `SMALLINT`、`INTEGER` 和 `BIGINT`。

#### 简介

![postgresql integer](/imgs/postgresql-integer-300x59.png)

若要在PostgreSQL中存储整数，请使用下列整数类型之一：`SMALLINT`、`INTEGER` 和 `BIGINT`。

下表说明了每种整数类型的规范：  

| 名称       | 存储大小 | 最小值                     | 最大值                     |
| ---------- | -------- | -------------------------- | -------------------------- |
| `SMALLINT` | 2 bytes  | -32,768                    | +32,767                    |
| `INTEGER`  | 4 bytes  | -2,147,483,648             | +2,147,483,647             |
| `BIGINT`   | 8 bytes  | -9,223,372,036,854,775,808 | +9,223,372,036,854,775,807 |

如果试图存储超出允许范围的值，PostgreSQL将发出错误。

与[MySQL整数](http://www.mysqltutorial.org/mysql-int/)不同，PostgreSQL不提供无符号整数类型。 

##### SMALLINT

`SMALLINT` 需要2个字节的存储大小，可以存储(-32767，32767)范围内的任何整数。

您可以使用 `SMALLINT` 类型来存储诸如人的年龄、一本书的页数等内容。

下面的语句[创建](content/postgresql-create-table.md)一个名为book的表：  

```sql
CREATE TABLE books (
    book_id SERIAL PRIMARY KEY,
    title VARCHAR (255) NOT NULL,
    pages SMALLINT NOT NULL CHECK (pages > 0)
);
```

在本例中，`pages` 列类型是 `SMALLINT` 。因为一本书的页数必须是正数，所以我们添加了 `CHECK` 约束来强制执行此规则。

##### INTEGER

`INTEGER` 是整数类型之间最常见的选择，因为它在存储大小，范围和性能之间提供了最佳平衡。

`INTEGER` 类型需要4个字节的存储大小，可以存储（-2147483648，2147483647）范围内的数字。

您可以将 `INTEGER` 类型用于存储相当大的整数的列，例如城市人口甚至国家/地区，如下例所示：

```sql
CREATE TABLE cities (
    city_id serial PRIMARY KEY,
    city_name VARCHAR (255) NOT NULL,
    population INT NOT NULL CHECK (population >= 0)
);
```

请注意，`INT` 是 `INTEGER` 的同义词。

##### BIGINT

如果要存储超出 `INTEGER` 类型范围的整数，可以使用 `BIGINT` 类型。

`BIGINT` 类型需要8个字节的存储大小，可以存储（-9,223,372,036,854,775,808，+ 9,223,372,036,854,775,807）范围内的任何数字。

使用 `BIGINT` 类型不仅消耗大量存储空间而且还降低了数据库的性能，因此，您使用它应该有充分的理由。