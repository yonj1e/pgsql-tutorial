## DROP TABLESPACE

> 摘要：在本教程中，您将学习如何使用PostgreSQL DROP TABLESPACE语句删除表空间。

#### 简介

要删除表空间，请使用 `DROP TABLESPACE` 语句，如下所示：

```sql
DROP TABLESPACE IF EXISTS tablespace_name;
```

您需要在 `DROP TABLESPACE` 子句后指定表空间的名称。`IF EXISTS` 可帮助您避免删除不存在的表空间的错误。

只有表空间所有者或超级用户才能删除表空间。

在删除表空间之前，请确保它是空的，这意味着其中没有数据库对象。

#### 示例

首先，创建一个名为 `demo` 的新表空间，并将其映射到 `/work/test/tblspace` 目录。

```sql
CREATE TABLESPACE demo LOCATION '/work/test/tblspace';
```

其次，创建一个名为 `dbdemo` 的新数据库，将其表空间设置为 `demo`：

```sql
CREATE DATABASE dbdemo TABLESPACE = demo;
```

三，创建一个名为 `test` 的新表，将表空间设置为 `demo`：

```sql
CREATE TABLE test (
 ID serial PRIMARY KEY,
 title VARCHAR (255) NOT NULL
) TABLESPACE demo;
```

You can get all objects in the `demo`tablespace by using the following [query](content/postgresql-select.md):

您可以使用以下查询获取 `demo` 表空间中的所有对象：

```sql
SELECT
 ts.spcname,
 cl.relname
FROM
 pg_class cl
JOIN pg_tablespace ts ON cl.reltablespace = ts.oid
WHERE
 ts.spcname = 'demo';
```

第四，尝试删除 `demo` 表空间：

```sql
DROP TABLESPACE demo;
```

我们收到一条错误消息：

```sql
More Actions1[Err] ERROR: tablespace "demo" is not empty
```

由于 `demo` 表空间不为空，我们无法删除它。

五，登录 `postgres` 数据库并删除 `dbdemo` 数据库：

```sql
DROP DATABASE dbdemo;
```

您可以使用[ALTER TABLE]()语句将其移动到另一个表空间，例如 `pg_default`，而不是[删除数据库]()，如下所示：

```sql
ALTER DATABASE dbdemo
SET TABLESPACE = pg_default;
```

六，再次删除 `demo` 表空间：

```sql
DROP TABLESPACE demo;
```

`demo` 表空间已删除。

在本教程中，我们向您展示了如何使用PostgreSQL `DROP TABLESPACE` 语句逐步删除表空间。