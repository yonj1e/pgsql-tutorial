## WITH CHECK OPTION

> 摘要：在本教程中，我们将向您展示如何使用 `WITH CHECK OPTION` 子句创建可更新视图，以确保通过视图对基表的更改满足视图定义条件。

#### 简介

在创建[可更新视图](content/postgresql-updatable-views.md)教程中，您已学习如何创建可更新视图，该视图允许您通过视图更改基表的数据。

我们来看看[示例数据库](content/postgresql-sample-database.md)中的 `city` 和 `country` 表。

以下语句创建一个名为 `usa_city` 的可更新视图，该视图返回美国的所有城市。

```sql
CREATE VIEW usa_city AS 
SELECT city_id, city, country_id
FROM city
WHERE country_id = 103
ORDER BY city;
```

以下语句通过 `usa_city` 将新行插入到 `city` 表中。

```sql
INSERT INTO usa_city (city, country_id)
VALUES ('Birmingham', 102);
 
INSERT INTO usa_city (city, country_id)
VALUES ('Cambridge', 102);
```

问题是插入的新行在视图中不可见。 它可能会造成安全问题，因为我们可能会授权用户更新美国的城市，而不是英国。

要防止用户插入或更新在视图不可见的行，请在创建视图时使用 `WITH CHECK OPTION` 子句。

让我们更改 `usa_city` 视图以包含 `WITH CHECK OPTION` 子句

```sql
CREATE OR REPLACE VIEW usa_city AS 
SELECT city_id, city, country_id
FROM city
WHERE country_id = 103
ORDER BY city 
WITH CHECK OPTION;
```

现在，运行以下语句为英国国家插入另一个城市。

```sql
INSERT INTO usa_city (city, country_id)
VALUES ('Cambridge', 102);
```

PostgreSQL拒绝了插入并发出错误。

```sql
ERROR:  new row violates check option for view "usa_city"
DETAIL:  Failing row contains (607, Cambridge, 102, 2018-10-08 15:32:48.85413).
```

它按预期工作。

以下声明将城市ID 135的国家[更新](content/postgresql-update.md)为英国。

```sql
UPDATE usa_city
SET country_id = 102
WHERE city_id = 135;
```

PostgreSQL拒绝了更新并发出错误。

```sql
ERROR:  new row violates check option for view "usa_city"
DETAIL:  Failing row contains (135, Dallas, 102, 2018-10-08 15:32:32.884608).
```

这是因为 `UPDATE` 语句导致正在更新的行在 `usa_city` 视图不可见。

#### LOCAL，CASCADED

首先，创建一个视图，返回名称以字母A开头的所有城市。

```sql
CREATE VIEW city_a AS 
SELECT city_id, city, country_id
FROM city
WHERE city LIKE 'A%';
```

`city_a` 视图没有 `WITH CHECK OPTION` 子句。

其次，创建另一个视图，该视图返回名称以字母A开头并位于美国的城市。 此 `city_a_usa` 视图基于 `city_a` 视图。

```sql
CREATE OR REPLACE VIEW city_a_usa AS 
SELECT city_id, city, country_id
FROM city_a
WHERE country_id = 103 
WITH CASCADED CHECK OPTION;
```

`city_a_usa` 视图具有 `WITH CASCADED CHECK OPTION` 子句。 注意 `CASCADED` 选项。

以下语句通过 `city_a_usa` 表在 `city` 表中[插入](content/postgresql-insert.md)一行。

```sql
INSERT INTO city_a_usa (city, country_id)
VALUES ('Houston', 103);
```

PostgreSQL拒绝了插入并发出以下错误：

```sql
ERROR:  new row violates check option for view "city_a"
DETAIL:  Failing row contains (606, Houston, 103, 2018-10-08 15:31:46.028541).
```

该错误消息表明，即使 `city_a` 视图没有 `WITH CHECK OPTION` 子句，也违反了 `city_a` 视图的视图定义条件。

这是因为当我们对 `city_a_usa` 视图使用 `WITH CASCADED CHECK OPTION` 时，PostgreSQL检查了 `city_a_usa` 视图的视图定义条件以及所有底层视图，在这种情况下，它是 `city_a` 视图。

要检查您插入或更新的视图的视图定义条件，请使用 `WITH LOCAL CHECK OPTION`，如下所示：

```sql
CREATE OR REPLACE VIEW city_a_usa AS 
SELECT city_id, city, country_id
FROM city_a
WHERE country_id = 103 
WITH LOCAL CHECK OPTION;
```

让我们再次通过 `city_a_usa` 视图在城市表中插入一个新行。

```sql
INSERT INTO city_a_usa (city, country_id)
VALUES ('Houston', 103);
```

这次成功，因为新行满足 `city_a_usa` 视图的视图定义条件。 PostgreSQL没有检查基本视图的视图定义条件。

在本教程中，您学习了如何使用 `WITH CHECK OPTION` 子句创建可更新视图，以便在通过视图对基础表进行更改时检查视图定义条件。