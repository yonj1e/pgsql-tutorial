## Show Databases

> 摘要：在本教程中，我们将向您展示如何在PostgreSQL数据库服务器中显示数据库。

在MySQL中，您可以使用[SHOW DATABASES](http://www.mysqltutorial.org/mysql-show-databases/)语句显示数据库服务器中的所有数据库。PostgreSQL不直接提供此语句，但为您提供类似的功能。PostgreSQL为您提供了两种显示数据库的方法。

#### 使用psql工具列出数据库

如果使用[psql](content/psql-commands.md)工具连接到PostgreSQL数据库服务器，则可以发出 `\l` 命令以显示当前服务器中的所有数据库，如下所示：

```
\l
```

例如，您可以连接到[dvdrental示例数据库](content/postgresql-sample-database.md)，并列出该服务器中的所有数据库，如下所示：

```sql
dvdrental=# \l
                                List of databases
   Name    |  Owner  | Encoding |   Collate   |    Ctype    |  Access privileges
-----------+---------+----------+-------------+-------------+---------------------
 dvdrental | postgres | UTF8     | en_US.UTF-8 | en_US.UTF-8 |
 postgres  | postgres | UTF8     | en_US.UTF-8 | en_US.UTF-8 |
 template0 | postgres | UTF8     | en_US.UTF-8 | en_US.UTF-8 | =c/postgres         +
           |         |          |             |             | postgres=CTc/postgres
 template1 | postgres | UTF8     | en_US.UTF-8 | en_US.UTF-8 | =c/postgres         +
           |         |          |             |             | postgres=CTc/postgres
(4 rows)
```

#### 使用SELECT语句列出数据库

除了使用 `\l` 命令外，还可以使用[SELECT语句](content/postgresql-select.md)从存储有关所有可用数据库的数据的 `pg_database` 系统表中查询数据库名称。

```sql
SELECT datname FROM pg_database;
  datname
-----------
 postgres
 template1
 template0
 dvdrental
(4 rows)
```

它表明我们在当前数据库服务器中有四个数据库。

#### 相关教程

- [Show Tables](content/postgresql-show-tables.md)
- [List Users](content/postgresql-list-users.md)
- [Describe Table](content/postgresql-describe-table.md)