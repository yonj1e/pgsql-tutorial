## REGEXP_REPLACE

`REGEXP_REPLACE()` 函数用新的子字符串替换与[POSIX正则表达式](https://en.wikibooks.org/wiki/Regular_Expressions/POSIX-Extended_Regular_Expressions)匹配的子字符串。

请注意，如果要执行简单的子串替换，可以使用 `REPLACE()` 函数。

#### 语法

`REGEXP_REPLACE()` 函数的语法如下：

```sql
REGEXP_REPLACE(source, pattern, replacement_string,[, flags])
```

#### 参数

 `REGEXP_REPLACE()` 函数接受四个参数：

**1) source**

`source` 是一个应该替换的字符串。

**2) pattern**

`pattern` 是POSIX正则表达式，用于匹配应替换的子字符串。

**3) replacement_string**

`replacement_string` 是一个字符串，用于替换与正则表达式模式匹配的子字符串。

**4) flags**

`flags` 参数是控制函数匹配行为的一个或多个字符，例如，`i` 允许不区分大小写的匹配，`n` 允许匹配任何字符以及换行符。

#### 返回值

`REGEXP_REPLACE()` 函数返回一个带有子串的新字符串，该子串与正则表达式模式匹配，由新的子字符串替换。

#### 示例

让我们看一些例子来理解 `REGEXP_REPLACE()` 函数的工作原理。

**A）重新排列**

假设您有一个人的名字，格式如下：

```
first_name last_name
```

例如，`John Doe`

并且您希望为报告目的重新排列此名称，如下所示。

```
last_name, first_name
```

为此，您可以使用 `REGEXP_REPLACE()` 函数，如下所示：

```sql
SELECT REGEXP_REPLACE('John Doe','(.*) (.*)','\2, \1');
 regexp_replace
----------------
 Doe, John
(1 row)
```

**B）删除字符串**

想象一下，您有包含混合字母和数字的字符串数据，如下所示：

```
ABC12345xyz
```

以下语句从源字符串中删除所有字母，例如A，B，C等：

```sql
SELECT REGEXP_REPLACE('ABC12345xyz','[[:alpha:]]','','g');
 regexp_replace
----------------
 12345
(1 row)
```

在这个例子中，

- `[[：alpha：]]` 匹配任何字母
- `''` 是替换字符串
- `'g'` 指示函数删除所有字母，而不仅仅是第一个字母。

同样，您可以使用以下语句删除源字符串中的所有数字：

```sql
SELECT REGEXP_REPLACE('ABC12345xyz','[[:digit:]]','','g');
 regexp_replace
----------------
 ABCxyz
(1 row)
```

**C）去除空格**

以下示例删除在字符串中出现的不需要的空格。

```sql
-- error
SELECT REGEXP_REPLACE('This  is    a   test   string','( ){2,}',' ');
        regexp_replace
------------------------------
 This is a test string
(1 row)
```

在本教程中，您学习了如何使用 `REGEXP_REPLACE()` 函数将正则表达式与新子字符串匹配的子字符串替换。