## ROLLUP

> 摘要：在本教程中，您将学习如何使用 `ROLLUP` 生成多个分组集。

#### 简介

The PostgreSQL `ROLLUP` is a subclause of the `GROUP BY` clause that offers a shorthand for defining multiple [grouping sets](http://www.postgresqltutorial.com/postgresql-grouping-sets/). A grouping set is a set of columns to which you want to group. Check it out the [grouping sets](http://www.postgresqltutorial.com/postgresql-grouping-sets/) tutorial for further information.

Different from the `CUBE` subclause, `ROLLUP` does not generate all possible grouping sets based on the specified columns. It just makes a subset of those.

The `ROLLUP` assumes a hierarchy among the input columns and generates all grouping sets that make sense considering the hierarchy. This is the reason why `ROLLUP` is often used to generate the subtotals and the grand total for reports.

For example, the `CUBE (c1,c2,c3)` makes all eight possible grouping sets:

ROLLUP是GROUP BY子句的子句，它提供了定义多个[分组集](content/postgresql-grouping-sets.md)的简要方法。 分组集是要分组的一组列。 查看[分组集](content/postgresql-grouping-sets.md)教程以获取更多信息。

与CUBE子条款不同，ROLLUP不会根据指定的列生成所有可能的分组集。 它只是制作了一部分。

ROLLUP假设输入列之间存在层次结构，并生成考虑层次结构有意义的所有分组集。 这就是ROLLUP经常用于生成小计和报告总计的原因。

例如，CUBE（c1，c2，c3）生成所有八个可能的分组集：

```
(c1, c2, c3)
(c1, c2)
(c2, c3)
(c1,c3)
(c1)
(c2)
(c3)
()
```

但是，ROLLUP（c1，c2，c3）仅生成四个分组集，假设层次结构c1> c2> c3如下：

However, the `ROLLUP(c1,c2,c3)` generates only four grouping sets, assuming the hierarchy `c1 > c2 > c3` as follows:

```
(c1, c2, c3)
(c1, c2)
(c1)
()
```

ROLLUP的一个常见用途是按年份，月份和日期计算数据的聚合，考虑层次结构年份>月份>日期

以下说明了PostgreSQL ROLLUP的语法：

A common use of  `ROLLUP` is to calculate the aggregations of data by year, month, and date, considering the hierarchy `year > month > date`

The following illustrates the syntax of the PostgreSQL `ROLLUP`:

```sql
SELECT
    c1,
    c2,
    c3,
    aggregate(c4)
FROM
    table_name
GROUP BY
    ROLLUP (c1, c2, c3);
```

也可以进行部分汇总以减少生成的小计数。

It is also possible to do a partial roll up to reduce the number of subtotals generated.

```sql
SELECT
    c1,
    c2,
    c3,
    aggregate(c4)
FROM
    table_name
GROUP BY
    c1, 
    ROLLUP (c2, c3);
```

#### 示例

The following query uses the `ROLLUP` subclause to find the number of products sold by brand (subtotal) and by all brands and segments (total).

以下查询使用ROLLUP子条款查找按品牌（小计）和所有品牌和细分（总计）销售的产品数量。

```sql
SELECT
    brand,
    segment,
    SUM (quantity)
FROM
    sales
GROUP BY
    ROLLUP (brand, segment)
ORDER BY
    brand,
    segment;
```



![PostgreSQL ROLLUP example](http://www.postgresqltutorial.com/wp-content/uploads/2018/03/PostgreSQL-ROLLUP-example.png)

As you can see from the output, the third row shows the number of products sold for the `ABC`brand, the sixth row displays the number of products show for the `XYZ` brand. The last row shows the grand total which displays the total products sold for all brands and segments. In this example, the hierarchy is brand > segment.

从输出中可以看出，第三行显示了ABCbrand销售的产品数量，第六行显示了XYZ品牌的产品数量。 最后一行显示总计，显示所有品牌和细分市场的总销售产品。 在此示例中，层次结构是品牌>细分。

如果您更改品牌和细分的顺序，结果将如下所示：

If you change the order of brand and segment, the result will be different as follows:

```sql
SELECT
    segment,
    brand,
    SUM (quantity)
FROM
    sales
GROUP BY
    ROLLUP (segment, brand)
ORDER BY
    segment,
    brand;
```



![PostgreSQL ROLLUP example 2](http://www.postgresqltutorial.com/wp-content/uploads/2018/03/PostgreSQL-ROLLUP-example-2.png)

In this case, the hierarchy is the segment > brand.

The following statement performs a partial roll-up:

在这种情况下，层次结构是段>品牌。

以下语句执行部分汇总：

```sql
SELECT
    segment,
    brand,
    SUM (quantity)
FROM
    sales
GROUP BY
    segment,
    ROLLUP (brand)
ORDER BY
    segment,
    brand;
```



![PostgreSQL ROLLUP - partial roll up](http://www.postgresqltutorial.com/wp-content/uploads/2018/03/PostgreSQL-ROLLUP-partial-roll-up.png)

See the following `rental` table from the [sample database](http://www.postgresqltutorial.com/postgresql-sample-database/).

请参阅示例数据库中的以下租赁表。

![Rental Table](/imgs/rental.png)

The following statement finds the number of rental per day, month, and year by using the `ROLLUP`:

以下语句使用ROLLUP查找每天，每月和每年的租赁数量：

```sql
SELECT
    EXTRACT (YEAR FROM rental_date) y,
    EXTRACT (MONTH FROM rental_date) M,
    EXTRACT (DAY FROM rental_date) d,
    COUNT (rental_id)
FROM
    rental
GROUP BY
    ROLLUP (
        EXTRACT (YEAR FROM rental_date),
        EXTRACT (MONTH FROM rental_date),
        EXTRACT (DAY FROM rental_date)
    );
```



![PostgreSQL ROLLUP example with year month and date](http://www.postgresqltutorial.com/wp-content/uploads/2018/03/PostgreSQL-ROLLUP-example-with-year-month-and-date.png)

In this tutorial, you have learned how to use the PostgreSQL `ROLLUP` to generate multiple grouping sets.

在本教程中，您学习了如何使用PostgreSQL ROLLUP生成多个分组集。