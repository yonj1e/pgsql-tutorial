## SERIAL



![PostgreSQL Serial](/imgs/PostgreSQL-Serial-268x300.png)

> 摘要：在本教程中，我们将向您介绍PostgreSQL SERIAL，并向您展示如何使用serial在数据库表中创建自增列。

#### 简介

在PostgreSQL中，序列是一种特殊的数据库对象，它生成一个整数序列。序列通常用作[主键](content/postgresql-primary-key.md)列。PostgreSQL中序列的概念类似于[MySQL中的 AUTO_INCREMENT 概念](http://www.mysqltutorial.org/mysql-sequence/)。

[创建新表](content/postgresql-create-table.md)时，序列是通过 `SERIAL` 伪类型创建的，如下所示：

```sql
CREATE TABLE table_name(id SERIAL);
```

通过将 `SERIAL` 伪类型分配给 `id` 列，PostgreSQL将执行以下操作：

- 创建序列对象并将序列生成的下一个值设置为列的默认值。
- 向列添加[NOT NULL约束](content/postgresql-not-null-constraint.md)，因为序列始终生成一个整数，这是一个非空值。
- 将序列的所有者分配给 `id` 列; 因此，删除 `id` 列或表时将删除序列对象

以下声明：

```sql
CREATE TABLE table_name(id SERIAL);
```

等同于：

```sql
CREATE SEQUENCE table_name_id_seq;
 
CREATE TABLE table_name (
    id integer NOT NULL DEFAULT nextval('table_name_id_seq')
);
 
ALTER SEQUENCE table_name_id_seq
OWNED BY table_name.id;
```

PostgreSQL提供三 `serial` 伪类型 `SMALLSERIAL`，`SERIAL` 和 `BIGSERIAL`，具有以下特征：

| 类型        | 存储大小 | 范围                          |
| ----------- | -------- | ----------------------------- |
| SMALLSERIAL | 2 bytes  | 1 to 32,767                   |
| SERIAL      | 4 bytes  | 1 to 2,147,483,647            |
| BIGSERIAL   | 8 bytes  | 1 to 922,337,2036,854,775,807 |

#### 示例

请务必注意，`SERIAL` 不会隐式地在列上创建索引或将列作为[主键](content/postgresql-primary-key.md)列。但是，通过为 `SERIAL` 列指定 `PRIMARY KEY` 约束，可以轻松完成此操作。

以下语句创建 `fruits` 表，其`id` 列为 `SERIAL` 列：

```sql
CREATE TABLE fruits(
   id SERIAL PRIMARY KEY,
   name VARCHAR NOT NULL
);
```

要为 `serial` 列分配默认值，请忽略该列或在 [INSERT](content/postgresql-insert.md) 语句中使用 `DEFAULT` 关键字。

请参阅以下示例：

```sql
INSERT INTO fruits(name) VALUES('orange');
```

和

```sql
INSERT INTO fruits(id,name) VALUES(DEFAULT,'apple');
```

PostgreSQL在fruits表中插入两行，id列的值为1和2。

```sql
SELECT * FROM fruits;
 id |  name
----+--------
  1 | orange
  2 | apple
(2 rows)
```

要获取表中 `SERIAL` 列的序列名，请使用 `pg_get_serial_sequence()` 函数，如下所示：

```sql
pg_get_serial_sequence('table_name','column_name')
```

您可以将序列名称传递给 `currval() `函数以获取序列生成的最新值。例如，以下语句返回 `fruits_id_seq` 对象最近生成的值：

```sql
SELECT currval(pg_get_serial_sequence('fruits', 'id'));
 currval
---------
       2
(1 row)
```

如果要在向表中插入新行时获取序列生成的值，请在 `INSERT` 语句中使用 `RETURNING id` 子句。以下语句在 `fruits` 表中插入一个新行，并返回为id列生成的值。

```sql
INSERT INTO fruits(name) VALUES('banana')
RETURNING id;
 id
----
  3
(1 row)

INSERT 0 1
```

序列生成器操作不是事务安全的。这意味着如果两个并发数据库连接尝试从序列中获取下一个值，则每个客户端将获得不同的值。如果其中一个客户端回滚事务，则该客户端的序列号将不被使用，从而在序列中创建间隙。

在本教程中，您学习了如何使用 `serial` 数据类型为数据库表创建自增列。

#### 相关教程

- [数据类型](content/postgresql-data-types.md)