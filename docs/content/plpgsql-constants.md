## PL/pgSQL常量

> 摘要：在本教程中，我们将向您展示如何声明PL/pgSQL常量。

与[变量](content/plpgsql-variables.md)不同，常量值一旦初始化就无法更改。在函数中使用常量有很多原因。

首先，常量使代码更具可读性，例如，假设我们有一个如下公式：

```
selling_price = net_price + net_price * 0.1;
```

0.1意味着什么？ 它可以被解释为任何东西。但是，当我们使用以下公式时，我们知道计算销售价格等于净价加上增值税（VAT）的含义。

```sql
selling_price = net_price + net_price * VAT;
```

其次，常量减少了维护工作，想象你有计算函数中所有地方的销售价格的公式。当增值税变化时，例如，从0.1到0.5，您需要更改所有这些值。但是，通过使用常量，您只需要在声明和初始化常量的位置进行更改。

那么我们如何在PL/pgSQL中声明一个常量？

#### 语法

要在PL/pgSQL中声明常量，请使用以下语法：

```sql
constant_name CONSTANT data_type := expression;
```

让我们更详细地研究语法的每个组成部分：

- 首先，指定常量名称。按照惯例，它是大写形式，如 `VAT`，`DISCOUNT` 等。
- 其次，使用 `CONSTANT` 关键字并指定与常量关联的[数据类型](content/postgresql-data-types.md)。
- 第三，必须初始化常量的值。

#### 示例

以下示例为增值税声明一个名为 `VAT` 的常数，并根据净价计算销售价格：

```sql
DO $$ 
DECLARE
 VAT CONSTANT numeric := 0.1;
 net_price numeric := 20.5;
BEGIN 
 RAISE NOTICE 'The selling price is %', net_price * ( 1 + VAT );
END $$;
```

```sql
NOTICE:  The selling price is 22.55
```

现在，如果您尝试更改常量的值，如下所示：

```sql
DO $$ 
DECLARE
 VAT constant numeric := 0.1;
 net_price numeric := 20.5;
BEGIN 
 RAISE NOTICE 'The selling price is %', net_price * ( 1 + VAT );
 VAT := 0.05;
END $$;
```

您将收到一条错误消息：

```sql
ERROR: "vat" is declared CONSTANT
SQL state: 22005
Character: 155
```

请注意，PostgreSQL在运行时输入块时计算常量的值，而不是在编译时。考虑以下示例：

```sql
DO $$ 
DECLARE
 start_at CONSTANT time := now();
BEGIN 
 RAISE NOTICE 'Start executing block at %', start_at;
END $$;
```

```sql
NOTICE:  Start executing block at 17:49:59.791
```

PostgreSQL每次调用块时都会计算 `now()` 函数。为了证明这一点，我们再次执行该块：

```sql
NOTICE:  Start executing block at 17:50:44.956
```

我们得到了不同的结果。

在本教程中，您学习了如何在PL/pgSQL中声明和使用常量。