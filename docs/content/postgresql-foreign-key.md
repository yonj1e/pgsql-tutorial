## 外键

> 摘要：在本教程中，您将了解PostgreSQL外键以及如何使用外键约束向表中添加外键。

外键是表中的字段或字段组，用于唯一标识另一个表中的行。换句话说，在引用另一个表的[主键](content/postgresql-primary-key.md)的表中定义外键。

包含外键的表称为引用表或子表。并且外键引用的表称为引用表或父表。

一个表可以有多个外键，具体取决于它与其他表的关系。

在PostgreSQL中，您通过外键约束定义外键。外键约束指示子表中的列或列组中的值与父表的列或列组中的值匹配。我们说外键约束维护子表和父表之间的引用完整性。

#### 定义简单的外键约束

假设我们有一个名为 `so_headers` 的表，用于存储销售订单标题信息，例如销售订单ID，客户ID和发货地址：

```sql
CREATE TABLE so_headers ( 
  id SERIAL PRIMARY KEY, 
  customer_id INTEGER, 
  ship_to VARCHAR (255)
);
```

销售订单的项目存储在另一个销售订单项目表（`so_items`）中：

```sql
CREATE TABLE so_items (  
  item_id INTEGER NOT NULL,   
  so_id INTEGER,  
  product_id INTEGER,  
  qty INTEGER,  
  net_price numeric,  
  PRIMARY KEY (item_id,so_id)
);
```

销售订单项目表的主键由两列组成：项目ID（`item_id`）和销售订单ID（`so_id`）。

![Postgresql foreign key](/imgs/Postgresql-foreign-key.png)

我们假设销售订单项目表（`so_items`）包含存在的销售订单数据。为此，我们在 `so_items` 表中定义一个外键约束，该约束引用 [CREATE TABLE](content/postgresql-create-table.md) 语句中的 `so_headers` 表，如下所示：

```sql
CREATE TABLE so_items (  
	item_id INTEGER NOT NULL,   
	so_id INTEGER REFERENCES so_headers(id),  
	product_id INTEGER,  qty INTEGER,  
	net_price numeric,  
	PRIMARY KEY (item_id,so_id)
);
```

请注意，我们使用 `REFERENCES` 子句为 `so_items` 表定义外键约束。这意味着 `so_items` 表中的 `so_id` 列引用了 `so_headers` 表的 `id` 列。

```sql
REFERENCES so_headers(id)
```

![Postgresql foreign key illustration](/imgs/Postgresql-foreign-key-illustration.png)

定义外键约束的另一种方法是使用表约束，如下所示：

```sql
CREATE TABLE so_items ( 
	item_id INTEGER NOT NULL, 
	so_id INTEGER, product_id INTEGER, 
	qty INTEGER, net_price NUMERIC, 
	PRIMARY KEY (item_id, so_id), 
	FOREIGN KEY (so_id) REFERENCES so_headers (id)
);
```

因为我们没有明确指定外键约束的名称，所以PostgreSQL分配了一个名称为：`table_column_fkey` 的名称。在我们的示例中，PostgreSQL创建一个外键约束为 `so_items_so_id_fkey`。

销售订单的每个行项目必须属于特定的销售订单。每个销售订单可以包含一个或多个订单项。这是一对多的关系。我们不能在 `so_items` 中插入一行而不引用 `so_items` 表中的有效 so_id`。

删除 `so_headers` 中的行时，`so_items` 表中的行会发生什么？ PostgreSQL为我们提供了以下主要选项：`DELETE RESTRICT`，`DELETE CASCADE` 和 `NO ACTION`。

在删除 `so_items` 中的所有引用行之前，PostgreSQL不会删除 `so_headers` 表中的行。为实现这一点，我们在定义外键约束时使用 `ON DELETE RESTRICT` 表达式。

```sql
CREATE TABLE so_items (  
    item_id INTEGER NOT NULL,   
    so_id int4 REFERENCES so_headers(id) ON DELETE RESTRICT,  
    product_id INTEGER,  
    qty INTEGER,  
    net_price numeric,  
    PRIMARY KEY (item_id,so_id)
);
```

PostgreSQL将删除 `so_items` 表中引用 so_headers` 表中正在删除的行的所有行。为了指示PostgreSQL执行此操作，我们使用 ON DELETE CASCADE`，如下所示：

```sql
CREATE TABLE so_items (  
    item_id int4 NOT NULL,  
    so_id int4 REFERENCES so_headers(id) ON DELETE CASCADE,  
    product_id int4,  
    qty int4,  
    net_price numeric,  
    PRIMARY KEY (item_id,so_id)
);
```

If we don’t specify `RESTRICT` or `DELETE` action, PostgreSQL will use `NO ACTION` by default. With `NO ACTION`, PostgreSQL will raise an error if the referencing rows still exist when the constraint is checked.

Notice that actions for deleting is also applied for updating. It means you can have `ON UPDATE RESTRICT`, `ON UPDATE CASCADE` and `ON UPDATE NO ACTION`.

如果我们不指定 `RESTRICT` 或 `DELETE` 操作，默认情况下PostgreSQL将使用 `NO ACTION`。如果没有操作，如果在检查约束时引用行仍然存在，PostgreSQL将引发错误。

请注意，删除操作也适用于更新。这意味着您可以拥有 `ON UPDATE RESTRICT`，`ON UPDATE CASCADE` 和 `ON UPDATE NO ACTION`。

#### 将一组列定义为外键

如果外键是一组列，我们使用以下语法定义外键约束：

```sql
CREATE TABLE child_table(  
    c1 INTEGER PRIMARY KEY,  
    c2 INTEGER,  
    c3 INTEGER,  FOREIGN KEY (c2, c3) REFERENCES parent_table (p1, p2)
);
```

#### 现有表添加外键约束

要将外键约束添加到现有表，请使用 [ALTER TABLE](content/postgresql-alter-table.md) 语句，如下所示：

```sql
ALTER TABLE child_table 
ADD CONSTRAINT constraint_name 
FOREIGN KEY (c1) REFERENCES parent_table (p1);
```

最后要注意的是，如果要将 `ON DELETE CASCADE` 的外键约束添加到现有表中，则需要执行以下步骤：

1. 删除现有的外键约束。
2. 使用 `ON DELETE CASCADE` 操作添加新的外键约束。

```sql
ALTER TABLE child_table DROP CONSTRAINT constraint_fkey;
```



```sql
ALTER TABLE child_table
ADD CONSTRAINT constraint_fk
FOREIGN KEY (c1)
REFERENCES parent_table(p1)
ON DELETE CASCADE;
```

在本教程中，我们向您介绍了PostgreSQL外键以及如何使用 `CREATE TABLE` 和 `ALTER TABLE` 语句创建和更改外键约束。

#### 相关教程

- [CREATE TABLE](content/postgresql-create-table.md)
- [UNIQUE Constraint](content/postgresql-unique-constraint.md)
- [Not-Null Constraint](content/postgresql-not-null-constraint.md)
- [Primary Key](content/postgresql-primary-key.md)
- [CHECK Constraint](content/postgresql-check-constraint.md)