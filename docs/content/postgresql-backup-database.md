## 备份数据库

> 摘要：在本教程中，我们将向您展示如何使用PostgreSQL 备份工具备份数据库，包括 `pg_dump` 和 `pg_dumpall`。

备份数据库是数据库管理中最关键的任务之一。 在备份数据库之前，您应该考虑以下几点：

- 完整/部分数据库
- 数据和结构，或仅结构
- 时间点恢复
- 恢复性能

PostgreSQL提供了 `pg_dump` 和 `pg_dumpall` 工具来帮助您轻松有效地备份数据库。

命令：

```shell
pg_dump -U username -W -F t database_name > /work/test/pgbackup/backup_file.tar
```

在下一节中，我们将向您展示备份一个数据库，所有数据库以及仅备份数据库对象的实用方法。

#### 如何备份一个数据库

要备份一个数据库，可以使用 `pg_dump` 工具。 `pg_dump` 将所有数据库对象的内容转储到单个文件中。

首先，切换到到PostgreSQL BIN文件夹：

```shell
cd /work/pgsql/pgsql-10-stable/bin/
```

其次，执行 `pg_dump` 程序并使用以下选项将 `dvdrental` 数据库备份到 `/work/test/pgbackup` 文件夹中的 `dvdrental.tar` 文件。

```shell
./pg_dump -U postgres -W -F t dvdrental > /work/test/pgbackup/dvdrental.tar
```

让我们更详细地研究这些选项。

`-U postgres`：指定连接到PostgreSQL数据库服务器的用户。 我们在这个例子中使用了postgres。

`-W`：强制 `pg_dump` 在连接到PostgreSQL数据库服务器之前提示输入密码。 点击Enter后，`pg_dump` 将提示输入 `postgres` 用户的密码。

`-F`：指定输出文件格式，可以是以下之一：

- c：自定义格式的归档文件格式
- d：目录格式存档
- t：tar
- p：纯文本SQL脚本文件）。

因为我们希望输出文件是tar格式的归档文件，所以在本例中我们使用 `-F t`。

`dvdrental`：是我们想要备份的数据库的名称

`/work/test/pgbackup/dvdrental.tar` 是输出备份文件路径。

#### 如何备份所有数据库

要备份所有数据库，可以按顺序运行上面的单个 `pg_dump` 命令，或者如果要加快备份过程，则可以并行运行。

- 首先，从psql中，使用命令 `\list` 列出群集中的所有可用数据库
- 其次，使用 `pg_dump` 程序备份每个单独的数据库，如上一节所述。

除了 `pg_dump` 程序，PostgreSQL还为您提供了 `pg_dumpall` 工具，允许您一次备份所有数据库。但是，由于以下原因，不建议使用此工具：

- `pg_dumpall` 程序将所有数据库一个接一个地导出到单个脚本文件中，这会阻止您执行并行还原。如果以这种方式备份所有数据库，则还原过程将花费更多时间。
- 转储所有数据库的处理时间比每个数据库的处理时间长，因此您不知道每个数据库的哪个转储与特定时间点相关。

如果您有充分的理由使用 `pg_dumpall` 来备份所有数据库，则以下命令是：

```shell
./pg_dumpall -U postgres > /work/test/pgbackup/all.sql
```

`pg_dumpall` 程序的选项类似于 `pg_dump` 程序的选项。 我们省略了 `-W` 选项以避免为每个单独的数据库键入密码，这非常繁琐。

#### 如何备份数据库对象定义

有时，您只想备份数据库对象定义，以便只能还原结构。 这在测试阶段很有用，您不希望在测试期间保留旧的测试数据。

要备份所有数据库中的所有对象，包括角色，表空间，数据库，模式，表，索引，触发器，函数，约束，视图，所有权和特权，请使用以下命令：

```shell
./_dumpall --schema-only > /work/test/pgbackup/definitiononly.sql
```

如果只想备份角色定义，请使用以下命令：

```shell
./pg_dumpall --roles-only > /work/test/pgbackup/allroles.sql
```

如果要备份[表空间](content/postgresql-create-tablespace.md)定义，请使用以下命令：

```shell
./pg_dumpall --tablespaces-only > /work/test/pgbackup/allroles.sql
```

#### 进一步阅读

- http://www.postgresql.org/docs/current/static/app-pgdump.html - 如何使用 `pg_dump` 工具。

#### 相关教程

- [Restore Database](content/postgresql-restore-database.md)