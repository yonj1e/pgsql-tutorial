## TRIM

> 摘要：在本教程中，您将学习如何使用 `TRIM()` 函数从字符串中删除包含空格或特定字符的最长字符串。

#### 简介

`TRIM()` 函数从字符串中删除包含特定字符的最长字符串。默认情况下，如果未明确指定要删除的字符，则 `TRIM()` 函数会删除空格（' '）。

使用 `TRIM()` 函数，您可以从字符串的开头，结尾或开头和结尾两者中删除包含字符的最长字符串。请注意，字符串可以是以下任何[数据类型](content/postgresql-data-types.md): [char, varchar 和 text](content/postgresql-char-varchar-text.md)。

当我们想要从数据库中的字符串中删除不需要的字符时， `TRIM()` 函数非常有用。我们经常用它来进行数据清理。

以下说明了 `TRIM()` 函数的语法。

```sql
TRIMT ([LEADING | TRAILING | BOTH] [characters] FROM string)
```

例如，如果要从字符串的开头删除空格，请使用以下语法：

```sql
TRIM(LEADING FROM string)
```

`TRIM()` 函数的以下语法从字符串末尾删除所有空格。

```sql
TRIM(TRAILING FROM string)
```

要删除字符串开头和结尾的所有空格，请使用以下语法：

```sql
TRIM(BOTH FROM string)
```

或者只是简单地：

```sql
TRIM(string)
```

#### 示例

请参阅以下示例，从字符串中删除前导，尾随以及前导和尾随空格。

```sql
SELECT
 TRIM (
 LEADING
 FROM
 '  PostgreSQL TRIM'
 ),
 TRIM (
 TRAILING
 FROM
 'PostgreSQL TRIM   '
 ),
 TRIM ('  PostgreSQL TRIM  ');
       ltrim      |      rtrim      |      btrim
-----------------+-----------------+-----------------
 PostgreSQL TRIM | PostgreSQL TRIM | PostgreSQL TRIM
(1 row)
```

以下语句使用没有前导和尾随空格的值[更新](content/postgresql-update.md)示例数据库中 `customer` 表的 `first_name` 和 `last_name` 列。它使用 `TRIM()` 函数从 `first_name` 和 `last_name` 列中删除前导和尾随空格。

```sql
UPDATE customer
SET first_name = TRIM (first_name),
    last_name = TRIM (last_name);
```
以下语句从数字中删除前导零（0）。因为 `TRIM()` 函数只接受一个字符串作为参数，所以我们必须使用[类型转换](content/postgresql-cast.md)将数字转换为字符串，然后再将其传递给 `TRIM()` 函数。
```sql
SELECT
 TRIM (
 LEADING '0'
 FROM
 CAST (0009100 AS TEXT)
 ); -- 9100
```

#### LTRIM, RTRIM, BTRIM

PostgreSQL为您提供 `LTRIM`， `RTRIM` 和 `BTRIM` 函数，它们是 `TRIM()` 函数的较短版本。

- `LTRIM()` 函数从字符串的开头删除所有字符，默认情况下为空格。
- `RTRIM()` 函数从字符串末尾删除所有字符，默认情况下为空格。
- `BTRIM` 函数是 `LTRIM()`和 `RTRIM()` 函数的组合。

`LTRIM()` 和 `RTRIM()` 函数的语法如下：

```sql
LTRIM(string, [character]);
RTRIM(string, [character]);
BTRIM(string, [character]);
```

相当于 `TRIM()` 函数的以下语法：

```sql
TRIM(LEADING character FROM string); -- LTRIM(string,character)
TRIM(TRAILING character FROM string); -- RTRIM(string,character)
TRIM(BOTH character FROM string); -- BTRIM(string,character)
```

让我们看一下使用 `LTRIM()`，`RTRIM()` 和 `BTRIM()` 函数从企业字符串中删除字符e的以下示例：

```sql
SELECT LTRIM('enterprise', 'e');
   ltrim
-----------
 nterprise
(1 row)
```

```sql
SELECT RTRIM('enterprise', 'e');
   rtrim
-----------
 enterpris
(1 row)
```

```sql
SELECT BTRIM('enterprise', 'e');
  btrim
----------
 nterpris
(1 row)
```

#### 空白字符

有时，您的字符串可能包含要删除的[空格](https://en.wikipedia.org/wiki/Whitespace_character)，制表符，换行符等空格字符。但是， `TRIM()` 函数只允许您删除前导和尾随空格，而不是所有其他空白字符。您可以多次调用 `TRIM()` 函数，但效率不高。

从字符串中删除前导和尾随空格字符的一种方法是使用 ·REGEXP_REPLACE()· 函数。例如，以下语句从 `enterprise` 字符串末尾删除空格和制表符，其中字符串末尾有1个空格和1个制表符。

```sql
SELECT REGEXP_REPLACE('enterprise 	', '\s+$', '');
 regexp_replace
----------------
 enterprise
(1 row)
```

`\s+$` 模式解释如下：

- `\s` ：空格的正则表达式类简写。
- `+` 表示一个或多个连续匹配。
- `$` 表示字符串的结尾。

如果要删除前导空白字符，请使用 `^\s+` 正则表达式。

在本教程中，我们向您展示了如何使用 `TRIM()`， `LTRIM()`， `RTRIM()` 和 `BTRIM()` 函数从字符串中的开头，结尾和两者中删除字符。