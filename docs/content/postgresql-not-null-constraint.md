## 非空约束

> 摘要：在本教程中，您将了解PostgreSQL非空约束，以确保列的值不为null。

在数据库理论中，NULL是未知或缺少的信息。NULL值不同于空字符串或数字零。例如，您可以向某人询问电子邮件地址，如果您不知道，则使用NULL值插入电子邮件列。这表示插入时的信息未知。如果此人没有任何电子邮件地址，您可以将其更新为空字符串。

NULL值非常特殊。例如，NULL甚至不等于任何NULL。要检查值是否为NULL，请使用布尔运算符IS NULL或IS NOT NULL。表达式NULL = NULL返回NULL。

PostgreSQL提供非空约束来强制列不能接受NULL值。这意味着无论何时[插入]()或[更新](content/postgresql-update.md)数据，都必须指定一个与NULL值不同的值。

#### 在创建新表时，给列添加非空约束。

以下 [CREATE TABLE](content/postgresql-create-table.md) 语句创建一个带not-null约束新表 `invoice`。

```sql
CREATE TABLE invoice(
  id serial PRIMARY KEY,
  product_id int NOT NULL,
  qty numeric NOT NULL CHECK(qty > 0),
  net_price numeric CHECK(net_price > 0) 
);
```

您在列的[数据类型](content/postgresql-data-types.md)后使用 `NOT NULL` 来声明非空约束。在这种情况下，它是列约束。

请注意，列可以具有多个约束，例如not-null, [check](content/postgresql-check-constraint.md), [unique](content/postgresql-unique-constraint.md)，[外键](content/postgresql-foreign-key.md)等。顺序并不重要，因此PostgreSQL可以按任何顺序检查列表中的任何空约束。

如果使用NULL而不是NOT NULL，则列将接受null和非null值。如果不同时使用NOT NULL和NULL，PostgreSQL默认使用NULL。

#### 现有表的列添加非空约束

要向现有表的列添加非空约束，请使用 `ALTER TABLE` 语句，如下所示：

```sql
ALTER  TABLE table_name
ALTER COLUMN column_name SET NOT NULL;
```

若要将非空约束添加到现有表的多个列，请使用以下语法：

```sql
ALTER  TABLE table_name
ALTER COLUMN column_name_1 SET NOT NULL,
ALTER COLUMN column_name_2 SET NOT NULL;
```

我们来看看下面的例子。

首先，创建一个名为生产订单（`production_orders`）的新表：

```sql
CREATE TABLE production_orders (
 ID serial PRIMARY KEY,
 description VARCHAR (40) NOT NULL,
 material_id VARCHAR (16),
 qty NUMERIC,
 start_date DATE,
 finish_date DATE
);
```

接下来，在 `production_orders` 表中插入一个新行：

```sql
INSERT INTO production_orders (description)
VALUES
 ('make for infosys inc.');
```

然后，您要确保 `qty` 字段不为null，因此您将not-null约束添加到 `qty` 列。但是，该表中已有数据。如果您尝试添加非空约束，PostgreSQL将发出错误消息。

因此，在添加非空约束之前，需要先更新数据。

```sql
UPDATE production_orders
SET qty = 1;
```

`qty` 列中的值更新为1。

```sql
ALTER TABLE production_orders ALTER COLUMN qty
SET NOT NULL;
```

之后，您可以更新 `material_id`，`start_date` 和 `finish_date` 列的非空约束：

```sql
UPDATE production_orders
SET material_id = 'ABC',
    start_date = '2015-09-01',
    finish_date = '2015-09-01';
```

向多列添加非空约束：

```sql
ALTER TABLE production_orders 
ALTER COLUMN material_id SET NOT NULL,
ALTER COLUMN start_date SET NOT NULL,
ALTER COLUMN finish_date SET NOT NULL;
```

最后，让我们尝试打破非空约束：

```sql
UPDATE production_orders
SET qty = NULL;
```

PostgreSQL发出错误消息：

```sql
[Err] ERROR:  null value in column "qty" violates not-null constraint
DETAIL:  Failing row contains (1, make for infosys inc., ABC, null, 2015-09-01, 2015-09-01).
```

#### Not-null约束特例

您可以使用[检查约束](content/postgresql-check-constraint.md)来表示非空约束。所以以下

```sql
NOT NULL
```

相当于

```sql
CHECK(column IS NOT NULL)
```



This is useful because sometimes you may want either column a or b is not null, but not both. For example, in the `users` table, you want either `username` or `email` column is not null or empty, you can use the check constraint as follows:

这很有用，因为有时您可能希望列a或b不为空，但不是两者。例如，在 `users` 表中，您希望 `username` 或 `email` 列不为null或为空，您可以使用check约束，如下所示：

```sql
CREATE TABLE users (
 ID serial PRIMARY KEY,
 username VARCHAR (50),
 PASSWORD VARCHAR (50),
 email VARCHAR (50),
 CONSTRAINT username_email_notnull CHECK (
   NOT (
     ( username IS NULL  OR  username = '' )
     AND
     ( email IS NULL  OR  email = '' )
   )
 )
);
```

以下插入语句可以工作。

```sql
INSERT INTO users (username, email)
VALUES
 ('user1', NULL),
 (NULL, 'email1@example.com'),
 ('user2', 'email2@example.com'),
 ('user3', '');
```

但是，这个不起作用，因为它违反了NOT NULL约束：

```sql
INSERT INTO users (username, email)
VALUES
 (NULL, NULL),
 (NULL, ''),
 ('', NULL),
 ('', '');
```

```sql
[Err] ERROR:  new row for relation "users" violates check constraint "username_email_notnull"
```

在本教程中，我们向您介绍了NULL概念以及如何使用PostgreSQL非空约束来确保列中的值不是NULL。

#### 相关教程

- [CREATE TABLE](content/postgresql-create-table.md)
- [UNIQUE Constraint](content/postgresql-unique-constraint.md)
- [Foreign Key](content/postgresql-foreign-key.md)
- [Primary Key](content/postgresql-primary-key.md)
- [CHECK Constraint](content/postgresql-check-constraint.md)