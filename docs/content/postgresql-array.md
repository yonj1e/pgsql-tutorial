## 数组类型

> 摘要：在本教程中，我们将向您展示如何使用PostgreSQL数组并向您介绍一些方便的数组操作函数。

Array在PostgreSQL中扮演着重要的角色。每种[数据类型](content/postgresql-data-types.md)都有相应的数组类型，例如，整数有一个 `integer[]` 数组类型，字符有 `character[]` 数组类型等。如果你定义自己的数据类型，PostgreSQL会在后台为你创建一个相应的数组类型。

PostgreSQL允许您将列定义为任何有效数据类型的数组，包括内置类型，用户定义类型或枚举类型。

以下[CREATE TABLE](content/postgresql-create-table.md)语句创建 `contacts` 表，其中 `phones` 列被定义为文本数组。

```sql
CREATE TABLE contacts (
 id serial PRIMARY KEY,
 name VARCHAR (100),
 phones TEXT []
);
```

`phones` 列是一维数组，其中包含联系人可能拥有的各种电话号码。

#### 插入数据

以下语句将新联系人插入到 `contacts` 表中。

```sql
INSERT INTO contacts (name, phones)
VALUES
 (
 'John Doe',
 ARRAY [ '(408)-589-5846',
 '(408)-589-5555' ]
 );
```

我们使用 `ARRAY` 构造函数构造一个数组并将其插入到 `contacts` 表中。您还可以使用花括号，如下所示：

```sql
INSERT INTO contacts (name, phones)
VALUES
 (
 'Lily Bush',
 '{"(408)-589-5841"}'
 ),
 (
 'William Gate',
 '{"(408)-589-5842","(408)-589-58423"}'
 );
```

上面的语句在 `contacts` 表中插入两行。请注意，当您使用花括号时，您使用单引号 `'` 包装数组和双引号 ``来包装文本数组项。

#### 查询数据

您可以使用 [SELECT](content/postgresql-select.md) 语句查询数组数据，如下所示：

```sql
SELECT name, phones FROM contacts;
     name     |              phones
--------------+----------------------------------
 John Doe     | {(408)-589-5846,(408)-589-5555}
 Lily Bush    | {(408)-589-5841}
 William Gate | {(408)-589-5842,(408)-589-58423}
(3 rows)
```

我们使用方括号 `[]` 中的下标访问数组元素。默认情况下，PostgreSQL对数组元素使用基于1的编号。这意味着第一个数组元素以数字1开头。假设我们想获取联系人的姓名和第一个电话号码，我们使用以下查询：

```sql
SELECT name, phones[1] FROM contacts;
     name     |     phones
--------------+----------------
 John Doe     | (408)-589-5846
 Lily Bush    | (408)-589-5841
 William Gate | (408)-589-5842
(3 rows)
```

我们可以在[WHERE](content/postgresql-where.md)子句中使用array元素作为过滤行的条件。例如，要找出谁将电话号码 `（408）-589-58423` 作为第二个电话号码，我们使用以下查询。

```sql
SELECT name
FROM contacts
WHERE phones [ 2 ] = '(408)-589-58423';
     name
--------------
 William Gate
(1 row)
```

#### 修改数组

PostgreSQL允许您更新数组或整个数组的每个元素。以下声明更新了 `William Gate` 的第二个电话号码。

```sql
UPDATE contacts
SET phones[2] = '(408)-589-5843'
WHERE ID = 3;
```

我们再来看看吧。

```sql
SELECT id, name, phones[2]
FROM contacts
WHERE id = 3;
 id |     name     |     phones
----+--------------+----------------
  3 | William Gate | (408)-589-5843
(1 row)
```

以下语句将整个数组更新。

```sql
UPDATE contacts
SET phones = '{"(408)-589-5843"}'
WHERE ID = 3;
```

我们使用以下语句验证更新。

```sql
SELECT name, phones
FROM contacts
WHERE id = 3;
     name     |      phones
--------------+------------------
 William Gate | {(408)-589-5843}
(1 row)
```

#### 在数组中搜索

假设，无论电话号码中电话号码的位置如何，我们都想知道谁有电话号码 `（408）-589-5555` ，我们使用 `ANY()` 函数如下：

```sql
SELECT name, phones
FROM contacts
WHERE '(408)-589-5555' = ANY (phones);
   name   |             phones
----------+---------------------------------
 John Doe | {(408)-589-5846,(408)-589-5555}
(1 row)
```

#### 展开数组

PostgreSQL提供了 `unexst()` 函数来将数组扩展为行列表。例如，以下查询将展开电话数组的所有电话号码。

```sql
SELECT name, unnest(phones)
FROM contacts;
     name     |     unnest
--------------+----------------
 John Doe     | (408)-589-5846
 John Doe     | (408)-589-5555
 Lily Bush    | (408)-589-5841
 William Gate | (408)-589-5843
(4 rows)
```

在本教程中，我们向您展示了如何使用PostgreSQL数组数据类型，并向您介绍了一些最重要的数组运算符和函数。