## 小数类型

> 摘要：本教程向您介绍PostgreSQL NUMERIC类型，用于存储最多1000位精度的数字。

#### 简介

`NUMERIC` 类型可以存储多位精度的数字。通常，对于需要精确计算的金额等使用 `NUMERIC`。

某列要声明 `NUMERIC` 类型，使用以下语法：

```sql
NUMERIC(precision, scale)
```

`precision` 是精度， `scale` 是标度。例如，数字 `1234.567` 的精度为7，标度为3。

`NUMERIC` 类型的标度可以为零或正数，因此以下语法定义了一个标度为零的 `NUMERIC` 列：

```sql
NUMERIC(precision)
```

如果您省略了精度和标度，您可以存储任何精度和标度，以达到上述精度和标度的极限。

```sql
NUMERIC
```

在PostgreSQL中，`NUMERIC` 和 `DECIMAL` 类型是等价的，它们都是SQL标准的一部分。

如果不需要精度，则不应使用 `NUMERIC` 类型，因为对数值的计算要比[整数](content/postgresql-integer.md)、浮点数和双精度要慢。

#### 示例

##### NUMERIC

当字段声明了标度，超过小数点位数的标度会被自动四舍五入。参阅以下示例。

首先，为演示创建一个名为 `products` 的新表：

```sql
CREATE TABLE IF NOT EXISTS products (
    id serial PRIMARY KEY,
    name VARCHAR NOT NULL,
    price NUMERIC (5, 2)
);
```

第二，插入一些标度超过标度声明的产品：

```sql
INSERT INTO products (NAME, price)
VALUES
    ('Phone',500.215), 
    ('Tablet',500.214);
```

由于 `price` 列的标度为2，PostgreSQL将值 `500.215` 舍入为 `500.22`，将值 `500.214` 舍入为 `500.21`：

以下查询返回 `products` 表的所有行：

```sql
SELECT * FROM products;
 id |  name  | price
----+--------+--------
  1 | Phone  | 500.22
  2 | Tablet | 500.21
(2 rows)
```

如果存储精度超过声明精度的值，PostgreSQL将引发错误，如以下示例所示：

```sql
INSERT INTO products (name, price)
VALUES
    ('Phone',123456.21);
```

在这个例子中，PostgreSQL发出以下错误：

```sql
ERROR:  numeric field overflow
DETAIL:  A field with precision 5, scale 2 must round to an absolute value less than 10^3.
```

##### NaN

除了存储数值外，`NUMERIC` 列还可以存储一个名为not-a-number或 `NaN` 的特殊值。

```sql
UPDATE products
SET price = 'NaN'
WHERE id = 1;
```

在此示例中，我们将id为1的产品的价格更新为 `NaN`。请注意，必须使用引号来包含 `NaN`，如上面的 `UPDATE` 语句所示。

通常，`NaN` 不等于任何数字，包括其自身。这意味着表达式 `NaN = NaN` 返回false。但是，PostgreSQL将 `NaN` 值视为相等，并且 `NaN` 大于任何非 `NaN` 值。此实现允许PostgreSQL对 `NUMERIC` 值进行排序，并在基于树的索引中使用这些值。

以下查询根据价格对产品进行排序：

```sql
SELECT *
FROM PRODUCTS
ORDER BY price DESC;
 id |  name  | price
----+--------+--------
  1 | Phone  |    NaN
  2 | Tablet | 500.21
(2 rows)
```

如您所见，`NaN` 大于 `500.21`

在本教程中，您了解了PostgreSQL `NUMERIC` 数据类型以及如何使用 `NUMERIC` 存储需要精度的值。