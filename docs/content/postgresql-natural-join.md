## NATURAL JOIN

> 摘要：在本教程中，您将学习如何使用 `NATURAL JOIN` 查询来自两个或多个表的数据。

#### 简介

自然连接是一种连接，它根据连接表中的相同列名创建隐式连接。请参阅PostgreSQL自然连接的以下语法：

```sql
SELECT * FROM T1
NATURAL [INNER, LEFT, RIGHT] JOIN T2;
```

自然连接可以是[内连接](content/postgresql-inner-join.md)，[左连接](content/postgresql-left-join.md)或右连接。如果没有明确指定连接，例如 `INNER JOIN`，`LEFT JOIN`，`RIGHT JOIN`，PostgreSQL默认使用 `INNER JOIN`。

如果在选择列表中使用星号（*），则结果将包含以下列：

- 所有公共列，即两个表中具有相同名称的列
- 第一个和第二个表中不是公共列的每一列

#### 示例

为了演示PostgreSQL自然连接，我们将创建两个表：`categories` 和 `products`。

以下 `CREATE TABLE` 语句创建 `categories` 和 `products` 表。

```sql
CREATE TABLE categories (
 category_id serial PRIMARY KEY,
 category_name VARCHAR (255) NOT NULL
);
 
CREATE TABLE products (
 product_id serial PRIMARY KEY,
 product_name VARCHAR (255) NOT NULL,
 category_id INT NOT NULL,
 FOREIGN KEY (category_id) REFERENCES categories (category_id)
);
```

每个类别都有零个或多个产品，而每个产品属于一个且只有一个类别。`products` 表中的 `category_id` 列是引用类别表的主键的[外键](content/postgresql-foreign-key.md)。`category_id` 是我们将用于执行自然连接的公共列。

以下[INSERT](content/postgresql-insert.md)语句将一些示例数据插入到类别和产品表中。

```sql
INSERT INTO categories (category_name)
VALUES ('Smart Phone'), ('Laptop'), ('Tablet');
 
INSERT INTO products (product_name, category_id)
VALUES ('iPhone', 1), ('Samsung Galaxy', 1), ('HP Elite', 2),
	('Lenovo Thinkpad', 2), ('iPad', 3), ('Kindle Fire', 3);
```

以下语句使用 `NATURAL JOIN` 子句将 `products` 表与 `categories` 表连接：

```sql
SELECT * FROM products
NATURAL JOIN categories;
 category_id | product_id |  product_name   | category_name
-------------+------------+-----------------+---------------
           1 |          1 | iPhone          | Smart Phone
           1 |          2 | Samsung Galaxy  | Smart Phone
           2 |          3 | HP Elite        | Laptop
           2 |          4 | Lenovo Thinkpad | Laptop
           3 |          5 | iPad            | Tablet
           3 |          6 | Kindle Fire     | Tablet
(6 rows)
```

上述语句等效于以下使用 `INNER JOIN` 子句的语句。

```sql
SELECT * FROM products
INNER JOIN categories USING (category_id);
```

`NATURAL JOIN` 的便利之处在于它不需要您指定 `join` 子句，因为它使用基于公共列的隐式连接子句。

但是，应尽可能避免使用 `NATURAL JOIN`，因为有时可能会导致意外结果。

例如，让我们看看 `city` 和 `country` 表。两个表都具有相同的 `country_id` 列，因此我们可以使用 `NATURAL JOIN` 连接这些表，如下所示：

```sql
SELECT * FROM city
NATURAL JOIN country;
 country_id | last_update | city_id | city | country
------------+-------------+---------+------+---------
(0 rows)
```

查询返回空结果集。

原因是......

两个表都有一个名为 `last_update` 的公共列，不能用于连接。但是，`NATURAL JOIN` 子句只使用 `last_update` 列。

在本教程中，我们向您解释了 `NATURAL JOIN` 的工作原理，并向您展示了如何使用它来查询来自两个或多个表的数据。

#### 相关教程

- [LEFT JOIN](http://www.postgresqltutorial.com/postgresql-left-join/)
- [Cross Join](http://www.postgresqltutorial.com/postgresql-cross-join/)
- [FULL OUTER JOIN](http://www.postgresqltutorial.com/postgresql-full-outer-join/)
- [INNER JOIN](http://www.postgresqltutorial.com/postgresql-inner-join/)