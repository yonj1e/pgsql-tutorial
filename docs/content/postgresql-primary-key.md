## 主键

> 摘要：在本教程中，我们将向您展示主键是什么以及如何通过SQL语句管理PostgreSQL主键约束。

主键是用于在表中唯一标识行的列或列组。

您可以通过主键约束定义主键。从技术上讲，主键约束是[非空约束](content/postgresql-not-null-constraint.md)和[UNIQUE约束](content/postgresql-unique-constraint.md)的组合。

一个表只能有一个主键。向每个表添加主键是一种很好的做法。向表中添加主键时，PostgreSQL会在列或用于定义主键的一组列上创建唯一的B树索引。

#### 创建表时定义主键

通常，我们在使用 [CREATE TABLE](content/postgresql-create-table.md) 语句定义表的结构时将主键添加到表中。

```sql
CREATE TABLE TABLE (
 column_1 data_type PRIMARY KEY,
 column_2 data_type,
 …
);
```

The following statement creates a purchase order (PO) header table with the name `po_headers`.

以下语句创建名为 `po_headers` 的采购订单表。

```sql
CREATE TABLE po_headers (
 po_no INTEGER PRIMARY KEY,
 vendor_no INTEGER,
 description TEXT,
 shipping_address TEXT
);
```

`po_no` 是 `po_headers` 表的主键，它在 `po_headers` 表中唯一标识采购订单。

如果主键由两列或更多列组成，则按如下方式定义主键约束：

```sql
CREATE TABLE TABLE (
 column_1 data_type,
 column_2 data_type,
 … 
        PRIMARY KEY (column_1, column_2)
);
```

例如，以下语句创建采购订单项目表，其主键是采购订单编号（`po_no`）和项目编号（`item_no`）的组合。

```sql
CREATE TABLE po_items (
 po_no INTEGER,
 item_no INTEGER,
 product_no INTEGER,
 qty INTEGER,
 net_price NUMERIC,
 PRIMARY KEY (po_no, item_no)
);
```

如果未明确指定主键约束的名称，PostgreSQL将为主键约束指定默认名称。默认情况下，PostgreSQL使用 `table-name_pkey` 作为主键约束的默认名称。在此示例中，PostgreSQL为 `po_items` 表创建名为`po_items_pkey` 的主键约束。

如果要指定主键约束的名称，请使用 `CONSTRAINT` 子句，如下所示：

```sql
CONSTRAINTC  constraint_name PRIMARY KEY(column_1, column_2,...);
```

#### 更改现有表结构时定义主键

很少为现有表定义主键。如果必须这样做，可以使用 [ALTER TABLE](content/postgresql-alter-table.md) 语句添加主键约束。

```sql
ALTER TABLE table_name ADD PRIMARY KEY (column_1, column_2);
```

以下语句创建名为 `products` 的表，而不定义任何主键。

```sql
CREATE TABLE products (
 product_no INTEGER,
 description TEXT,
 product_cost NUMERIC
);
```

假设您要向 `products` 表添加主键约束，可以执行以下语句：

```sql
ALTER TABLE products 
ADD PRIMARY KEY (product_no);
```

#### 如何将自动递增的主键添加到现有表

假设，我们有一个供应商表没有任何主键。

```sql
CREATE TABLE vendors (name VARCHAR(255));
```

我们使用 [INSERT](content/postgresql-insert.md) 语句向 `vendor` 表添加几行：

```sql
INSERT INTO vendors (NAME)
VALUES
 ('Microsoft'),
 ('IBM'),
 ('Apple'),
 ('Samsung');
```

要验证插入操作，我们使用以下[SELECT](content/postgresql-select.md)语句从 `vendor` 表中查询数据：

```sql
SELECT * FROM vendors;
   name
-----------
 Microsoft
 IBM
 Apple
 Samsung
(4 rows)
```

现在，如果我们想在 `vendor` 表中添加一个名为 `id` 的主键，并且 `id` 字段自动递增1，我们使用以下语句：

```sql
ALTERA  TABLE vendors ADD COLUMN ID SERIAL PRIMARY KEY;
```

让我们再次检查 `vendors` 表。

```sql
SELECT id,name FROM vendors;
 id |   name
----+-----------
  1 | Microsoft
  2 | IBM
  3 | Apple
  4 | Samsung
(4 rows)
```

#### 删除主键

要删除现有主键约束，还可以使用带有以下语法的 `ALTER TABLE` 语句：

```sql
ALTER TABLE table_name DROP CONSTRAINT primary_key_constraint;
```

例如，要删除 `products` 表的主键约束，请使用以下语句：

```sql
ALTER TABLE products DROP CONSTRAINT products_pkey;
```

在本教程中，您学习了如何使用 `CREATE TABLE` 和 `ALTER TABLE` 语句添加和删除主键约束。

#### 相关教程

- [CREATE TABLE](content/postgresql-create-table.md)
- [Foreign Key](content/postgresql-foreign-key.md)
- [UNIQUE Constraint](content/postgresql-unique-constraint.md)
- [Not-Null Constraint](content/postgresql-not-null-constraint.md)
- [CHECK Constraint](content/postgresql-check-constraint.md)