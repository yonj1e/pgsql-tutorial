## ANY

> 摘要：在本教程中，您将学习如何使用 `ANY` 运算符将标量值与子查询返回的一组值进行比较。

#### 简介

`ANY` 运算符将值与子查询返回的一组值进行比较。 以下说明了 `ANY` 运算符的语法：

```sql
expresion operator ANY(subquery)
```

在这个语法中：

- [子查询](content/postgresql-subquery.md)必须返回一列。
- `ANY` 运算符必须在以下比较运算符后面：=，<=，>，<，>和<>之一
- 如果子查询的任何值满足条件，则 `ANY` 运算符返回true，否则返回false。

请注意，`SOME` 是 `ANY` 的同义词，这意味着您可以在任何SQL语句中用 `SOME` 替代 `ANY`。

#### 示例

我们将在[示例数据库](content/postgresql-sample-database.md)中使用以下 `film` 和 `film_category` 进行演示。

![film and film_category table](/imgs/film-and-film_category-tables.png)

以下示例返回按电影类别分组的电影的[最大](content/postgresql-max-function.md)长度：

```sql
SELECT MAX( length )
FROM film
INNER JOIN film_category USING(film_id)
GROUP BY category_id;
```

您可以在以下语句中将此查询用作子查询，以查找长度大于或等于任何电影类别的最大长度的电影：

```sql
SELECT title
FROM film
WHERE length >= ANY(
    SELECT MAX( length )
    FROM film
    INNER JOIN film_category USING(film_id)
    GROUP BY  category_id );
             title
------------------------
 Alley Evolution
 Analyze Hoosiers
 Anonymous Human
 Baked Cleopatra
 Casualties Encino
 Born Spinal
 Catch Amistad
 Cause Date
```

对于每个电影类别，子查询找到最大长度。 外部查询查看所有这些值并确定哪个影片的长度大于或等于任何影片类别的最大长度。

请注意，如果子查询未返回任何行，则整个查询将返回空结果集。

#### ANY vs. IN

`= ANY` 等效于 `IN` 运算符。

以下示例获取其类别为 `Action` 或 `Drama` 的电影。

```sql
SELECT title, category_id
FROM film
INNER JOIN film_category USING(film_id)
WHERE
    category_id = ANY(
        SELECT category_id
        FROM category
        WHERE NAME = 'Action' OR NAME = 'Drama'
    );
          title          | category_id
-------------------------+-------------
 Amadeus Holy            |           1
 American Circus         |           1
 Antitrust Tomatoes      |           1
 Apollo Teen             |           7
 Ark Ridgemont           |           1
 Barefoot Manchurian     |           1
 Beauty Grease           |           7
 Beethoven Exorcist      |           7
```

以下语句使用I `N` 运算符生成相同的结果：

```sql
SELECT title, category_id
FROM film
INNER JOIN film_category USING(film_id)
WHERE 
	category_id IN(
        SELECT category_id
        FROM category
        WHERE NAME = 'Action' OR NAME = 'Drama'
    );
```

请注意，`<> ANY` 运算符与 `NOT IN` 不同。 以下表达式：

```sql
x <> ANY (a,b,c)
```

相当于

```sql
x <> a OR <> b OR x <> c
```

在本教程中，您学习了如何使用 `ANY` 运算符将值与子查询返回的一组值进行比较。