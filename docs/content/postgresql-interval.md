## 时间间隔类型

> 摘要：在本教程中，您将了解PostgreSQL间隔数据类型以及如何操作间隔值。

#### 简介

间隔数据类型允许您以年，月，日，小时，分钟，秒等存储和操作一段时间。以下说明间隔类型：

```sql
@ interval [ fields ] [ (p) ]   
```

间隔值需要16字节的存储大小，可以存储允许范围为 `-178,000,000` 年到 `178,000,000` 年的周期。

此外，间隔值可以具有可选的精度值 `p`，允许的范围是 `0` 到 `6` 。精度 `p` 是第二个字段中保留的小数位数。

at符号（`@`）是可选的，因此您可以省略它。

以下示例显示了一些间隔值：

```sql
interval '2 months ago';
interval '3 hours 20 minutes';
```

在内部，PostgreSQL将间隔值存储为月，日和秒。月份和日期值是[整数](content/postgresql-integer.md)，而秒数字段可以包含分数。

当执行[日期](content/postgresql-date.md)或时间算术时，间隔值非常有用。例如，如果您想知道去年当前时间的3小时2分钟前的时间，可以使用以下语句：

```sql
SELECT now(), now() - INTERVAL '1 year 3 hours 20 minutes' 
AS "3 hours 20 minutes ago of last year";
              now              | 3 hours 20 minutes ago of last year
-------------------------------+-------------------------------------
 2018-09-20 02:45:03.511695-04 | 2017-09-19 23:25:03.511695-04
(1 row)
```

让我们看看如何格式化输入和输出的间隔值。

## 输入格式

PostgreSQL为您提供以下详细语法来编写间隔值：

```sql
quantity unit [quantity unit...] [direction]
```

- 数量是数字，符号 `+` 或 `-` 也被接受
- `unit` 可以是千年，世纪，十年，年，月，周，日，小时，分钟，秒，毫秒，微秒或缩写（y，m，d等）或复数形式（months, days, etc.）中的任何一种。
- `direction` 可以是前或空字符串 `''` 。

这种格式称为 `postgres_verbose`，它也用于间隔输出格式。以下示例说明了使用详细语法的一些间隔值：

```sql
INTERVAL '1 year 2 months 3 days';
INTERVAL '2 weeks ago';
```

##### ISO 8601间隔格式

除了上面的详细语法之外，PostgreSQL还允许您使用 `ISO 8601` 时间间隔以两种方式编写间隔值：格式与指示符和替代格式。

带有指示符的 `ISO 8601` 格式如下：

```sql
P quantity unit [ quantity unit ...] [ T [ quantity unit ...]]
```

在此格式中，间隔值必须以字母 `P` 开头。字母 `T` 用于确定时间单位。

下表说明了 `ISO 8601` 间隔单位缩写：

| 缩写 | 描述                       |
| ---- | -------------------------- |
| Y    | Years                      |
| M    | Months (in the date part)  |
| W    | Weeks                      |
| D    | Days                       |
| H    | Hours                      |
| M    | Minutes (in the time part) |
| S    | Seconds                    |

请注意，`M` 可以是几个月或几分钟，具体取决于它是出现在字母 `T` 之前还是之后。

例如，6年5个月4天3小时2分1秒的间隔可以用 `ISO 8601` 指示符格式写成如下：

```sql
P6Y5M4DT3H2M1SP
```

`ISO 8601` 的替代形式是：

```sql
P [ years-months-days ] [ T hours:minutes:seconds ]
```

它也必须以字母 `P` 开头，字母 `T` 分隔间隔值的日期和时间部分。例如，6年5个月4天3小时2分1秒的间隔可以用 `ISO 8601` 替代形式写成：

```
P0006P -05-04T03:02:01
```

##### 输出格式

使用 `SET intervalstyle` 命令设置间隔值的输出样式，例如：

```sql
SET intervalstyle = 'sql_standard';
```

PostgreSQL提供了四种输出格式：`sql standard`，`postgres`，`postgresverbose` 和 `iso_8601`。PostgresQL默认使用 `postgres` 样式来格式化间隔值。

以下代表4种风格的6年5个月4天3小时2分1秒的间隔：

```sql
SET intervalstyle = 'sql_standard';
 
SELECT
 INTERVAL '6 years 5 months 4 days 3 hours 2 minutes 1 second';
 
 
SET intervalstyle = 'postgres';
 
SELECT
 INTERVAL '6 years 5 months 4 days 3 hours 2 minutes 1 second';
 
 
SET intervalstyle = 'postgres_verbose';
 
SELECT
 INTERVAL '6 years 5 months 4 days 3 hours 2 minutes 1 second';
 
 
SET intervalstyle = 'iso_8601';
 
SELECT
 INTERVAL '6 years 5 months 4 days 3 hours 2 minutes 1 second';
```

| sql standard       | postgres                         | postgres verbose                               | iso_8601         |
| ------------------ | -------------------------------- | ---------------------------------------------- | ---------------- |
| `+6-5 +4 +3:02:01` | `6 years 5 mons 4 days 03:02:01` | `@ 6 years 5 mons 4 days 3 hours 2 mins 1 sec` | `P6Y5M4DT3H2M1S` |

#### 相关的运算符和函数

##### 运算符

您可以将算术运算符（`+`，`-` ，`*` 等）应用于间隔值，例如：

```sql
SELECT
INTERVAL '2h 50m' + INTERVAL '10m'; -- 03:00:00
 
SELECT
INTERVAL '2h 50m' - INTERVAL '50m'; -- 02:00:00
 
SELECT
600 * INTERVAL '1 minute'; -- 10:00:00
```

##### 将间隔转换为字符串

要将间隔值转换为字符串，请使用TO_CHAR()函数。

```sql
TO_CHAR(interval,format)
```

`TO_CHAR()` 函数将第一个参数作为间隔值，第二个参数作为格式，并返回表示指定格式的间隔的字符串。

请参阅以下示例：

```sql
SELECT TO_CHAR(INTERVAL '17h 20m 05s', 'HH24:MI:SS');
 to_char
----------
 17:20:05
(1 row
```

##### 从间隔中截取数据

要从间隔中提取年，月，日等字段，请使用 `EXTRACT()` 函数。

```sql
EXTRACT(field FROM interval)
```

该字段可以是您要从间隔中提取的年，月，日，小时，分钟等。extract函数返回双精度类型的值。

请参阅以下示例：

```sql
SELECT
    EXTRACT (
        MINUTE
        FROM
            INTERVAL '5 hours 21 minutes'
    );
 date_part
-----------
        21
(1 row)
```

在这个例子中，我们从5小时21分钟的时间间隔中提取出分钟，并且预期返回21。

##### 调整间隔值

PostgreSQL提供两个函数 `justifydays` 和 `justifyhours`，允许您将30天的间隔调整为一个月，将24小时的间隔调整为一天：

```sql
SELECT
    justify_days(INTERVAL '30 days'),
    justify_hours(INTERVAL '24 hours');
     justify_days | justify_hours
--------------+---------------
 1 mon        | 1 day
(1 row)
```

此外，`justify_interval` 函数使用 `justifydays` 和 `justifyhours` 调整间隔，并附加符号调整：

```sql
SELECT justify_interval(interval '1 year -1 hour');
     justify_interval
--------------------------
 11 mons 29 days 23:00:00
(1 row)
```

在本教程中，您了解了PostgreSQL间隔数据类型以及如何操作间隔值。