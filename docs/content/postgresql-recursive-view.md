## 递归视图

> 摘要：在本教程中，您将学习如何使用 `CREATE RECURSIVE VIEW` 语句创建递归视图。

#### 简介

PostgreSQL 9.3添加了一种新的语法，用于创建标准SQL中指定的递归视图。`CREATE RECURSIVE VIEW` 语句是标准[递归查询](content/postgresql-recursive-query.md)的语法。

以下说明了 `CREATE RECURSIVE VIEW` 语法：

```sql
CREATE RECURSIVE VIEW view_name(columns) AS
SELECT columns;
```

首先，在 `CREATE RECURSIVE VIEW` 子句中指定要创建的视图的名称。您可以向视图名称添加可选的模式限定。

其次，添加 [SELECT](content/postgresql-select.md) 语句以查询基表中的数据。`SELECT` 语句引用 `view_name` 以使视图递归。

上述声明等同于以下声明：

```sql
CREATE VIEW view_name 
AS
  WITH RECURSIVE cte_name (columns) AS (
    SELECT ...)
  SELECT columns FROM cte_name;
```

#### 示例

我们将使用[递归查询](content/postgresql-recursive-query.md)教程中创建的employees表进行演示。

以下递归查询使用公用表表达式或CTE将员工及其经理返回直到CEO级别。

```sql
WITH RECURSIVE reporting_line AS (
 SELECT
 employee_id,
 full_name AS subordinates
 FROM
 employees
 WHERE
 manager_id IS NULL
 UNION ALL
 SELECT
 e.employee_id,
 (
 rl.subordinates || ' > ' || e.full_name
 ) AS subordinates
 FROM
 employees e
 INNER JOIN reporting_line rl ON e.manager_id = rl.employee_id
) SELECT
 employee_id,
 subordinates
FROM reporting_line
ORDER BY employee_id;
 employee_id |                         subordinates
-------------+--------------------------------------------------------------
           1 | Michael North
           2 | Michael North > Megan Berry
           3 | Michael North > Sarah Berry
           4 | Michael North > Zoe Black
           5 | Michael North > Tim James
           6 | Michael North > Megan Berry > Bella Tucker
           7 | Michael North > Megan Berry > Ryan Metcalfe
           8 | Michael North > Megan Berry > Max Mills
           9 | Michael North > Megan Berry > Benjamin Glover
          10 | Michael North > Sarah Berry > Carolyn Henderson
          11 | Michael North > Sarah Berry > Nicola Kelly
          12 | Michael North > Sarah Berry > Alexandra Climo
          13 | Michael North > Sarah Berry > Dominic King
          14 | Michael North > Zoe Black > Leonard Gray
          15 | Michael North > Zoe Black > Eric Rampling
          16 | Michael North > Megan Berry > Ryan Metcalfe > Piers Paige
          17 | Michael North > Megan Berry > Ryan Metcalfe > Ryan Henderson
          18 | Michael North > Megan Berry > Max Mills > Frank Tucker
          19 | Michael North > Megan Berry > Max Mills > Nathan Ferguson
          20 | Michael North > Megan Berry > Max Mills > Kevin Rampling
(20 rows)
```

您可以使用 `CREATE RECURSIVE VIEW` 语句将查询转换为递归视图，如下所示：

```sql
CREATE RECURSIVE VIEW reporting_line (employee_id, subordinates) AS 
SELECT employee_id, full_name AS subordinates
FROM employees
WHERE manager_id IS NULL
UNION ALL
 SELECT
  e.employee_id,
  (rl.subordinates || ' > ' || e.full_name) AS subordinates
 FROM employees e
 INNER JOIN reporting_line rl ON e.manager_id = rl.employee_id;
```

要查看员工ID 10的行，您可以直接从视图中进行查询：

```sql
SELECT subordinates
FROM reporting_line
WHERE employee_id = 10;
                  subordinates
-------------------------------------------------
 Michael North > Sarah Berry > Carolyn Henderson
(1 row)
```

在本教程中，您学习了如何基于递归查询创建递归视图。