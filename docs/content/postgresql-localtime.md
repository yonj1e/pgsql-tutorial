## LOCALTIME

`LOCALTIME` 函数返回当前事务开始的当前时间。

#### 语法

`LOCALTIME` 函数的语法如下：

```sql
LOCALTIME(precision)
```

#### 参数

`LOCALTIME` 函数有一个可选参数：

**1) precision**

`precision` 参数指定第二个字段的小数秒精度。

如果省略参数，则默认为6。

#### 返回值

`LOCALTIME` 函数返回 `TIME` 值，该值表示当前事务开始的时间。

#### 示例

以下查询说明了如何获取当前事务的时间：

```sql
SELECT LOCALTIME;
    localtime
-----------------
 11:01:40.988461
(1 row)
```

使用指定的小数秒精度获取时间，使用以下语句：

```sql
SELECT LOCALTIME(2);
  localtime
-------------
 11:01:55.11
(1 row)
```

#### 备注

注意到 `LOCALTIME` 函数返回没有时区的 `TIME` 值，而 `CURRENT_TIME` 函数返回带时区的 `TIME`。

在本教程中，您学习了如何使用 `LOCALTIME` 函数来获取当前事务开始的时间。