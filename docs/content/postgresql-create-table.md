## CREATE TABLE

> 摘要：在本教程中，您将学习如何使用PostgreSQL CREATE TABLE语句创建新表。

#### 语法

要在PostgreSQL中创建新表，请使用 `CREATE TABLE` 语句。以下说明了 `CREATE TABLE` 语句的语法：

```sql
CREATE TABLE table_name (
 column_name TYPE column_constraint,
 table_constraint table_constraint
) INHERITS existing_table_name;
```

让我们更详细地研究一下 `CREATE TABLE` 语句的语法。

- 首先，在 `CREATE TABLE` 子句之后指定新表的名称。`TEMPORARY` 关键字用于创建临时表，我们将在[临时表](content/postgresql-temporary-table.md)教程中讨论。
- 接下来，列出列名称，[数据类型](content/postgresql-data-types.md)和列约束。表可以有多个以逗号（，）分隔的列。列约束定义列的规则，例如，NOT NULL。
- 然后，在列列表之后，您定义一个表级约束，该约束定义表中数据的规则。
- 之后，指定新表继承的现有表。这意味着新表包含现有表的所有列以及 `CREATE TABLE` 语句中定义的列。这是PostgreSQL对SQL的扩展。

##### 列约束

以下是PostgreSQL中常用的列约束：

- [NOT NULL](content/postgresql-not-null-constraint.md) - 列的值不能为 `NULL`。
- [UNIQUE](content/postgresql-unique-constraint.md) - 列的值在整个表中必须是唯一的。但是，该列可以有许多 `NULL` 值，因为PostgreSQL将每个 `NULL` 值视为唯一。请注意，SQL标准仅允许具有 `UNIQUE` 约束的列中的有一个 `NULL` 值。
- [PRIMARY KEY](content/postgresql-primary-key.md) - 此约束是 `NOT NULL` 和 `UNIQUE` 约束的组合。您可以使用列级约束将一列定义为 `PRIMARY KEY`。如果主键包含多个列，则必须使用表级约束。
- [CHECK](content/postgresql-check-constraint.md) - 允许在插入或更新数据时检查条件。例如，`product` 表的 `price` 列中的值必须为正值。
- [REFERENCES](content/postgresql-foreign-key.md) - 约束另一个表中列中存在的列的值。您使用 `REFERENCES` 来定义外键约束。

##### 表约束

表约束类似于列约束，除了它们应用于整个表而不是单个列。

以下是表约束：

- `UNIQUE (column_list)` - 强制存储在括号内列出的列中的值是唯一的。
- `PRIMARY KEY(column_list)` - 定义由多列组成的主键。
- `CHECK (condition)` - 在插入或更新数据时检查条件。
- `REFERENCES` - 约束存储在列中的值，该值必须存在于另一个表的列中。

#### 示例

我们将创建一个名为 `account` 的新表，其中包含以下具有相应约束的列：

- user_id – primary key
- username – unique and not null
- password – not null
- email – unique and not null
- created_on – not null
- last_login – null

以下语句创建了 `account` 表：

```sql
CREATE TABLE account(
 user_id serial PRIMARY KEY,
 username VARCHAR (50) UNIQUE NOT NULL,
 password VARCHAR (50) NOT NULL,
 email VARCHAR (355) UNIQUE NOT NULL,
 created_on TIMESTAMP NOT NULL,
 last_login TIMESTAMP
);
```

![postgresql create table example](/imgs/postgresql-create-table-example.jpg)

以下语句创建由两列组成的 `role` 表：`role_id` 和 `role_name`：

```sql
CREATE TABLE role(
 role_id serial PRIMARY KEY,
 role_name VARCHAR (255) UNIQUE NOT NULL
);
```

![postgresql create table - role table example](/imgs/postgresql-create-table-role-table.jpg)

以下语句创建 `account_roles` 表，该表包含三列：`user_id`，`role_id` 和 `grant_date`。

```sql
CREATE TABLE account_role
(
  user_id integer NOT NULL,
  role_id integer NOT NULL,
  grant_date timestamp without time zone,
  PRIMARY KEY (user_id, role_id),
  CONSTRAINT account_role_role_id_fkey FOREIGN KEY (role_id)
      REFERENCES role (role_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT account_role_user_id_fkey FOREIGN KEY (user_id)
      REFERENCES account (user_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
);
```

![postgresql create tables many to many](/imgs/postgresql-create-tables-many-to-many.jpg)

让我们更详细地研究一下上面的陈述。

`account_role` 表的主键由两列组成：`user_id` 和 `role_id`，因此我们必须使用主键表级约束来定义主键，如下所示：

```sql
PRIMARY KEY (user_id, role_id)
```

因为 `user_id` 列引用了帐户表中的 `user_id` 列，所以我们需要为 `user_id` 列定义[外键约束](content/postgresql-foreign-key.md)：

```sql
CONSTRAINT account_role_user_id_fkey FOREIGN KEY (user_id)
      REFERENCES account (user_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
```

`role_id` 列用角色表中的 `role_id` 列，我们还需要为 `role_id` 列定义外键约束。

```sql
CONSTRAINT account_role_role_id_fkey FOREIGN KEY (role_id)
      REFERENCES role (role_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
```

在本教程中，我们向您展示了如何使用PostgreSQL `CREATE TABLE` 语句创建新表。我们还向您展示了如何在列和表级别应用不同类型的约束。

#### 相关教程

- [Primary Key](content/postgresql-primary-key.md)
- [CHECK Constraint](content/postgresql-check-constraint.md)
- [UNIQUE Constraint](content/postgresql-unique-constraint.md)
- [Not-Null Constraint](content/postgresql-not-null-constraint.md)
- [ADD COLUMN](content/postgresql-add-column.md)
- [Temporary Table](content/postgresql-temporary-table.md)
- [Foreign Key](content/postgresql-foreign-key.md)