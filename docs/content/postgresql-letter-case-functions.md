## 字母大小写函数

> 摘要：在本教程中，我们将向您展示如何使用 `LOWER`，`UPPER` 和 `INITCAP` 函数将字符串表达式，列中的值等转换为小写，大写和适当的大小写。

#### LOWER

要将字符串，表达式或列中的值转换为小写，请使用 `LOWER` 大小写函数。以下说明了 `LOWER` 函数的语法：

```sql
LOWER(string_expression)
```

`LOWER` 函数接受一个字符串的参数，例如char，varchar或text，并将其转换为小写格式。如果参数是字符串可转换的，则使用 `CAST` 函数将其显式转换为字符串。

以下语句使用 `LOWER` 函数和 [CONCAT_WS](ontent/postgresql-concat-function.md) 函数来获取客户的全名：

```sql
SELECT
 concat_ws(', ', LOWER(last_name), LOWER(first_name)) as name
FROM customer
ORDER BY last_name;
          name
------------------------
 abney, rafael
 adam, nathaniel
 adams, kathleen
 alexander, diana
 allard, gordon
 allen, shirley
 alvarez, charlene
 anderson, lisa
 andrew, jose
 andrews, ida
```

以下语句使用 `CONCAT_WS` 函数和 `LOWER` 函数来获取 `film` 表中电影的标题，描述和年份。因为 `release_year` 是一个数字，所以我们必须使用类型转换将其转换为字符串。

请注意，此示例仅用于演示。您可以删除适用于 `release_year` 列的 `LOWER` 函数，因为它不是必需的。

```sql
SELECT
 CONCAT_WS (
 ' - ',
 title,
 LOWER (description),
 LOWER (CAST(release_year AS TEXT))
 ) AS film_info
FROM film;
                                                                              film_info
---------------------------------------------------------------------------------------------------------------------------------------------------------------------
 Chamber Italian - a fateful reflection of a moose and a husband who must overcome a monkey in nigeria - 2006
 Grosse Wonderful - a epic drama of a cat and a explorer who must redeem a moose in australia - 2006
 Airport Pollock - a epic tale of a moose and a girl who must confront a monkey in ancient india - 2006
 Bright Encounters - a fateful yarn of a lumberjack and a feminist who must conquer a student in a jet boat - 2006
 Academy Dinosaur - a epic drama of a feminist and a mad scientist who must battle a teacher in the canadian rockies - 2006
 Ace Goldfinger - a astounding epistle of a database administrator and a explorer who must find a car in ancient china - 2006
 Adaptation Holes - a astounding reflection of a lumberjack and a car who must sink a lumberjack in a baloon factory - 2006
 Affair Prejudice - a fanciful documentary of a frisbee and a lumberjack who must chase a monkey in a shark tank - 2006
```

#### UPPER

要将字符串转换为大写，请使用 `UPPER` 函数。以下说明了 `UPPER` 函数的语法。

```sql
UPPER(string_expression)
```

与 `LOWER` 函数一样， `UPPER` 函数接受字符串表达式或字符串可转换表达式，并将其转换为大写格式。如果参数不是字符串，则必须使用 `CAST` 函数显式转换它。

以下语句使用 `CONCAT` 函数和 `UPPER` 函数以大写形式返回工作人员的全名：

```sql
SELECT
 CONCAT (
 UPPER (first_name),
 UPPER (last_name)
 ) as full_name
FROM staff;
  full_name
-------------
 MIKEHILLYER
 JONSTEPHENS
(2 rows)
```

#### INITCAP

`INITCAP` 函数将字符串表达式转换为适当的大小写或标题大小写，每个单词的第一个字母为大写，其余字符为小写。

以下说明了 `INITCAP` 函数的语法：

```sql
INITCAP(string_expression)
```

我们经常使用 `INITCAP` 函数来格式化博客标题，作者姓名等。例如，以下语句在适当的情况下格式化客户的名称：

```sql
SELECT
 INITCAP(
 CONCAT (first_name, ' ', last_name)
 ) AS full_name
FROM customer
ORDER BY first_name;
       full_name
-----------------------
 Aaron Selby
 Adam Gooch
 Adrian Clary
 Agnes Bishop
 Alan Kahn
 Albert Crouse
 Alberto Henning
 Alex Gresham
 Alexander Fennell
```

在本教程中，我们向您介绍了三个有用的字符串函数，包括 `LOWER`，`UPPER` 和 `INITCAP`，以转换字符串大小写。