## Upsert

> 摘要：如果表中已存在正在插入的行，本教程将向您展示如何使用PostgreSQL upsert功能插入或更新数据。

#### 简介

在关系数据库中，术语upsert被称为合并。其原理是，当向表中[插入](content/postgresql-insert.md)新行时，如果该行已经存在，PostgreSQL将[更新](content/postgresql-update.md)该行，否则PostgreSQL将插入新行。这就是为什么我们称操作为upsert(update or insert)的原因。

要在PostgreSQL中使用upsert功能，请使用 `INSERT ON CONFLICT` 语句，如下所示：

```sql
INSERT INTO table_name(column_list) VALUES (value_list)
ON CONFLICT target action;
```

PostgreSQL将 `ON CONFLICT target action` 子句添加到INSERT语句以支持upsert功能

目标可以是：

- `(column_name)` -  列名。
- `ON CONSTRAINT constraint_name` - 其中约束名称可以是[UNIQUE约束](content/postgresql-unique-constraint.md)的名称。
- `WHERE predicate` - [WHERE](content/postgresql-where.md)子句

行动可以是：

- `DO NOTHING` - 如果表中已存在行，则什么都不做。
- `DO UPDATE SET column_1 = value_1，.. WHERE condition` - 更新表中的某些字段。

请注意，`ON CONFLICT` 子句仅适用于PostgreSQL 9.5。如果您使用的是早期版本，则需要一种解决方法来获得upsert功能。

如果您还在使用MySQL，您会发现upsert功能类似于MySQL中的[重复键更新语句中的插入](http://www.mysqltutorial.org/mysql-insert-or-update-on-duplicate-key-update/)。

#### 示例

让我们创建一个名为 `customers` 的新表来演示PostgresQL upsert功能。

```sql
CREATE TABLE customers (
     customer_id serial PRIMARY KEY,
     name VARCHAR UNIQUE,
     email VARCHAR NOT NULL,
     active bool NOT NULL DEFAULT TRUE
);
```

`customers` 表由四列组成：`customer_id`，`name`，`email` 和 `active`。`name` 列具有关联的唯一约束，以保证客户的唯一性。

```sql
#\d customers
                                     Table "public.customers"
   Column    |       Type        |                            Modifiers
-------------+-------------------+-----------------------------------------------------------------
 customer_id | integer           | not null default nextval('customers_customer_id_seq'::regclass)
 name        | character varying |
 email       | character varying | not null
 active      | boolean           | not null default true
Indexes:
    "customers_pkey" PRIMARY KEY, btree (customer_id)
    "customers_name_key" UNIQUE CONSTRAINT, btree (name)
```

以下[INSERT](content/postgresql-insert.md)语句将一些行插入到 `customers` 表中。

```sql
INSERT INTO customers (NAME, email)
VALUES
 ('IBM', 'contact@ibm.com'),
 ('Microsoft', 'contact@microsoft.com'),
 ('Intel', 'contact@intel.com');
```

```sql
#SELECT * FROM customers;
 customer_id |   name    |         email         | active
-------------+-----------+-----------------------+--------
           1 | IBM       | contact@ibm.com       | t
           2 | Microsoft | contact@microsoft.com | t
           3 | Intel     | contact@intel.com     | t
(3 rows)
```

假设Microsoft将联系电子邮件从contact@microsoft.com更改为hotline@microft.com，我们可以使用[UPDATE](content/postgresql-update.md)语句对其进行更新。但是，为了演示upsert功能，我们使用以下 `INSERT ON CONFLICT` 语句：

```sql
INSERT INTO customers (NAME, email)
VALUES ('Microsoft', 'hotline@microsoft.com') 
ON CONFLICT ON CONSTRAINT customers_name_key 
DO NOTHING;
```

该声明指出，如果客户名称存在于 `customers` 表中，则忽略它（不执行任何操作）。

以下语句等效于上述语句，但它使用 `name` 列而不是唯一约束名作为 `INSERT` 语句的目标。

```sql
INSERT INTO customers (name, email)
VALUES ( 'Microsoft', 'hotline@microsoft.com' ) 
ON CONFLICT (name) 
DO NOTHING;
```

假设您希望在插入已存在的客户时将新电子邮件与旧电子邮件连接起来，在这种情况下，您使用UPDATE子句作为INSERT语句的操作，如下所示：

```sql
INSERT INTO customers (name, email)
VALUES ( 'Microsoft', 'hotline@microsoft.com' ) 
ON CONFLICT (name) 
DO
 UPDATE
   SET email = EXCLUDED.email || ';' || customers.email;
```

```sql
#SELECT * FROM customers;
 customer_id |   name    |                    email                    | active
-------------+-----------+---------------------------------------------+--------
           1 | IBM       | contact@ibm.com                             | t
           3 | Intel     | contact@intel.com                           | t
           2 | Microsoft | hotline@microsoft.com;contact@microsoft.com | t
(3 rows)
```

在本教程中，我们向您展示了如何使用 `INSERT ON CONFLICT` 语句来使用PostgreSQL upsert功能。

#### 相关教程

- [INSERT](http://www.postgresqltutorial.com/postgresql-insert/)
- [UPDATE](http://www.postgresqltutorial.com/postgresql-update/)