## NULLIF

> 摘要：本教程介绍如何使用PostgreSQL NULLIF函数处理空值。我们将向您展示使用NULLIF函数的一些示例。

#### 语法

`NULLIF` 函数是PostgreSQL提供的最常见的条件表达式之一。以下说明了 `NULLIF` 函数的语法：

```sql
NULLIF(argument_1,argument_2);
```

如果 `argument_1` 等于 `argument_2`，则 `NULLIF` 函数返回空值，否则返回 `argument_1` 。

请参阅以下示例：

````sql
SELECT
 NULLIF (1, 1); -- return NULL
 
SELECT
 NULLIF (1, 0); -- return 1
 
SELECT
 NULLIF ('A', 'B'); -- return A
````

#### 示例

让我们看一下使用 `NULLIF` 函数的示例。

首先，我们创建一个名为 `posts` 的表，如下所示：

```sql
CREATE TABLE posts (
 id serial primary key,
 title VARCHAR (255) NOT NULL,
 excerpt VARCHAR (150),
 body TEXT,
 created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
 updated_at TIMESTAMP
);
```

其次，我们将一些示例数据[插入](content/postgresql-insert.md) `posts` 表中。

```sql
INSERT INTO posts (title, excerpt, body)
VALUES
      ('test post 1','test post excerpt 1','test post body 1'),
      ('test post 2','','test post body 2'),
      ('test post 3', null ,'test post body 3');
```

第三，我们的目标是显示帖子概述页面，其中显示每个帖子的title和excerpt。如果没有提供excerpt，我们使用帖子正文的前40个字符。我们可以简单地使用以下查询来获取 `posts` 表中的所有行。

```sql
SELECT ID, title, excerpt FROM posts;
 id |    title    |       excerpt
----+-------------+---------------------
  1 | test post 1 | test post excerpt 1
  2 | test post 2 |
  3 | test post 3 |
(3 rows)
```

我们在excerpt列中看到空值。要替换此null值，我们可以使用[COALESCE函数](content/postgresql-coalesce.md)，如下所示：

```sql
SELECT id, title, COALESCE (excerpt, LEFT(body, 40))
FROM posts;
 id |    title    |      coalesce
----+-------------+---------------------
  1 | test post 1 | test post excerpt 1
  2 | test post 2 |
  3 | test post 3 | test post body 3
(3 rows)
```

不幸的是，在excerpt列中存在 null 值和 ''(空) 之间的混合。这就是我们需要使用 `NULLIF` 函数的原因：

```sql
SELECT id, title,
 COALESCE (
  NULLIF (excerpt, ''),
  LEFT (body, 40)
 )
FROM posts;
 id |    title    |      coalesce
----+-------------+---------------------
  1 | test post 1 | test post excerpt 1
  2 | test post 2 | test post body 2
  3 | test post 3 | test post body 3
(3 rows)
```

让我们更详细地研究一下表达式：

- 首先，如果excerpt为空，则 `NULLIF` 函数返回空值，否则返回excerpt。`NULLIF` 函数的结果由 `COALESCE` 函数使用。
- 其次，`COALESCE` 函数检查 `NULLIF` 函数提供的第一个参数是否为null，然后返回正文的前40个字符; 否则它会在excerpt不为null的情况下返回excerpt。

#### 使用NULLIF可防止被零除错误

使用 `NULLIF` 函数的另一个很好的例子是防止被零除错误。我们来看看下面的例子。

首先，我们创建一个名为 `members` 的新表：

```sql
CREATE TABLE members (
 ID serial PRIMARY KEY,
 first_name VARCHAR (50) NOT NULL,
 last_name VARCHAR (50) NOT NULL,
 gender SMALLINT NOT NULL -- 1: male, 2 female
);
```

其次，我们[插入](content/postgresql-insert.md)一些行进行测试：

```sql
INSERT INTO members (
 first_name,
 last_name,
 gender
)
VALUES
 ('John', 'Doe', 1),
 ('David', 'Dave', 1),
 ('Bush', 'Lily', 2);
```

第三，如果我们想要计算男性和女性成员之间的比例，我们使用以下查询：

```sql
SELECT
 (SUM (
 CASE
  WHEN gender = 1 THEN 1
  ELSE 0
 END
 ) / SUM (
 CASE
  WHEN gender = 2 THEN 1
  ELSE 0
 END
 ) ) * 100 AS "Male/Female ratio"
FROM members;
```

要计算男性成员的总数，我们使用[SUM函数](content/postgresql-sum-function.md)和[CASE表达式](content/postgresql-case.md)。如果性别为1，则 `CASE` 表达式返回1，否则返回0; `SUM` 函数用于计算男性成员的总数。同样的逻辑也适用于计算女性成员的总数。

然后将男性成员的总数除以女性成员的总数来返回该比率。在这种情况下，它返回200％，这是正确的。

第四，让我们删除女性成员：

```sql
DELETE FROM members WHERE gender = 2;
```

并执行查询再次计算男/女比例，我们得到以下错误信息：

```sql
[Err] ERROR:  division by zero
```

原因是女性人数为零。为了防止这种除零错误，我们使用 `NULLIF` 函数，如下所示：

```sql
SELECT
 (
 SUM (
 CASE
  WHEN gender = 1 THEN 1
  ELSE 0
 END
 ) / NULLIF (
 SUM (
 CASE
  WHEN gender = 2 THEN 1
  ELSE 0
 END
 ),
 0)
 ) * 100 AS "Male/Female ratio"
FROM members;
 Male/Female ratio
-------------------

(1 row)
```

`NULLIF` 函数检查女性成员的数量是否为零，它返回null。男性成员的总数除以空值返回空值，这是正确的。

在本教程中，我们向您展示了如何应用NULLIF函数来替换空值以显示数据并防止除零错误。

#### 相关教程

- [CASE](content/postgresql-case.md)
- [ COALESCE](content/postgresql-coalesce.md)