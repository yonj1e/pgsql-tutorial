## JSON

> 摘要：在本教程中，我们将向您展示如何使用PostgreSQL JSON数据类型。此外，我们将向您介绍一些最常见的PostgreSQL JSON运算符和处理JSON数据的函数。

JSON代表JavaScript Object Notation。JSON是一种开放的标准格式，由键值对组成。JSON的主要用途是在服务器和Web应用程序之间传输数据。与其他格式不同，JSON是人类可读的文本。

从版本9.2开始，PostgreSQL支持本机JSON数据类型。它提供了许多用于操作JSON数据的函数和运算符。

让我们开始创建一个新表用于练习JSON数据类型。

```sql
CREATE TABLE orders (
 ID serial NOT NULL PRIMARY KEY,
 info json NOT NULL
);
```

`orders` 表包含两列：

1. `id` 列是标识订单的主键列。
2. `info` 列以JSON的形式存储数据。

#### 插入JSON数据

要将数据插入JSON列，您必须确保数据采用有效的JSON格式。以下 `INSERT` 语句将新行插入到 `orders` 表中。

```sql
INSERT INTO orders (info)
VALUES
 (
 '{ "customer": "John Doe", "items": {"product": "Beer","qty": 6}}'
 );
```

这意味着 `John Doe` 买了6瓶啤酒。

让我们同时插入多行。

```sql
INSERT INTO orders (info)
VALUES
 (
 '{ "customer": "Lily Bush", "items": {"product": "Diaper","qty": 24}}'
 ),
 (
 '{ "customer": "Josh William", "items": {"product": "Toy Car","qty": 1}}'
 ),
 (
 '{ "customer": "Mary Clark", "items": {"product": "Toy Train","qty": 2}}'
 );
```

#### 查询JSON数据

要查询JSON数据，请使用 `SELECT` 语句，这类似于查询其他本机数据类型：

```sql
SELECT info FROM orders;
                                  info
-------------------------------------------------------------------------
 { "customer": "John Doe", "items": {"product": "Beer","qty": 6}}
 { "customer": "Lily Bush", "items": {"product": "Diaper","qty": 24}}
 { "customer": "Josh William", "items": {"product": "Toy Car","qty": 1}}
 { "customer": "Mary Clark", "items": {"product": "Toy Train","qty": 2}}
(4 rows)
```

PostgreSQL以JSON的形式返回结果集。

PostgreSQL提供了两个本机运算符 `->` 和 `->>` 来帮助您查询JSON数据。

- 运算符 `->` 按键返回JSON对象字段。
- 运算符 `->>` 按文本返回JSON对象字段。

以下查询使用operator - >以JSON的形式获取所有客户：

```sql
SELECT info -> 'customer' AS customer
FROM orders;
    customer
----------------
 "John Doe"
 "Lily Bush"
 "Josh William"
 "Mary Clark"
(4 rows)
```

以下查询使用运算符 `->>` 以文本形式获取所有客户：

```sql
SELECT info ->> 'customer' AS customer
FROM orders;
   customer
--------------
 John Doe
 Lily Bush
 Josh William
 Mary Clark
(4 rows)
```

因为 `->` 运算符返回一个JSON对象，所以可以使用运算符 `->>` 将其链接以检索特定节点。例如，以下语句返回所有已售出的产品：

```sql
SELECT info -> 'items' ->> 'product' as product
FROM orders
ORDER BY product;
  product
-----------
 Beer
 Diaper
 Toy Car
 Toy Train
(4 rows)
```

第一个 `info -> 'items'` 将项目作为JSON对象返回。然后 `info->'items'->>'product'` 将所有产品作为文本返回。

#### 在WHERE子句中使用JSON运算符

我们可以使用 `WHERE` 子句中的JSON运算符来过滤返回的行。例如，要找出谁购买了 `Diaper`，我们使用以下查询：

```sql
SELECT info ->> 'customer' AS customer
FROM orders
WHERE info -> 'items' ->> 'product' = 'Diaper';
 customer
-----------
 Lily Bush
(1 row)
```

要了解谁一次购买两种产品，我们使用以下查询：

```sql
SELECT
 info ->> 'customer' AS customer,
 info -> 'items' ->> 'product' AS product
FROM
 orders
WHERE
 CAST (
 info -> 'items' ->> 'qty' AS INTEGER
 ) = 2;
  customer  |  product
------------+-----------
 Mary Clark | Toy Train
(1 row)
```

请注意，我们使用[类型转换](content/postgresql-cast.md)将 `qty` 字段转换为 `INTEGER` 类型并将其与两个进行比较。

#### 将聚合函数应用于JSON数据

我们可以将[MIN](content/postgresql-min-function.md), [MAX](content/postgresql-max-function.md), [AVERAGE](content/postgresql-avg-function.md), [SUM](content/postgresql-sum-function.md)等[聚合函数](content/postgresql-functions.md)应用于JSON数据。例如，以下语句返回最小数量，最大数量，平均数量和销售产品的总数量。

```sql
SELECT
 MIN (
 CAST (
 info -> 'items' ->> 'qty' AS INTEGER
 )
 ),
 MAX (
 CAST (
 info -> 'items' ->> 'qty' AS INTEGER
 )
 ),
 SUM (
 CAST (
 info -> 'items' ->> 'qty' AS INTEGER
 )
 ),
 AVG (
 CAST (
 info -> 'items' ->> 'qty' AS INTEGER
 )
 ) 
FROM
 orders;
 min | max | sum |        avg
-----+-----+-----+--------------------
   1 |  24 |  33 | 8.2500000000000000
(1 row)
```

#### JSON函数

PostgreSQL为我们提供了一些帮助您处理JSON数据的函数。

#### json_each函数

`json_each()` 函数允许我们将最外层的JSON对象扩展为一组键值对。请参阅以下声明：

```sql
SELECT json_each (info) FROM orders;
```

如果要将一组键值对作为文本，则使用 `json_each_text()` 函数。

#### json_object_keys函数

要在最外层的JSON对象中获取一组键，可以使用 `json_object_keys()` 函数。以下查询返回 `info` 列中嵌套`items` 对象的所有键

```sql
SELECT json_object_keys (info->'items')
FROM orders;
 json_object_keys
------------------
 product
 qty
 product
 qty
 product
 qty
 product
 qty
(8 rows)
```

#### json_typeof函数

`json_typeof()`函数以字符串形式返回最外层JSON值的类型。它可以是 `number`, `boolean`, `null`, `object`, `array`, 和 `string`。

以下查询返回项的数据类型：

```sql
SELECT
 json_typeof (info->'items')
FROM
 orders;
 json_typeof
-------------
 object
 object
 object
 object
(4 rows)
```

以下查询返回嵌套项JSON对象的qty字段的数据类型。

```sql
SELECT json_typeof (info->'items'->'qty')
FROM orders;
 json_typeof
-------------
 number
 number
 number
 number
(4 rows)
```

如果你想深入挖掘，还有更多的[PostgreSQL JSON函数](https://www.postgresql.org/docs/current/static/functions-json.html)。

在本教程中，我们向您展示了如何使用PostgreSQL JSON数据类型。我们向您展示了一些最重要的JSON运算符和函数，可帮助您更有效地处理JSON数据。

#### 相关教程

- [hstore](content/postgresql-hstore.md)