## MAX

> 摘要：本教程介绍如何使用 `MAX` 函数获取集合的最大值。

#### 简介

`MAX` 函数是一个聚合函数，它返回一组值中的最大值。`MAX` 函数在很多情况下都很有用。例如，找到薪水最高的员工，或找到最贵的产品等。

 `MAX` 函数的语法如下：

```sql
MAX(expression);
```

您不仅可以在 [SELECT](content/postgresql-select.md) 子句中使用 `MAX` 函数，还可以在 [WHERE](content/postgresql-where.md) 和 [HAVING](content/postgresql-having.md) 子句中使用 `MAX` 函数。我们来看看使用 `MAX` 函数的一些例子。

#### 示例

我们来看一下[示例数据库](content/postgresql-sample-database.md)中的 `payment` 表。

![payment table](/imgs/payment-table.png)

以下查询获取客户在付款表中支付的最高金额：

```sql
SELECT MAX(amount) FROM payment;
```

##### 子查询

要获得其他信息以及最高付款，请使用[子查询](content/postgresql-subquery.md)，如下所示：

```sql
SELECT * FROM payment 
WHERE amount = ( 
    SELECT MAX (amount) FROM payment
);
 payment_id | customer_id | staff_id | rental_id | amount |        payment_date
------------+-------------+----------+-----------+--------+----------------------------
      20403 |         362 |        1 |     14759 |  11.99 | 2007-03-21 21:57:24.996577
      22650 |         204 |        2 |     15415 |  11.99 | 2007-03-22 22:17:22.996577
      23757 |         116 |        2 |     14763 |  11.99 | 2007-03-21 22:02:26.996577
      24553 |         195 |        2 |     16040 |  11.99 | 2007-03-23 20:47:59.996577
      24866 |         237 |        2 |     11479 |  11.99 | 2007-03-02 20:46:39.996577
      28799 |         591 |        2 |      4383 |  11.99 | 2007-04-07 19:14:17.996577
      28814 |         592 |        1 |      3973 |  11.99 | 2007-04-06 21:26:57.996577
      29136 |          13 |        2 |      8831 |  11.99 | 2007-04-29 21:06:07.996577
(8 rows)
```

首先，子查询获得最大支付，然后外部查询选择所有行，其金额等于从子查询返回的最大支付金额。

下图说明了PostgreSQL执行查询的步骤：

![postgresql max function with subquery](/imgs/postgresql-max-function-with-subquery.png)

##### GROUP BY

您可以将 `MAX` 函数与 [GROUP BY](content/postgresql-group-by.md) 子句组合使用以获得每组的最大值。例如，以下查询获得每个客户支付的最大付款。

```sql
SELECT customer_id, MAX (amount)
FROM payment
GROUP BY customer_id;
 customer_id |  max
-------------+-------
         184 |  9.99
          87 | 10.99
         477 | 10.99
         273 |  8.99
         550 | 10.99
          51 |  9.99
         394 |  9.99
         272 | 10.99
          70 |  9.99
         190 |  9.99
         350 |  6.99
         539 |  7.99
         554 |  9.99
```

![PostgreSQL max function with group by](/imgs/PostgreSQL-max-function-with-group-by.png)

##### HAVING

如果在 [HAVING](content/postgresql-having.md) 子句中使用 `MAX` 函数，则可以为整个组应用过滤器。例如，以下查询仅选择每个客户支付的最大付款，并且最大付款大于 `8.99`。

```sql
SELECT customer_id, MAX (amount)
FROM payment
GROUP BY customer_id
HAVING MAX(amount) > 8.99;
 customer_id |  max
-------------+-------
         184 |  9.99
          87 | 10.99
         477 | 10.99
         550 | 10.99
          51 |  9.99
         394 |  9.99
         272 | 10.99
          70 |  9.99
         190 |  9.99
         554 |  9.99
         424 |  9.99
         292 | 10.99
```

![PostgreSQL max function with group by](/imgs/PostgreSQL-max-function-with-group-by.png)

#### 从两列或更多列中查找最大值

我们来看一个例子吧。

首先，为演示创建一个名为 `rank` 的新表。它有四列：1列用于存储用户ID，另外3列用于存储从1到3的等级。

```sql
CREATE TABLE IF NOT EXISTS ranks ( 
    user_id INT PRIMARY KEY, 
    rank_1 int4 NOT NULL, 
    rank_2 int4 NOT NULL, 
    rank_3 int4 NOT NULL
);
```

其次，将样本数据[插入](content/postgresql-insert.md)到 `rank` 表中，如下所示：

```sql
INSERT INTO ranks VALUES (1, 6, 3, 5), (2, 2, 8, 5), (3, 5, 9, 8);
```

如何获得每位用户的最高排名，如下所示：

```sql
select * from ranks ;
 user_id | rank_1 | rank_2 | rank_3
---------+--------+--------+--------
       1 |      6 |      3 |      5
       2 |      2 |      8 |      5
       3 |      5 |      9 |      8
(3 rows)
```

为此，您使用 `GREATEST` 函数而不是 `MAX` 函数。`GREATEST` 函数从值列表中返回最大值。

```sql
SELECT user_id, GREATEST (rank_1, rank_2, rank_3) AS largest_rank FROM ranks;
 user_id | largest_rank
---------+--------------
       1 |            6
       2 |            8
       3 |            9
(3 rows)
```

在本教程中，您学习了如何使用 `MAX` 函数来查找集合的最大值。

#### 相关教程

- [MIN](content/postgresql-min-function.md)
- [ AVG](content/postgresql-avg-function.md)
- [SUM](content/postgresql-sum-function.md)
- [COUNT](content/postgresql-count-function.md)