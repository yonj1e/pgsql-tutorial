## 比较两个表

> 摘要：在本教程中，您将学习各种方法来比较PostgreSQL中的两个表。

有几种方法可以比较两个表的内容，以找出它们之间的差异。我们将向您展示两种常用技术来比较两个表的数据。

#### 使用EXCEPT和UNION运算符比较两个表

首先，让我们创建两个名为 `foo` 和 `bar` 的表，并为演示[插入](content/postgresql-insert.md)一些示例数据。

```sql
CREATE TABLE foo (
 ID INT PRIMARY KEY,
 NAME VARCHAR (50)
);

INSERT INTO foo (ID, NAME)
VALUES (1, 'a'), (2, 'b');
 
CREATE TABLE bar (
 ID INT PRIMARY KEY,
 NAME VARCHAR (50)
);

INSERT INTO bar (ID, NAME)
VALUES (1, 'a'), (2, 'b');
```

 `foo` 表与 `bar` 表具有相同的结构和数据。

接下来，我们[更新](content/postgresql-update.md) `bar` 表中的一行。

```sql
UPDATE bar
SET name = 'c'
WHERE id = 2;
```

```sql
select * from foo ;
 id | name
----+------
  1 | a
  2 | b
(2 rows)

select * from bar ;
 id | name
----+------
  1 | a
  2 | c
(2 rows)
```

然后，要查找 `foo` 表中的行而不是 `bar` 表中的行，我们使用以下[查询](content/postgresql-select.md)：

```sql
SELECT ID, NAME, 'not in bar' AS note
FROM foo
EXCEPT
 SELECT ID, NAME, 'not in bar' AS note
 FROM bar;
 id | name |    note
----+------+------------
  2 | b    | not in bar
(1 row)
```

我们使用 `EXCEPT` 运算符返回 `foo` 表中的行，但不返回 `bar` 表中的行。我们可以应用相同的技术来查找 `bar` 表中但不在 `foo` 表中的行。

```sql
SELECT ID, NAME, 'not in foo' AS note
FROM bar
EXCEPT
 SELECT ID, NAME, 'not in foo' AS note
 FROM foo;
 id | name |    note
----+------+------------
  2 | c    | not in foo
(1 row)
```

最后，我们使用[UNION](content/postgresql-union.md)运算符组合两个查询的结果来查找：

-  `bar` 表中的行但不在 `foo` 表中
-  `foo` 表中的行但不在 `bar` 表中。

```sql
 id | name |    note
----+------+------------
  2 | c    | not in foo
  2 | b    | not in bar
(2 rows)
```

#### 使用OUTER JOIN比较两个表

我们可以使用外连接来比较两个表，如下所示：

```sql
SELECT id, name
FROM foo
FULL OUTER JOIN bar USING (id, name)
WHERE foo.id IS NULL OR bar.id IS NULL;
 id | name
----+------
  2 | b
  2 | c
(2 rows)
```

它返回两个表之间的差异：

要查找 `foo` 表中但不是 `bar` 表的行数，反之亦然，我们使用[COUNT](content/postgresql-count-function.md)函数，如下所示：

```sql
SELECT COUNT (*)
FROM foo
FULL OUTER JOIN bar USING (id, name)
WHERE foo.id IS NULL OR bar.id IS NULL;
 count
-------
     2
(1 row)
```

在本教程中，我们向您展示了两种比较PostgreSQL中两个表的方法。