## LIKE

> 摘要：在本教程中，您将学习如何使用 `LIKE` 和 `ILIKE` 运算符进行模式匹配来查询数据。

#### 简介

假设商店经理要求您找到一个他不记得名字的客户。 他只记得顾客的名字是以 `Jen` 开头的。 您如何找到商店经理要求的确切客户？ 您可以通过查看 `first_name` 列来查找客户表中的客户，以查看是否存在以 `Jen` 开头的任何值。 这有点麻烦，因为在自定义中有很多行。

幸运的是，您可以使用 `LIKE` 运算符作为以下查询：

```sql
SELECT first_name, last_name
FROM customer
WHERE first_name LIKE 'Jen%';
 first_name | last_name
------------+-----------
 Jennifer   | Davis
 Jennie     | Terry
 Jenny      | Castro
(3 rows)
```

请注意，`WHERE` 子句包含一个特殊表达式：`first_name`，`LIKE` 运算符和包含百分号 (`%`) 的字符串，称为模式。

该查询返回第一个名称列中的值以 `Jenand` 开头的行，其后面可以是任何字符序列。这种技术称为模式匹配。

您可以通过将字符串与通配符组合来构造模式，并使用 `LIKE` 或 `NOT LIKE` 运算符来查找匹配项。 PostgreSQL提供了两个通配符：

- 匹配任何字符序列的百分比（`％`）。
- 用于匹配任何单个字符的下划线（`_`）。

`LIKE` 运算符的语法如下：

```SQL
string LIKE pattern
```

如果字符串与模式匹配，则表达式返回 `true`，否则返回 `false`。

您可以将 `LIKE` 运算符与 `NOT` 运算符组合，如下所示：

```SQL
string NOT LIKE pattern
```

如果 `LIKE` 返回 `true` ，则表达式返回 `true` ，反之亦然。

如果模式不包含任何通配符，则 `LIKE` 运算符的作用类似于相等（`=`）运算符。

#### 模式匹配示例

##### LIKE

请参阅以下示例：

```SQL
SELECT
 'foo' LIKE 'foo', -- true
 'foo' LIKE 'f%', -- true
 'foo' LIKE '_o_', -- true
 'bar' LIKE 'b_'; -- false
```

- 第一个表达式返回 `true`，因为 `foo` 模式不包含任何通配符，因此 `LIKE` 运算符的作用类似于相等（`=`）运算符。
- 第二个表达式返回 `true` ，因为它匹配任何以字母 `f` 开头后跟任意数量字符的字符串。
- 第三个表达式返回 `true` ，因为模式（`_o_`）匹配以任何单个字符开头，后跟字母 `o` 并以任何单个字符结尾的任何字符串。
- 第四个表达式返回 `false`，因为模式 `b_` 匹配任何以字母 `b` 开头后跟任何单个字符的字符串。

您可以在模式的开头和或结尾使用通配符。 例如，以下查询返回其名字包含 `er`字符串的客户，例如 `Jenifer`，`Kimberly` 等。

```sql
SELECT first_name, last_name
FROM customer
WHERE first_name LIKE '%er%'；
 first_name  |  last_name
-------------+-------------
 Jennifer    | Davis
 Kimberly    | Lee
 Catherine   | Campbell
 Heather     | Morris
 Teresa      | Rogers
 Cheryl      | Murphy
 Katherine   | Rivera
 Theresa     | Watson
 Beverly     | Brooks
 Sherry      | Marshall
```

您可以将百分比（`％`）与下划线（`_`）组合在一起构建模式，如下例所示：

```sql
SELECT first_name, last_name
FROM customer
WHERE first_name LIKE '_her%';
 first_name | last_name
------------+-----------
 Cheryl     | Murphy
 Theresa    | Watson
 Sherry     | Marshall
 Sherri     | Rhodes
(4 rows)
```

表达式匹配客户，其名字以任何单个字符开头，后跟字符串 `her`，并以任意数量的字符结尾。

##### NOT LIKE

以下查询返回其名字不以 `Jen` 开头的客户：

```sql
SELECT first_name, last_name
FROM customer
WHERE first_name NOT LIKE 'Jen%';
 first_name  |  last_name
-------------+--------------
 Jared       | Ely
 Mary        | Smith
 Patricia    | Johnson
 Linda       | Williams
 Barbara     | Jones
 Elizabeth   | Brown
 Maria       | Miller
 Susan       | Wilson
 Margaret    | Moore
 Dorothy     | Taylor
```

请注意，我们在 `WHERE` 子句中使用了 `NOT LIKE` 运算符。

#### ILIKE

PostgreSQL提供了像 `LIKE` 运算符一样的 `ILIKE` 运算符。 此外，`ILIKE` 运算符不区分大小写的值。 请参阅以下示例：

```sql
SELECT first_name, last_name
FROM customer
WHERE first_name ILIKE 'BAR%';
 first_name | last_name
------------+-----------
 Barbara    | Jones
 Barry      | Lovelace
(2 rows)
```

`BAR％` 模式匹配任何以 `BAR`，`Bar`，`BaR` 等开头的字符串。如果使用 `LIKE` 运算符，查询将不会返回任何行。

PostgreSQL还提供了一些运算符，如 `LIKE`，`NOT LIKE`，`ILIKE` 和 `NOT ILIKE` 运算符，如下所示：

- ~~ 相当于 `LIKE`
- ~~ * 相当于 `ILIKE`
- !~~ 相当于 `NOT LIKE`
- !~~ * 相当于 `NOT ILIKE`

在本教程中，我们向您展示了如何使用 `LIKE` 和 `ILIKE`运算符根据模式查询数据。