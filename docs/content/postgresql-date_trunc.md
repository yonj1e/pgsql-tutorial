## DATE_TRUNC

> 摘要：本教程介绍如何使用 `date_trunc` 函数将时间戳或间隔截断为指定的精度级别。

#### 简介

`date_trunc` 函数根据指定的[日期](ontent/postgresql-date.md)部分（例如，小时，周或月）截断 [`TIMESTAMP`](content/postgresql-timestamp.md) 或  [`INTERVAL`](content/postgresql-interval.md) 值，并以精度级别返回截断的时间戳或间隔。

以下说明了 `date_trunc` 函数的语法：

```sql
date_trunc('datepart', field)
```

`datepart` 参数是用于截断字段的精度级别，可以是以下之一：

- millennium
- century
- decade
- year
- quarter
- month
- week
- day
- hour
- minute
- second
- milliseconds
- microseconds

`field` 参数是要截断的 `TIMESTAMP` 或 `INTERVAL` 值。它可以是一个计算时间戳或间隔的表达式。

 `date_trunc` 函数返回 `TIMESTAMP` 或 `INTERVAL` 值。

#### 示例

以下示例将 `TIMESTAMP` 值截断为小时日期部分：

```sql
SELECTS  DATE_TRUNC('hour', TIMESTAMP '2017-03-17 02:09:30');
     date_trunc
---------------------
 2017-03-17 02:00:00
(1 row)
```

`date_trunc` 函数返回小时精度的结果。

如果要将 `TIMESTAMP` 值截断为分钟，则将 `'minute'` 字符串作为第一个参数传递：

```sql
SELECT date_trunc('minute', TIMESTAMP '2017-03-17 02:09:30');
```

该函数返回 `TIMESTAMP`，其精度级别为分钟：

```sql
 date_trunc
---------------------
 2017-03-17 02:09:00
(1 row)
```

请参阅[示例数据库](content/postgresql-sample-database.md)中的以下 `rental` 表：

![Rental table - PostgreSQL date_trunc function demo](/imgs/rental-table.png)

您可以使用 `date_trunc` 函数按月计算租金数量，如下所示：

```sql
SELECT
 date_trunc('month', rental_date) m, 
 COUNT (rental_id)
FROM rental
GROUP BY m
ORDER BY m;
```

在此查询中， `date_trunc` 函数将租赁日期截断为月份部分。[COUNT](content/postgresql-count-function.md)函数计算租金的数量，[GROUP BY](content/postgresql-group-by.md)子句按月对租赁进行分组。

```sql
        month        | count
---------------------+-------
 2005-05-01 00:00:00 |  1156
 2005-06-01 00:00:00 |  2311
 2005-07-01 00:00:00 |  6709
 2005-08-01 00:00:00 |  5686
 2006-02-01 00:00:00 |   182
(5 rows)
```

同样，您可以计算每年员工的租金数量，如下所示：

```sql
SELECT staff_id,
 date_trunc('year', rental_date) y,
 COUNT (rental_id) rental
FROM rental
GROUP BY staff_id, y
ORDER BY staff_id；
 staff_id |          y          | rental
----------+---------------------+--------
        1 | 2006-01-01 00:00:00 |     85
        1 | 2005-01-01 00:00:00 |   7955
        2 | 2005-01-01 00:00:00 |   7907
        2 | 2006-01-01 00:00:00 |     97
(4 rows)
```

在本教程中，您学习了如何使用 `date_trunc` 函数截断时间戳或间隔值。