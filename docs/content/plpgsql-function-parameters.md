## PL/pgSQL函数参数

> 摘要：在本教程中，我们将向您介绍各种PL/pgSQL函数参数：IN，OUT，INOUT和VARIADIC。> 摘要

#### IN

让我们从创建名为 `get_sum()` 的新函数的示例开始，如下所示：

```sql
CREATE OR REPLACE FUNCTION get_sum(
 a NUMERIC, 
 b NUMERIC) 
RETURNS NUMERIC AS $$
BEGIN
 RETURN a + b;
END; $$
LANGUAGE plpgsql;
```

`get_sum()` 函数接受两个参数：`a` 和 `b` 并返回一个数字。两个参数的[数据类型](content/postgresql-data-types.md)是NUMERIC。默认情况下，PostgreSQL中参数的任何参数类型都是 `IN` 参数。您可以将 `IN` 参数传递给函数，但不能将它们作为结果的一部分返回。

```sql
SELECT get_sum(10,20);
 get_sum
---------
      30
(1 row)
```

#### OUT

`OUT` 参数被定义为函数参数列表的一部分，并作为结果的一部分返回。自8.1版以来，PostgreSQL支持 `OUT`参数

要定义 `OUT` 参数，请使用 `OUT` 关键字，如以下示例所示：

```sql
CREATE OR REPLACE FUNCTION hi_lo(
 a NUMERIC, 
 b NUMERIC,
 c NUMERIC, 
 OUT hi NUMERIC,
 OUT lo NUMERIC)
AS $$
BEGIN
 hi := GREATEST(a,b,c);
 lo := LEAST(a,b,c);
END; $$
LANGUAGE plpgsql;
```

`hi_lo` 函数接受5个参数：

- 三个 `IN` 参数：`a`，`b`，`c`。
- 两个 `OUT` 参数：`hi`（high）和 `lo`（low）。

在函数内部，我们使用 `GREATEST` 和 `LEAST` 内置函数获得三个 `IN` 参数的最大值最小值。因为我们使用 `OUT` 参数，所以我们不需要 `RETURNS` 语句。`OUT` 参数在需要返回多个值而不定义自定义类型的函数中很有用。

以下语句调用 `hi_lo` 函数：

```sql
SELECT hi_lo(10,20,30);
  hi_lo
---------
 (30,10)
(1 row)
```

该函数的输出是一个记录，它是一个自定义类型。要将输出分隔为列，请使用以下语法：

```sql
SELECT * FROM hi_lo(10,20,30);
 hi | lo
----+----
 30 | 10
(1 row)
```

#### INOUT

`INOUT` 参数是 `IN` 和 `OUT` 参数的组合。这意味着调用者可以将值传递给函数。然后，该函数更改参数并将值作为结果的一部分传回。

以下示例显示接受数字的 `square` 函数，并返回该数字的平方。

```sql
CREATE OR REPLACE FUNCTION square(
 INOUT a NUMERIC)
AS $$
BEGIN
 a := a * a;
END; $$
LANGUAGE plpgsql;
```

```sql
SELECT square(4);
 square
--------
     16
(1 row)
```

#### VARIADIC

PostgreSQL函数可以接受可变数量的参数，其中一个条件是所有参数都具有相同的数据类型。参数作为[数组](content/postgresql-array.md)传递给函数。请参阅以下示例：

`sum_avg()` 函数接受数字列表，计算总数和平均值，并返回两个值。

```sql
CREATE OR REPLACE FUNCTION sum_avg(
 VARIADIC list NUMERIC[],
 OUT total NUMERIC, 
 OUT average NUMERIC)
AS $$
BEGIN
   SELECT INTO total SUM(list[i])
   FROM generate_subscripts(list, 1) g(i);
 
   SELECT INTO average AVG(list[i])
   FROM generate_subscripts(list, 1) g(i); 
END; $$
LANGUAGE plpgsql;
```

```sql
SELECT * FROM sum_avg(10,20,30);
 total |       average
-------+---------------------
    60 | 20.0000000000000000
(1 row)
```

在本教程中，您学习了四种类型的函数参数，包括IN，OUT，INOUT和VARIADIC。