## 递归查询

> 摘要：在本教程中，您将了解使用递归公用表达式或CTE的PostgreSQL递归查询。

#### 简介

PostgreSQL提供 `WITH` 语句，允许您构造用于[查询](content/postgresql-select.md)的辅助语句。这些语句通常称为公用表达式或CTE。CTE就像临时表，只在查询执行期间存在。

递归查询是指引用递归CTE的查询。递归查询在许多情况下都很有用，例如查询分层数据，如组织结构，物料清单等。

以下说明了递归CTE的语法：

```sql
WITH cte_name(
    CTE_query_definition -- non-recursive term
    UNION [ALL]
    CTE_query definion  -- recursive term
) SELECT * FROM cte_name;
```

递归CTE有三个要素：

- 非递归项：非递归项是CTE查询定义，它构成CTE结构的基本结果集。
- 递归项：递归项是使用[UNION](content/postgresql-union.md)或UNION ALL运算符与非递归项连接的一个或多个CTE查询定义。递归术语引用CTE名称本身。
- 终止检查：当前一次迭代没有返回任何行时，递归停止。

PostgreSQL按以下顺序执行递归CTE：

1. 执行非递归项以创建基本结果集（R0）。
2. 以Ri作为输入执行递归项，以返回结果集Ri + 1作为输出。
3. 重复步骤2，直到返回空集。（终止检查）
4. 返回结果集R0，R1，... Rn的[UNION](content/postgresql-union.md)或UNION ALL的最终结果集

#### 示例

我们将创建一个新表来演示PostgreSQL递归查询。

```sql
CREATE TABLE employees (
 employee_id serial PRIMARY KEY,
 full_name VARCHAR NOT NULL,
 manager_id INT
);
```

employees表由三列组成：`employee_id`，`manager_id` 和 `full_name`。`manager_id` 列指定员工的经理ID。

以下语句将示例数据[插入](content/postgresql-insert.md) `employees` 表。

```sql
INSERT INTO employees (
 employee_id,
 full_name,
 manager_id
)
VALUES
 (1, 'Michael North', NULL),
 (2, 'Megan Berry', 1),
 (3, 'Sarah Berry', 1),
 (4, 'Zoe Black', 1),
 (5, 'Tim James', 1),
 (6, 'Bella Tucker', 2),
 (7, 'Ryan Metcalfe', 2),
 (8, 'Max Mills', 2),
 (9, 'Benjamin Glover', 2),
 (10, 'Carolyn Henderson', 3),
 (11, 'Nicola Kelly', 3),
 (12, 'Alexandra Climo', 3),
 (13, 'Dominic King', 3),
 (14, 'Leonard Gray', 4),
 (15, 'Eric Rampling', 4),
 (16, 'Piers Paige', 7),
 (17, 'Ryan Henderson', 7),
 (18, 'Frank Tucker', 8),
 (19, 'Nathan Ferguson', 8),
 (20, 'Kevin Rampling', 8)
```

以下查询返回ID为2的经理的所有下属。

```sql
WITH RECURSIVE subordinates AS (
 SELECT employee_id, manager_id, full_name
 FROM employees
 WHERE employee_id = 2
 UNION
 SELECT e.employee_id, e.manager_id, e.full_name
 FROM employees e
 INNER JOIN subordinates s ON s.employee_id = e.manager_id
) 
SELECT * FROM subordinates;
 employee_id | manager_id |    full_name
-------------+------------+-----------------
           2 |          1 | Megan Berry
           6 |          2 | Bella Tucker
           7 |          2 | Ryan Metcalfe
           8 |          2 | Max Mills
           9 |          2 | Benjamin Glover
          16 |          7 | Piers Paige
          17 |          7 | Ryan Henderson
          18 |          8 | Frank Tucker
          19 |          8 | Nathan Ferguson
          20 |          8 | Kevin Rampling
(10 rows)
```

怎么运行的。

- 递归CTE，下属，定义一个非递归项和一个递归项。
- 非递归项返回基本结果集R0，即具有id 2的雇员。

```sql
 employee_id | manager_id |  full_name
-------------+------------+-------------
           2 |          1 | Megan Berry
```

递归项返回员工ID 2的直接下属。这是在employees表和下属CTE之间加入的结果。递归项的第一次迭代返回以下结果集：

```sql
 employee_id | manager_id |    full_name
-------------+------------+-----------------
           6 |          2 | Bella Tucker
           7 |          2 | Ryan Metcalfe
           8 |          2 | Max Mills
           9 |          2 | Benjamin Glover
```

PostgreSQL重复执行递归术语。递归成员的第二次迭代使用上面的结果集作为输入值，并返回此结果集：

```sql
 employee_id | manager_id |    full_name
-------------+------------+-----------------
          16 |          7 | Piers Paige
          17 |          7 | Ryan Henderson
          18 |          8 | Frank Tucker
          19 |          8 | Nathan Ferguson
          20 |          8 | Kevin Rampling
```

第三次迭代返回一个空结果集，因为没有员工向员工报告ID为16,17,18,19和20的员工。

PostgreSQL返回最终结果集，该结果集是由非递归和递归项生成的第一次和第二次迭代中所有结果集的并集。

```
 employee_id | manager_id |    full_name
-------------+------------+-----------------
           2 |          1 | Megan Berry
           6 |          2 | Bella Tucker
           7 |          2 | Ryan Metcalfe
           8 |          2 | Max Mills
           9 |          2 | Benjamin Glover
          16 |          7 | Piers Paige
          17 |          7 | Ryan Henderson
          18 |          8 | Frank Tucker
          19 |          8 | Nathan Ferguson
          20 |          8 | Kevin Rampling
(10 rows)
```

在本教程中，您学习了如何使用递归CTE来构造PostgreSQL递归查询。