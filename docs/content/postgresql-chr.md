## CHR

`CHR()` 函数将整数ASCII码转换为字符或Unicode代码转换为UTF8字符。

#### 语法

以下显示了 `CHR()` 函数的语法：

```sql
CHR(num)
```

#### 参数

`CHR()` 函数需要一个参数：

**1) num**

num参数是一个转换为相应ASCII码的整数。

它可以是转换为UTF8字符的Unicode代码点。

#### 返回值

`CHR()` 函数返回一个与ASCII码值或Unicode代码点对应的字符。

#### 示例

以下示例显示如何使用 `CHR()` 函数获取ASCII码值为65和97的字符：

```sql
SELECT CHR(65), CHR(97);
 chr | chr
-----+-----
 A   | a
(1 row)
```

该查询返回字符A为65，字符a为97。

以下是基于Unicode代码点937获取UTF8字符的示例：

```sql
SELECT CHR(937);
 chr
-----
 Ω
(1 row)
```

Unicode代码点937的输出是 Ω，这是我们所期望的。

#### 备注

要获取整数的ASCII码或UTF-8字符，请使用 `ASCII()` 函数。

在本教程中，您学习了如何使用 `CHR()` 函数根据其ASCII值或Unicode代码点获取字符。