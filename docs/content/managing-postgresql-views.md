## 视图管理

> 摘要：在本教程中，您将了解PostgreSQL的视图以及如何管理视图。

视图是存储查询的数据库对象。视图可以作为虚拟表进行访问。换句话说，视图是一个逻辑表，它通过 [SELECT](content/postgresql-select.md) 语句表示一个或多个基础表的数据。请注意，除[物化视图](content/postgresql-materialized-views.md)外，视图不会物理存储数据。

![postgresql view](/imgs/postgresql-view.jpg)

在某些情况下，视图可能非常有用，例如：

- 视图有助于简化查询的复杂性，因为您可以使用简单的 `SELECT` 语句查询基于复杂查询的视图。
- 与表类似，您可以通过包含[用户](content/postgresql-roles.md)有权查看的特定数据的视图向用户授予权限。
- 即使基础表的列发生更改，视图也会提供一致的层。

#### 创建视图

要创建视图，我们使用 `CREATE VIEW` 语句。`CREATE VIEW` 语句的最简单语法如下：

```sql
CREATE VIEW view_name AS query;
```

首先，在 `CREATE VIEW` 子句后指定视图的名称，然后在 `AS` 关键字后面添加查询。查询可以是简单的 `SELECT` 语句，也可以是带有连接的复杂 `SELECT` 语句。

##### 示例

例如，在我们的[示例数据库](content/postgresql-sample-database.md)中，我们有四个表：

1. `customer` - 存储所有客户数据
2. `address` - 存储客户的地址
3. `city` - 存储城市数据
4. `country` - 存储国家数据

![posgresql view - tables](/imgs/posgresql-view-table.jpg)

如果要获取完整的客户数据，通常可以按如下方式构造[连接](content/postgresql-inner-join.md)语句：

```sql
SELECT cu.customer_id AS id,    
  cu.first_name || ' ' || cu.last_name AS name,    
  a.address,    
  a.postal_code AS "zip code",    
  a.phone,    
  city.city,    
  country.country,        
  CASE            
    WHEN cu.activebool THEN 'active'            
    ELSE ''        
  END AS notes,    
  cu.store_id AS sid   
FROM customer cu
INNER JOIN address a USING (address_id)     
INNER JOIN city USING (city_id)     
INNER JOIN country USING (country_id);
 id  |         name          |                address                 | zip code |    phone     |            city            |                country                | notes  | sid
-----+-----------------------+----------------------------------------+----------+--------------+----------------------------+---------------------------------------+--------+-----
 553 | Max Pitt              | 1917 Kumbakonam Parkway                | 11892    | 698182547686 | Novi Sad                   | Yugoslavia                            | active |   1
 432 | Edwin Burk            | 1766 Almirante Brown Street            | 63104    | 617567598243 | Newcastle                  | South Africa                          | active |   1
 524 | Jared Ely             | 1003 Qinhuangdao Street                | 25972    | 35533115997  | Purwakarta                 | Indonesia                             | active |   1
   1 | Mary Smith            | 1913 Hanoi Way                         | 35200    | 28303384290  | Sasebo                     | Japan                                 | active |   1
   2 | Patricia Johnson      | 1121 Loja Avenue                       | 17886    | 838635286649 | San Bernardino             | United States                         | active |   1
   3 | Linda Williams        | 692 Joliet Street                      | 83579    | 448477190408 | Athenai                    | Greece                                | active |   1
   4 | Barbara Jones         | 1566 Inegl Manor                       | 53561    | 705814003527 | Myingyan                   | Myanmar                               | active |   2
   5 | Elizabeth Brown       | 53 Idfu Parkway                        | 42399    | 10655648674  | Nantou                     | Taiwan                                | active |   1
   6 | Jennifer Davis        | 1795 Santiago de Compostela Way        | 18743    | 860452626434 | Laredo                     | United States                         | active |   2
   7 | Maria Miller          | 900 Santiago de Compostela Parkway     | 93896    | 716571220373 | Kragujevac                 | Yugoslavia                            | active |   1
   8 | Susan Wilson          | 478 Joliet Way                         | 77948    | 657282285970 | Hamilton                   | New Zealand                           | active |   2
   9 | Margaret Moore        | 613 Korolev Drive                      | 45844    | 380657522649 | Masqat                     | Oman                                  | active |   2
  10 | Dorothy Taylor        | 1531 Sal Drive                         | 53628    | 648856936185 | Esfahan                    | Iran                                  | active |   1
```

这个查询非常复杂。但是，您可以创建名为 `customer_master` 的视图，如下所示：

```sql
CREATE VIEW customer_master AS  
  SELECT cu.customer_id AS id,    
    cu.first_name || ' ' || cu.last_name AS name,    
    a.address,    
    a.postal_code AS "zip code",    
    a.phone,    
    city.city,    
    country.country,        
    CASE            
      WHEN cu.activebool THEN 'active'            
      ELSE ''        
    END AS notes,    
    cu.store_id AS sid   
  FROM customer cu
  INNER JOIN address a USING (address_id)     
  INNER JOIN city USING (city_id)     
  INNER JOIN country USING (country_id);
```

从现在开始，无论何时需要获取完整的客户数据，只需通过执行以下简单的 `SELECT` 语句从视图中查询：

```sql
SELECT * FROM customer_master;
```

此查询产生与上面具有连接的复杂查询相同的结果。

#### 修改视图

要更改视图的定义查询，请使用带有 `OR REPLACE` 的 `CREATE VIEW` 语句，如下所示：

```sql
CREATE OR REPLACE view_name AS query
```

PostgreSQL不支持删除视图中的现有列，版本9.4以前。如果您尝试这样做，您将收到一条错误消息：“[Err] ERROR:  cannot drop columns from view”。查询必须生成与创建视图时相同的列。更具体地说，新列必须具有相同的名称，相同的[数据类型](content/postgresql-data-types.md)，并且与创建它们的顺序相同。但是，PostgreSQL允许您在列列表的末尾附加其他列。

例如，您可以向 `customer_master` 视图添加电子邮件，如下所示：

```sql
CREATE OR REPLACE VIEW customer_master AS  
  SELECT cu.customer_id AS id,    
    cu.first_name || ' ' || cu.last_name AS name,    
    a.address,    
    a.postal_code AS "zip code",    
    a.phone,    
    city.city,    
    country.country,        
    CASE            
      WHEN cu.activebool THEN 'active'            
      ELSE ''        
    END AS notes,    
    cu.store_id AS sid,    
    cu.email   
    FROM customer cu     
    INNER JOIN address a USING (address_id)     
    INNER JOIN city USING (city_id)     
    INNER JOIN country USING (country_id);
```

现在，如果您从 `customer_master` 视图中查询数据，您将在列表末尾看到 `email` 列。

```sql
SELECT * FROM customer_master;
```

要更改视图的定义，请使用 `ALTER VIEW` 语句。例如，您可以使用以下语句将视图名称从 `customer_master` 更改为 `customer_info`：

```sql
ALTER VIEW customer_master RENAME TO customer_info;
```

PostgreSQL允许您为列名设置默认值，更改视图的架构，设置或重置视图的选项。有关更改视图定义的详细信息，请查看 [PostgreSQL ALTER VIEW](https://www.postgresql.org/docs/current/static/sql-alterview.html) 语句。

#### 删除视图

要删除现有视图，请使用 `DROP VIEW` 语句，如下所示：

```sql
DROP VIEW [ IF EXISTS ] view_name;
```

您可以在 `DROP VIEW` 子句后指定要删除的视图的名称。删除数据库中不存在的视图将导致错误。为避免这种情况，通常会在语句中添加 `IF EXISTS` 选项，以指示PostgreSQL删除视图（如果存在），否则不执行任何操作。

例如，要删除已创建的 `customer_info` 视图，请执行以下查询：

```sql
DROP VIEW IF EXISTS customer_info;
```

视图 `customer_infois` 已从数据库中删除。

在本教程中，我们向您展示了如何创建，更改和删除视图。

#### 相关教程

- [可更新视图](content/postgresql-updatable-views.md)
- [物化视图](content/postgresql-materialized-views.md)