## LENGTH 

> 摘要：在本教程中，我们将向您展示各种长度函数，它们返回字符数或字符串的字节数。

#### 简介

以下说明了 `length` 函数的语法：

```sql
LENGTH(string);
```

`length` 函数接受一个字符串作为参数。字符串可以是以下任何[数据类型](content/postgresql-data-types.md)：

- character 或 char
- character varying 或 varchar
- text

 `length` 函数返回字符串中的字符数。

#### 示例

请参阅以下使用 `length` 函数的示例：

```sql
SELECT LENGTH ('PostgreSQL Tutorial'); -- 19
```

请注意，字符串可以包含空字符串，该字符串不是空值。

```sql
SELECT LENGTH (''); -- 0
```

它返回零。但是，包含空格字符的字符串：

```sql
SELECT LENGTH (' '); -- 1
```

它返回1。

如果将NULL值传递给 `length` 函数，则返回NULL值。

```sql
SELECT LENGTH (NULL); -- NULL
```

以下查询获取 `customer` 表中客户的全名，并使用 `length` 函数获取其名称中的字符数。

```sql
SELECT
 first_name || ' ' || last_name AS name,
 LENGTH (first_name || ' ' || last_name) len
FROM customer
ORDER BY len;
         name          | len
-----------------------+-----
 Jim Rea               |   7
 Jay Robb              |   8
 Kim Cruz              |   8
 Tim Cary              |   8
 Todd Tan              |   8
 Mike Way              |   8
 Max Pitt              |   8
 Don Bone              |   8
 Gary Coy              |   8
 Dan Paine             |   9
```

有时，您可能希望测量数字的长度而不是字符串。在这种情况下，您使用[类型转换](content/postgresql-cast.md)将数字转换为字符串并使用 `length` 函数，如下例所示：

```sql
SELECT LENGTH(CAST(12345 AS TEXT)); --- 5
```

我们经常使用 `length` 函数和其他字符串函数（如[replace](content/postgresql-replace.md), [substring](content/postgresql-substring.md)等）来更有效地操作字符串。以下语句使用 `substring`，`strpos` 和 `length` 函数从电子邮件地址获取用户名和域。

```sql
SELECT
 SUBSTRING (
 'info@postgresqltutorial.com',
 1,
 strpos(
 'info@postgresqltutorial.com',
 '@'
 ) - 1
 ) AS user_name,
 SUBSTRING (
 'info@postgresqltutorial.com',
 strpos(
 'info@postgresqltutorial.com',
 '@'
 ) + 1,
 LENGTH (
 'info@postgresqltutorial.com'
 )
 ) AS domain_name;
  user_name |      domain_name
-----------+------------------------
 info      | postgresqltutorial.com
(1 row)
```

除了 `length` 函数，PostgreSQL还提供了 `char_length` 和 `character_length` 函数，它们提供相同的功能。

#### 字符串长度

要获取字符串中的字节数，请使用 `octet_ length` 函数，如下所示：

```sql
OCTET_LENGTH(string);
```

请参阅以下示例：

```sql
SELECT OCTET_LENGTH ('A'); -- 1 byte
```

它返回1个字节。

```sql
SELECT OCTET_LENGTH ('€'); -- 3 bytes
```

它返回3个字节。但是，使用 `length` 函数，它只返回1。

```sql
SELECT LENGTH ('€'); -- 1
```

要测量字符串的位数，请使用 `bit_length` 函数，如下所示：

```sql
BIT_LENGTH(string);
```

请参阅以下使用 `bit_ length` 函数的示例。

```sql
SELECT BIT_LENGTH ('A'); -- 8 bits
```

```sql
SELECT BIT_LENGTH ('€'); -- 24 bits
```

在本教程中，我们向您展示了如何使用 `length`，`bit_length` 和 `octet_length` 函数来返回字符数，位数和字符串的字节数。