## SUM

> 摘要：本教程介绍如何使用 `SUM` 函数计算值或不同值的总和。

#### 简介

`SUM` 函数返回值或不同值的总和。`SUM` 函数的语法如下：

```sql
SUM(DISTINCT column)
```

使用 `SUM` 函数时，您应该记住以下几点：

- `SUM` 函数忽略 `NULL` 值。这意味着在 `SUM` 函数的计算中不考虑 `NULL` 值。
- 如果使用 [DISTINCT](content/postgresql-select-distinct.md) 运算符，则 `SUM` 函数仅计算不同值的值。例如，如果没有 `DISTINCT` 运算符，则 `SUM` 函数在计算1,1,8和2的总和时返回12。但是，对于 `DISTINCT` 运算符， `SUM` 函数将返回11（1 + 8 + 2）。
- 如果在 [SELECT](content/postgresql-select.md) 子句中使用 `SUM` 函数，则在 `SELECT` 语句不返回任何行时，它将返回 `NULL` 值而不是零（0）。

#### 示例

我们将使用[示例数据库](content/postgresql-sample-database.md)中的 `payment` 表来演示 `SUM` 函数的功能。

![payment table](/imgs/payment-table.png)

让我们计算一下客户ID 2000支付的金额。

```sql
SELECT SUM (amount) AS total 
FROM payment 
WHERE customer_id = 2000;
 total
-------

(1 row)
```

由于 [WHERE](content/postgresql-where.md) 子句中的条件会过滤所有行，因此 `SUM` 函数会返回如上所述的 `NULL` 值。使用 `customer_id`  2000的 `payment` 表中不存在任何行。

有时，如果 `SELECT` 语句找不到匹配的行，则希望 `SUM` 函数返回零而不是 `NULL` 值。在这种情况下，如果第二个参数为 `NULL`，则使用 [COALESCE](content/postgresql-coalesce.md) 函数返回第一个参数作为默认值。

以下查询说明了使用 `COALESCE` 函数的 `SUM` 函数：

```sql
SELECT COALESCE(SUM(amount),0) AS total 
FROM payment 
WHERE customer_id = 2000;
 total
-------
     0
(1 row)
```

##### GROUP BY

要计算每个组的摘要，可以使用 [GROUP BY](content/postgresql-group-by.md) 子句将表中的行分组，并将 `SUM` 函数应用于每个组。

例如，要计算每个客户支付的总金额，请将 `SUM` 函数与 `GROUP BY` 子句一起使用，如下所示：

```sql
SELECT customer_id, SUM (amount) AS total 
FROM payment 
GROUP BY customer_id;
 customer_id | total
-------------+--------
         184 |  80.80
          87 | 137.72
         477 | 106.79
         273 | 130.72
         550 | 151.69
          51 | 123.70
         394 |  77.80
         272 |  65.87
          70 |  75.83
         190 | 102.75
```

以下查询返回支付最多的前5位客户：

```sql
SELECT customer_id, SUM (amount) AS total
FROM payment
GROUP BY customer_id
ORDER BY total DESC
LIMIT 5;
 customer_id | total
-------------+--------
         148 | 211.55
         526 | 208.58
         178 | 194.61
         137 | 191.62
         144 | 189.60
(5 rows)
```

下图说明了PostgreSQL如何执行查询：

![postgresql sum with group by](/imgs/postgresql-sum-with-group-by.jpg)

##### HAVING

要根据特定条件过滤组的总和，请使用 [HAVING](content/postgresql-having.md) 子句中的 `SUM` 函数。例如，以下查询仅返回支付超过200美元的客户：

```sql
SELECT customer_id, SUM (amount) AS total
FROM payment
GROUP BY customer_id
HAVING SUM(amount) > 200
ORDER BY total DESC;
 customer_id | total
-------------+--------
         148 | 211.55
         526 | 208.58
(2 rows)
```

在本教程中，您学习了如何使用 `SUM` 函数计算值或不同值的总和。

#### 相关教程

- [MAX](content/postgresql-max-function.md)
- [MIN](content/postgresql-min-function.md)
- [AVG](content/postgresql-avg-function.md)
- [COUNT](content/postgresql-count-function.md)