## 修改列的数据类型

> 摘要：本教程逐步向您展示如何使用 `ALTER TABLE` 语句更改列的数据类型。

#### 简介

要更改列的[数据类型](content/postgresql-time.md)，请使用[ALTER TABLE](content/postgresql-alter-table.md)语句，如下所示：

```sql
ALTER TABLE table_name
ALTER COLUMN column_name [SET DATA] TYPE new_data_type;
```

让我们更详细地研究一下这个陈述：

- 首先，在 `ALTER TABLE` 子句中指定要更改的列所属的表的名称。
- 其次，在 `ALTER COLUMN` 子句中给出其数据类型将被更改的列的名称。
- 第三，为 `TYPE` 关键字后面的列提供新的数据类型。可以使用 `SET DATA TYPE` 或 `TYPE`。

若要在单个语句中更改多列的数据类型，请使用以下语法：

```sql
ALTER TABLE table_name
ALTER COLUMN column_name_1 [SET DATA] TYPE new_data_type,
ALTER COLUMN column_name_2 [SET DATA] TYPE new_data_type,
...;
```

在此语法中，您将每个 `ALTER COLUMN` 子句用逗号（，）分隔，以便一次更改多个列的类型。

PostgreSQL允许您通过添加 `USING` 子句来更改列的数据类型，将旧列值转换为新列值，如下所示：

```sql
ALTER TABLE table_name
ALTER COLUMN column_name TYPE new_data_type USING expression;
```

`USING` 子句允许您从旧的列计算新列值。

如果省略 `USING` 子句，PostgreSQL将隐式地将旧列值转换为新列值。如果转换失败，PostgreSQL将发出错误并要求您为 `USING` 子句提供转换表达式。

表达式可以像 `column_name :: new_data_type` 一样简单，例如 `price :: numeric`，也可以像自定义函数一样复杂。

#### 示例

让我们创建一个名为 `assets` 的新表，并在表中[插入](content/postgresql-insert.md)一些行进行演示。

![PostgreSQL Change Column Type Sample Table](/imgs/PostgreSQL-Change-Column-Type-Sample-Table.png)

```sql
CREATE TABLE assets (
  id serial PRIMARY KEY,
  name TEXT NOT NULL,
  asset_no VARCHAR NOT NULL,
  description TEXT,
  LOCATION TEXT,
  acquired_date DATE NOT NULL
);
 
INSERT INTO assets 
  (NAME, asset_no, location, acquired_date)
VALUES
  ('Server', '10001', 'Server room', '2017-01-01'),
  ('UPS', '10002', 'Server room', '2017-01-01');
```

要将 `name` 列的数据类型更改为 [`VARCHAR`](content/postgresql-char-varchar-text.md)，请使用以下语句：

```sql
ALTER TABLE assets ALTER COLUMN name TYPE VARCHAR;
```

以下语句将 `description` 和 `location` 列的数据类型从 [`TEXT`](content/postgresql-char-varchar-text.md) 更改为 `VARCHAR`：

```sql
ALTER TABLE assets 
    ALTER COLUMN location TYPE VARCHAR,
    ALTER COLUMN description TYPE VARCHAR;
```

要将 `asset_no` 列的数据类型更改为[整数](content/postgresql-integer.md)，请使用以下语句：

```sql
ALTER TABLE assets ALTER COLUMN asset_no TYPE INT;
```

PostgreSQL发出错误并提供一个非常有用的提示，要求您指定 `USING` 子句：

```sql
ERROR:  column "asset_no" cannot be cast automatically to type integer
HINT:  You might need to specify "USING asset_no::integer".
```

让我们按照建议添加 `USING` 子句：

```sql
ALTER TABLE assets
ALTER COLUMN asset_no TYPE INT USING asset_no::integer;
```

在本教程中，您学习了如何使用 `ALTER TABLE` 语句更改列的类型。
