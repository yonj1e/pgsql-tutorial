## 布尔类型

> 摘要：在本教程中，您将了解PostgreSQL布尔数据类型以及如何在设计数据库表时使用它。

#### 简介

![PostgreSQL Boolean](/imgs/PostgreSQL-Boolean-300x146.png)

PostgreSQL支持单个布尔[数据类型](content/postgresql-data-types.md)：`BOOLEAN` 可以有三种状态：`TRUE`，`FALSE` 和 `NULL`。PostgreSQL使用一个字节在数据库中存储一个布尔值。`BOOLEAN` 可以缩写为 `BOOL`。

在标准SQL中，布尔值可以是 `TRUE`，`FALSE` 或 `NULL`。但是，PostgreSQL在处理 `TRUE` 和 `FALSE` 值时非常灵活。下表显示了PostgreSQL中 `TRUE` 和 `FALSE` 的有效值。

| True   | False   |
| ------ | ------- |
| true   | false   |
| ‘t’    | ‘f ‘    |
| ‘true’ | ‘false’ |
| ‘y’    | ‘n’     |
| ‘yes’  | ‘no’    |
| ‘1’    | ‘0’     |

请注意，前面或后面的空格并不重要，除 `true` 和 `false` 之外的所有常量值必须用单引号括起来。

#### 示例

让我们看一下使用PostgreSQL布尔数据类型的一些示例。

首先，创建一个新的表 `stock_availability` 以记录哪些产品可用。

```sql
CREATE TABLE stock_availability (
 product_id INT NOT NULL PRIMARY KEY,
 available BOOLEAN NOT NULL
);
```

其次，将一些样本数据[插入](content/postgresql-insert.md) `stock_availability` 表。我们对布尔值使用各种值。

```sql
INSERT INTO stock_availability (product_id, available)
VALUES
 (100, TRUE),
 (200, FALSE),
 (300, 't'),
 (400, '1'),
 (500, 'y'),
 (600, 'yes'),
 (700, 'no'),
 (800, '0');
```

第三，要检查可用的产品，请使用以下语句：

```sql
SELECT *
FROM stock_availability
WHERE available = 'yes';
  product_id | available
------------+-----------
        100 | t
        300 | t
        400 | t
        500 | t
        600 | t
(5 rows)
```

您可以使用不带任何运算符的布尔列来表示真值。以下查询返回所有可用的产品：

```sql
SELECT *
FROM stock_availability
WHERE available;
 product_id | available
------------+-----------
        100 | t
        300 | t
        400 | t
        500 | t
        600 | t
(5 rows)
```

同样，如果要查找false值，可以将Boolean列的值与任何有效的布尔常量进行比较。以下查询返回不可用的产品。

```sql
SELECT *
FROM stock_availability
WHERE available = 'no';
  product_id | available
------------+-----------
        200 | f
        700 | f
        800 | f
(3 rows)
```

或者，您可以使用NOT运算符检查布尔列中的值是否为false，如下例所示：

```sql
SELECT *
FROM stock_availability
WHERE NOT available;
 product_id | available
------------+-----------
        200 | f
        700 | f
        800 | f
(3 rows)
```

#### 默认值

若要为现有布尔列设置默认值，请使用[ALTER TABLE](content/postgresql-alter-table.md)语句中的 `SET EFAULT` 子句。

例如，以下 `ALTER TABLE` 语句设置 `stock_availability` 表中可用列的默认值：

```sql
ALTER TABLE stock_availability ALTER COLUMN available
SET DEFAULT FALSE;
```

如果在未指定 `available` 列的值的情况下插入行，则PostgreSQL默认使用FALSE值。

```sql
INSERT INTO stock_availability (product_id)
VALUES (900);
```

```sql
SELECT *
FROM stock_availability
WHERE product_id = 900;
 product_id | available
------------+-----------
        900 | f
(1 row)
```

同样，如果要在[创建表](content/postgresql-create-table.md)时为布尔列设置默认值，请在列定义中使用 `DEFAULT` 子句，如下所示：

```sql
CREATE TABLE boolean_demo(
   ...
   is_ok BOOL DEFAULT 't'
);
```

在本教程中，您了解了PostgreSQL BOOLEAN数据类型以及如何在数据库表设计中使用它。

#### 相关教程

- [数据类型](content/postgresql-data-types.md)