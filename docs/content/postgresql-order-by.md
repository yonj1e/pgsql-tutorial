## ORDER BY

> 摘要：在本教程中，您将学习如何使用  `ORDER BY` 子句对 `SELECT` 语句返回的结果集进行排序。

#### 简介

当您从表中查询数据时，PostgreSQL按照它们插入表中的顺序返回行。 要对结果集进行排序，请在 [`SELECT`](/content/postgresql-select.md) 语句中使用 `ORDER BY` 子句。

`ORDER BY` 子句允许您根据指定的条件按升序或降序对 `SELECT` 语句返回的行进行排序。

以下说明了 `ORDER BY` 子句的语法：

```sql
SELECT column_1, column_2 FROM tbl_name ORDER BY column_1 ASC, column_2 DESC;
```

让我们更详细地了解 `ORDER BY` 子句的语法：

- 首先，在 `ORDER BY` 子句中指定要排序的列。 如果基于多个列对结果集进行排序，请使用逗号分隔两列。
- 其次，使用 `ASC` 按升序对结果集进行排序，然后使用 `DESC` 按降序对结果集进行排序。 如果将其留空，则 `ORDER BY` 子句将默认使用 `ASC`。

我们来看一些使用 `ORDER BY` 子句的例子。

#### 示例

我们将使用[示例数据库](/content/postgresql-sample-database.md)中的 `customer`表进行演示。

![customer table](/imgs/customer-table.png)

以下查询按名字升序对客户进行排序：

```sql
SELECT first_name, last_name 
FROM customer 
ORDER BY first_name ASC;
 first_name  |  last_name
-------------+--------------
 Aaron       | Selby
 Adam        | Gooch
 Adrian      | Clary
 Agnes       | Bishop
 Alan        | Kahn
 Albert      | Crouse
 Alberto     | Henning
 Alex        | Gresham
```

因为默认使用 `ASC`，所以可以在语句中省略它。

如果要按降序对姓氏进行排序，可以使用 `DESC` 关键字，如以下查询所示：

```sql
SELECT first_name, last_name 
FROM customer 
ORDER BY last_name DESC;
 first_name  |  last_name
-------------+--------------
 Cynthia     | Young
 Marvin      | Yee
 Luis        | Yanez
 Brian       | Wyman
 Brenda      | Wright
 Tyler       | Wren
 Florence    | Woods
```

如果要先按升序排序 `first_name` ，按降序排序 `last_name` 的客户结果集，请使用以下语句：

```sql
SELECT first_name, last_name 
FROM customer 
ORDER BY first_name ASC, last_name DESC;
 first_name  |  last_name
-------------+--------------
 Aaron       | Selby
 Adam        | Gooch
 Adrian      | Clary
 Agnes       | Bishop
 Alan        | Kahn
 Albert      | Crouse
 Alberto     | Henning
 Alex        | Gresham
 Alexander   | Fennell
```

请注意，SQL标准仅允许您根据 `SELECT` 子句中显示的列对行进行排序。 但是，PostgreSQL允许您根据甚至未出现在选择列表中的列对行进行排序。

最好遵循SQL标准来使代码可移植并适应PostgreSQL下一版本中可能发生的更改。

在本教程中，我们向您展示了如何使用  `ORDER BY` 子句基于一列或多列按升序和降序对结果集进行排序。

#### 相关教程

- [LIMIT](/content/postgresql-limit.md)