## UPDATE

> 摘要：在本教程中，您将学习如何使用PostgreSQL UPDATE语句更新表中的现有数据。

#### 语法

若要更改表中列的值，请使用 `UPDATE` 语句。以下说明了 `UPDATE` 语句的语法：

```sql
UPDATE table
SET column1 = value1,
    column2 = value2 ,...
WHERE
 condition;
```

让我们详细检查一下语句的语法：

首先，在 `UPDATE` 子句之后指定要更新数据的表名。

其次，列出要在 `SET` 子句中更改其值的列。如果更新多列中的值，则使用逗号（，）分隔每对列和值。不在列表中的列保留其原始值。

第三，确定要在 `WHERE` 子句的条件下更新哪些行。如果省略 `WHERE` 子句，则表中的所有行都将更新。

#### 示例

我们将使用 `INSERT` 教程中创建的 `link` 表进行演示。

我们来看看 `link` 表的内容：

```sql
SELECT * FROM link;
 id |                url                |        name         | description | rel | last_update
----+-----------------------------------+---------------------+-------------+-----+-------------
  1 | http://www.postgresqltutorial.com | PostgreSQL Tutorial |             |     |
  2 | http://www.oreilly.com            | O'Reilly Media      |             |     |
  3 | http://www.google.com             | Google              |             |     |
  4 | http://www.yahoo.com              | Yahoo               |             |     |
  5 | http://www.bing.com               | Bing                |             |     |
  6 | http://www.facebook.com           | Facebook            |             |     | 2013-06-01
  7 | https://www.tumblr.com/           | Tumblr              |             |     | 2018-09-14
  8 | http://www.postgresql.org         | PostgreSQL          |             |     | 2018-09-14
(8 rows)
```

##### 更新部分列

要将 `last_update` 列的 `NULL` 值更改为当前日期，请使用以下语句：

```sql
UPDATE link
SET last_update = DEFAULT
WHERE last_update IS NULL;

SELECT * FROM link;
 id |                url                |        name         | description | rel | last_update
----+-----------------------------------+---------------------+-------------+-----+-------------
  6 | http://www.facebook.com           | Facebook            |             |     | 2013-06-01
  7 | https://www.tumblr.com/           | Tumblr              |             |     | 2018-09-14
  8 | http://www.postgresql.org         | PostgreSQL          |             |     | 2018-09-14
  1 | http://www.postgresqltutorial.com | PostgreSQL Tutorial |             |     | 2018-09-14
  2 | http://www.oreilly.com            | O'Reilly Media      |             |     | 2018-09-14
  3 | http://www.google.com             | Google              |             |     | 2018-09-14
  4 | http://www.yahoo.com              | Yahoo               |             |     | 2018-09-14
  5 | http://www.bing.com               | Bing                |             |     | 2018-09-14
(8 rows)
```

`WHERE` 子句仅更新 `last_update` 列中的值为 `NULL` 的行。我们使用了 `DEFAULT` 关键字，因为 `last_update` 列接受[当前日期](content/postgresql-current_date.md)作为默认值。

##### 更新全部列

要将 `link` 表中所有行的 `rel` 列值更新为 `nofollow`，请在 `UPDATE` 语句中省略 `WHERE` 子句，如下所示：

```sql
UPDATE link SET rel = 'nofollow';
SELECT * FROM link;
 id |                url                |        name         | description |   rel    | last_update
----+-----------------------------------+---------------------+-------------+----------+-------------
  6 | http://www.facebook.com           | Facebook            |             | nofollow | 2013-06-01
  7 | https://www.tumblr.com/           | Tumblr              |             | nofollow | 2018-09-14
  8 | http://www.postgresql.org         | PostgreSQL          |             | nofollow | 2018-09-14
  1 | http://www.postgresqltutorial.com | PostgreSQL Tutorial |             | nofollow | 2018-09-14
  2 | http://www.oreilly.com            | O'Reilly Media      |             | nofollow | 2018-09-14
  3 | http://www.google.com             | Google              |             | nofollow | 2018-09-14
  4 | http://www.yahoo.com              | Yahoo               |             | nofollow | 2018-09-14
  5 | http://www.bing.com               | Bing                |             | nofollow | 2018-09-14
(8 rows)
```



You can also update data of a column from another column within the same table. The following statement copies the values of the `name`column to the `description`column of the `link`table:

您还可以从同一个表中的另一列更新列的数据。以下语句将 `name` 列的值复制到 `link` 表的 `description` 列：

```sql
UPDATE link SET description = name;
SELECT * FROM link;
 id |                url                |        name         |     description     |   rel    | last_update
----+-----------------------------------+---------------------+---------------------+----------+-------------
  6 | http://www.facebook.com           | Facebook            | Facebook            | nofollow | 2013-06-01
  7 | https://www.tumblr.com/           | Tumblr              | Tumblr              | nofollow | 2018-09-14
  8 | http://www.postgresql.org         | PostgreSQL          | PostgreSQL          | nofollow | 2018-09-14
  1 | http://www.postgresqltutorial.com | PostgreSQL Tutorial | PostgreSQL Tutorial | nofollow | 2018-09-14
  2 | http://www.oreilly.com            | O'Reilly Media      | O'Reilly Media      | nofollow | 2018-09-14
  3 | http://www.google.com             | Google              | Google              | nofollow | 2018-09-14
  4 | http://www.yahoo.com              | Yahoo               | Yahoo               | nofollow | 2018-09-14
  5 | http://www.bing.com               | Bing                | Bing                | nofollow | 2018-09-14
(8 rows)
```

##### update join

让我们检查 `link_tmp` 表，它与 `link` 表具有相同的结构：

```sql
SELECT * FROM link_tmp;
 id |           url           |   name   | description | rel | last_update
----+-------------------------+----------+-------------+-----+-------------
  6 | http://www.facebook.com | Facebook |             |     | 2013-06-01
  7 | https://www.tumblr.com/ | Tumblr   |             |     | 2018-09-14
(2 rows)
```

以下语句使用的 `link` 表中的值更新 `link_tmp` 表中列：

```sql
UPDATE link_tmp
SET rel = link.rel,
 description = link.description,
 last_update = link.last_update
FROM link
WHERE link_tmp.id = link.id;

SELECT * FROM link_tmp;
 id |           url           |   name   | description |   rel    | last_update
----+-------------------------+----------+-------------+----------+-------------
  6 | http://www.facebook.com | Facebook | Facebook    | nofollow | 2013-06-01
  7 | https://www.tumblr.com/ | Tumblr   | Tumblr      | nofollow | 2018-09-14
(2 rows)
```

请注意，我们在 `UPDATE` 语句中使用 `FROM` 子句来指定更新中涉及的第二个表（`link`）。

这种 `UPDATE` 语句有时称为 [`UPDATE JOIN`](content/postgresql-update-join.md) 或 `UPDATE INNER JOIN`，因为 `UPDATE` 语句中涉及两个或更多表。连接条件在 `WHERE` 子句中指定。

##### returning 子句

The `UPDATE`statement returns the number of affected rows by default. The PostgreSQL `UPDATE`statement also returns updated entries using the `RETURNING`clause. This addition is a PostgreSQL’s extension to the [SQL standard](http://www.sqltutorial.org/).

The following statement updates the row with id 1 in the `link`table and returns the updated entries:

`UPDATE` 语句默认返回受影响的行数。PostgreSQL `UPDATE` 语句还使用 `RETURNING` 子句返回更新的条目。此功能是PostgreSQL对SQL标准的扩展。

以下语句更新 `link` 表中id为1的行，并返回更新的条目：

```sql
UPDATE link
SET description = 'Learn PostgreSQL fast and easy', rel = 'follow'
WHERE ID = 1 
RETURNING id, description, rel;
 id |          description           |  rel
----+--------------------------------+--------
  1 | Learn PostgreSQL fast and easy | follow
(1 row)

UPDATE 1
```

要验证更新，您可以从 `link` 表中选择数据作为以下查询：

```sql
SELECT * FROM link WHERE ID = 1;
 id |                url                |        name         |          description           |  rel   | last_update
----+-----------------------------------+---------------------+--------------------------------+--------+-------------
  1 | http://www.postgresqltutorial.com | PostgreSQL Tutorial | Learn PostgreSQL fast and easy | follow | 2018-09-14
(1 row)
```

在本教程中，您学习了如何使用PostgreSQL UPDATE语句从表中更新数据。

#### 相关教程

- [Upsert](http://www.postgresqltutorial.com/postgresql-upsert/)