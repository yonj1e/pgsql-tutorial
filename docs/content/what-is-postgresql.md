## PostgreSQL简要概述

> 摘要：在本教程中，您将了解PostgreSQL以及使PostgreSQL相比其他数据库管理系统脱颖而出的特性。

#### PostgreSQL是什么

![What is PostgreSQL](/imgs/What-is-PostgreSQL.png)

让我们从一个简单的问题开始：什么是PostgreSQL？

PostgreSQL是一个通用的对象关系数据库管理系统，是最先进的开源数据库系统。PostgreSQL是在加州大学伯克利计算机科学系的POSTGRES 4.2基础上开发的。

PostgreSQL被设计为在类UNIX平台上运行。然而，PostgreSQL后来也被设计为可移植的，因此它可以在各种平台上运行，例如Mac OS X，Solaris和Windows。

PostgreSQL是免费的开源软件。它的源代码可以在PostgreSQL许可证下获得，这是一个自由开源许可证。您可以以任何形式自由使用，修改和分发PostgreSQL。

PostgreSQL由于其稳定性，只需要进行最少的维护工作。因此，如果开发基于PostgreSQL的应用程序，与其他数据库管理系统相比，总体拥有成本较低。

#### PostgreSQL的亮点

PostgreSQL具有其他企业数据库管理系统提供的许多高级功能，例如：

- 用户自定义类型
- 表继承
- 先进的锁机制
- [外键参照完整性](/content/postgresql-foreign-key.md)
- [视图](/content/postgresql-views.md)，规则，[子查询](/content/postgresql-subquery.md)
- 嵌套事务（savepoints）
- 多版本并发控制（MVCC）
- 异步复制

- 表空间
- 时间点恢复

每个新版本都会添加更多新功能。

#### 是什么让PostgreSQL脱颖而出

PostgreSQL是第一个实现多版本并发控制（MVCC）功能的数据库管理系统，甚至在Oracle之前。MVCC功能在Oracle中称为快照隔离。

PostgreSQL是一个通用的对象 - 关系数据库管理系统。它允许您添加使用不同编程语言（如C / C ++，Java等）开发的自定义函数。

PostgreSQL旨在实现可扩展性。在PostgreSQL中，您可以定义自己的数据类型，索引类型，函数语言等。如果您不喜欢系统的任何部分，您可以开发自定义插件来增强它以满足您的要求，例如，添加新的优化器。

如果您需要任何支持，一个活跃的社区可以提供帮助。在使用PostgreSQL时可能遇到的问题，您都可以在PostgreSQL社区找到答案。当然，也有许多公司提供商业支持服务。

#### 谁在使用PostgreSQL 

许多公司使用PostgreSQL构建产品和解决方案。一些特色公司是Apple，Fujitsu，Red Hat，Cisco，Juniper Network等。查看[PostgreSQL的使用用户](http://www.postgresql.org/about/users/)部分，了解使用PostgreSQL的完整组织列表。

PostgreSQL的社区将PostgreSQL称为/ˈpoʊstɡrɛs ˌkjuː ˈɛl/。因此PostgreSQL的原始名称是Postgres，有时PostgreSQL被称为Postgres。

我们很高兴您选择使用PostgreSQL。让我们[下载并安装PostgreSQL](/content/install-postgresql.md)以开始学习PostgreSQL