## CASE

> 摘要：在本教程中，我们将向您展示如何使用PostgreSQL CASE条件表达式来形成条件查询。

#### 通用CASE表达式

PostgreSQL CASE表达式与其他编程语言中的 `IF/ELSE` 语句相同。PostgreSQL提供了两种形式的CASE表达式。以下说明了CASE语句的通用形式：

```sql
CASE 
     WHEN condition_1  THEN result_1
     WHEN condition_2  THEN result_2
     [WHEN ...]
     [ELSE result_n]
END
```

在这种通用形式中，每个条件都是一个返回布尔值的表达式，可以是 `true` 或 `false`。如果条件计算结果为true，则返回条件后面的结果，并且所有其他 `CASE` 分支根本不处理。如果所有条件都计算为false，则 `CASE` 表达式将返回 `ELSE` 部分中的结果。如果省略 `ELSE` 子句，`CASE` 表达式将返回null。

请注意，所有结果表达式必须具有可转换为单个数据类型的数据类型，例如字符串，数字等。

我们来看一下电影表。假设您要为电影分配价格段：

- 如果租金为0.99，则为大众 mass。
- 如果租金为1.99，则为经济 economic。
- 租金为4.99，则为奢侈 luxury。

而你想知道属于大众的、经济的或奢侈的价格的电影数量。在这种情况下，您可以使用 `CASE` 表达式构造查询，如下所示：

```sql
SELECT
 SUM (
  CASE
   WHEN rental_rate = 0.99 THEN 1
   ELSE 0
  END
 ) AS "Mass",
 SUM (
  CASE
   WHEN rental_rate = 2.99 THEN 1
   ELSE 0
  END
 ) AS "Economic",
 SUM (
  CASE
   WHEN rental_rate = 4.99 THEN 1
   ELSE 0
  END
 ) AS "Luxury"
FROM film;
 Mass | Economic | Luxury
------+----------+--------
  341 |      323 |    336
(1 row)
```

如果租金满足要求，我们使用 `CASE` 表达式返回1或0。然后我们应用[SUM函数](content/postgresql-sum-function.md)来计算每个价格段的影片总数。

#### 简单CASE表达式

PostgreSQL提供了另一种形式的CASE表达式，称为简单形式，如下所示：

```sql
CASE expression
WHEN value_1 THEN
 result_1
WHEN value_2 THEN
 result_2 
[WHEN ...]
ELSE
 result_n
END;
```

PostgreSQL首先计算表达式，然后将其与 `WHEN` 子句中的每个值进行比较，直到找到与表达式的返回值匹配的值。如果PostgreSQL没有找到任何匹配项，它将在 `ELSE` 部分返回 `result_n`，如果省略 `ELSE` 部分则返回 `NULL` 值。这类似于其他编程语言中的 `switch` 语句，如 C/C++，Java 等。

我们可以使用如下简单形式重写通用 `CASE` 表达式：

```sql
SELECT
 SUM (
  CASE rental_rate
   WHEN 0.99 THEN 1
   ELSE 0
  END
 ) AS "Mass",
 SUM (
  CASE rental_rate
   WHEN 2.99 THEN 1
   ELSE 0
  END
 ) AS "Economic",
 SUM (
  CASE rental_rate
   WHEN 4.99 THEN 1
   ELSE 0
  END
 ) AS "Luxury"
FROM film;
 Mass | Economic | Luxury
------+----------+--------
  341 |      323 |    336
(1 row)
```

在本教程中，您学习了如何使用两种形式的PostgreSQL CASE表达式来形成复杂查询。

#### 相关教程

- [NULLIF](content/postgresql-nullif.md)