## LEFT

`LEFT()` 函数返回字符串中的前n个字符。

#### 语法

下面说明了 `LEFT()` 函数的语法：

```sql
LEFT(string, n)
```

#### 参数

`LEFT()` 函数需要两个参数：

**1) string**

是一个字符串，其中返回了最左边的字符数。

**2) n**

是一个整数，指定应返回字符串中最左侧字符的数量。

如果n为负数，则 `LEFT()` 函数返回除了 `|n|` （绝对值）个字符外字符串中最左边的字符。

#### 返回值

`LEFT()` 函数返回字符串中的前 `n` 个字符。

#### 示例

让我们看一下使用 `LEFT()` 函数的一些例子。

以下示例显示如何获取字符串 `'ABC'` 的第一个字符：

```sql
SELECT LEFT('ABC',1);
 left
------
 A
(1 row)
```

要获取字符串 `'ABC'` 的前两个字符，请使用2而不是1作为 `n` 参数：

```sql
SELECT LEFT('ABC',2);
 left
------
 AB
(1 row)
```

以下语句演示了如何使用负整数：

```sql
SELECT LEFT('ABC',-2);
 left
------
 A
(1 row)
```

在这个例子中，n是-2，因此， `LEFT()` 函数返回除最后2个字符之外的所有字符，返回：

```sql
 left
------
 A
(1 row)
```

请参阅示例数据库中的以下 `customer` 表：

以下语句使用 `LEFT()` 函数获取首字母，使用 `COUNT()` 函数返回每个首字母的客户数。

```sql
SELECT LEFT(first_name, 1) initial, COUNT(*)
FROM customer
GROUP BY initial
ORDER BY initial;
 initial | count
---------+-------
 A       |    44
 B       |    32
 C       |    49
 D       |    40
 E       |    31
 F       |    13
 G       |    22
 H       |    15
```

在此示例中，首先， `LEFT()` 函数返回所有客户的首字母。然后，`GROUP BY` 子句按客户的首字母对客户进行分组。最后，`COUNT()` 函数返回每个组的客户数。

#### 备注

如果你想获得最右边的n个字符，请参阅 `RIGHT()` 函数了解详细信息。

在本教程中，您学习了如何使用 `LEFT()` 函数来获取字符串中最左边的n个字符。