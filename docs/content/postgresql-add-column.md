## 添加列



![PostgreSQL Add Column](/imgs/PostgreSQL-Add-Column-300x128.png)

> 摘要：在本教程中，我们将向您展示如何使用PostgreSQL ADD COLUMN语句将一个或多个列添加到现有数据库表。

#### 简介

要将新列添加到现有表，请使用 [ALTER TABLE](content/postgresql-alter-table.md) `ADD COLUMN` 语句，如下所示：

```sql
ALTER TABLE table_name
ADD COLUMN new_column_name data_type;
```

让我们更详细地研究一下这个陈述。

- 首先，在 `ALTER TABLE` 子句中指定要添加新列的表。
- 其次，在 `ADD COLUMN` 子句中指明列名及其属性，如数据类型，默认值等。

向表中添加新列时，PostgreSQL会将其附加到表的末尾。PostgreSQL没有选项来指定表中新列的位置。

若要向现有表添加多个列，请在 `ALTER TABLE` 语句中使用多个 `ADD COLUMN` 子句，如下所示：

```sql
ALTER TABLE table_name
ADD COLUMN new_column_name_1 data_type constraint,
ADD COLUMN new_column_name_2 data_type constraint,
...
ADD COLUMN new_column_name_n data_type constraint;
```

#### 示例

以下[CREATE TABLE](content/postgresql-create-table.md)语句创建一个名为 `customers` 的新表，其中包含两列：`id` 和 `customer_name`：

```sql
CREATE TABLE customers (
 id SERIAL PRIMARY KEY,
 customer_name VARCHAR NOT NULL
);
```

要将 `phone` 列添加到 `customers` 表，请使用以下语句：

```sql
ALTER TABLE customers ADD COLUMN phone VARCHAR;
```

以下语句将 `fax` 和 `email` 列添加到 `customers` 表：

```sql
ALTER TABLE custoemr  ADD COLUMN fax VARCHAR, ADD COLUMN email VARCHAR;
```

以下命令[描述](content/postgresql-describe-table.md)了 `customers` 表结构。

```sql
#\d customers
                                  Table "public.customers"
    Column     |       Type        |                       Modifiers
---------------+-------------------+--------------------------------------------------------
 id            | integer           | not null default nextval('customers_id_seq'::regclass)
 customer_name | character varying |
 phone         | character varying |
 fax           | character varying |
 email         | character varying |
Indexes:
    "customers_pkey" PRIMARY KEY, btree (id)
```

如您所见，我们在 `customers` 表的列列表末尾添加了 `phone`, `fax` 和 `email` 列。

#### 添加带有NOT NULL约束的列

我们将数据[插入](content/postgresql-insert.md)到 `customers` 表中。

```sql
INSERT INTO customers (customer_name)
VALUES
 ('Apple'),
 ('Samsung'),
 ('Sony');
```

假设您要将 `contact_name` 列添加到 `customers` 表：

```sql
ALTER TABLE customers 
ADD COLUMN contact_name VARCHAR NOT NULL;
```

PostgreSQL发出错误：

```sql
ERROR:  column "contact_name" contains null values
```

这是因为 `contact_name` 列具有 `NOT NULL` 约束。当PostgreSQL添加列时，此新列采用 `NULL` 值，这违反了 `NOT NULL` 约束。

要解决这个问题......

首先，您需要添加没有 `NOT NULL` 约束的列。

```sql
ALTER TABLE customers 
ADD COLUMN contact_name VARCHAR;
```

其次，更新 `contact_name` 列的值。

```sql
UPDATE customers
SET contact_name = 'John Doe'
WHERE
 ID = 1;
 
UPDATE customers
SET contact_name = 'Mary Doe'
WHERE
 ID = 2;
 
UPDATE customers
SET contact_name = 'Lily Bush'
WHERE
 ID = 3;
```

第三，为 `contact_name` 列设置 `NOT NULL` 约束。

```sql
ALTER TABLE customers
ALTER COLUMN contact_name SET NOT NULL;
```

解决问题的另一种方法是......

首先，使用默认值添加列。

```sql
ALTER TABLE customers
ADD COLUMN contact_name NOT NULL DEFAULT 'foo';
```

其次，更新联系人数据。

第三，从列中删除默认值。

```sql
ALTER TABLE customers 
ALTER COLUMN contact_name 
DROP DEFAULT;
```

在本教程中，我们向您展示了如何使用PostgresQL `ADD COLUMN` 语句向表中添加一个或多个列。

#### 相关教程

- [ALTER TABLE](content/postgresql-alter-table.md)
- [CREATE TABLE](content/postgresql-create-table.md)