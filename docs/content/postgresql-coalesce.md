## COALESCE

> 摘要：在本教程中，您将了解返回第一个非null参数的PostgreSQL COALESCE函数。您将学习如何在[SELECT语句](content/postgresql-select.md)中应用此函数以有效地处理空值。

#### 语法

`COALESCE` 函数的语法如下：

```sql
COALESCE (argument_1, argument_2, …);
```

`COALESCE` 函数接受无限数量的参数。它返回第一个非null的参数。如果所有参数都为null，则 `COALESCE` 函数将返回null。

`COALESCE` 函数从左到右计算参数，直到找到第一个非null参数。不评估第一个非null参数的所有剩余参数。

`COALESCE` 函数提供与SQL标准提供的 `NVL` 或 `IFNULL` 函数相同的功能。MySQL具有IFNULL函数，而Oracle提供 `NVL` 函数。

请参阅以下示例：

```sql
SELECT COALESCE (1, 2);
 coalesce
----------
        1
(1 row)

SELECT COALESCE (NULL, 2 , 1);
 coalesce
----------
        2
(1 row)
```

在查询数据时，我们经常使用 `COLAESCE` 函数替换空值的默认值。例如，我们想要显示博文中的摘录，如果没有提供摘录，我们可以使用帖子内容的前150个字符。为此，我们可以使用 `COALESCE` 函数，如下所示：

```sql
SELECT COALESCE (excerpt, LEFT(CONTENT, 150))
FROM posts;
```

#### 示例

我们来看一个使用 `COALESCE` 函数的例子。首先，我们使用 [CREATE TABLE](content/postgresql-create-table.md) 语句创建一个名为 `items` 的表，如下所示：

```sql
CREATE TABLE items (
 ID serial PRIMARY KEY,
 product VARCHAR (100) NOT NULL,
 price NUMERIC NOT NULL,
 discount NUMERIC
);
```

`items` 表中有四个字段：

- id：标识items表中项目的主键。
- product：产品名称。
- price：产品的价格。
- discount：产品的折扣。

其次，我们使用 [INSERT](content/postgresql-insert.md) 语句将一些记录插入到 `items` 表中，如下所示：

```sql
INSERT INTO items (product, price, discount)
VALUES
 ('A', 1000 ,10),
 ('B', 1500 ,20),
 ('C', 800 ,5),
 ('D', 500, NULL);
```

第三，我们使用以下公式查询产品的净价：

```sql
net_price = price - discount;
```

```sql
SELECT product, (price - discount) AS net_price
FROM items;
 product | net_price
---------+-----------
 A       |       990
 B       |      1480
 C       |       795
 D       |
(4 rows)
```

如果你看第四行，你会注意到产品D的净价是零，这看起来不正确。问题是产品D的折扣为空，因此当我们采用空值来计算净价时，PostgreSQL返回null。

得到合适的价格，我们需要假设如果折扣为空，则为零。然后我们可以使用 `COALESCE` 函数如下：

```sql
SELECT product, (price - COALESCE(discount,0)) AS net_price
FROM items;
 product | net_price
---------+-----------
 A       |       990
 B       |      1480
 C       |       795
 D       |       500
(4 rows)
```

现在产品D的净价格是500，因为我们在计算净价时使用零而不是零值。

除了使用 `COALESCE` 函数之外，您还可以使用 [CASE](content/postgresql-case.md) 表达式来处理这种情况下的空值。请参阅以下使用 `CASE` 表达式的查询以获得上述相同的结果。

```sql
SELECT
 product,
 (
 price - CASE
 WHEN discount IS NULL THEN 0
 ELSE discount
 END
 ) AS net_price
FROM items;
 product | net_price
---------+-----------
 A       |       990
 B       |      1480
 C       |       795
 D       |       500
(4 rows)
```

在上面的查询中，我们说如果折扣为null，则使用零（0），否则在计算净价的表达式中使用折扣值。

在性能方面，`COALESCE` 函数和 `CASE` 表达式是相同的。我们更喜欢 `COALESCE` 函数而不是 `CASE` 表达式，因为 `COALESCE` 函数使查询更简单，更容易阅读。

在本教程中，您学习了如何使用 `COALESCE` 函数替换查询中的空值。

#### 相关教程

- [NULLIF](content/postgresql-nullif.md)