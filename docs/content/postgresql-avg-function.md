## AVG

> 摘要：在本教程中，您将学习如何使用 `AVG()` 函数来计算集合的平均值。

#### 简介 

`AVG` 函数是最常用的[聚合函数](content/postgresql-aggregate-functions.md)之一。`AVG()` 函数允许您计算数字列的平均值。

`AVG` 函数的语法如下：

```sql
AVG(column)
```

您可以在 `SELECT` 和 `HAVING` 子句中使用 `AVG()` 函数。我们来看看一些使用的例子

我们来看一些使用 `AVG` 函数的例子。

我们将使用[dvdrental示例数据库](content/postgresql-sample-database.md)中的以下 `payment` 表进行演示：

![payment table](/imgs/payment-table.png)

#### 示例

如果您想知道客户支付的平均金额，您可以在 `amount` 列上应用 `AVG` 函数，如下所示：

```sql
SELECT to_char(AVG(amount), '99999999999999999D99') AS average_amount
FROM payment;
    average_amount
-----------------------
                  4.20
(1 row)
```

请注意，我们使用 `to_char()` 函数将结果转换为格式化字符串。

##### DISTINCT

要计算一组的平均值，条件是只计算不同的值，可以使用 [DISTINCT](content/postgresql-select-distinct.md)，如下所示：

```sql
AVG(DISTINCT column)
```

例如，以下查询返回客户的平均付款。因为我们使用 `DISTINCT`，所以PostgreSQL只获取唯一数量并计算平均值。

```sql
SELECT TO_CHAR(AVG(DISTINCT amount), 'FM999999999.00')
FROM payment;
```

请注意，结果与第一个示例不同。

##### SUM

以下查询使用[SUM函数](content/postgresql-sum-function.md)和 `AVG` 函数来计算客户的总付款和所有交易的平均值。

```sql
SELECT TO_CHAR(AVG(amount), 'FM999999999.00') AS "Average", 
TO_CHAR(SUM(amount), 'FM999999999.00') AS "Total"
FROM payment;
 Average |  Total
---------+----------
 4.20    | 61312.04
(1 row)
```

##### GROUP BY

要计算组的平均值，请使用带有[GROUP BY](content/postgresql-group-by.md)子句的 `AVG` 函数。首先，`GROUP BY` 子句将表的行划分为组，然后为每个组应用 `AVG` 函数。

例如，要计算每个客户支付的平均金额，请使用以下查询：

```sql
SELECT customer.customer_id, first_name, last_name, 
to_char(AVG(amount), '99999999999999999D99') AS average_amount
FROM payment
INNER JOIN customer ON customer.customer_id = payment.customer_id
GROUP BY customer.customer_id
ORDER BY customer_id;
 customer_id | first_name  |  last_name   |    average_amount
-------------+-------------+--------------+-----------------------
           1 | Mary        | Smith        |                  3.82
           2 | Patricia    | Johnson      |                  4.76
           3 | Linda       | Williams     |                  5.45
           4 | Barbara     | Jones        |                  3.72
           5 | Elizabeth   | Brown        |                  3.85
           6 | Jennifer    | Davis        |                  3.39
           7 | Maria       | Miller       |                  4.67
           8 | Susan       | Wilson       |                  3.73
           9 | Margaret    | Moore        |                  3.94
          10 | Dorothy     | Taylor       |                  3.95
          11 | Lisa        | Anderson     |                  4.34
```

在查询中，我们使用[内联接](content/postgresql-inner-join.md)将 `customer` 表连接了 `payment` 表。我们使用 `GROUP BY` 子句将客户分组并应用 `AVG` 函数来计算每组的平均值。

下图说明了PostgreSQL如何执行查询。

![PostgreSQL AVG function with GROUP BY query explain](/imgs/postgresql-avg-function-with-GROUP-BY-query-explain.jpg)

##### HAVING

您可以使用 `HAVING` 子句中的 `AVG` 函数根据特定条件筛选组。例如，对于所有客户，您可以获得支付平均金额大于5美元的客户。以下查询可帮助您执行此操作：

```sql
SELECT customer.customer_id, first_name, last_name, 
to_char(AVG(amount), '99999999999999999D99') AS average_amount
FROM payment
INNER JOIN customer ON customer.customer_id = payment.customer_id
GROUP BY customer.customer_id
HAVING AVG (amount) > 5
ORDER BY customer_id;
 customer_id | first_name | last_name |    average_amount
-------------+------------+-----------+-----------------------
           3 | Linda      | Williams  |                  5.45
          19 | Ruth       | Martinez  |                  5.49
         137 | Rhonda     | Kennedy   |                  5.04
         181 | Ana        | Bradley   |                  5.08
         187 | Brittany   | Riley     |                  5.62
         209 | Tonya      | Chapman   |                  5.09
         259 | Lena       | Jensen    |                  5.16
         272 | Kay        | Caldwell  |                  5.07
         285 | Miriam     | Mckinney  |                  5.12
         293 | Mae        | Fletcher  |                  5.13
         310 | Daniel     | Cabral    |                  5.30
         311 | Paul       | Trout     |                  5.39
         321 | Kevin      | Schuler   |                  5.52
         470 | Gordon     | Allard    |                  5.09
         472 | Greg       | Robins    |                  5.07
         477 | Dan        | Paine     |                  5.09
         508 | Milton     | Howland   |                  5.29
         522 | Arnold     | Havens    |                  5.05
         542 | Lonnie     | Tirado    |                  5.30
         583 | Marshall   | Thorn     |                  5.12
(20 rows)
```

此查询与上面的查询类似，带有一个额外的 `HAVING` 子句。我们在 `HAVING` 子句中使用了 `AVG` 函数来过滤平均数量小于或等于5的组。

##### NULL

`AVG` 函数在计算平均值时如何处理空值？ 我们来试试吧。

首先，我们创建一个名为 t1 的表。

```sql
CREATE TABLE t1 ( ID serial PRIMARY KEY, amount INTEGER);
```

其次，我们插入一些示例数据：

```sql
INSERT INTO t1 (amount) VALUES (10), (NULL), (30);
```

t1表的数据如下：

```sql
SELECT * FROM t1;
 id | amount
----+--------
  1 |     10
  2 |
  3 |     30
(3 rows)
```

第三，我们使用 `AVG` 函数计算金额列中的平均值。

```sql
SELECT AVG (amount) FROM t1;
         avg
---------------------
 20.0000000000000000
(1 row)
```

我们得到20。这意味着 `AVG` 函数在计算平均值时会忽略 `NULL` 值。

在本教程中，我们向您展示了使用 `AVG` 函数计算集合平均值的各种示例。

#### 相关教程

- [MAX](content/postgresql-max-function.md)
- [MIN](content/postgresql-min-function.md)
- [SUM](content/postgresql-sum-function.md)
- [COUNT](content/postgresql-count-function.md)