## 重命名表

> 摘要：本教程介绍如何使用 `ALTER TABLE` 语句的 `RENAME` 表子句重命名表。

#### 语法

要重命名现有表，请使用[ALTER TABLE](content/postgresql-alter-table.md)语句，如下所示：

```sql
ALTER TABLE table_name
RENAME TO new_table_name;
```

在这个声明中：

- 首先，在 `ALTER TABLE` 子句后指定要重命名的表的名称。
- 其次，在 `RENAME TO` 子句之后给出新的表名。

如果您尝试重命名不存在的表，PostgreSQL将发出错误。要避免这种情况，请按如下方式添加 `IF EXISTS` 选项：

```sql
ALTER TABLE IF EXISTS table_name
RENAME TO new_table_name;
```

在这种情况下，如果 `table_name` 不存在，PostgreSQL将发出通知。

要重命名多个表，必须执行多个 `ALTER TABLE RENAME TO` 语句。它不可能在一个声明中完成。

#### 示例

为了演示，我们创建了一个名为 `vendos` 的新表。

```sql
CREATE TABLE vendors (
    id serial PRIMARY KEY,
    name VARCHAR NOT NULL
);
```

要将 `vendors` 表重命名为 `suppliers`，请使用以下 `ALTER TABLE RENAME TO` 语句：

```sql
ALTER TABLE vendors RENAME TO suppliers;
```

假设每个vendor或supplier属于一个组。要管理此关系，我们需要添加 `supplier_groups` 表，如下所示：

```sql
CREATE TABLE supplier_groups (
    ID serial PRIMARY KEY,
    name VARCHAR NOT NULL
);
```

我们还需要在名为 `group_id` 的 `suppliers` 表中添加一个新列。此列是链接到 `supplier_groups` 表的 `id` 列的[外键](content/postgresql-foreign-key.md)列。

```sql
ALTER TABLE suppliers 
ADD COLUMN group_id INT NOT NULL;
 
ALTER TABLE suppliers 
ADD FOREIGN KEY (group_id) REFERENCES supplier_groups (ID);
```

为了节省查询完整供应商数据的时间，我们创建了一个针对 `suppliers` 和 `supplier_groups` 表的[视图](content/postgresql-materialized-views.md)，如下所示：

```sql
CREATE VIEW supplier_data AS SELECT
  s.id, s.name, g.name group
FROM suppliers s
INNER JOIN supplier_groups g ON g.id = s.group_id;
```

将表重命名为新表时，PostgreSQL将自动更新其依赖对象，如[外键约束](content/postgresql-foreign-key.md)，[视图](content/postgresql-views.md)和索引。

我们先来看看 `suppliers` 表：

```sql
test=# \d suppliers;
                              Table "public.suppliers"
  Column  |       Type        |                      Modifiers
----------+-------------------+------------------------------------------------------
 id       | integer           | not null default nextval('vendors_id_seq'::regclass)
 name     | character varying | not null
 group_id | integer           | not null
Indexes:
    "vendors_pkey" PRIMARY KEY, btree (id)
Foreign-key constraints:
    "suppliers_group_id_fkey" FOREIGN KEY (group_id) REFERENCES supplier_groups(id)
```

输出显示 `suppliers` 表具有引用 `supplier_groups` 表的外键约束。

现在，我们将 `supplier_groups` 表重命名为 `groups`，如下所示：

```sql
ALTER TABLE supplier_groups RENAME TO groups;
```

您可以通过[描述](content/postgresql-describe-table.md) `suppliers` 表来验证 `suppliers` 表中的外键约束，如下所示：

```sql
test-# \d suppliers;
                              Table "public.suppliers"
  Column  |       Type        |                      Modifiers
----------+-------------------+------------------------------------------------------
 id       | integer           | not null default nextval('vendors_id_seq'::regclass)
 name     | character varying | not null
 group_id | integer           | not null
Indexes:
    "vendors_pkey" PRIMARY KEY, btree (id)
Foreign-key constraints:
    "suppliers_group_id_fkey" FOREIGN KEY (group_id) REFERENCES groups(id)
```

如您所见，外键约束已更新并引用了 `groups` 表。

以下语句显示 `supplier_data` 视图：

```sql
test-# \d+ supplier_data;
                       View "public.supplier_data"
     Column     |       Type        | Modifiers | Storage  | Description
----------------+-------------------+-----------+----------+-------------
 id             | integer           |           | plain    |
 name           | character varying |           | extended |
 supplier_group | character varying |           | extended |
View definition:
 SELECT s.id,
    s.name,
    g.name AS supplier_group
   FROM suppliers s
     JOIN groups g ON g.id = s.group_id;
```

输出显示视图的[SELECT](content/postgresql-select.md)语句中的 `supplier_groups` 表也更新为 `groups` 表。

在本教程中，您学习了如何使用 `ALTER TABLE` 语句的PostgreSQL `RENAME` 表子句将表重命名为新表。