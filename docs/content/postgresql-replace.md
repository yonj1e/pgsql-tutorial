## REPLACE

> 摘要：在本教程中，我们将向您介绍PostgreSQL替换函数，这些函数在字符串中搜索子字符串并将子字符串替换为新的子字符串。

#### REPLACE

有时，您希望使用新的字符串搜索和替换列中的字符串，例如替换过时的电话号码，损坏的URL和拼写错误。

要使用新的字符串搜索和替换所有出现的字符串，请使用 `REPLACE()` 函数。

以下说明了 `REPLACE()` 函数的语法：

```sql
REPLACE(source, old_text, new_text );
```

##### 示例

`REPLACE()` 函数接受三个参数：

- `source` 是您要替换的字符串。
- `old_text` 是您要搜索和替换的文本。如果 `old_text` 在字符串中多次出现，则将替换它的所有出现次数。
- `new_text` 是将替换旧文本（`old_text`）的新文本。

`REPLACE()` 函数示例

请参阅以下使用 `REPLACE()` 函数的示例：

```sql
SELECT REPLACE ('ABC AA', 'A', 'Z');
 replace
---------
 ZBC ZZ
(1 row)
```

在这个例子中，我们用字符串中的字符 “Z” 替换了所有字符 “A”。

以下示例将URL中的子字符串 `tt` 替换为 `xx`：

```sql
SELECT
 REPLACE (
 'http://www.postgresqltutorial.com',
 'tt',
 'xx'
 );
              replace
-----------------------------------
 hxxp://www.postgresqltutorial.com
(1 row)
```

如果要在表列中搜索和替换子字符串，请使用以下语法：

```sql
UPDATE 
   table_name
SET 
   column_name = REPLACE(column,old_text,new_text)
WHERE 
   condition
```

让我们使用[示例数据库](content/postgresql-sample-database.md)中的 `customer` 表进行演示：

```sql
SELECT first_name, last_name, email FROM customer;
 first_name  |  last_name   |                  email
-------------+--------------+------------------------------------------
 Jared       | Ely          | jared.ely@sakilacustomer.org
 Mary        | Smith        | mary.smith@sakilacustomer.org
 Patricia    | Johnson      | patricia.johnson@sakilacustomer.org
 Linda       | Williams     | linda.williams@sakilacustomer.org
 Barbara     | Jones        | barbara.jones@sakilacustomer.org
 Elizabeth   | Brown        | elizabeth.brown@sakilacustomer.org
 Jennifer    | Davis        | jennifer.davis@sakilacustomer.org
 Maria       | Miller       | maria.miller@sakilacustomer.org
 Susan       | Wilson       | susan.wilson@sakilacustomer.org
 Margaret    | Moore        | margaret.moore@sakilacustomer.org
 Dorothy     | Taylor       | dorothy.taylor@sakilacustomer.org
```

现在，假设您要[更新](content/postgresql-update.md)电子邮件列以使用 `postgresqltutorial.com` 替换域 `sakilacustomer.org`，请使用以下语句：

```sql
UPDATE 
   customer
SET 
   email = REPLACE (
   email,
 'sakilacustomer.org',
 'postgresqltutorial.com'
   );
```

因为我们省略了 [WHERE](content/postgresql-where.md) 子句，所以更新了 `customer` 表中的所有行。

让我们验证是否已经进行了替换。

```sql
SELECT first_name, last_name, email FROM customer;
 first_name  |  last_name   |                    email
-------------+--------------+----------------------------------------------
 Max         | Pitt         | max.pitt@postgresqltutorial.com
 Jared       | Ely          | jared.ely@postgresqltutorial.com
 Mary        | Smith        | mary.smith@postgresqltutorial.com
 Patricia    | Johnson      | patricia.johnson@postgresqltutorial.com
 Linda       | Williams     | linda.williams@postgresqltutorial.com
 Barbara     | Jones        | barbara.jones@postgresqltutorial.com
 Elizabeth   | Brown        | elizabeth.brown@postgresqltutorial.com
 Jennifer    | Davis        | jennifer.davis@postgresqltutorial.com
 Maria       | Miller       | maria.miller@postgresqltutorial.com
 Susan       | Wilson       | susan.wilson@postgresqltutorial.com
 Margaret    | Moore        | margaret.moore@postgresqltutorial.com
 Dorothy     | Taylor       | dorothy.taylor@postgresqltutorial.com
 Lisa        | Anderson     | lisa.anderson@postgresqltutorial.com
```

#### REGEXP_REPLACE

如果您需要更高级的匹配，可以使用 `REGEXP_REPLACE()` 函数。

`REGEXP_REPLACE()` 函数允许您替换与正则表达式匹配的子字符串。以下说明了 `REPGEX_REPLACE()` 函数的语法。

```sql
REGEXP_REPLACE(source, pattern, new_text [,flags])
```

`REGEXP_REPLACE()` 函数接受四个参数：

- `source` 是您将查找与模式匹配的子字符串并将其替换为 `new_text` 的字符串。如果未找到匹配项，则源不变。
- `pattern` 是一个正则表达式。它可以是任何模式，例如：电子邮件，URL，电话号码等。
- `next_text` 是替换子字符串的文本。
- `flags` 包含零个或多个单字母标志，用于控制 `REPGEX_REPLACE()` 函数的行为。例如，i表示不区分大小写的匹配或忽略大小写。`g` 代表全球; 如果使用 `g` 标志，则该函数将替换与模式匹配的所有子串。 `flags` 参数是可选的。

##### 示例

需要付出努力和实践才能理解 `REGEXP_REPLACE()` 函数的工作原理。

以下是使用 `REGEXP_REPLACE()` 函数的示例。

```sql
SELECT regexp_replace( 'foo bar foobar barfoo', 'foo', 'bar' );
    regexp_replace
-----------------------
 bar bar foobar barfoo
(1 row)
```

在下面的示例中，因为我们使用 `i` 标志，它会忽略大小写并用 `foo` 替换第一次出现的 `Bar` 或 `bar`。

```sql
SELECT regexp_replace( 'Bar foobar bar bars', 'Bar', 'foo', 'i' );
   regexp_replace
---------------------
 foo foobar bar bars
(1 row)
```

在下面的例子中，我们使用 `g` 标志，所有出现的 `bar` 都被 `foo` 替换。请注意，`Bar`，`BAR` 或 `bAR` 不会更改。

```sql
SELECT regexp_replace( 'Bar sheepbar bar bars barsheep', 'bar', 'foo', 'g' );
         regexp_replace
--------------------------------
 Bar sheepfoo foo foos foosheep
(1 row)
```

在下面的示例中，我们使用 `g` 和 `i` 标志，因此所有出现的 `bar` 或 `Bar`，`BAR` 等都被 `foo` 替换。

```sql
SELECT regexp_replace( 'Bar sheepbar bar bars barsheep', 'bar', 'foo', 'gi' );
         regexp_replace
--------------------------------
 foo sheepfoo foo foos foosheep
(1 row)
```

`\m` 表示仅在每个单词的开头匹配。在任何情况下以 `bar` 开头的所有单词都被 `foo` 替换。带栏的单词结尾不会更改。

```sql
SELECT regexp_replace( 'Bar sheepbar bar bars barsheep', '\mbar', 'foo', 'gi' );
         regexp_replace
--------------------------------
 foo sheepbar foo foos foosheep
(1 row)
```

`\M` 表示仅在每个单词的末尾匹配。在任何情况下都以 `bar` 结尾的所有单词都被 `foo` 取代。以 `bar` 开头的单词不会被替换。

```sql
SELECT regexp_replace( 'Bar sheepbar bar bars barsheep', 'bar\M', 'foo', 'gi' );
         regexp_replace
--------------------------------
 foo sheepfoo foo bars barsheep
(1 row)
```

`\m` 和 `\M` 表示在每个单词的开头和结尾都匹配。在任何情况下以 `bar` 开头和 `/` 或结尾的所有单词都被 `foo` 替换。

```sql
SELECT regexp_replace( 'Bar sheepbar bar bars barsheep', '\mbar\M', 'foo', 'gi' );
         regexp_replace
--------------------------------
 foo sheepbar foo bars barsheep
(1 row)
```

#### TRANSLATE

除了 `REPLACE()` 和 `REGEXP_REPLACE()` 函数之外，PostgreSQL还为您提供了另一个名为 `TRANSLATE()` 的函数用于字符串替换。

给定一组字符集， `TRANSLATE()` 函数将 `source` 集合中与集合匹配的任何字符替换为 `new_set` 中的字符。

```sql
TRANSLATE(source, set, new_set);
```

`TRANSLATE()` 函数接受三个参数：

- `source` 是您要搜索和替换的字符串。
- `set` 是一组用于匹配的字符。
- `new_set` 是一组字符，用于替换与该 `set` 匹配的字符。

请注意，如果集合的字符数多于 `new_set`，则PostgreSQL会从 `source` 字符串中删除 `set` 中的额外字符。

##### 示例

在下面的示例中，我们将所有特殊元音转换为普通元音。

```sql
SELECT TRANSLATE ( 'LÒ BÓ VÔ XÕ', 'ÒÓÔÕ', 'OOOO' );
  translate
-------------
 LO BO VO XO
(1 row)
```

见下图。

![postgresql translate function](/imgs/postgresql-translate-function.jpg)

在本教程中，我们向您展示了各种函数：`REPLACE()`，`REGEXP_REPLACE()` 和 `TRANSLATE()` 来搜索和替换新的子字符串。