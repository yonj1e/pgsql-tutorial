## 标识列

> 摘要：在本教程中，您将学习如何使用 `GENERATED AS IDENTITY` 约束为表创建PostgreSQL标识列。

#### 简介

PostgreSQL 10版本引入了一项名为 `GENERATED AS IDENTITY` 约束的新功能，允许您自动为列分配唯一值。`GENERATED AS IDENTITY` 约束是符合SQL标准的PostgreSQL的SERIAL列的变体。

以下说明了GENERATED AS IDENTITY约束的语法：

```sql
column_name type GENERATED { ALWAYS | BY DEFAULT } AS IDENTITY[ ( sequence_option ) ]
```

在这个语法中：

- 类型可以是 `SMALLINT`，`INT` 或 `BIGINT`。
- `GENERATED ALWAYS` 指示PostgreSQL始终为标识列生成值。如果您尝试将值[插入](content/postgresql-insert.md)（或[更新](content/postgresql-update.md)）到 `GENERATED ALWAYS AS IDENTITY` 列中，PostgreSQL将发出错误。
- `GENERATED BY DEFAULT` 还指示PostgreSQL为标识列生成值。但是，如果为插入或更新提供值，PostgreSQL将使用该值插入标识列，而不是使用系统生成的值。

PostgreSQL允许您在表中拥有多个标识列。与 `SERIAL` 约束类似，`GENERATED AS IDENTITY` 约束也在内部使用 `SEQUENCE` 对象。

#### 示例

#### A）`GENERATED ALWAYS`

首先，使用 `color_id` 作为标识列创建一个名为 `color` 的表：

```sql
CREATE TABLE color (
    color_id INT GENERATED ALWAYS AS IDENTITY,
    color_name VARCHAR NOT NULL
);
```

其次，在 `color` 表中插入一个新行：

```sql
INSERT INTO color (color_name)
VALUES ('Red');
```

因为 `color_id` 列具有 `GENERATED AS IDENTITY` 约束，所以PostgreSQL为它生成一个值，如下面的查询所示：

```sql
SELECT * FROM color;
 color_id | color_name
----------+------------
        1 | Red
(1 row)
```

第三，通过为 `color_id` 和 `color_name` 列提供值来插入新行：

```sql
INSERT INTO color (color_id, color_name)
VALUES (2, 'Green');
```

PostgreSQL发出以下错误：

```sql
[Err] ERROR:  cannot insert into column "color_id"
DETAIL:  Column "color_id" is an identity column defined as GENERATED ALWAYS.
HINT:  Use OVERRIDING SYSTEM VALUE to override.
```

要修复错误，在这种情况下，您可以使用 `OVERRIDING SYSTEM VALUE` 子句，如下所示：

```sql
INSERT INTO color (color_id, color_name)
OVERRIDING SYSTEM VALUE 
VALUES (2, 'Green');
```

```sql
SELECT * FROM color;
 color_id | color_name
----------+------------
        1 | Red
        2 | Green
(2 rows)
```

或者使用 `GENERATED BY DEFAULT AS IDENTITY` 代替。

#### B）`GENERATED BY DEFAULT AS IDENTITY`

首先，删除 `color` 表并重新创建它。这次我们使用 `GENERATED BY DEFAULT AS IDENTITY`：

```sql
DROP TABLE color;
 
CREATE TABLE color (
    color_id INT GENERATED BY DEFAULT AS IDENTITY,
    color_name VARCHAR NOT NULL
);
```

其次，在 `color` 表中插入一行：

```sql
INSERT INTO color (color_name)
VALUES ('White');
```

第三，插入另一行，其中包含 `color_id` 列的值：

```sql
INSERT INTO color (color_id, color_name)
VALUES (2, 'Yellow');
```

与之前使用 `GENERATED ALWAYS AS IDENTITY` 约束的示例不同，此语句也有效。

#### C）序列选项

由于 `GENERATED AS IDENTITY` 约束使用 `SEQUENCE` 对象，因此您可以为系统生成的值指定序列选项。

例如，您可以指定起始值和增量，如下所示：

```sql
DROP TABLE color;
 
CREATE TABLE color (
    color_id INT GENERATED BY DEFAULT AS IDENTITY 
    (START WITH 10 INCREMENT BY 10),
    color_name VARCHAR NOT NULL
); 
```

在此示例中，`color_id` 列的系统生成值以10开头，增量值也为10。

首先，在 `color` 表中插入一个新行：

```sql
INSERT INTO color (color_name)
VALUES ('Orange');
```

`color_id` 列的起始值为10，如下所示：

```sql
SELECT * FROM color;
 color_id | color_name
----------+------------
       10 | Orange
(1 row)
```

其次，在 `color` 表中插入另一行：

```sql
INSERT INTO color (color_name)
VALUES ('Purple');
```

由于increment选项，第二行的 `color_id` 的值为20。

```sql
SELECT * FROM color;
 color_id | color_name
----------+------------
       10 | Orange
       20 | Purple
(2 rows)
```

#### 将标识列添加到现有表

您可以使用以下形式的 `ALTER TABLE` 语句将标识列添加到现有表：

```sql
ALTER TABLE table_name 
ALTER COLUMN column_name 
ADD GENERATED { ALWAYS | BY DEFAUT } AS IDENTITY { ( sequence_option ) }
```

我们来看下面的例子。

首先，创建一个名为 `shape` 的新表：

```sql
CREATE TABLE shape (
    shape_id INT NOT NULL,
    shape_name VARCHAR NOT NULL
);
```

其次，将 `shape_id` 列更改为标识列：

```sql
ALTER TABLE shape 
ALTER COLUMN shape_id ADD GENERATED ALWAYS AS IDENTITY;
```

查看 `shape` 表：

```sql
\d shape
                                 Table "public.shape"
   Column   |       Type        | Collation | Nullable |           Default
------------+-------------------+-----------+----------+------------------------------
 shape_id   | integer           |           | not null | generated always as identity
 shape_name | character varying |           | not null |
```

#### 更改标识列

您可以使用以下 `ALTER TABLE` 语句更改现有标识列的特征：

```sql
ALTER TABLE table_name 
ALTER COLUMN column_name 
{ SET GENERATED { ALWAYS| BY DEFAULT } | 
  SET sequence_option | RESTART [ [ WITH ] restart ] }
```

例如，以下语句将 `shape` 表的 `shape_id` 列更改为 `GENERATED BY DEFAULT`：

```sql
ALTER TABLE shape
ALTER COLUMN shape_id SET GENERATED BY DEFAULT;
```

查看 `shape` 表：

```sql
\d shape
                                   Table "public.shape"
   Column   |       Type        | Collation | Nullable |             Default
------------+-------------------+-----------+----------+----------------------------------
 shape_id   | integer           |           | not null | generated by default as identity
 shape_name | character varying |           | not null |
```

从输出中可以看出，`shape_id` 列已从 `GENERATED ALWAYS` 更改为 `GENERATED BY DEFAULT` 。

#### 删除`GENERATED AS IDENTITY`约束

以下语句从现有表中删除 `GENERATED AS IDENTITY` 约束：

```sql
ALTER TABLE table_name 
ALTER COLUMN column_name 
DROP IDENTITY [ IF EXISTS ] 
```

例如，您可以从 `shape` 表的 `shape_id` 列中删除 `GENERATED AS IDENTITY` 约束列，如下所示：

```sql
ALTER TABLE shape
ALTER COLUMN shape_id
DROP IDENTITY IF EXISTS;
```

在本教程中，您学习了如何使用PostgreSQL标识列以及如何通过 `GENERATED AS IDENTITY` 约束来管理它。