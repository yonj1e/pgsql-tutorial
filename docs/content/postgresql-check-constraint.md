## CHECK 约束

> 摘要：在本教程中，我们将向您介绍CHECK约束，该约束基于布尔表达式约束表中列的值。

`CHECK` 约束是一种约束，允许您指定列中的值是否必须满足特定要求。`CHECK` 约束使用布尔表达式在插入或更新列之前评估值。如果值通过检查，PostgreSQL将在列中[插入](content/postgresql-insert.md)或[更新](content/postgresql-update.md)这些值。

#### 为新表定义 CHECK 约束

您通常在使用`CREATE TABLE 语句创建表时使用 `CHECK`约束。以下语句定义了 `employees` 表。

```sql
CREATE TABLE employees (
 id serial PRIMARY KEY,
 first_name VARCHAR (50),
 last_name VARCHAR (50),
 birth_date DATE CHECK (birth_date > '1900-01-01'),
 joined_date DATE CHECK (joined_date > birth_date),
 salary numeric CHECK(salary > 0)
);
```

`employees` 表有三个 `CHECK` 约束：

- 首先，员工的出生日期（`birth_date`）必须大于 `01/01/1900`。如果您尝试插入出生在 `01/01/1900` 之前的日期，您将收到一条错误消息。
- 其次，入职日期（`joined_date`）必须大于出生日期（`birth_date`）。此检查将防止更新无效日期的语义。
- 第三，薪水必须大于零，这是显而易见的。

让我们尝试在 `employees` 表中插入一个新行：

```sql
INSERT INTO employees (
 first_name,
 last_name,
 birth_date,
 joined_date,
 salary
)
VALUES
 (
 'John',
 'Doe',
 '1972-01-01',
 '2015-07-01',
 - 100000
 );
```

该声明试图将负薪水插入 `salary` 列。但是，PostgreSQL返回以下错误消息：

```sql
[Err] ERROR:  new row for relation "employees" violates check constraint "employees_salary_check"
DETAIL:  Failing row contains (1, John, Doe, 1972-01-01, 2015-07-01, -100000).
```

插入失败，因为 `salary` 列上的 `CHECK` 约束只接受正值。

默认情况下，PostgreSQL使用以下模式为 `CHECK` 约束赋予名称：

```sql
{table}{column}check
```

例如，`salary` 列的约束具有以下约束名称：

```sql
employees_salary_check
```

但是，如果要为特定名称指定 `CHECK` 约束，可以在 `CONSTRAINT` 表达式之后指定它，如下所示：

```sql
column_name data_type CONSTRAINT constraint_name CHECK(...)
```

请参阅以下示例：

```sql
...
salary numeric CONSTRAINT positive_salary CHECK(salary > 0)
...
```

#### 为现有表定义 CHECK 约束

要将 `CHECK` 约束添加到现有表，请使用 `ALTER TABLE` 语句。假设您在数据库中有一个名为 `prices_list` 的现有表

```sql
CREATE TABLE prices_list (
 id serial PRIMARY KEY,
 product_id INT NOT NULL,
 price NUMERIC NOT NULL,
 discount NUMERIC NOT NULL,
 valid_from DATE NOT NULL,
 valid_to DATE NOT NULL
);
```

现在，您可以使用 `ALTER TABLE` 语句将 `CHECK` 约束添加到 `prices_list` 表。价格和折扣必须大于零，折扣小于价格。请注意，我们使用包含 `AND` 运算符的布尔表达式。

```sql
ALTER TABLE prices_list ADD CONSTRAINT price_discount_check CHECK (
 price > 0
 AND discount >= 0
 AND price > discount
);
```

到日期的有效期（`valid_to`）必须大于或等于date（`valid_from`）的有效期。

```sql
ALTER TABLE prices_list 
ADD CONSTRAINT valid_range_check CHECK (valid_to >= valid_from);
```

`CHECK` 约束对于放置附加逻辑来限制列可以在数据库层接受的值非常有用。通过使用 `CHECK` 约束，可以确保数据正确更新到数据库。

在本教程中，您学习了如何使用PostgreSQL `CHECK` 约束来检查基于布尔表达式的列的值。

#### 相关教程

- [CREATE TABLE](content/postgresql-create-table.md)
- [ Primary Key](content/postgresql-primary-key.md)
- [Foreign Key](content/postgresql-foreign-key.md)
- [UNIQUE Constraint](content/postgresql-unique-constraint.md)
- [Not-Null Constraint](content/postgresql-not-null-constraint.md)