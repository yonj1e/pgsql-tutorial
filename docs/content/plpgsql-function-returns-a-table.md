## PL/pgSQL函数返回表

> 摘要：在本教程中，我们将向您展示如何开发返回表的PostgreSQL函数。

我们将使用[示例数据库](content/postgresql-sample-database.md)中的 `file` 表进行演示：

![Film Table](/imgs/film-table.png)

以下函数使用 [ILIKE](content/postgresql-like.md) 运算符返回标题与特定模式匹配的所有影片。

```sql
CREATE OR REPLACE FUNCTION get_film (p_pattern VARCHAR) 
 RETURNS TABLE (
 film_title VARCHAR,
 film_release_year INT
) 
AS $$
BEGIN
 RETURN QUERY SELECT
  title,
  cast( release_year as integer)
 FROM film
 WHERE title ILIKE p_pattern ;
END; $$ 
LANGUAGE 'plpgsql';
```

此 `get_film(varchar)` 函数接受一个参数 `p_pattern`，它是您要与电影标题匹配的模式。

要从函数返回表，请使用 `RETURNS TABLE` 语法并指定表的列。每列用逗号（，）分隔。

在函数中，我们返回一个 [SELECT](content/postgresql-select.md) 语句结果的查询。请注意，`SELECT` 语句中的列必须与我们要返回的表的列匹配。因为电影表的 `release_year` 的[数据类型](content/postgresql-data-types.md)不是整数，所以我们必须使用[类型转换](content/postgresql-cast.md)将其转换为整数。

我们可以使用以下语句测试该函数。

```sql
SELECT * FROM get_film ('Al%');
    film_title    | film_release_year
------------------+-------------------
 Alabama Devil    |              2006
 Aladdin Calendar |              2006
 Alamo Videotape  |              2006
 Alaska Phantom   |              2006
 Ali Forever      |              2006
 Alice Fantasia   |              2006
 Alien Center     |              2006
 Alley Evolution  |              2006
 Alone Trip       |              2006
 Alter Victory    |              2006
(10 rows)
```

我们调用了 `get_film(varchar)` 函数来获取标题以Al开头的所有电影。

请注意，如果使用以下语句调用该函数：

```sql
SELECT get_film ('Al%');
         get_film
---------------------------
 ("Alabama Devil",2006)
 ("Aladdin Calendar",2006)
 ("Alamo Videotape",2006)
 ("Alaska Phantom",2006)
 ("Ali Forever",2006)
 ("Alice Fantasia",2006)
 ("Alien Center",2006)
 ("Alley Evolution",2006)
 ("Alone Trip",2006)
 ("Alter Victory",2006)
(10 rows)
```

PostgreSQL返回一个包含一列电影的表格。

实际上，在将其附加到函数的结果集中之前，通常会处理每一行。以下示例说明了这个想法。

```sql
CREATE OR REPLACE FUNCTION get_film (p_pattern VARCHAR,p_year INT) 
 RETURNS TABLE (
 film_title VARCHAR,
 film_release_year INT
) AS $$
DECLARE 
 var_r record;
BEGIN
 FOR var_r IN(SELECT 
  title, 
  release_year 
 FROM film 
 WHERE title ILIKE p_pattern AND 
  release_year = p_year)  
 LOOP
  film_title := upper(var_r.title) ; 
  film_release_year := var_r.release_year;
  RETURN NEXT;
 END LOOP;
END; $$ 
LANGUAGE 'plpgsql';
```

我们创建了一个名为 `get_film(varchar，int)` 的函数，但接受了两个参数：

1. 第一个参数是 `p_pattern`，如果标题与此模式匹配，我们用它来搜索电影。我们使用 [ILIKE](content/postgresql-like.md) 运算符来执行搜索。
2. 第二个参数是电影的发行年份。

顺便说一句，这在PostgreSQL中称为[重载函数](content/plpgsql-function-overloading.md)。

因为我们想在返回之前处理每一行，所以我们使用 [FOR LOOP](content/plpgsql-loop-statements.md) 语句来处理它。在每次迭代中，我们使用 [UPPER](content/postgresql-letter-case-functions.md) 函数将电影标题转换为大写。此操作仅用于演示目的。

`RETURN NEXT` 语句在函数的结果集中添加一行。执行继续，结果集在循环的每次迭代中构建。

请参阅以下测试用例：

```sql
SELECT * FROM get_film ('%er', 2006);
         film_title          | film_release_year
-----------------------------+-------------------
 ACE GOLDFINGER              |              2006
 ALI FOREVER                 |              2006
 ALIEN CENTER                |              2006
 AMISTAD MIDSUMMER           |              2006
 ARACHNOPHOBIA ROLLERCOASTER |              2006
 DYING MAKER                 |              2006
 BIRDCAGE CASPER             |              2006
 BOUND CHEAPER               |              2006
 BREAKFAST GOLDFINGER        |              2006
 BUTCH PANTHER               |              2006
 CAMPUS REMEMBER             |              2006
 CASABLANCA SUPER            |              2006
 CINCINATTI WHISPERER        |              2006
 DANCING FEVER               |              2006
```

现在您应该了解并知道如何使用 `RETURN QUERY` 和 `RETURN NEXT` 语句开发一个返回表的函数。