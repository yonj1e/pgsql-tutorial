## TO_DATE

> 摘要：本教程向您介绍 `to_date` 函数，该函数可帮助您将字符串转换为日期。

#### 简介

`to_date()` 函数将[字符串](content/postgresql-char-varchar-text.md)文字转换为[日期](content/postgresql-date.md)值。以下说明了 `to_date()` 函数的语法：

```sql
to_date(text,format);
```

`to_date()` 函数接受两个字符串参数。第一个参数是要转换为日期的字符串。第二个是输入格式。 `to_date()` 函数返回日期值。

请参阅以下示例：

```sql
SELECT to_date('20170103','YYYYMMDD');
  to_date
------------
 2017-01-03
(1 row)
```

在此示例中，字符串 `20170103` 基于输入格式 `YYYYMMDD` 被转换为日期。

- `YYYY`：四位数格式的年份
- `MM`：2位数格式的月份
- `DD`：2位数格式的日期

结果，该功能将 `January 3rd 2017` 返回。

下表说明了格式化日期值的模板模式：

| **Pattern**             | **Description**                                              |
| ----------------------- | ------------------------------------------------------------ |
| Y,YYY                   | year in 4 digits with comma                                  |
| YYYY                    | year in 4 digits                                             |
| YYY                     | last 3 digits of year                                        |
| YY                      | last 2 digits of year                                        |
| Y                       | The last digit of year                                       |
| IYYY                    | ISO 8601 week-numbering year (4 or more digits)              |
| IYY                     | Last 3 digits of ISO 8601 week-numbering year                |
| IY                      | Last 2 digits of ISO 8601 week-numbering year                |
| I                       | Last digit of ISO 8601 week-numbering year                   |
| BC, bc, AD or ad        | Era indicator without periods                                |
| B.C., b.c., A.D. ora.d. | Era indicator with periods                                   |
| MONTH                   | English month name in uppercase                              |
| Month                   | Full capitalized English month name                          |
| month                   | Full lowercase English month name                            |
| MON                     | Abbreviated uppercase month name e.g., JAN, FEB, etc.        |
| Mon                     | Abbreviated capitalized month name e.g, Jan, Feb,  etc.      |
| mon                     | Abbreviated lowercase month name e.g., jan, feb, etc.        |
| MM                      | month number from 01 to 12                                   |
| DAY                     | Full uppercase day name                                      |
| Day                     | Full capitalized day name                                    |
| day                     | Full lowercase day name                                      |
| DY                      | Abbreviated uppercase day name                               |
| Dy                      | Abbreviated capitalized day name                             |
| dy                      | Abbreviated lowercase day name                               |
| DDD                     | Day of year (001-366)                                        |
| IDDD                    | Day of ISO 8601 week-numbering year (001-371; day 1 of the year is Monday of the first ISO week) |
| DD                      | Day of month (01-31)                                         |
| D                       | Day of the week, Sunday (1) to Saturday (7)                  |
| ID                      | ISO 8601 day of the week, Monday (1) to Sunday (7)           |
| W                       | Week of month (1-5) (the first week starts on the first day of the month) |
| WW                      | Week number of year (1-53) (the first week starts on the first day of the year) |
| IW                      | Week number of ISO 8601 week-numbering year (01-53; the first Thursday of the year is in week 1) |
| CC                      | Century e.g, 21, 22, etc.                                    |
| J                       | Julian Day (integer days since November 24, 4714 BC at midnight UTC) |
| RM                      | Month in uppercase Roman numerals (I-XII; I=January)         |
| rm                      | Month in lowercase Roman numerals (i-xii; i=January)         |

#### 示例

以下语句将字符串 `10 Feb 2017` 转换为日期值：

```sql
SELECT to_date('10 Feb 2017', 'DD Mon YYYY');
  to_date
------------
 2017-02-10
(1 row)
```

假设您要将字符串 `2017 Feb 10` 转换为日期值，您可以应用模式 `YYYY Mon DD`，如下所示：

```sql
SELECT to_date('2017 Feb 20','YYYY Mon DD');
  to_date
------------
 2017-02-20
(1 row)
```

#### 问题

如果传递无效的日期字符串， `to_date` 函数将尝试将其转换为您可能不需要的有效日期。请参阅以下示例：

```sql
-- PostgreSQL 9.x
SELECT to_date('2017/02/30', 'YYYY/MM/DD');
  to_date
------------
 2017-03-02
(1 row)

-- PostgreSQL 10
SELECT to_date('2017/02/30', 'YYYY/MM/DD');
ERROR:  date/time field value out of range: "2017/02/30"
```

要将此字符串转换为日期，您应该明确地将字符串文字转换为日期：

```sql
SELECT '2017/02/30'::date;
```

PostgreSQL发出以下错误：

```
ERROR:  date/time field value out of range: "2017/02/30"
LINE 1: SELECT '2017/02/30'::date;
```

在本教程中，您学习了如何使用 `to_date` 函数将字符串文字转换为日期值。