## DATE_PART

> 摘要：在本教程中，我们将向您介绍 `DATE_PART()` 函数，该函数允许您从日期或时间值中检索子字段，例如年，月，周。

#### 简介

`DATE_PART()` 函数从日期或时间值中提取子字段。以下说明了 `DATE_PART()` 函数：

```sql
DATE_PART(field,source)
```

该字段是一个标识符，用于确定从 `source` 中提取的字段 `field`。该字段的值必须位于下面提到的允许值列表中：

- century
- decade
- year
- month
- day
- hour
- minute
- second
- microseconds
- milliseconds
- dow
- doy
- epoch
- isodow
- isoyear
- timezone
- timezone_hour
- timezone_minute

`source` 是一个时间表达式，其计算结果为[`TIMESTAMP`](content/postgresql-timestamp.md), [`TIME`](content/postgresql-time.md),  [`INTERVAL`](content/postgresql-interval.md)。如果 `source` 的计算结果为[`DATE`](content/postgresql-date.md)，则该函数将转换为 `TIMESTAMP`。

`DATE_PART()` 函数返回一个类型为双精度的值。

#### 示例

以下示例从时间戳中提取世纪：

```sql
SELECT date_part('century',TIMESTAMP '2017-01-01');
 date_part
-----------
        21
(1 row)
```

要从同一时间戳中提取年份，请将年份传递给 `field` 参数：

```sql
SELECT date_part('year',TIMESTAMP '2017-01-01');
 date_part
-----------
      2017
(1 row)
```

要提取季度，请使用以下语句：

```sql
SELECT date_part('quarter',TIMESTAMP '2017-01-01');
 date_part
-----------
         1
(1 row)  
```

要获得月份，请将 `month` 传递给 `DATE_PART()` 函数：

```sql
 SELECT date_part('month',TIMESTAMP '2017-09-30');
 date_part
-----------
         9
(1 row)
```

要从时间戳中获得十年，请使用以下语句：

```sql
 SELECT date_part('decade',TIMESTAMP '2017-09-30');
 date_part
-----------
       201
(1 row)
```

要从时间戳中提取周数，请将周作为第一个参数传递：

```sql
SELECT date_part('week',TIMESTAMP '2017-09-30');
 date_part
-----------
        39
(1 row)
```

要获得当前的千年，您可以使用带有 `NOW()` 函数的 `DATE_PART()` 函数，如下所示：

```sql
 SELECT date_part('millennium',now());
 date_part
-----------
         3
(1 row)
```

要从时间戳中提取日期部分，请将日期值传递给 `DATE_PART()` 函数：

```sql
SELECT date_part('day',TIMESTAMP '2017-03-18 10:20:30');
 date_part
-----------
        18
(1 row)
```

要从时间戳中提取小时，分钟，秒，请将相应的值hour, minute 和 second传递给 `DATE_PART()` 函数：

```sql
SELECT date_part('hour',TIMESTAMP '2017-03-18 10:20:30') h,
       date_part('minute',TIMESTAMP '2017-03-18 10:20:30') m,
       date_part('second',TIMESTAMP '2017-03-18 10:20:30') s;
 h  | m  | s
----+----+----
 10 | 20 | 30
(1 row)
```

要从时间戳中提取星期几或某一天，请使用以下语句：

```sql
SELECT date_part('dow',TIMESTAMP '2017-03-18 10:20:30') dow,
       date_part('doy',TIMESTAMP '2017-03-18 10:20:30') doy;
 
 dow | doy
-----+-----
   6 |  77
(1 row)
```

在本教程中，您学习了如何使用 `DATE_PART()` 函数来提取时间戳的子字段。