## FETCH

> 摘要：在本教程中，您将学习如何使用 `FETCH` 子句检索查询返回的部分行。

#### 简介

要限制查询返回的行数，通常使用 `LIMIT` 子句。`LIMIT` 句被许多关系数据库管理系统广泛使用，例如MySQL，H2和HSQLDB。 但是，`LIMIT` 子句不是SQL标准。

为了符合SQL标准，PostgreSQL提供了 `FETCH` 子句来检索查询返回的一部分行。 请注意，`FETCH` 子句是在SQL:2008中引入的。

以下说明 `FETCH` 子句的语法：

```sql
OFFSET start { ROW | ROWS } FETCH { FIRST | NEXT } [ row_count ] { ROW | ROWS } ONLY
```

在这个语法中：

- `ROW` 和 `FIRST` 分别是 `ROWS` 和 `NEXT` 的同义词。
- `start` 是一个必须为零或正数的整数。 默认情况下，如果未指定 `OFFSET` 子句，则它为零。 如果 `start` 大于结果集中的行数，则不返回任何行;
- `row_count` 是1或更大。 默认情况下，如果未指定 `row_count` 的值，则为1。

由于存储在表中的行的顺序是不可预测的，因此应使用带有 `ORDER BY` 子句的 `FETCH` 子句使结果集保持一致。

请注意，在SQL:2008中，`OFFSET` 子句必须位于 `FETCH` 子句之前。 但是，`OFFSET` 和 `FETCH` 子句可以在PostgreSQL中以任何顺序出现。

#### 示例

让我们使用[示例数据库](content/postgresql-sample-database.md)中的电影表进行演示。

![Film Table](http://www.postgresqltutorial.com/wp-content/uploads/2018/03/film_table.png)

以下查询返回按标题排序的电影的第一行：

```sql
SELECT film_id, title
FROM film
ORDER BY title 
FETCH FIRST ROW ONLY;
 film_id |      title
---------+------------------
       1 | Academy Dinosaur
(1 row)
```

它等同于以下查询：

```sql
SELECT film_id, title
FROM film
ORDER BY title 
FETCH FIRST 1 ROW ONLY;
 film_id |      title
---------+------------------
       1 | Academy Dinosaur
(1 row)
```

以下查询返回按标题排序的前五部电影：

```sql
SELECT film_id, title
FROM film
ORDER BY title 
FETCH FIRST 5 ROW ONLY;
 film_id |      title
---------+------------------
       1 | Academy Dinosaur
       2 | Ace Goldfinger
       3 | Adaptation Holes
       4 | Affair Prejudice
       5 | African Egg
(5 rows)
```

以下查询返回在按照标题排序的前五部电影之后接下来的五部电影：

```sql
SELECT film_id, title
FROM film
ORDER BY title 
OFFSET 5 ROWS 
FETCH FIRST 5 ROW ONLY; 
 film_id |      title
---------+------------------
       6 | Agent Truman
       7 | Airplane Sierra
       8 | Airport Pollock
       9 | Alabama Devil
      10 | Aladdin Calendar
(5 rows)
```

在本教程中，您学习了如何使用 `FETCH` 子句检索查询返回的部分行。
