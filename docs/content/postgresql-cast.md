## CAST

> 摘要：在本教程中，我们将向您展示如何使用PostgreSQL CAST将一种数据类型转换为另一种数据类型。

#### 简介

在许多情况下，您希望将一种[数据类型](content/postgresql-data-types.md)转换为另一种数据类型，例如，需求发生了更改，从而[更改了列的数据类型](content/postgresql-alter-table.md)。PostgreSQL提供了将一种类型转换为另一种类型的语法。以下说明了CAST类型转换的语法：

```sql
CAST ( expression AS type );
```

首先，指定一个表达式，该表达式可以是要转换的常量、表列等。然后，指定要转换为的目标类型。

#### 示例

以下语句将字符串常量转换为整数：

```sql
SELECT CAST ('100' AS INTEGER);
```

如果表达式无法转换为目标类型，PostgreSQL将发出错误。请参阅以下示例：

```sql
SELECT
 CAST ('10C' AS INTEGER);
[Err] ERROR:  invalid input syntax for integer: "10C"
LINE 2:  CAST ('10C' AS INTEGER);
```

在以下示例中，我们将各种字符串转换为日期数据类型：

```sql
SELECT CAST ('2015-01-01' AS DATE);
 
SELECT CAST ('01-OCT-2015' AS DATE);
```

首先，我们将 `2015-01-01` 字符串转换为 `January 1st 2015`。其次，我们将 `01-OCT-2015` 转换为 `October 1st 2015`。

在以下示例中，我们尝试将字符串转换为double值：

```sql
SELECT CAST ('10.2' AS DOUBLE);
```

哎呀，我们收到以下错误消息：

```sql
[Err] ERROR:  type "double" does not exist
LINE 2:  CAST ('10.2' AS DOUBLE)
```

要解决此问题，您需要使用 `DOUBLE PRECISION` 而不是 `DOUBLE`，如下所示;

```sql
SELECT CAST ('10.2' AS DOUBLE PRECISION);
```


除了CAST类型语法之外，您还可以使用以下语法将类型转换为另一种类型：

```sql
expression::type
```

请参阅以下示例：

```sql
SELECT '100'::INTEGER;
 
SELECT '01-OCT-2015'::DATE;
```

请注意，使用 `::` 的转换语法是PostgreSQL特有的，并且不符合SQL标准。

#### 使用示例

在本节中，我们将向您展示使用 `CAST` 类型的实际示例。

假设，我们有一个有两列的 `tatings` 表：`id` 和 `rate`。rate列的数据类型为varchar，因为 `rating` 表将评级存储为A，B和C。我们可以按如下方式创建 `ratings` 表：

```sql
CREATE TABLE ratings (
 ID serial PRIMARY KEY,
 rate VARCHAR (1)
);
```

我们将一些样本数据插入到 `ratings` 表中。

```sql
INSERT INTO ratings (rate)
VALUES
 ('A'),
 ('B'),
 ('C');
```

由于要求发生变化，我们使用相同的 `ratings` 表来存储数字评级。

```sql
INSERT INTO ratings (rate)
VALUES
 (1),
 (2),
 (3);
```

因此，`ratings`表存储混合值，包括数字和字符串。

```sql
SELECT * FROM ratings;
 id | rate
----+------
  1 | A
  2 | B
  3 | C
  4 | 1
  5 | 2
  6 | 3
(6 rows)
```

现在，我们必须将 `rate` 列中的所有值转换为整数，所有其他A，B，C等级将显示为零。为此，您可以使用CASE运算符进行类型转换，如下所示：

```sql
SELECT
 id,
 CASE
 WHEN rate~E'^\\d+$' THEN
 CAST (rate AS INTEGER)
 ELSE
 0
 END as rate
FROM
 ratings;
  id | rate
----+------
  1 |    0
  2 |    0
  3 |    0
  4 |    1
  5 |    2
  6 |    3
(6 rows)
```

`CASE` 运算符检查 `rate`，如果它与整数模式匹配，它将 `rate` 转换为整数，否则返回0。

在本教程中，我们向您展示了如何使用PostgreSQL CAST将数据类型转换为另一种数据类型。