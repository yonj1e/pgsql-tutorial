# PostgreSQL触发器

PostgreSQL触发器是在发生事件(例如，[INSERT](content/postgresql-insert.md)、[UPDATE](content/postgresql-update.md)或[DELETE](content/postgresql-delete.md))时自动调用的[函数](content/postgresql-create-function.md)。在本节中，我们将向您介绍数据库触发器，并说明如何在数据库中应用它们。 

- [触发器简介](content/introduction-postgresql-trigger.md) – 简要概述PostgreSQL触发器，为什么要使用触发器以及何时使用它们。
- [创建触发器](content/creating-first-trigger-postgresql.md) – 展示如何在PostgreSQL中创建第一个触发器。
- [管理触发器](content/managing-postgresql-trigger.md) –  为您提供一些方便的语句来修改，禁用和删除触发器。