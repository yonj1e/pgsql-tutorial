# PostgreSQL视图

视图是命名查询, 它提供了另一种方法来显示数据库表中的数据。 视图是基于一个或多个表定义的，这些表称为基表。 [创建视图](content/managing-postgresql-views.md)时，基本上是创建一个查询并为其指定名称，因此视图对于包装常用的复杂查询很有用。

请注意，普通视图不会存储任何数据，[物化视图](content/postgresql-materialized-views.md)除外。 在PostgreSQL中，您可以创建一个称为物化视图的特殊视图，该视图用于物理存储数据并定期从基表刷新数据。 物化视图在许多场景中具有许多优点，例如从远程服务器更快地访问数据，数据缓存等。

在本节中，我们将向您介绍PostgreSQL视图概念，并向您展示如何管理视图，例如从数据库创建，修改和删除视图。 此外，我们将说明如何创建[可更新视图](content/postgresql-updatable-views.md)并为您提供物化视图的完整示例，这是PostgreSQL的一个非常强大的功能。

- [管理视图](content/managing-postgresql-views.md) – 介绍视图概念并展示如何创建、修改和删除 PostgreSQL 视图。

- [可更新视图](content/postgresql-updatable-views.md) – 给出创建可更新视图的示例, 允许发出[INSERT](content/postgresql-insert.md)、[UPDATE](content/postgresql-update.md)和[DELETE](content/postgresql-delete.md)语句, 以便通过视图更新基表中的数据。

- [物化视图](content/postgresql-materialized-views.md)  – 介绍物化视图的概念，并为您提供为物化视图创建和刷新数据的步骤。 

- [使用WITH CHECK OPTION子句创建视图](content/postgresql-views-with-check-option.md) – 演示如何在通过视图更改基表时使用 `WITH CHECK OPTION` 子句检查视图定义条件。 我们还讨论了检查的范围，包括 `LOCAL` 和 `CASCADED`。 

- [递归视图](content/postgresql-recursive-view.md) – 介绍递归视图，并展示在PostgreSQL中创建递归视图的示例。 
