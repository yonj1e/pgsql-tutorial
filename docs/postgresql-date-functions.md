# PostgreSQL日期函数

以下页面显示了最常用的PostgreSQL日期函数，它允许您更有效地操作日期和时间值。

| 函数                                                         | 返回类型         | 描述                                                         |
| ------------------------------------------------------------ | ---------------- | ------------------------------------------------------------ |
| [AGE](content/postgresql-age.md)                             | INTERVAL         | 计算两个时间戳之间的年龄，并返回使用年和月的“ symbolic ”结果 |
| [AGE](content/postgresql-age.md)                             | INTERVAL         | 计算当前日期(午夜)和时间戳之间的年龄，并返回使用年和月的“符号”结果 |
| CLOCK_TIMESTAMP                                              | TIMESTAMPTZ      | 返回在语句执行期间更改的当前日期和时间                       |
| [CURRENT_DATE](content/postgresql-current_date.md)           | DATE             | 返回当前日期                                                 |
| [CURRENT_TIME](content/postgresql-current_time.md)           | TIMESTAMPTZ      | 返回当前日期                                                 |
| [CURRENT_TIMESTAMP](content/postgresql-current_timestamp.md) | TIMESTAMPTZ      | 返回当前事务开始的时区的当前日期和时间                       |
| [DATE_PART](content/postgresql-date_part.md)                 | DOUBLE PRECISION | 获取时间戳或间隔的字段，如年、月、日等。                     |
| [DATE_TRUNC](content/postgresql-date_trunc.md)               | TIMESTAMP        | 返回截断到指定精度的时间戳                                   |
| [EXTRACT](content/postgresql-extract.md)                     | DOUBLE PRECISION | 与DATE_PART()函数相同                                        |
| ISFINITE                                                     | BOOLEAN          | 检查日期、时间戳或间隔是否是有限的 (not +/-infinity)         |
| JUSTIFY_DAYS                                                 | INTERVAL         | 调整间隔，将30天的时间段表示为月                             |
| JUSTIFY_HOURS                                                | INTERVAL         | 调整间隔，将24小时时间段表示为天                             |
| JUSTIFY_INTERVAL                                             | INTERVAL         | 使用LOGULE_DYD和RIGURE_HUTER调整间隔，并附加符号调整         |
| [LOCALTIME](content/postgresql-localtime.md)                 | TIME             | 返回当前事务开始的时间                                       |
| [LOCALTIMESTAMP](content/postgresql-localtimestamp.md)       | TIMESTAMP        | 返回当前事务开始的日期和时间                                 |
| [NOW](content/postgresql-now.md)                             | TIMESTAMPTZ      | 返回当前事务开始的时区的日期和时间                           |
| STATEMENT_TIMESTAMP                                          | TIMESTAMPTZ      | 返回当前语句执行的当前日期和时间                             |
| TIMEOFDAY                                                    | TEXT             | 以文本字符串的形式返回当前日期和时间，比如Clock_TIMESTAMP)   |
| TRANSACTION_TIMESTAMP                                        | TIMESTAMPTZ      | 相同的函数                                                   |
| [TO_DATE](content/postgresql-to_date.md)                     | DATE             | 将字符串转换为日期                                           |
| [TO_TIMESTAMP](content/postgresql-to_timestamp.md)           | TIMESTAMPTZ      | 将字符串转换为时间戳                                         |
