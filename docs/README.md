# PostgreSQL教程

![PostgreSQL Tutorial](imgs/home/postgresqltutorial2.png)

本**PostgreSQL教程**帮助您快速理解PostgreSQL。您将通过许多实用示例快速学习PostgreSQL。我们将向您展示的不仅仅是问题，还有如何在PostgreSQL中创造性地解决这些问题。 

如果你是…

- 想快速轻松地学习PostgreSQL。 
- 使用PostgreSQL作为后端数据库管理系统开发应用程序。 
- 从其他数据库管理系统(如MySQL、Oracle、Microsoft SQL Server)迁移到PostgreSQL。  

您可以在我们的网站上找到快速有效地开始使用PostgreSQL所需的所有知识。

我们开发了PostgreSQL教程来演示PostgreSQL的独特功能，这些特性使其成为最先进的开源数据库管理系统。