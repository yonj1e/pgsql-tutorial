## PostgreSQL基础教程

首先，你将了解如何从单表查询数据，使用基本的数据选择技术，如选择列、排序结果集和筛选行。然后，你将了解高级查询，如多表连接，使用set操作和构造子查询。最后，你将学习如何管理数据库表，例如创建新表或修改已有表的结构。

### 第1节 PostgreSQL入门

如果您是PostgreSQL新手，按照以下3个简单的步骤快速开始使用PostgreSQL。

- 首先，通过[简要概述](content/what-is-postgresql.md)了解PostgreSQL是什么。 

- 其次，本地[安装PostgreSQL](content/install-postgresql.md)并使用客户端应用程序(如psql或pgAdmin)[连接到数据库服务器](content/connect-to-postgresql-database.md)。 
- 第三，下载[示例数据库](content/postgresql-sample-database.md)并将其[加载到数据库服务器](content/load-postgresql-sample-database.md)中。 

### 第2节 查询数据

- [Select](content/postgresql-select.md) – 如何从单表查询数据。
- [Order By](content/postgresql-order-by.md) – 如何对查询结果进行排序。
- [Select Distinct ](content/postgresql-select-distinct.md) – 删除查询结果中的重复行。

### 第3节 过滤数据

- [Where](content/postgresql-where.md) – 根据指定条件过滤行。
- [Limit](content/postgresql-limit.md) – 获取由查询生成的行的子集 
- [Fetch](content/postgresql-fetch.md)– 限制查询返回的行数。
- [In](content/postgresql-in.md) – 查询与值列表中任一值匹配的数据
- [Between](content/postgresql-between.md) – 查询介于两个值之间的数据
- [Like](content/postgresql-like.md) – 基于模式匹配过滤数据

### 第4节 多表连接

- [Inner Join ](content/postgresql-inner-join.md)– 从一个表中选择具有其他表中相应行的行。
- [Left Join](content/postgresql-left-join.md) – 从一个表中选择可能在其他表中不具有相应行的行。
- [Self-join](content/postgresql-self-join.md) – 通过将表与自身进行比较, 将表联接到自身。
- [Full Outer Join](content/postgresql-full-outer-join.md) – 使用完全联接在表中查找在另一个表中没有匹配行的行。
- [Cross Join](content/postgresql-cross-join.md) – 在两个或多个表中生成行的笛卡尔积。
- [Natural Join](content/postgresql-natural-join.md) – 基于联接表中的公共列名, 使用隐式联接条件联接两个或多个表。

### 第5节 分组数据

- [Group By](content/postgresql-group-by.md) – 将行划分为组, 并对每个列应用聚合函数。
- [Having](content/postgresql-having.md) – 对组应用条件。

### 第6节 执行设置操作

- [Union](content/postgresql-union.md) – 将多个查询的结果集组合为单个结果集。
- [Intersect](content/postgresql-intersect.md) – 组合两个或多个查询的结果集, 并返回一个结果集, 其中的行显示在两个结果集中。
- [Except](content/postgresql-tutorial/postgresql-except.md) – 返回第一个查询中未出现在第二个查询的输出中的行。

### 第7节 Grouping Sets

- [Grouping Sets](content/postgresql-grouping-sets.md)  – 在报告中生成多个分组集。
- [Cube](content/postgresql-cube.md) – 定义包含所有可能的维度组合的多个分组集。
- [Rollup](content/postgresql-rollup.md) – 生成包含汇总的汇总的报表。

### 第8节 子查询

- [Subquery](content/postgresql-subquery.md) – 写入嵌套在另一个查询中的查询。
- [ANY](content/postgresql-any.md)  – 通过将值与子查询返回的一组值进行比较来检索数据。
- [ALL](content/postgresql-all.md) – 通过将值与子查询返回的值列表进行比较, 查询数据。
- [EXISTS](content/postgresql-exists.md)  – 检查子查询返回的行是否存在。

### 第9节 修改数据

在本节中，您将学习如何使用INSERT语句将数据插入表中，使用UPDATE语句修改现有数据，以及使用DELETE语句删除数据。 此外，您还将学习如何使用upsert语句合并数据。

- [Insert](content/postgresql-insert.md) – 将数据插入表中。
- [Update](content/postgresql-update.md) – 更新表中的现有数据。
- [Update join](content/postgresql-update-join.md) – 根据另一个表中的值更新表中的值。
- [Delete](content/postgresql-delete.md) – 删除表中的数据。
- [Upsert ](content/postgresql-upsert.md)– 如果表中已存在新行，则插入或更新数据。

### 第10节 导入和导出

您将学习如何使用copy命令从PostgreSQL数据导入和导出CSV文件格式。

- [将CSV文件导入表](content/import-csv-file-into-posgresql-table.md) – 介绍如何将CSV文件导入表。
- [将表导出CSV文件](content/export-postgresql-table-to-csv-file.md) – 介绍如何将表导出到CSV文件。

### 第11节 管理表

在本节中，我们将开始探索PostgreSQL数据类型，并向您展示如何使用CREATE TABLE语句创建新表。 我们还将介绍一些其他功能，例如修改表结构和删除表。 此外，您将学习使用TRUNCATE语句从表中删除所有行的有效方法。

- [数据类型](content/postgresql-data-types.md) – 涵盖了最常用的PostgreSQL数据类型。
- [创建表](content/postgresql-create-table.md) – 指导您如何在数据库中创建新表。
- [Select Into](content/postgresql-select-into.md) & [Create table as](content/ch/postgresql-create-table-as.md)– 向您展示如何从查询的结果集创建新表。
- [自动增长](content/postgresql-serial.md) 具有SERIAL的列 - 使用SERIAL将自动增量列添加到表中。
- [修改表结构](content/postgresql-alter-table.md)– 更改现有表的结构。
- [修改表名](content/postgresql-rename-table.md) – 将表的名称更改为新的名称。
- [修改数据库名](content/postgresql-rename-database.md) – 将数据库的名称更改为新的名称。
- [添加列](content/postgresql-add-column.md) – 向您展示如何使用向现有表添加一个或多个列。
- [删除列](content/postgresql-drop-column.md) – 演示了如何删除表的列。
- [修改列数据类型](content/postgresql-change-column-type.md) – 向您展示如何更改列的数据类型。
- [修改列名](content/postgresql-rename-column.md) – 说明了如何重命名表的一列或多列。
- [删除表](content/postgresql-drop-table.md) – 删除现有表及其所有依赖对象。
- [临时表](content/postgresql-temporary-table.md) – 向您展示如何使用临时表。
- [Truncate](content/postgresql-truncate-table.md) – 快速有效地删除大表中的所有数据。

### 第12节 数据类型

- [Boolean](content/postgresql-boolean.md) – 使用布尔数据类型存储TRUE和FALSEvalues。
- [CHAR, VARCHAR 和 TEXT](content/postgresql-char-varchar-text.md) – 学习如何使用各种字符类型，包括CHAR，VARCHAR和TEXT。
- [NUMERIC](content/postgresql-numeric.md) – 向您展示如何使用NUMERIC类型来存储需要精度的值。
- [Integer](content/postgresql-integer.md) – 介绍PostgreSQL中的各种整数类型，包括SMALLINT，INT和BIGINT。
- [SERIAL](content/postgresql-serial.md) – 向您展示如何使用SERIAL假型创建自动增量列。
- [DATE](content/postgresql-date.md)  – 介绍用于存储日期值的DATE数据类型。
- [Timestamp](content/postgresql-timestamp.md) – 快速了解时间戳数据类型。
- [Interval](content/postgresql-interval.md) – 向您展示如何使用间隔数据类型有效地处理一段时间。
- [TIME](content/postgresql-time.md) – 使用TIME数据类型来管理时间值。
- [UUID](content/postgresql-uuid.md) – 指导您使用UUID数据类型以及如何使用提供的模块生成UUID值。
- [Array](content/postgresql-array.md) – 向您展示如何使用数组并向您介绍一些方便的数组操作函数。
- [hstore](content/postgresql-hstore.md) – 介绍hstore数据类型，它是一组存储在PostgreSQL中的单个值中的键/值对。
- [JSON](content/postgresql-json.md) – 说明了如何使用JSON数据类型，并向您展示了如何使用一些最重要的JSON运算符和函数。
- [用户自定义类型](content/postgresql-user-defined-data-types.md) – 向您展示如何使用CREATE DOMAIN和CREATE TYPE语句来创建用户定义的数据类型。

### 第13节 约束

- [主键](content/postgresql-primary-key.md) – 说明了在创建表或将主键添加到现有表时如何定义主键。
- [外键](content/postgresql-foreign-key.md) – 介绍如何在创建新表时定义外键约束或为现有表添加外键约束。
- [检查约束](content/postgresql-check-constraint.md) – 添加逻辑以基于布尔表达式检查值。
- [唯一约束](content/postgresql-unique-constraint.md) – 确保表中的列或列组中的值是唯一的。
- [非空约束](content/postgresql-not-null-constraint.md) – 确保列中的值不为NULL。

### 第14节 条件表达式和运算符

- [`CASE`](content/postgresql-case.md) – 向您展示如何使用CASE表达式形成条件查询。
- [`COALESCE`](content/postgresql-coalesce.md) – 返回第一个非null参数。 您可以使用它以默认值替换NULL。
- [`NULLIF`](content/postgresql-nullif.md) – 如果第一个参数等于第二个参数，则返回NULL。
- [`CAST`](content/postgresql-cast.md) – 从一种数据类型转换为另一种数据类型，例如，从字符串转换为整数，从字符串转换为日期。

### 第15节 实用程序

- [psql 命令](content/psql-commands.md) – 向您展示最常见的psql命令，它们可以帮助您更快，更有效地与psql交互。

### 第16节 技巧

- [如何删除重复行](content/how-to-delete-duplicate-rows-in-postgresql.md) – 显示了从表中删除重复行的各种方法。
- [如何复制表](content/postgresql-copy-table.md) – 向您展示如何将表复制到新表。
- [如何复制数据库](content/postgresql-copy-database.md) – 学习如何在同一服务器上或从服务器复制数据库到另一个服务器。
- [如何生成随机数 ](content/postgresql-random-range.md) – 说明了如何生成特定范围内的随机数。
- [如何使用递归查询](content/postgresql-recursive-query.md) – 讨论递归查询并学习如何在各种上下文中应用它。
- [如何使用窗口函数](content/postgresql-window-function.md) – 向您介绍最常用的PostgreSQL窗口函数
