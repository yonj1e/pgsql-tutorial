# PostgreSQL存储过程

这个PostgreSQL存储过程部分逐步向您展示了如何开发PostgreSQL用户定义的函数。

在PostgreSQL中，诸如PL/pgSQL，C，Perl，Python和Tcl之类的过程语言被称为存储过程。存储过程添加许多过程元素，例如控制结构，循环和复杂计算，以扩展SQL标准。它允许您在PostgreSQL中开发使用纯SQL语句可能无法实现的复杂功能。

我们将重点关注类似于Oracle PL/SQL的PL/pgSQL过程语言。选择PL/pgSQL的原因是：

- PL/pgSQL简单易学。
- PL/pgSQL是PostgreSQL默认带有的。PL/pgSQL中开发的用户定义函数可以像任何内置函数一样使用。
- PL/pgSQL具有许多功能，允许您开发复杂的用户定义函数。

让我们开始用PL/pgSQL编程。

- [存储过程简介](content/introduction-to-postgresql-stored-procedures.md) – 简要介绍PostgreSQL存储过程。
- [用户自定义的函数](content/postgresql-create-function.md) – 向您展示如何使用CREATE FUNCTION语句在PL/pgSQL中开发第一个用户定义的函数。
- [PL/pgSQL函数参数](content/plpgsql-function-parameters.md) – 介绍各种功能参数：IN，OUT，INOUT和VARIADIC。
- [PL/pgSQL函数重载](content/plpgsql-function-overloading.md) – 定义具有相同名称但不同参数列表的多个函数。
- [返回表的PL/pgSQL函数](content/plpgsql-function-returns-a-table.md) – 开发一个返回表的函数。
- [PL/pgSQL 块结构](content/plpgsql-block-structure.md) – 说明了PL/pgSQL函数的块结构。您将学习如何编写匿名块，并将大块划分为更多逻辑子块。
- [PL/pgSQL 错误和消息](content/plpgsql-errors-messages.md) – 向您展示如何使用RAISE语句报告消息并引发错误。另外，我们将向您介绍ASSERT语句，以便将调试检查插入到PostgreSQL函数中。
- [PL/pgSQL 变量](content/plpgsql-variables.md) – 指导您在PL/pgSQL中声明变量的各种方法。
- [PL/pgSQL 常量](content/plpgsql-constants.md) – 与变量不同，常量值一旦初始化就无法更改。我们将向您展示如何使用常量使代码更易读，更易于维护。
- [PL/pgSQL IF语句](content/plpgsql-if-else-statements.md) – 向您介绍三种形式的IF语句，以根据特定条件执行命令。
- [PL/pgSQL CASE语句](content/plpgsql-case-statement.md) – 指导两种形式的CASE语句：简单CASE和搜索CASE语句。
- [PL/pgSQL 循环语句s](content/plpgsql-loop-statements.md) – 循环语句重复执行一系列语句。PostgreSQL为您提供各种循环语句，如LOOP，WHILE，FOR和FOREACH。我们将向您展示另外两个控制循环的语句：CONTINUE和EXIT。
- [PL/pgSQL 游标](content/plpgsql-cursor.md) – 向您展示了使用CURSOR变量处理查询返回的大量行的有效方法。
- 使用PL/pgSQL的触发器过程-应用PL/pgSQL来定义触发器过程。