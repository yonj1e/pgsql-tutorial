# PostgreSQL管理

PostgreSQL管理涵盖了最重要的PostgreSQL数据库服务器管理活动。 我们将讨论角色和组管理，数据库创建，表空间管理，数据库备份和恢复。

## 数据库和模式管理

在本节中，我们将向您展示如何在PostgreSQL中管理数据库，包括创建数据库，修改现有数据库和删除数据库。 此外的功能，还涵盖了模式管理。

- [创建数据库](content/postgresql-create-database.md) – 使用 `CREATE DATABASE` 语句创建新数据库。
- [修改数据库](content/postgresql-alter-database.md) – 使用 `ALTER DATABASE` 语句修改已有的数据库。
- [删除数据库](content/postgresql-drop-database.md) – 使用 `DROP DATABASE` 语句永久删除数据库。
- [复制数据库](content/postgresql-copy-database.md) – 将数据库复制到数据库服务器或从服务器复制到另一个服务器。
- [获取数据库对象大小](content/postgresql-database-indexes-table-size.md) – 介绍各种方便的函数来获取数据库，表，索引等的大小。

## 角色管理

在PostgreSQL中，角色是一个帐户。 具有登录权限的角色称为用户user。 角色可以是其他角色的成员。 包含其他角色的角色称为组。 在本节中，您将学习如何有效地管理角色和组。

- [角色管理](content/postgresql-roles.md): 介绍PostgreSQL角色概念，并演示如何使用 `CREATE ROLE` 语句创建用户角色和组角色。

## 备份和恢复数据库

本节介绍如何使用各种PostgreSQL备份和还原工具（包括`pg_dump`，`pg_dumpall`，`psql`，`pg_restore`和`pgAdmin`）来备份和恢复数据库。

- [备份 ](content/postgresql-backup-database.md)–通过使用PostgreSQL备份工具（包括 `pg_dump` 和 `pg_dumpall`）向您介绍备份数据库的实用方法。
- [恢复](content/postgresql-restore-database.md) – 向您展示了使用 `psql` 和 `pg_restore` 工具恢复PostgreSQL数据库的各种方法。

## 表空间管理

PostgreSQL表空间允许您控制数据在文件系统中的存储方式。 在许多情况下，表空间非常有用，例如管理大表和提高数据库性能。 在本节中，我们将向您展示如何有效地管理PostgreSQL中的表空间。

- [创建表空间](content/postgresql-create-tablespace.md) – 向您介绍PostgreSQL表空间，并向您展示如何使用 `CREATE TABLESPACE` 语句创建表空间。
- [更改表空间](content/postgresql-alter-tablespace.md) – 向您展示如何使用 `ALTER TABLESPACE` 语句重命名，更改所有者和设置表空间的参数。
- [删除表空间](content/postgresql-drop-tablespace.md) – 学习如何使用 `DROP TABLESPACE` 语句删除表空间。

## 提示

- [重置密码](content/postgresql-reset-password.md) – 演示如何重置Postgres用户忘记的密码。 
- [psql 命令](content/psql-commands.md) – 提供最常见的psql命令，帮助您更快、更有效地从PostgreSQL查询数据。 
- [查看表](content/postgresql-describe-table.md) – 获取特定表的信息。 
- [列出数据库](content/postgresql-show-databases.md) – 列出当前数据库服务器中的所有数据库。 
- [列出表 ](content/postgresql-show-tables.md)– 显示当前数据库中的所有表。 