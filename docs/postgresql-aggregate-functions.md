# PostgreSQL聚合函数

简介：在本教程中，您将学习如何使用PostgreSQL聚合函数，如`AVG()`, `COUNT()`, `MIN()`, `MAX()`, and `SUM()`.

## 聚合函数简介

聚合函数对一组行执行计算并返回单个行。 PostgreSQL提供所有标准SQL的聚合函数，如下所示：

- [`AVG()`](/content/postgresql-avg-function.md)– 返回平均值。
- [`COUNT()`](/content/postgresql-count-function.md) – 返回值的数量。
- [`MAX()`](/conent/postgresql-max-function.md) – 返回最大值。
- [`MIN()`](/content/postgresql-min-function.md) – 返回最小值。
- [`SUM()`](/content/postgresql-sum-function.md) – 返回所有值或不同值的总和。 

我们经常在 `SELECT` 语句中使用聚合函数和 `GROUP BY`子句。 在这些情况下，`GROUP BY` 子句将结果集划分为行组，并且聚合函数对每个组执行计算，例如最大值，最小值，平均值等。

您只能在以下子句中将聚合函数用作表达式：

-  `SELECT` 子句
-  [`HAVING`](content/postgresql-having.md) 子句

## 聚合函数示例

让我们使用[示例数据库](content/postgresql-sample-database.md)中的 `film` 表进行演示。

![img](/content/imgs/film-table.png)

## `AVG()` 函数示例

以下语句使用 `AVG()` 函数计算所有电影的平均重置成本：

```sql
SELECT ROUND( AVG( replacement_cost ), 2 ) avg_replacement_cost
FROM film;
 avg_replacement_cost
----------------------
                19.98
(1 row)
```

注意到 `ROUND()` 函数用于将结果舍入到2个小数位。

要计算类别ID为7的戏剧电影的平均重置成本，可以使用以下语句：

```sql
SELECT ROUND( AVG( replacement_cost ), 2 ) avg_replacement_cost
FROM film
INNER JOIN film_category USING(film_id)
INNER JOIN category USING(category_id)
WHERE category_id = 7;
 avg_replacement_cost
----------------------
                21.09
(1 row)
```

## `COUNT()` 函数示例

要获得电影的数量，可以使用 `Count(*)` 函数，如下所示： 

```sql
SELECT COUNT(*) FROM film;
 count
-------
  1000
(1 row
```

要获得戏剧电影的数量，可以使用以下语句： 

```sql
SELECT COUNT(*) drama_films
FROM film
INNER JOIN film_category USING(film_id)
INNER JOIN category USING(category_id)
WHERE category_id = 7;
 drama_films
-------------
          62
(1 row)
```

结果显示，我们有62部戏剧电影。

## `MAX()` 函数示例

下面示例使用 `max()` 函数返回胶片的最大替换成本。 

```sql
SELECT MAX(replacement_cost) FROM film;
  max
-------
 29.99
(1 row)
```

要获得具有最大替换成本的胶片，可以使用以下查询： 

```sql
SELECT film_id, title
FROM film
WHERE
 replacement_cost =(
 SELECT MAX( replacement_cost )
 FROM film
 )
ORDER BY title;
 film_id |          title
---------+-------------------------
      34 | Arabia Dogma
      52 | Ballroom Mockingbird
      81 | Blindness Gun
      85 | Bonnie Holocaust
     138 | Chariots Conspiracy
     157 | Clockwork Paradise
     163 | Clyde Theory
     196 | Cruelty Unforgiven
     199 | Cupboard Sinners
     224 | Desperate Trainspotting
     ···
```

子查询返回最大替换成本，然后由外部查询用于检索胶片的信息。 

## `MIN()` 函数示例

下面的示例使用 `min()` 函数返回胶片的最小替换成本： 

```sql
SELECT MIN(replacement_cost) FROM film;
 min
------
 9.99
(1 row)
```

要获得替换成本最小的胶片，可以使用以下查询： 

```sql
SELECT film_id, title
FROM film
WHERE
 replacement_cost =(
 SELECT MIN( replacement_cost )
 FROM film
 )
ORDER BY title;
 film_id |         title
---------+------------------------
      23 | Anaconda Confessions
     150 | Cider Desire
     182 | Control Anthem
     203 | Daisy Menagerie
     221 | Deliverance Mulholland
     260 | Dude Blindness
     272 | Edge Kissing
     281 | Encino Elf
     299 | Factory Dragon
     307 | Fellowship Autumn
     ···
```

## `SUM()` 函数示例

下面的语句使用 `sum()` 函数计算按电影等级分组的电影总长度： 

```sql
SELECT rating, SUM( rental_duration )
FROM film
GROUP BY rating
ORDER BY rating;
 rating | sum
--------+------
 G      |  861
 PG     |  986
 PG-13  | 1127
 R      |  931
 NC-17  | 1080
(5 rows)
```

在本教程中，您了解了PostgreSQL聚合函数，并将它们应用于汇总数据。 