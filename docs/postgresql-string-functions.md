# PostgreSQL字符串函数

此页面为您提供了最常用的PostgreSQL字符串函数，允许您有效地操作字符串数据。

| 函数                                                   | 描述                                                         | 示例                                             | 返回值             |
| ------------------------------------------------------ | ------------------------------------------------------------ | ------------------------------------------------ | ------------------ |
| [ASCII](content/postgresql-ascii.md)                   | 返回UTF8字符的字符或Unicode代码点的ASCII代码值               | ASCII(‘A’)                                       | 65                 |
| [CHR](content/postgresql-chr.md)                       | 将ASCII代码转换为字符或将Unicode代码转换为UTF8字符           | CHR(65)                                          | ‘A’                |
| [CONCAT](content/postgresql-concat-function.md)        | 将两个或多个字符串连接成一个                                 | CONCAT(‘A’,’B’,’C’)                              | ‘ABC’              |
| [CONCAT_WS](content/postgresql-concat-function.md)     | 用分隔符连接字符串                                           | CONCAT(‘,’,’A’,’B’,’C’)                          | ‘A,B,C’            |
| [FORMAT](content/postgresql-format.md)                 | 基于格式字符串设置参数格式                                   | FORMAT(‘Hello %s’,’PostgreSQL’)                  | ‘Hello PostgreSQL’ |
| [INITCAP](content/postgresql-letter-case-functions.md) | 将字符串中的字词转换为标题大小写                             | INITCAP(‘hI tHERE’)                              | Hi There           |
| [LEFT](content/postgresql-left.md)                     | 返回字符串中的第一个 n 个字符                                | LEFT(‘ABC’,1)                                    | ‘A’                |
| [LENGTH](content/postgresql-length-function.md)        | 返回字符串中的字符数                                         | LENGTH(‘ABC’)                                    | 3                  |
| [LOWER](content/postgresql-letter-case-functions.md)   | 将字符串转换为小写                                           | LOWER(‘hI tHERE’)                                | ‘hi there’         |
| [LPAD](content/postgresql-lpad.md)                     | 在左边垫一个字符到一定长度的字符串                           | LPAD(‘123′, 5, ’00’)                             | ‘00123’            |
| [LTRIM](content/postgresql-trim-function.md)           | 从输入字符串的左侧移除包含指定字符的最长字符串               | LTRIM(‘00123’)                                   | ‘123’              |
| [MD5](content/postgresql-md5.md)                       | 以十六进制形式返回字符串的 MD5 哈希值                        | MD5(‘ABC’)                                       |                    |
| [POSITION](content/postgresql-position.md)             | 返回字符串中子字符串的位置                                   | POSTION(‘B’ in ‘A B C’)                          | 3                  |
| [REGEXP_MATCHES](content/postgresql-regexp_matches.md) | 将 POSIX 正则表达式与字符串匹配并返回匹配的子字符串          | SELECT REGEXP_MATCHES(‘ABC’, ‘^(A)(..)$’, ‘g’);  | {A,BC}             |
| [REGEXP_REPLACE](content/regexp_replace.md)            | 替换匹配 POSIX 常规 expressionby 的子字符串                  | REGEXP_REPLACE(‘John Doe’,'(.*) (.*)’,’\2, \1′); | ‘Doe, John’        |
| REPEAT                                                 | 重复指定次数的字符串                                         | REPEAT(‘*’, 5)                                   | ‘*****’            |
| [REPLACE](content/postgresql-replace.md)               | 将子字符串中的所有匹配项从子字符串替换为                     | REPLACE(‘ABC’,’B’,’A’)                           | ‘AAC’              |
| REVERSE                                                | 返回反向字符串。                                             | REVERSE(‘ABC’)                                   | ‘CBA’              |
| [RIGHT](content/postgresql-right.md)                   | 返回字符串中的最后 n 个字符。当 n 为负值时, 返回所有但第一个woxiagn字符。 | RIGHT(‘ABC’, 2)                                  | ‘BC’               |
| RPAD                                                   | 在具有一定长度字符的字符串右侧填充                           | RPAD(‘ABC’, 6, ‘xo’)                             | ‘ABCxox’           |
| [RTRIM](content/postgresql-trim-function.md)           | 从输入字符串的右侧移除包含指定字符的最长字符串               | RTRIM(‘abcxxzx’, ‘xyz’)                          | ‘abc’              |
| [SPLIT_PART](content/postgresql-split_part.md)         | 拆分指定分隔符上的字符串并返回第 n 个子字符串                | SPLIT_PART(‘2017-12-31′,’-‘,2)                   | ’12’               |
| [SUBSTRING](content/postgresql-substring.md)           | 从字符串中提取子字符串                                       | SUBSTRING(‘ABC’,1,1)                             | A’                 |
| [TRIM](content/postgresql-trim-function.md)            | 从输入字符串的左侧、右侧或两者中移除包含指定字符的最长字符串 | SUBSTRING(‘ABC’,1,1)                             | ‘A’                |
| [UPPER](content/postgresql-letter-case-functions.md)   | 将字符串转换为大写                                           | UPPER(‘hI tHERE’)                                | ‘HI THERE’         |