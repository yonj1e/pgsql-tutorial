## 函数

This section provides you with the most useful PostgreSQL functions including aggregate functions, string functions, and date & time functions.

#### 聚合函数

- [`AVG`](http://www.postgresqltutorial.com/postgresql-avg-function/) – calculates the average value of a list of values.
- [`COUNT`](http://www.postgresqltutorial.com/postgresql-count-function/) – counts the number of values in a group.
- [`SUM`](http://www.postgresqltutorial.com/postgresql-sum-function/) – calculates the total of a list of values.
- [`MAX`](http://www.postgresqltutorial.com/postgresql-max-function/) – get the maximum value of in a set of values.
- `MIN `– gets the minimum value of in a set of values.

#### 字符函数

PostgreSQL string functions and operators help you manipulate string values effectively. Strings mean [character](http://www.postgresqltutorial.com/postgresql-char-varchar-text/), [character varying](http://www.postgresqltutorial.com/postgresql-char-varchar-text/), and [text](http://www.postgresqltutorial.com/postgresql-char-varchar-text/).

- [`CONCAT`](http://www.postgresqltutorial.com/postgresql-concat-function/) – shows you how to use `CONCAT` and `CONCAT_WS` functions to concatenate two or more strings into one.
- [`LENGTH`](http://www.postgresqltutorial.com/postgresql-length-function/) – returns the number of characters in a string. The `OCTET_LENGTH`, `BIT_LENGTH`, and `CHAR_LENGTH` functions are also covered.
- [`LOWER, UPPER, and INITCAP functions`](http://www.postgresqltutorial.com/postgresql-letter-case-functions/) – gives you three useful string functions to format letter case of a string expression.
- [`REPLACE`](http://www.postgresqltutorial.com/postgresql-replace/) – searches and replaces all occurrences of substrings with a new substring.
- [`SUBSTRING`](http://www.postgresqltutorial.com/postgresql-substring/) – extracts a substring from a string using start position and length, as well as regular expression.
- [`TRIM`](http://www.postgresqltutorial.com/postgresql-trim-function/) – removes the longest string that contains a character from the beginning, ending, and both beginning and ending of a string. We will also introduce you to the `LTRIM`, `RTRIM`, and `BTRIM` functions.

#### 日期函数

The following section shows you the most commonly used PostgreSQL date/time functions that allow you to manipulate date and time values more effectively.

-  [`AGE`](http://www.postgresqltutorial.com/postgresql-age/) – calculate ages and return a  result as an interval.
-  [`DATE_TRUNC`](http://www.postgresqltutorial.com/postgresql-date_trunc/) – truncates a date to a level of precision e.g., hour, minute, second etc.
-  [`DATE_PART`](http://www.postgresqltutorial.com/postgresql-date_part/) – extracts a subfield e.g., year, month, week, day, etc., from a date or time value.
-  [`NOW`](http://www.postgresqltutorial.com/postgresql-now/) – returns the current date and time with the time zone information.
-  [`TO_DATE`](http://www.postgresqltutorial.com/postgresql-to_date/) – converts a string into a date value.