# PostgreSQL数学函数

此页面为您提供最常用的PostgreSQL Math函数，可帮助您快速有效地执行各种数学运算。

| 函数                                  | 描述                                                       | 示例         | 返回值      |
| ------------------------------------- | ---------------------------------------------------------- | ------------ | ----------- |
| [ABS](content/postgresql-abs.md)      | 计算一个数字的绝对值                                       | ABS(-10)     | 10          |
| CBRT                                  | 计算数字的立方根                                           | CBRT(8)      | 2           |
| [CEIL](content/postgresql-ceil.md)    | 将数字舍入到最接近的整数，该整数大于或等于数字。           | CEIL(-12.8)  | -12         |
| [CEILING](content/postgresql-ceil.md) | Same as CEIL                                               |              |             |
| DEGREES                               | 将弧度转换为度数                                           | DEGREES(0.8) | 45.83662361 |
| DIV                                   | 返回两个数值的整数商                                       | DIV(8,3)     | 2           |
| EXP                                   | 返回一个数字的科学记数法中的指数值                         | EXP(1)       | 2.718281828 |
| [FLOOR](content/postgresql-floor.md)  | 把一个数字的整数舍入到最近的整数，这个整数小于或等于数字。 | FLOOR(10.6)  | 10          |
| LN                                    | 返回数值的自然对数                                         | LN(3)        | 1.098612289 |
| LOG                                   | 返回数值的以10为底的对数                                   | LOG(1000)    | 3           |
| LOG                                   | 将数值的对数返回到指定的基数                               | LOG(2, 64)   | 4           |
| [MOD](content/postgresql-mod.md)      | 将第一个参数除以第二个参数，然后返回余数                   | MOD(10,4)    | 1           |
| PI                                    | 返回PI的值                                                 | PI()         | 3.141592654 |
| POWER                                 | 将数值提升为第二个数值的幂                                 | POWER(5, 3)  | 125         |
| RADIANS                               | 将度数转换为弧度                                           | RADIANS(60)  | 1.047197551 |
| [ROUND](content/postgresql-round.md)  | 将数字舍入到最接近的整数或指定的小数位数                   | ROUND(10.3)  | 10          |
| SCALE                                 | 返回小数部分中的小数位数                                   | SCALE(1.234) | 3           |
| SIGN                                  | 返回数值的符号（正数，负数）                               | SIGN(-1)     | -1          |
| SQRT                                  | 返回数值的平方根                                           | SQRT(3.0)    | 1.732050808 |
| [TRUNC](content/postgresql-trunc.md)  | 将数值截断为指定小数位的整数                               | TRUNC(12.3)  | 12          |
| WIDTH_BUCKET                          | 在等宽直方图中将值分配给存储桶。                           |              |             |
| RANDOM                                | 返回一个从0到1的随机数                                     |              | 0.968435665 |